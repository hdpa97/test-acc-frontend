(function () {
  "use strict";

  angular
    .module("main.services", [])
    .factory("Main", function ($q, $timeout, $http, $localStorage, $rootScope) {
      // production environment
      // var hostname = "https://talents-api.acc.co.id";
      // var dpa = "https://www.dapenastra.com";
      // var environment = "production";
      // var phphost = "https://talents-report.acc.co.id/index.php";
      // var baseurlWebCareer = "https://career.acc.co.id";
      // var mapApiKey = "AIzaSyBgLAsvhRh_2N4JnDl6t9bE9dE3HZ64aDw";
      // var dpaUploadForm =
      //   "https://dpa.acc.co.id/rest/com/hr/in/httprest/dpa2/uploadformdpa";
      // var imageServletPrefix = "https://talents.acc.co.id/public/getFile?path=";
      // end of production environment

      // test environment
      // var hostname = "https://talents-api-test.acc.co.id";
      // var dpa = "https://www.dapenastra.com";
      // var environment = "development";
      // var phphost = "https://talents-report-test.acc.co.id/index.php";
      // var baseurlWebCareer = "https://career.acc.co.id";
      // var mapApiKey = "AIzaSyBgLAsvhRh_2N4JnDl6t9bE9dE3HZ64aDw";
      // var dpaUploadForm =
      //   "https://sofiadev.acc.co.id/rest/com/hr/in/httprest/dpa2/uploadformdpa";
      // var imageServletPrefix =
      //   "https://talents-test.acc.co.id/public/getFile?path=";
      // end of test environment

      // development environment
      var hostname = "https://talents-api-dev.acc.co.id";
      var dpa = "https://www.dapenastra.com";
      var environment = "development";
      var phphost = "https://talents-report-dev.acc.co.id/index.php";
      var baseurlWebCareer = 'https://career.acc.co.id';
      var mapApiKey = "AIzaSyBgLAsvhRh_2N4JnDl6t9bE9dE3HZ64aDw";
      var dpaUploadForm = "https://sofiadev.acc.co.id/rest/com/hr/in/httprest/dpa2/uploadformdpa";
      var imageServletPrefix = "https://talents-dev.acc.co.id/public/getFile?path=";
      // end of development environment

      var appmode = "web"; // mobile , web
      var baseUrl = hostname;
      var version = 161;
      var versionName = "1.6.1";

      // Technical Competency Daily Test (TECAT)
      // var tecat = "https://tecat.acc.co.id";
      var tecat = "http://localhost:8000";
      var appKeyTecat = "vU2kaXo8FSHuUuE@7^CvM!x4";
      var checkQuiz = 0;

      // end of Technical Competency Daily Test (TECAT)

      // Peraturan Perusahaan
      // var pp = "http://149.129.234.93/tecat-v2/public/api/restv2/talents"; // old
      var pp = "https://tecat.acc.co.id/api/restv2/talents";
      var appKeyPP = "vU2kaXo8FSHuUuE@7^CvM!x4";

      // end of Peraturan Perusahaan

      //production
      var printBaseUrl = phphost + "/payslippdf";
      var printSptBaseUrl = phphost + "/downloadspt";
      var printAmandmentBaseUrl = phphost + "/amandemen/amandemen";
      var printPkwtBaseUrl = phphost + "/pkwtnew/pkwt";
      var printReportUrl = phphost + "/printpdf";
      var printTaxBaseUrl = phphost + "/pernyataanpajak/pernyataanpajak";
      var printAssignationUrl = phphost + "/suratpengangkatan/pengangkatan";
      var printStatmentUrl = phphost + "/statement/keterangan";
      var printReportCheckUrl = phphost + "/reportcheckio/reportcheckio";
      var printReportOvtUrl = phphost + "/reportovtplan/reportovtplan";
      var printReportOvtRealUrl = phphost + "/reportovtreal/reportovtreal";
      var printReportMangkirUrl = phphost + "/reportmangkir/reportmangkir";
      var printReportKehadiranTanpaKabarUrl =
        phphost + "/reportkehadirantk/reportkehadirantk";
      var printReportAttendanceUrl =
        phphost + "/reportattendancehr/reportattendancehr";
      var printReportTmsUrl = phphost + "/reporttms/reporttms";
      var printReportKehadiranDptInUrl = phphost + "/reportdptin/reportdptin";
      var printReportKehadiranInUrl = phphost + "/reportkin/reportkin";

      var printReportCutiPerkaryawanUrl =
        phphost + "/reportcutikaryawan/reportcutikaryawan";
      var printReportIzinKaryawanUrl =
        phphost + "/reportizinkaryawan/reportizinkaryawan";
      var printCutiUrl = phphost + "/reportcuti/reportcuti";

      var printReportSpd = phphost + "/Reportspd/reportspd";

      var printFormSpd = phphost + "/Formspd/formspd";

      var basicAuthentication = "Basic dGFsZW50czpzZWNyZXQ=";
      var basicAuthenticationWebCareer =
        "Basic " + btoa("force@test.com:jWyMDI-XKQ6Z-WuSbJF-9n29v");
      var timeoutms = 35000; // 35 sec

      var takePictureOptions = {
        quality: 100,
        targetWidth: 1280,
        targetHeight: 720,
      };

      var selectHour = [{
          id: 0,
        },
        {
          id: 1,
        },
        {
          id: 2,
        },
        {
          id: 3,
        },
        {
          id: 4,
        },
        {
          id: 5,
        },
        {
          id: 6,
        },
        {
          id: 7,
        },
        {
          id: 8,
        },
        {
          id: 9,
        },
        {
          id: 10,
        },
        {
          id: 11,
        },
        {
          id: 12,
        },
        {
          id: 13,
        },
        {
          id: 14,
        },
        {
          id: 15,
        },
        {
          id: 16,
        },
        {
          id: 17,
        },
        {
          id: 18,
        },
        {
          id: 19,
        },
        {
          id: 20,
        },
        {
          id: 21,
        },
        {
          id: 22,
        },
        {
          id: 23,
        },
        {
          id: 24,
        },
      ];
      var selectFiveteenMin = [{
          id: 0,
        },
        {
          id: 15,
        },
        {
          id: 30,
        },
        {
          id: 45,
        },
      ];
      var dataDisplaySize = 15;
      var selectMonth = [{
          name: "JAN",
          id: "01",
        },
        {
          name: "FEB",
          id: "02",
        },
        {
          id: "03",
          name: "MAR",
        },
        {
          id: "04",
          name: "APR",
        },
        {
          id: "05",
          name: "MAY",
        },
        {
          id: "06",
          name: "JUN",
        },
        {
          id: "07",
          name: "JUL",
        },
        {
          id: "08",
          name: "AUG",
        },
        {
          id: "09",
          name: "SEP",
        },
        {
          id: "10",
          name: "OCT",
        },
        {
          id: "11",
          name: "NOV",
        },
        {
          id: "12",
          name: "DEC",
        },
      ];
      var selectBloodType = [{
          id: "A",
        },
        {
          id: "B",
        },
        {
          id: "AB",
        },
        {
          id: "O",
        },
      ];
      var selectCountry = [{
        id: "Indonesia",
      }, ];

      function changeUser(user) {
        angular.extend(currentUser, user);
      }

      function urlBase64Decode(str) {
        var output = str.replace("-", "+").replace("_", "/");
        switch (output.length % 4) {
          case 0:
            break;
          case 2:
            output += "==";
            break;
          case 3:
            output += "=";
            break;
          default:
            throw "Illegal base64url string!";
        }
        return window.atob(output);
      }

      function getUserFromToken() {
        var token = $localStorage.token;
        var user = {};
        if (typeof token !== "undefined") {
          var encoded = token.split(".")[1];
          user = JSON.parse(urlBase64Decode(encoded));
        }
        return user;
      }

      function getValuefromId(array, id) {
        var val = "";
        for (var i = array.length - 1; i >= 0; i--) {
          if (array[i].id == id) val = array[i].name;
        }
        return val;
      }

      function getIdfromValue(array, val) {
        var id = "";
        for (var i = array.length - 1; i >= 0; i--) {
          if (array[i].name == val) id = array[i].id;
        }
        return id;
      }

      var currentUser = getUserFromToken();
      // var getValuefromId = getValuefromId(array,id);

      return {
        save: function (data, success, error) {
          $http
            .post(baseUrl + "/signin", data)
            .success(success)
            .error(error);
        },

        getSelectHour: function () {
          return selectHour;
        },
        getVersion: function () {
          return version;
        },
        getVersionName: function () {
          return versionName;
        },
        getAppMode: function () {
          return appmode;
        },
        getSelectFiveteenMin: function () {
          return selectFiveteenMin;
        },
        getSelectBloodType: function () {
          return selectBloodType;
        },
        getEnvironment: function () {
          return environment;
        },

        getDataDisplaySize: function () {
          return dataDisplaySize;
        },

        getValuefromId: function (array, id) {
          return getValuefromId(array, id);
        },
        getIdfromValue: function (array, val) {
          return getIdfromValue(array, val);
        },
        getSelectCountry: function () {
          return selectCountry;
        },

        getPrintBaseUrl: function () {
          return printBaseUrl;
        },

        getPrintSptBaseUrl: function () {
          return printSptBaseUrl;
        },

        getPrintAmandmentBaseUrl: function () {
          return printAmandmentBaseUrl;
        },

        getPrintPkwtBaseUrl: function () {
          return printPkwtBaseUrl;
        },

        getPrintTaxBaseUrl: function () {
          return printTaxBaseUrl;
        },

        getPrintAssignationBaseUrl: function () {
          return printAssignationUrl;
        },

        getPrintStatmentBaseUrl: function () {
          return printStatmentUrl;
        },

        getPrintReportUrl: function () {
          return printReportUrl;
        },

        getPrintReportCheckUrl: function () {
          return printReportCheckUrl;
        },

        getPrintReportOvtUrl: function () {
          return printReportOvtUrl;
        },

        getPrintReportMangkirUrl: function () {
          return printReportMangkirUrl;
        },

        getPrintReportKehadiranTanpaKabar: function () {
          return printReportKehadiranTanpaKabarUrl;
        },

        getPrintReportOvtRealUrl: function () {
          return printReportOvtRealUrl;
        },

        getPrintReportAttendanceUrl: function () {
          return printReportAttendanceUrl;
        },

        getPrintReportTmsUrl: function () {
          return printReportTmsUrl;
        },

        getPrintReportKehadiranDptIn: function () {
          return printReportKehadiranDptInUrl;
        },

        getPrintReportKehadiranIn: function () {
          return printReportKehadiranInUrl;
        },

        getPrintCutiPerkaryawanUrl: function () {
          return printReportCutiPerkaryawanUrl;
        },

        getPrintReportIzinKaryawanUrl: function () {
          return printReportIzinKaryawanUrl;
        },

        getPrintCutiUrl: function () {
          return printCutiUrl;
        },

        getPrintSpd: function () {
          return printReportSpd;
        },

        getPrintFormSpd: function () {
          return printFormSpd;
        },

        getPhpHost: function () {
          return hostname;
        },

        getUrlApi: function () {
          return baseUrl;
        },

        getPPUrlApi: function () {
          return pp;
        },

        getTecatUrlApi: function () {
          return tecat;
        },

        getDPAUrlApi: function () {
          return dpa;
        },

        getDpaUploadForm: function () {
          return dpaUploadForm;
        },

        getUrlApiWebCareer: function () {
          return baseurlWebCareer;
        },

        getSelectMonth: function () {
          return selectMonth;
        },

        signin: function (data, success, error) {
          // $http.defaults.headers.common['Authorization'] = 'Basic dGFsZW50czpzZWNyZXQ=';
          $http.defaults.headers.common["Authorization"] = basicAuthentication;
          $http.defaults.headers.common["Content-Type"] =
            "application/x-www-form-urlencoded";
          var deferred = $q.defer();
          // $http.post(baseUrl + '/oauth/token?grant_type=password&username='+data.username+'&password='+data.password, data)
          var dataBody =
            "username=" + data.username + "&password=" + data.password;
          $http
            .post(baseUrl + "/oauth/token?grant_type=password", dataBody, {
              headers: {
                Authorization: "Basic dGFsZW50czpzZWNyZXQ=",
                "Content-Type": "application/x-www-form-urlencoded",
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);

          $timeout(function () {
            deferred.resolve(); // this aborts the request!
          }, timeoutms);
        },
        forgot: function (data, success, error) {
          //(data);
          var deferred = $q.defer();
          $http
            .post(baseUrl + "/api/resetpassword", data, {
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);

          $timeout(function () {
            deferred.resolve(); // this aborts the request!
          }, timeoutms);
        },
        getProfile: function (token, success, error) {
          //(token);
          var url = "/api/myprofile";
          $http.defaults.headers.common["Authorization"] = basicAuthentication;
          var headers = {
            Authorization: "Basic dGFsZW50czpzZWNyZXQ=",
            "Content-Type": "application/json",
          };
          var deferred = $q.defer();
          // $http.post(baseUrl + '/oauth/token?grant_type=password&username='+data.username+'&password='+data.password, data)
          $http
            .get(baseUrl + url, {
              headers: headers,
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);

          $timeout(function () {
            deferred.resolve(); // this aborts the request!
          }, timeoutms);
        },

        postRequestPresreeningApi: function (url, data, success, error) {
          $http
            .post(url, data, {
              headers: {
                Authorization: basicAuthenticationWebCareer,
                "Content-Type": "application/json",
              },
            })
            .success(success)
            .error(error);
        },

        refreshToken: function (refresh_token, success, error) {
          $http.defaults.headers.common["Authorization"] =
            "Basic dGFsZW50czpzZWNyZXQ=";
          var url =
            baseUrl +
            "/oauth/token?grant_type=refresh_token&refresh_token=" +
            refresh_token;
          var deferred = $q.defer();
          $http
            .post(url, {
              headers: {
                Authorization: basicAuthentication,
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);
          $timeout(function () {
            deferred.resolve(); // this aborts the request!
          }, timeoutms);
        },

        requestApi: function (access_token, url, success, error) {
          var bearerAuthentication = "Bearer " + access_token;
          $http
            .get(url, {
              headers: {
                Authorization: bearerAuthentication,
              },
            })
            .success(success)
            .error(error);
        },

        postRequestApi: function (
          access_token,
          url,
          data,
          success,
          error,
          preErrFunc
        ) {
          var bearerAuthentication = "Bearer " + access_token;
          var deferred = $q.defer();
          // $http.post(baseUrl + '/oauth/token?grant_type=password&username='+data.username+'&password='+data.password, data)
          $http
            .post(url, data, {
              headers: {
                Authorization: bearerAuthentication,
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(function (data, status) {
              if (preErrFunc != undefined && preErrFunc != null) {
                preErrFunc();
              }
              error(data, status);
            });
          // .error(error)

          $timeout(function () {
            deferred.resolve(); // this aborts the request!
          }, timeoutms);
        },

        // PP API METHOD
        postRequestApiPP: function (url, data, success, error) {
          var deferred = $q.defer();
          $http
            .post(url, data, {
              headers: {
                "APP-KEY": appKeyTecat,
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);

          $timeout(function () {
            deferred.resolve();
          }, timeoutms);
        },

        // TECAT API METHOD
        getApi: function (url, success, error) {
          $http
            .get(url, {
              headers: {
                "APP-KEY": appKeyTecat,
              },
            })
            .success(success)
            .error(error);
        },

        postApi: function (url, data, success, error) {
          var deferred = $q.defer();
          $http
            .post(url, data, {
              headers: {
                "APP-KEY": appKeyTecat,
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);

          $timeout(function () {
            deferred.resolve();
          }, timeoutms);
        },
        // END OF TECAT API METHOD

        //DPA API METHOD
        postRequestDPAApi: function (access_token, url, data, success, error) {
          //var dataApi = 'keyMitra=84eabfedca14f5c3f70ad822a0222fd1&NPK=8713&KodePerusahaan=706';
          var bearerAuthentication = "Bearer " + access_token;
          var deferred = $q.defer();
          var result = $http
            .post(url, data, {
              headers: {
                //'Authorization' : bearerAuthentication,
                "Content-Type": "application/x-www-form-urlencoded",
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);
          $timeout(function () {
            deferred.resolve();
          }, timeoutms);
        },
        postRequestApproveDPAApi: function (
          access_token,
          url,
          data,
          success,
          error
        ) {
          var xhr = new XMLHttpRequest();
          xhr.withCredentials = true;

          xhr.addEventListener("readystatechange", function () {
            if (this.readyState === 4) {
              console.log(this.responseText);
            }
          });

          xhr.open("POST", url);
          xhr.setRequestHeader("Content-Type", "application/json");
          xhr.setRequestHeader("Key", "bgck6AWZbAG8mjCdApQ66z3zE3Q3Ykwd");

          xhr.send(data);
        },
        ////DPA API METHOD

        putRequestApi: function (access_token, url, data, success, error) {
          var bearerAuthentication = "Bearer " + access_token;
          var deferred = $q.defer();
          // $http.post(baseUrl + '/oauth/token?grant_type=password&username='+data.username+'&password='+data.password, data)
          $http
            .put(url, data, {
              headers: {
                Authorization: bearerAuthentication,
              },
              timeout: deferred.promise,
            })
            .success(success)
            .error(error);

          $timeout(function () {
            deferred.resolve(); // this aborts the request!
          }, timeoutms);
        },

        requestUrl: function (url, success, error) {
          $http.get(url).success(success).error(error);
        },
        logout: function (success) {
          changeUser({});
          delete $localStorage.token;
          success();
        },
        setSession: function (key, value) {
          return localStorage.setItem(key, JSON.stringify(value));
        },
        getSession: function (key) {
          if (
            localStorage.getItem(key) === null ||
            localStorage.getItem(key) === undefined
          )
            return null;
          else return JSON.parse(localStorage.getItem(key));
        },
        destroySession: function (key) {
          return localStorage.removeItem(key);
        },
        getTakePictureOptions: function () {
          return takePictureOptions;
        },
        getDataReference: function (dataRef, category, subCategory, field) {
          var result = null;
          if (dataRef.length > 0) {
            for (var i = dataRef.length - 1; i >= 0; i--) {
              var obj = dataRef[i];
              if (
                obj.category == category &&
                obj.subCategory == subCategory &&
                obj.field == field
              )
                return obj.value;
            }
          }
          return result;
        },
        cleanData: function () {
          localStorage.removeItem("profile");
          localStorage.removeItem("token");
          localStorage.removeItem("balance");
          localStorage.removeItem("categoryType");
          localStorage.removeItem("tmCategoryType");
          localStorage.removeItem("value");
          localStorage.removeItem("atcode");
          localStorage.removeItem("totalpages");
          localStorage.removeItem("totalpagesapproval");
          $rootScope.refreshRequestApprovalCtrl = true;

          if ($rootScope.user != undefined) {
            delete $rootScope.user;
          }

          if ($rootScope.countApproval != undefined)
            delete $rootScope.countApproval;

          if ($rootScope.countAnnouncement != undefined)
            delete $rootScope.countAnnouncement;

          if ($rootScope.team != undefined) delete $rootScope.team;

          if ($rootScope.needApprovalTM != undefined)
            delete $rootScope.needApprovalTM;

          if ($rootScope.selectEmployeeSubstitute != undefined)
            delete $rootScope.selectEmployeeSubstitute;
        },

        saveDataBenefit: function (databenefit) {
          return localStorage.setItem("benefit", databenefit);
        },

        getDataBenefit: function () {
          return localStorage.getItem("benefit");
        },

        saveEmployee: function (dataemployee) {
          return localStorage.setItem("employee", dataemployee);
        },

        getDataEmployee: function () {
          return localStorage.getItem("employee");
        },

        saveDataEmployeeNpk: function (npk) {
          return localStorage.setItem("npk", npk);
        },

        getDataEmployeeNpk: function () {
          return localStorage.getItem("npk");
        },

        saveDataEmployeeId: function (id) {
          return localStorage.setItem("employeeId", id);
        },

        getDataEmployeeId: function () {
          return localStorage.getItem("employeeId");
        },

        saveGroupCode: function (id) {
          return localStorage.setItem("groupcode", id);
        },

        getGroupCode: function () {
          return localStorage.getItem("groupcode");
        },

        saveGroupName: function (id) {
          return localStorage.setItem("groupname", id);
        },

        getGroupName: function () {
          return localStorage.getItem("groupname");
        },

        saveDatapatternCode: function (id) {
          return localStorage.setItem("patterncode", id);
        },

        getDatapatternCode: function () {
          return localStorage.getItem("patterncode");
        },

        saveDataattendanceGroupCode: function (id) {
          return localStorage.setItem("atcode", id);
        },

        saveDataattendanceGroupName: function (id) {
          return localStorage.setItem("atcode", id);
        },

        saveDataattendanceGroupWorkingHours: function (id) {
          return localStorage.setItem("atcode", id);
        },

        saveDataattendanceGroupHoliday: function (id) {
          return localStorage.setItem("atcode", id);
        },

        getDataattendanceGroupName: function () {
          return localStorage.getItem("atcode");
          // localStorage.removeItem('atcode');
        },

        getDataattendanceGroupCode: function () {
          return localStorage.getItem("atcode");
          // localStorage.removeItem('atcode');
        },

        getDataattendanceGroupWorkingHours: function (id) {
          return localStorage.setItem("atcode", id);
        },

        getDataattendanceGroupHoliday: function (id) {
          return localStorage.setItem("atcode", id);
        },

        saveDataTotalPage: function (totalpages) {
          return localStorage.setItem("totalpages", totalpages);
        },

        getDataTotalPage: function () {
          return localStorage.getItem("totalpages");
        },

        saveDataTotalPageApproval: function (totalpages) {
          return localStorage.setItem("totalpagesapproval", totalpages);
        },

        getDataTotalPageApproval: function () {
          return localStorage.getItem("totalpagesapproval");
        },

        getApiKey: function () {
          return mapApiKey;
        },

        getImageServletPrefix: function () {
          return imageServletPrefix;
        },
      };
    })

    .factory("mapsInit", [
      "$window",
      "$q",
      "Main",
      function ($window, $q, Main) {
        var data = Main.getApiKey();
        var asyncUrl =
          "https://maps.googleapis.com/maps/api/js?key=" +
          data +
          "&libraries=geometry&callback=";
        var mapsDefer = $q.defer();
        $window.googleMapsInitialized = mapsDefer.resolve;
        var asyncLoad = function (asyncUrl, callbackName) {
          var script = document.createElement("script");
          script.src = asyncUrl + callbackName;
          document.body.appendChild(script);
        };

        //Start loading google maps
        asyncLoad(asyncUrl, "googleMapsInitialized");
        return {
          mapsInitialized: mapsDefer.promise,
        };
      },
    ])
    .run(function ($ionicPlatform) {
      $ionicPlatform.ready(function () {
        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }
      });
    });
})();
