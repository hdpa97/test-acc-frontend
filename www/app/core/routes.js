angular.module('talent.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('tabs', {
        url: '/tabs',
        templateUrl: 'app/core/sidemenu.html?custv=161',
        abstract: true
      })

      .state('authentication', {
        url: '/authentication',
        templateUrl: 'app/intro/authentication.html?custv=161',
        controller: 'appCtrl',
      })

      .state('intro', {
        url: '/intro',
        templateUrl: 'app/intro/intro.html?custv=161',
      })

      // talent
      .state('app', {
        url: '/app',
        templateUrl: 'app/core/sidemenu.html?custv=161',
        abstract: true
      })

      .state('login', {
        url: '/login',
        templateUrl: 'app/talents/intro/login-form.html?custv=161',
        controller: 'LoginCtrl'
      })


      .state('signup', {
        url: '/signup',
        controller: "RegisterCtrl",
        templateUrl: 'app/talents/intro/signup-form.html?custv=161'
      })

      .state('downloads', {
        url: '/downloads',
        controller: "DownloadsCtrl",
        templateUrl: 'app/talents/intro/downloads.html?custv=161'
      })

      .state('forgetpassword', {
        url: '/resetpassword/:id/:token',
        controller: 'ForgetPasswordCtrl',
        templateUrl: 'app/talents/intro/forget-password.html?custv=161'
      })

      .state('app.forgot', {
        url: '/forgot',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/intro/forgot.html?custv=161',
            controller: 'appCtrl'
          }
        }
      })

      .state('app.myhr', {
        url: '/myhr',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/myhr-home.html?custv=161',
            controller: 'MyHRCtrl'
          }
        }
      })

      .state('app.biodata', {
        url: '/biodata',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/information.html?custv=161',
            controller: 'PersonalCtrl'
          }
        }
      })

      .state('app.checkinout', {
        url: '/checkinout',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/checkinout.html?custv=161',
            controller: 'CheckInOutCtrl'
          }
        }
      })

      .state('app.check', {
        url: '/check/:type',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/check.html?custv=161',
            controller: 'CheckCtrl'
          }
        }
      })



      .state('app.historycheckin', {
        url: '/historycheckin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/checkinout/history.html?custv=161',
            controller: 'HistoryCheckinCtrl'
          }
        }
      })

      .state('app.dailycheckin', {
        url: '/dailycheckin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/checkinout/daily.html?custv=161',
            controller: 'DailyCheckinCtrl'
          }
        }
      })


      .state('app.detailpurpose', {
        url: '/detailpurpose/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/checkinout/detailpurpose.html?custv=161',
            controller: 'DetailPurposeCtrl'
          }
        }
      })

      .state('app.subordinatecheckin', {
        url: '/subordinatecheckin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/checkinout/subordinate.html?custv=161',
            controller: 'SubordinateCheckinCtrl'
          }
        }
      })

      .state('app.changemaritalstatus', {
        url: '/changemaritalstatus/:currentStatus/:dataApprovalId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/changemaritalstatus.html?custv=161',
            controller: 'ChangeMaritalStatusCtrl'
          }
        }
      })


      .state('app.changenpwp', {
        url: '/changenpwp/:currentStatus/:dataApprovalId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/changenpwp.html?custv=161',
            controller: 'ChangeNpwpCtrl'
          }
        }
      })

      .state('app.changenircno', {
        url: '/changenircno/:currentStatus/:dataApprovalId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/changenircno.html?custv=161',
            controller: 'ChangeNircNoCtrl'
          }
        }
      })


      .state('app.changektpname', {
        url: '/changektpname/:currentStatus/:dataApprovalId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/changektpname.html?custv=161',
            controller: 'ChangeKtpNameCtrl'
          }
        }
      })




      .state('app.changefamilycardno', {
        url: '/changefamilycardno/:currentStatus/:dataApprovalId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/changefamilycard.html?custv=161',
            controller: 'ChangeFamilyCardNoCtrl'
          }
        }
      })

      .state('app.edit-biodata', {
        url: '/edit-biodata',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/information/edit-biodata.html?custv=161',
            controller: 'appCtrl'
          }
        }
      })



      .state('app.family', {
        url: '/family',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/family/family.html?custv=161',
            controller: 'FamilyCtrl'
          }
        }
      })

      .state('app.addfamily', {
        url: '/addfamily/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/family/add-family.html?custv=161',
            controller: 'AddFamilyCtrl'
          }
        }
      })

      .state('app.editfamily', {
        url: '/editfamily/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/family/add-family.html?custv=161',
            controller: 'EditFamilyCtrl'
          }
        }
      })

      .state('app.detailfamily', {
        url: '/detailfamily/:idx/:edit',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/family/detail-family.html?custv=161',
            controller: 'DetailFamilyCtrl'
          }
        }
      })



      .state('app.certification', {
        url: '/certification',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/certification/certification.html?custv=161',
            controller: 'CertificationCtrl'
          }
        }
      })

      .state('app.addcertification', {
        url: '/addcertification',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/certification/add-certification.html?custv=161',
            controller: 'AddCertificationCtrl'
          }
        }
      })


      .state('app.detailcertification', {
        url: '/detailcertification/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/certification/detail-certification.html?custv=161',
            controller: 'DetailCertificationCtrl'
          }
        }
      })

      .state('app.viewimagecertification', {
        url: '/viewimagecertification/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/certification/view-image.html?custv=161',
            controller: 'ViewImageCertificationCtrl'
          }
        }
      })


      .state('app.selfservice', {
        url: '/selfservice',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/selfservice-home.html?custv=161',
            controller: 'SelfServiceCtrl'
          }
        }
      })

      .state('app.selfservicesuccess', {
        url: '/selfservicesuccess',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/selfservice-success.html?custv=161',
            controller: 'SelfServiceCtrl'
          }
        }
      })

      .state('app.printreport', {
        url: '/printreport',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printreport/report-home.html?custv=161',
            controller: 'PrintReportPhpCtrl'
          }
        }
      })

      .state('app.homeleave', {
        url: '/homeleave',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/home-leave.html?custv=161',
            controller: 'HomeLeaveCtrl'
          }
        }
      })

      .state('app.chooseleavecategory', {
        url: '/chooseleavecategory',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/choose-category.html?custv=161',
            controller: 'ChooseLeaveCategoryCtrl'
          }
        }
      })


      .state('app.addleave', {
        url: '/addleave/:category',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/add-leave.html?custv=161',
            controller: 'AddLeaveCtrl'
          }
        }
      })

      .state('app.leaves', {
        url: '/leaves',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/leave.html?custv=161',
            controller: 'ListLeaveCtrl'
          }
        }
      })

      .state('app.detailleave', {
        url: '/detailleave/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/detail-leave.html?custv=161',
            controller: 'DetailLeaveCtrl'
          }
        }
      })

      .state('app.listempdaily', {
        url: '/listempdaily',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/list-empdaily.html?custv=161',
            controller: 'ListEmpDailyCtrl'
          }
        }
      })

      .state('app.detailempdaily', {
        url: '/detailempdaily/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/detail-empdaily.html?custv=161',
            controller: 'DetailEmpDailyCtrl'
          }
        }
      })


      .state('app.submitattendance', {
        url: '/submitattendance',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/attendance/attendance.html?custv=161',
            controller: 'SimpleMapCtrl'
          }
        }
      })



      .state('app.choicepayslip', {
        url: '/choicepayslip',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/payslip/choice-payslip.html?custv=161',
            controller: 'ChoicePayslipCtrl'
          }
        }
      })

      .state('app.detailpayslip', {
        url: '/detailpayslip/:year/:month/:type',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/payslip/detail-payslip.html?custv=161',
            controller: 'DetailPayslipCtrl'
          }
        }
      })

      //Note Here CekSaldoDPA
      .state('app.choicesaldodpa', {
        url: '/choicesaldodpa',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/ceksaldodpa/choice-ceksaldodpa.html?custv=161',
            controller: 'ChoiceCheckSaldoDPACtrl'
          }
        }
      })

      .state('app.daftardpa', {
        url: '/daftardpa',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/daftardpa/daftardpa.html?custv=161',
            controller: 'DaftarDPACtrl'
          }
        }
      })

      .state('app.menudpa', {
        url: '/menudpa',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/menudpa/menudpa.html?custv=161',
            controller: 'MenuDPACtrl'
          }
        }
      })

      .state('app.menudpalist', {
        url: '/menudpalist',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/menudpa/menudpalist.html?custv=161',
            controller: 'MenuDPAListCtrl'
          }
        }
      })
      .state('app.detailrequest', {
        url: '/detailRequest/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/menudpa/detailRequest.html?custv=161',
            controller: 'DetailRequestCtrl'
          }
        }
      })
      //Note Here CekSaldoDPA

      .state('app.submitclaim', {
        url: '/submitclaim',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/submitclaim.html?custv=161',
            controller: 'SubmitClaimCtrl'
          }
        }
      })

      .state('app.claimchoice', {
        url: '/claimchoice',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/claimchoice.html?custv=161',
            controller: 'ClaimChoice'
          }
        }
      })

      .state('app.benefitclaimlist', {
        url: '/benefitclaimlist',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/benefitclaimlist.html?custv=161',
            controller: 'BenefitClaimListCtrl'
          }
        }
      })

      .state('app.benefitclaimneedreport', {
        url: '/benefitclaimneedreport',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/benefitclaimneedreport.html?custv=161',
            controller: 'BenefitClaimNeedReportCtrl'
          }
        }
      })



      .state('app.benefitdetail', {
        url: '/benefitdetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/benefitdetail.html?custv=161',
            controller: 'BenefitDetailCtrl'
          }
        }
      })

      .state('app.spdadvanceadd', {
        url: '/spdadvanceadd/:categoryType/:extId/:workflow',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/spdadvanceadd.html?custv=161',
            controller: 'SpdAdvanceAdd'
          }
        }
      })


      .state('app.benefitlisttype', {
        url: '/benefitlisttype/:categoryType/:extId/:workflow/:singleinput',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/benefitlisttype.html?custv=161',
            controller: 'BenefitListtypeCtrl'
          }
        }
      })

      .state('app.benefitconfirmation', {
        url: '/benefitconfirmation',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/claim/benefitconfirmation.html?custv=161',
            controller: 'BenefitConfirmationCtrl'
          }
        }
      })
      .state('app.leaveconfirmation', {
        url: '/leaveconfirmation',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/leaveconfirmation.html?custv=161',
            controller: 'LeaveConfirmationCtrl'
          }
        }
      })


      .state('app.changepassword', {
        url: '/changepassword',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/profile/change-password.html?custv=161',
            controller: 'ChangePasswordCtrl'
          }
        }
      })

      .state('app.address', {
        url: '/address',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/address/address.html?custv=161',
            controller: 'AddressCtrl'
          }
        }
      })

      .state('app.addaddress', {
        url: '/addaddress/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/address/add-address.html?custv=161',
            controller: 'AddAddressCtrl'
          }
        }
      })

      .state('app.editaddress', {
        url: '/editaddress/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/address/add-address.html?custv=161',
            controller: 'EditAddressCtrl'
          }
        }
      })

      .state('app.detailaddress', {
        url: '/detailaddress/:idx/:edit',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myhr/address/detail-address.html?custv=161',
            controller: 'DetailAddressCtrl'
          }
        }
      })



      .state('app.home', {
        url: '/home',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/home/dashboard.html?custv=161',
            controller: 'HomeCtrl'
          }
        }
      })

      .state('app.myteam', {
        url: '/myteam',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/myteam-home.html?custv=161',
            controller: 'MyTeamCtrl'
          }
        }
      })

      .state('app.myteamdetail', {
        url: '/myteamdetail/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/myteam-detail-home.html?custv=161',
            controller: 'DetailTeamCtrl'
          }
        }
      })

      .state('app.requestapproval', {
        url: '/requestapproval/:submodule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request-approval.html?custv=161',
            controller: 'RequestApprovalCtrl'
          }
        }
      })

      .state('app.requestapprovalhome', {
        url: '/requestapprovalhome',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request-approval-home.html?custv=161',
            controller: 'RequestApprovalHomeCtrl'
          }
        }
      })

      .state('app.requestapprovalsub', {
        url: '/requestapprovalsub/:submodule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request-approval-sub.html?custv=161',
            controller: 'RequestApprovalSubCtrl'
          }
        }
      })

      .state('app.requestapprovalspdadvance', {
        url: '/requestapprovalspdadvance/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html?custv=161',
            controller: 'RequestApprovalSpdCtrl'
          }
        }
      })

      .state('app.requestapprovalspdrealization', {
        url: '/requestapprovalspdrealization/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html?custv=161',
            controller: 'RequestApprovalSpdCtrl'
          }
        }
      })

      .state('app.formhistoryapproval', {
        url: '/formhistoryapproval',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/approval/form-history-approval.html?custv=161',
            controller: 'FormHistoryApprovalCtrl'
          }
        }
      })

      .state('app.historyapproval', {
        url: '/historyapproval/:startDate/:endDate',
        params: {
          data: null
        },
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/approval/history-approval.html?custv=161',
            controller: 'HistoryApprovalCtrl'
          }
        }
      })

      .state('app.approvalhistory', {
        url: '/approvalhistory',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/approval/approval-history-home.html?custv=161',
            controller: 'ApprovalHistoryHomeCtrl'
          }
        }
      })

      .state('app.approvalhistorysub', {
        url: '/approvalhistorysub/:submodule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/approval/approval-history-sub.html?custv=161',
            controller: 'ApprovalHistorySubCtrl'
          }
        }
      })

      .state('app.approvalhistoryspdadvance', {
        url: '/approvalhistoryspdadvance/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html?custv=161',
            controller: 'ApprovalHistorySpdCtrl'
          }
        }
      })

      .state('app.approvalhistoryspdrealization', {
        url: '/approvalhistoryspdrealization/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html?custv=161',
            controller: 'ApprovalHistorySpdCtrl'
          }
        }
      })

      .state('app.myrequest', {
        url: '/myrequest/:submodule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequest.html?custv=161',
            controller: 'MyRequestCtrl'
          }
        }
      })

      .state('app.myrequesthome', {
        url: '/myrequesthome',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequest-home.html?custv=161',
            controller: 'MyRequestHomeCtrl'
          }
        }
      })

      .state('app.myrequestsub', {
        url: '/myrequestsub/:submodule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequestsub.html?custv=161',
            controller: 'MyRequestSubCtrl'
          }
        }
      })

      .state('app.myrequestspdadvance', {
        url: '/myrequestspdadvance/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html',
            controller: 'MyRequestSpdCtrl'
          }
        }
      })

      .state('app.myrequestspdrealization', {
        url: '/myrequestspdrealization/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html',
            controller: 'MyRequestSpdCtrl'
          }
        }
      })

      .state('app.listattendancegroup', {
        url: '/listattendancegroup',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/listattendancegroup.html?custv=161',
            controller: 'ListAttendanceGroupCtrl'
          }
        }
      })




      .state('app.requestdetail', {
        url: '/requestdetail/:id/:needApproval',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/request-detail.html?custv=161',
            controller: 'RequestDetailCtrl'
          }
        }
      })


      .state('app.myrequestdetail', {
        url: '/myrequestdetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequest-detail.html?custv=161',
            controller: 'MyRequestDetailCtrl'
          }
        }
      })



      .state('app.myrequestdetailletter', {
        url: '/myrequestdetailletter/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequest-detail-letter.html?custv=161',
            controller: 'RequestDetailLeterCtrl'
          }
        }
      })

      .state('app.formrequestsearching', {
        url: '/formrequestsearching',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/formrequestsearching.html?custv=161',
            controller: 'FormRequestSearchingCtrl'
          }
        }
      })


      .state('app.contacts', {
        url: '/contacts',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/contacts/contacts.html?custv=161',
            controller: 'appCtrl'
          }
        }
      })

      .state('app.profile', {
        url: '/profile',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/profile/profile.html?custv=161',
            controller: 'ProfileCtrl'
          }
        }
      })


      .state('app.about', {
        url: '/about',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/others/about.html?custv=161',
            controller: 'AboutCtrl'
          }
        }
      })


      .state('app.support', {
        url: '/support',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/others/support.html?custv=161',
            controller: 'SupportCtrl'
          }
        }
      })

      .state('app.announcement', {
        url: '/announcement',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/others/announcement.html?custv=161',
            controller: 'AnnouncementCtrl'
          }
        }
      })

      .state('app.mycompany', {
        url: '/mycompany',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/others/mycompany.html?custv=161',
            controller: 'MyCompanyCtrl'
          }
        }
      })



      .state('app.edit-profile', {
        url: '/edit-profile',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/profile/edit-profile.html?custv=161',
            controller: 'EditProfileCtrl'
          }
        }
      })



      .state('app.addtimesheet', {
        url: '/addtimesheet',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/timesheet/add-timesheet.html?custv=161',
            controller: 'AddTimesheetCtrl'
          }
        }
      })

      .state('app.detailtimesheet', {
        url: '/detailtimesheet',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/timesheet/detail-timesheet.html?custv=161',
            controller: 'DetailTimesheetCtrl'
          }
        }
      })

      .state('app.calendartimesheet', {
        url: '/calendartimesheet',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/timesheet/calendar-timesheet.html?custv=161',
            controller: 'CalendarTimesheetCtrl'
          }
        }
      })

      .state('app.listtimesheet', {
        url: '/listtimesheet',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/timesheet/list-timesheet.html?custv=161',
            controller: 'ListTimesheetCtrl'
          }
        }
      })


      .state('app.choicesptai', {
        url: '/choicesptai',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/sptai/choice-sptai.html?custv=161',
            controller: 'ChoiceSptaiCtrl'
          }
        }
      })


      .state('app.payrollaccount', {
        url: '/payrollaccount',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/payrollaccount/payrollaccount.html?custv=161',
            controller: 'PayrollAccountCtrl'
          }
        }
      })

      .state('app.printletter', {
        url: '/printletter',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/choiceletter.html?custv=161',
            controller: 'ChoiceLetterCtrl'
          }
        }
      })



      .state('app.approvaldelegation', {
        url: '/approvaldelegation',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/approvaldelegation/delegation.html?custv=161',
            controller: 'ApprovalDelegationCtrl'
          }
        }
      })



      .state('app.perfomance', {
        url: '/perfomance',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/perfomance/perfomance.html?custv=161',
            controller: 'PerfomanceCtrl'
          }
        }
      })


      .state('app.perfomance_list', {
        url: '/perfomancelist',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/perfomance/perfomance_list.html?custv=161',
            controller: 'PerfomanceListCtrl'
          }
        }
      })


      .state('app.perfomance_input', {
        url: '/perfomanceinput',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/perfomance/perfomance_input.html?custv=161',
            controller: 'PerfomanceInputCtrl'
          }
        }
      })

      .state('app.perfomance_detail', {
        url: '/perfomancedetail/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/perfomance/perfomance_detail.html?custv=161',
            controller: 'PerfomanceDetailCtrl'
          }
        }
      })


      .state('app.shift', {
        url: '/shift',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/shift.html?custv=161',
            controller: 'ShiftCtrl'
          }
        }
      })


      .state('app.addshift', {
        url: '/addshift/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/add-shift.html?custv=161',
            controller: 'AddShiftCtrl'
          }
        }
      })



      .state('app.pattern', {
        url: '/pattern',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/pattern.html?custv=161',
            controller: 'PatternCtrl'
          }
        }
      })


      .state('app.addpattern', {
        url: '/addpattern/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/add-pattern.html?custv=161',
            controller: 'AddPatternCtrl'
          }
        }
      })





      .state('app.detailpattern', {
        url: '/detailpattern/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/detail-pattern.html?custv=161',
            controller: 'DetailPatternCtrl'
          }
        }
      })



      .state('app.addpatterndetail', {
        url: '/addpatterndetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/add-pattern-detail.html?custv=161',
            controller: 'AddPatternDetailCtrl'
          }
        }
      })



      .state('app.attendancegroup', {
        url: '/attendancegroup',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/attendancegroup.html?custv=161',
            controller: 'AttendancegroupCtrl'
          }
        }
      })


      .state('app.addattendancegroup', {
        url: '/addattendancegroup/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/add-attendancegroup.html?custv=161',
            controller: 'AddAttendancegroupCtrl'
          }
        }
      })




      .state('app.detailattendancegroup', {
        url: '/detailattendancegroup/:id/:patternId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/detail-attendancegroup.html?custv=161',
            controller: 'DetailAttendancegroupCtrl'
          }
        }
      })



      .state('app.addattendancegroupdetail', {
        url: '/addattendancegroupdetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/add-attandancegroup-detail.html?custv=161',
            controller: 'AddAttendancegroupDetailCtrl'
          }
        }
      })



      // assign attendance group
      .state('app.assignattendancegroupsearch', {
        url: '/assignattendancegroupsearch',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/assignattendancegroupsearch.html?custv=161',
            controller: 'AssignAttendancegroupSearchCtrl'
          }
        }
      })


      .state('app.assignattendancegroup', {
        url: '/assignattendancegroup/:npk',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/assignattendancegroup.html?custv=161',
            controller: 'AssignAttendancegroupCtrl'
          }
        }
      })


      .state('app.assignattendancegroupbygroup', {
        url: '/assignattendancegroupbygroup/:groupCode',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/assignattendancegroupbygroup.html?custv=161',
            controller: 'AssignAttendancegroupbygroupCtrl'
          }
        }
      })


      .state('app.addassignattendancegroup', {
        url: '/addassignattendancegroup/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/add-assignattendancegroup.html?custv=161',
            controller: 'AddAssignAttendancegroupCtrl'
          }
        }
      })


      .state('app.detailassignattendancegroup', {
        url: '/detailassignattendancegroup/:id/:patternId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/detail-assignattendancegroup.html?custv=161',
            controller: 'DetailAssignAttendancegroupCtrl'
          }
        }
      })


      .state('app.SRT_AMANDEMENT', {
        url: '/amandement',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/amandement.html?custv=161',
            controller: 'AmandementLetterCtrl'
          }
        }
      })


      .state('app.SRT_PKWT', {
        url: '/pkwt',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/pkwt.html?custv=161',
            controller: 'PkwtLetterCtrl'
          }
        }
      })


      .state('app.SRT_PER_PERUSAHAAN', {
        url: '/suratperperusahaan',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/peraturanperusahaan/perperusahaan.html?custv=161',
            controller: 'PPLetterCtrl'
          }
        }
      })

      .state('app.SRT_KET_KERJA', {
        url: '/statment',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/statment.html?custv=161',
            controller: 'StatmentLetterCtrl'
          }
        }
      })


      .state('app.SRT_PENGANGKATAN', {
        url: '/assignation',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/assignation.html?custv=161',
            controller: 'AssignationLetterCtrl'
          }
        }
      })



      .state('app.SRT_PERNYATAAN_PAJAK', {
        url: '/tax',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/tax.html?custv=161',
            controller: 'TaxLetterCtrl'
          }
        }
      })


      .state('app.SRT_VISA', {
        url: '/visa',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/printletter/visa.html?custv=161',
            controller: 'VisaLetterCtrl'
          }
        }
      })








      .state('app.addovertime', {
        url: '/addovertime/:category',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/add-overtime.html?custv=161',
            controller: 'AddOvertimeCtrl'
          }
        }
      })

      .state('app.overtimerealization', {
        url: '/overtimerealization/:category',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/overtime-realization.html?custv=161',
            controller: 'OvertimeRealizationCtrl'
          }
        }
      })



      .state('app.addovertimerealization', {
        url: '/addovertimerealization/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/add-overtime-realization.html?custv=161',
            controller: 'AddOvertimeRealizationCtrl'
          }
        }
      })



      .state('app.absencetransaction', {
        url: '/absencetransaction/:category',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/absencetransaction.html?custv=161',
            controller: 'AbsencetransactionCtrl'
          }
        }
      })


      .state('app.editabsencetransaction', {
        url: '/editabsencetransaction/:idx',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/editabsencetransaction.html?custv=161',
            controller: 'EditabsencetransactionCtrl'
          }
        }
      })







      .state('app.approvalcheckinout', {
        url: '/approvalcheckinout',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/checkinout/approval.html?custv=161',
            controller: 'ApprovalCheckinoutCtrl'
          }
        }
      })

      .state('app.approvalovertime', {
        url: '/approvalovertime',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/leave/approval-overtime.html?custv=161',
            controller: 'ApprovalOvertimeCtrl'
          }
        }
      })



      .state('app.worklocation', {
        url: '/worklocation',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/list-work-location.html?custv=161',
            controller: 'WorkLocationCtrl'
          }
        }
      })
      .state('app.worklocationdetail', {
        url: '/worklocationdetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/timeattendance/detail-work-location.html?custv=161',
            controller: 'WorkLocationDetailCtrl'
          }
        }
      })




      .state('app.reassignpic', {
        url: '/reassignpic',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/reassignpic/list-reassignpic.html?custv=161',
            controller: 'ListReassignPicCtrl'
          }
        }
      })


      .state('app.reassigndetail', {
        url: '/reassigndetail/:location',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/reassignpic/detail-reassignpic.html?custv=161',
            controller: 'ReassigndetailCtrl'
          }
        }
      })



      .state('app.applicant-selection', {
        url: '/applicant-selection/:applicantId',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/applicant-selection.html?custv=161',
            controller: 'ApplicantSelectionCtrl'
          }
        }
      })


      .state('app.profile-candidate', {
        url: '/profile-candidate/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/profile-candidate.html?custv=161',
            controller: 'ProfileCandidateCtrl'
          }
        }
      })

      .state('app.interview-main', {
        url: '/interview-main',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/interview-main.html?custv=161',
            controller: 'InterviewMainCtrl'
          }
        }
      })

      .state('app.interview-schedule', {
        url: '/interview-schedule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/interview-schedule.html?custv=161',
            controller: 'InterviewScheduleCtrl'
          }
        }
      })

      .state('app.prescreening-result', {
        url: '/prescreening-result/:idUser/:idJob',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/prescreening-result.html?custv=161',
            controller: 'PrescreeningResultCtrl'
          }
        }
      })

      .state('app.interview-result', {
        url: '/interview-result/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/interview-result.html?custv=161',
            controller: 'InterviewResultCtrl'
          }
        }
      })

      .state('app.interview-result-detail', {
        url: '/interview-result-detail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/form-interview.html?custv=161',
            controller: 'InterviewResultDetailCtrl'
          }
        }
      })


      .state('app.form-interview', {
        url: '/form-interview/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/form-interview.html?custv=161',
            controller: 'FormInterviewCtrl'
          }
        }
      })

      .state('app.interview-schedule-admin', {
        url: '/interview-schedule-admin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/interview-schedule-admin.html?custv=161',
            controller: 'InterviewScheduleAdminCtrl'
          }
        }
      })

      .state('app.interview-supporting-doc', {
        url: '/interview-supporting-doc/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/supporting-document.html?custv=161',
            controller: 'IntvSupportingDocCtrl'
          }
        }
      })

      .state('app.interview-supporting-doc-upload', {
        url: '/interview-supporting-doc-upload/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/career/application-candidate/supporting-document-upload.html?custv=161',
            controller: 'IntvSupportingDocUploadCtrl'
          }
        }
      })

      // begin section of Self service SPD

      .state('app.spd-home', {
        url: '/spd-home',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-home.html?custv=161',
            controller: 'SpdHomeCtrl'
          }
        }
      })


      .state('app.spd-advance', {
        url: '/spd-advance',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-list.html?custv=161',
            controller: 'SpdListCtrl'
          }
        }
      })

      .state('app.spd-advance-req', {
        url: '/spd-advance-req/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html?custv=161',
            controller: 'SpdRequestCtrl'
          }
        }
      })

      .state('app.spd-realization', {
        url: '/spd-realization',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-list.html?custv=161',
            controller: 'SpdListCtrl'
          }
        }
      })

      .state('app.spd-realization-req', {
        url: '/spd-realization-req/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/spd/spd-detail.html?custv=161',
            controller: 'SpdRequestCtrl'
          }
        }
      })

      // end of section Self service SPD


      // admin section
      .state('app.administration', {
        url: '/administration',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/admin/administration-home.html?custv=161',
            controller: 'AdministrationCtrl'
          }
        }
      })

      .state('app.admincatitem', {
        url: '/admincatitem',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/admin/category-item/admin-cat-item.html?custv=161',
            controller: 'AdminCategoryItemCtrl'
          }
        }
      })

      .state('app.admincatitemdetail', {
        url: '/admincatitemdetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/admin/category-item/admin-cat-item-detail.html?custv=161',
            controller: 'AdminCategoryItemDetailCtrl'
          }
        }
      })

      .state('app.adminsecuritygroups', {
        url: '/adminsecuritygroups',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/admin/security-groups/admin-security-groups.html?custv=161',
            controller: 'AdminSecurityGroupCtrl'
          }
        }
      })

      .state('app.adminsecuritygroupdetail', {
        url: '/securitygroupsdetail/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/admin/security-groups/admin-security-groups-detail.html?custv=161',
            controller: 'AdminSecurityGroupDetailCtrl'
          }
        }
      })

      // end of admin section

      // admin transaction history section begin

      .state('app.adminrequestsub', {
        url: '/adminrequestsub/:submodule',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/admin/trxhistory/history-list.html?custv=161',
            controller: 'AdminRequestSubCtrl'
          }
        }
      })

      .state('app.adminrequestspdadvance', {
        url: '/adminrequestspdadvance/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequest-fkp.html?custv=161',
            controller: 'AdminRequestSpdAdvanceCtrl'
          }
        }
      })

      .state('app.adminrequestspdrealization', {
        url: '/adminrequestspdrealization/:id',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/myteam/request/myrequest-fkp.html?custv=161',
            controller: 'AdminRequestSpdRealizationCtrl'
          }
        }
      })

      // end of admin transaction history section

      // ROUTE TECAT

      .state('app.dailyTestReport', {
        url: '/dailyTestReport',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/dailyTest/report/dailyTestReport.html',
            controller: 'DailyTestTeamReportCtrl'
          }
        }
      })

      .state('app.individualReport', {
        url: '/individualReport',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/dailyTest/report/individualReport.html',
            controller: 'DailyTestIndividualReportCtrl'
          }
        }
      })

      .state('app.detailPoin', {
        url: '/detailPoin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/dailyTest/points/detailPoin.html',
            controller: 'DetailPoinCtrl'
          }
        }
      })

      .state('app.pendingPoin', {
        url: '/pendingPoin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/dailyTest/points/pendingPoin.html',
            controller: 'PendingPoinCtrl'
          }
        }
      })

      .state('app.summaryProfile', {
        url: '/summaryProfile',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/dailyTest/points/summaryProfile.html',
            controller: 'SummaryProfileCtrl'
          }
        }
      })

      .state('app.historyPoin', {
        url: '/historyPoin',
        views: {
          'menuContent': {
            templateUrl: 'app/talents/selfservice/dailyTest/points/historyPoin.html',
            controller: 'HistoryPoinCtrl'
          }
        }
      })

    // END OF ROUTE TECAT

    ;

    $urlRouterProvider.otherwise('/app/myhr')



  })

  .config(function ($ionicConfigProvider, calendarConfig, ChartJsProvider) {

    // $ionicConfigProvider.tabs.style('standard').position('top');
    // $ionicConfigProvider.navBar.alignTitle('center');

    ChartJsProvider.setOptions({
      colours: ['#26a69a', '#29b6f6', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
    });

    calendarConfig.titleFormats.week = 'MMMM';
    calendarConfig.dateFormatter = 'moment';
    calendarConfig.allDateFormats.moment.date.hour = 'HH:mm';
    calendarConfig.allDateFormats.moment.title.day = 'ddd D MMM';
    calendarConfig.i18nStrings.weekNumber = 'Week {week}';
    calendarConfig.dateFormats.weekDay = 'ddd';
    calendarConfig.dateFormats.day = 'D';
    calendarConfig.displayAllMonthEvents = true;
    calendarConfig.displayEventEndTimes = true;
  })
