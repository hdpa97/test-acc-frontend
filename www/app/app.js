angular.module('talent',
    [
      'ngIdle',
      'ionic',
      'cera.ionicSuperPopup',
      'ionic-timepicker',
      'ionic-modal-select',
      'ngCordova',
      'onezone-datepicker',
      'ngStorage',
      'profile.controllers',
      'timesheet.controllers',
      'leave.controllers',
      'selfservice.controllers',
      'others.controllers',
      'home.controllers',
      'myteam.controllers',
      'myhr.controllers',
      'intro.controllers',
      'talent.controllers',
      'talent.routes',
      'main.services',
      'authentication.services',
      'talent.services',
      'talent.directives',
      'tm.controllers',
      'tm.services',
      'career.controllers',
      'global.constant',
      'ngFileUpload',
      'spd.controllers',
      'admin.controllers',
      'admin.categoryitem.controllers',
      'admin.trxhistory.controllers',
      'admin.securitygroup.controller'
    ]
  )

  .run(function (Idle, $ionicPlatform, appService, $ionicHistory) {

    $ionicPlatform.ready(function () {

      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });

    // Disable BACK button on home
    $ionicPlatform.registerBackButtonAction(function (e) {
      e.preventDefault();

      function showConfirm() {
        var lastTimeBackPress = 0;
        var timePeriodToExit = 2000;

        function onBackKeyDown(e) {
          e.preventDefault();
          e.stopPropagation();
          if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
            navigator.app.exitApp();
          } else {
            window.plugins.toast.showWithOptions({
              message: "Press again to exit.",
              duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
              position: "bottom",
              addPixelsY: -40 // added a negative value to move it up a bit (default 0)
            });

            lastTimeBackPress = new Date().getTime();
          }
        };
        document.addEventListener("backbutton", onBackKeyDown, false);
      };

      if ($ionicHistory.backView()) {
        $ionicHistory.backView().go();
      } else {
        showConfirm();
      }

      return false;
    }, 101);
  })

  .filter('round', function () {
    return function (value, mult, dir) {
      dir = dir || 'nearest';
      mult = mult || 1;
      value = !value ? 0 : Number(value);
      if (dir === 'up') {
        return Math.ceil(value / mult) * mult;
      } else if (dir === 'down') {
        return Math.floor(value / mult) * mult;
      } else {
        return Math.round(value / mult) * mult;
      }
    };
  })
  .filter('timeWithoutSecond', ['$filter', function ($filter) {
    return function (input, arg) {
      if (input != undefined || input != null) {
        var parts = input.split(':');
        var date = new Date(0, 0, 0, parts[0], parts[1], parts[2]);
        return $filter('date')(date, arg || 'HH:mm');
      }
    };
  }])


  .filter('customSplit', function () {
    return function (input) {
      if (input != undefined || input != null) {
        var ar = input.split(','); // this will make string an array 
        return ar[0];
      }
    };
  })



  .filter('customSplit2', function () {
    return function (input) {
      if (input != undefined || input != null) {
        var ar = input.split(','); // this will make string an array 
        return ar[1];
      }
    };
  })


  .filter('customSplit3', function () {
    return function (input) {
      if (input != undefined || input != null) {
        var ar = input.split(','); // this will make string an array 
        return ar[2];
      }
    };
  })



  .filter('customSplit4', function () {
    return function (input) {
      if (input != undefined || input != null) {
        var ar = input.split(','); // this will make string an array 
        return ar[3];
      }
    };
  })

  .filter('concatImage', ['Main', function (Main) {
    return function (input) {
      if (input != undefined && input != null) {
        var tmpImage = Main.getImageServletPrefix() + input;
        return tmpImage;
      }
    };
  }])

  .config(['KeepaliveProvider', 'IdleProvider', function (KeepaliveProvider, IdleProvider) {
    // IdleProvider.idle(15*60);
    // IdleProvider.timeout(30);
    // KeepaliveProvider.interval(5*10);


    IdleProvider.idle(15 * 60); // 10 minutes idle
    IdleProvider.timeout(30); // after 30 seconds idle, time the user out
    KeepaliveProvider.interval(5 * 10); // 5 minute keep-alive ping
  }]);
