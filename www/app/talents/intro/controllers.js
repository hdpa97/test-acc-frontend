angular.module('intro.controllers', [])
  .controller('LoginCtrl', function (Idle, $timeout, $ionicHistory, $ionicLoading, $state, $rootScope, $scope, Main) {
    //$scope.usertalent = {username:'',password:'',rememberflag:true};
    /**
     * @author:fredy
     * this condition checks whether the user is remembered or not
     *
     */

    function rememberMe() {
      if ($scope.usertalent.rememberflag) {
        // rememberMeService('7ZXYZ@L', Base64.encode(usertalent.username));
        // rememberMeService('UU@#90', Base64.encode(usertalent.password));

        Main.setSession('7ZXYZ@L', btoa($scope.usertalent.username));
        Main.setSession('UU@#90', btoa($scope.usertalent.password));
      } else {

        Main.setSession('7ZXYZ@L', '');
        Main.setSession('UU@#90', '');
      }
    }

    var messageValidation = "";
    $scope.inputType = 'password';
    $scope.forgotAction = function () {
      alert("Forgot Action");
    };

    $scope.goToSignup = function () {
      $state.go('signup');
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
    };

    $scope.goToDownloads = function () {
      $state.go('downloads');
    };

    var successUserReference = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        res.talents = {};
        var feAuthorities = {};

        for (var i = 0; i < res.authorities.length; i++) {
          feAuthorities[res.authorities[i].authority] = true;
        }

        res.talentsAuthorities = feAuthorities;

        Main.setSession('profile', res);
        $state.go("app.myhr");
      }, 2000);

    };

    $scope.hideShowPassword = function () {
      if ($scope.inputType == 'password')
        $scope.inputType = 'text';
      else
        $scope.inputType = 'password';
    };

    function getUserReference() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      //var urlApi = Main.getUrlApi() + '/api/user/profile';
      var urlApi = Main.getUrlApi() + '/api/user/profile?mobileversion=' + Main.getVersion();
      Main.requestApi(accessToken, urlApi, successUserReference, $scope.errorRequest);
    }
    $scope.loginAction = function () {
      if (verificationForm($scope.usertalent)) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        var formData = {
          username: $scope.usertalent.username,
          password: $scope.usertalent.password,
          grant_type: 'password'
        };

        Main.signin(formData, function (res) {
          $ionicLoading.hide();
          if (res.type == false) {

          } else {
            Idle.watch();
            Main.setSession('token', res);
            rememberMe();
            getUserReference();
            $rootScope.dataUser = {};
            //$state.go("app.home");
          }
        }, function (error, status) {
          $ionicLoading.hide();
          var err = "Failed to signin. Please check your internet connection";
          // $rootScope.error = 'Failed to signin';
          if (status == 400) {
            err = "Error : " + error.error_description;
          } else if (status == 401) {
            err = "Could not process this operation. Please contact Admin.";
          } else {
            err = "Can't connect to server. Please try again later !"
          }
          $scope.errorAlert(err);


        });
      } else {
        $scope.warningAlert(messageValidation);
      }


    };

    function initData() {
      $scope.width = screen.width;
      $scope.usertalent = {
        username: '',
        password: '',
        rememberflag: ''
      };
      if (Main.getSession('7ZXYZ@L') && Main.getSession('UU@#90')) {

        $scope.usertalent.rememberflag = true;
        $scope.usertalent.username = atob(Main.getSession('7ZXYZ@L'));
        $scope.usertalent.password = atob(Main.getSession('UU@#90'));
      }
    }

    function initModule() {
      $timeout(function () {
        $ionicHistory.clearCache();
      }, 200);
      initData();
      Main.cleanData();
    }

    function verificationForm(usertalent) {

      if (usertalent.username == undefined || usertalent.username == '') {
        messageValidation = "Email can't be empty or wrong format.";
        return false;
      } else if (usertalent.password == undefined || usertalent.password == '') {
        messageValidation = "Password can't be empty";
        return false;
      }
      return true;
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  .controller('RegisterCtrl', function ($ionicNavBarDelegate, $ionicHistory, $ionicLoading, $state, $scope, Main) {
    var messageValidation = "";
    $scope.user = {};

    $scope.goBack = function (ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();

      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    };

    $scope.goTo = function (page) {
      $state.go(page);
      $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
      });
    };

    $scope.resetForm = function () {
      $scope.user = {};
    };
    $scope.submitForm = function () {

      if (verificationForm($scope.user)) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = null;
        var urlApi = Main.getUrlApi() + '/api/register';
        var data = JSON.stringify($scope.user);
        Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
      } else {
        $scope.warningAlert(messageValidation);
      }

    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      // $scope.goTo('tabs.thanks');
      $scope.successAlert(res.message);
      $scope.user = res;
      $scope.goBack("login");
    };

    /* var errorRequest = function (err, status){
       $ionicLoading.hide();
       if(status == 500) {
         $scope.errorAlert(err.message);
       }else {
         $scope.errorAlert("Check your connection");
       }
     }*/

    var successRefreshToken = function (res) {

    };
    var errRefreshToken = function (err, status) {}

    initMethod();

    function initMethod() {
      $ionicNavBarDelegate.showBackButton(true);
    }
    // invalid access token error: "invalid_token" 401
    function verificationForm(dataUser) {

      if (dataUser.companyCode == undefined) {
        messageValidation = "Company code can't empty";
        // alert("Company code can't empty");
        return false;
      }

      if (dataUser.email == undefined) {
        messageValidation = "Email code can't empty";
        // alert("Email code can't empty");
        return false;
      }

      if (dataUser.password == undefined) {
        messageValidation = "Password code can't empty";
        //alert("Password code can't empty");
        return false;
      }

      if (dataUser.repassword == undefined) {
        messageValidation = "Confirm Password code can't empty";
        // alert("Confirm Password code can't empty");
        return false;
      }

      if (dataUser.password != dataUser.repassword) {
        messageValidation = "Password and Confirm Password doesn't match";
        //alert("Password and Confirm Password doesn't match");
        return false;
      }

      return true;


    }

  })

  .controller('DownloadsCtrl', function ($cordovaInAppBrowser, $scope, $state) {
    var talentsUrl = 'https://drive.google.com/file/d/1oimbHznaUhE3oAqNv4p7D8yTLyUss2F1/view?usp=sharing';
    // var talentsUrl = 'https://drive.google.com/drive/folders/0Bw-3SAMXMtC_UnVTVFl3MW1aWUk';
    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };

    function initModule() {
      $cordovaInAppBrowser.open(talentsUrl, '_blank', options);
    }

    $scope.goToLogin = function () {
      $state.go('login');
    };
    initModule();
  })

  .controller('ForgetPasswordCtrl', function ($ionicLoading, $scope, $state, Main, $stateParams) {
    var messageValidation = "";
    $scope.password = {};


    // if(Main.getSession("token") == null || Main.getSession("token") == undefined) {
    //     $state.go("login");
    // }

    $scope.$on('$ionicView.enter', function () {
      initData();

    });


    function initData() {
      $scope.password = {
        newPassword: "",
        confirmPassword: ""
      };
      $scope.inputType = 'password';
      $scope.inputTypeConfirm = 'password';
    }

    // function changeSessionProfile(){
    //     $scope.profile.isChangePassword = true;
    //     Main.setSession('profile',$scope.profile);
    // }

    $scope.hideShowPassword = function () {
      if ($scope.inputType == 'password')
        $scope.inputType = 'text';
      else
        $scope.inputType = 'password';
    };

    $scope.hideShowPasswordConfirm = function () {
      if ($scope.inputTypeConfirm == 'password')
        $scope.inputTypeConfirm = 'text';
      else
        $scope.inputTypeConfirm = 'password';
    };
    var successRequest = function (res) {
      //   changeSessionProfile();
      initData();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $state.go("login");
    };

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 500) {
        alert(err.message);
      } else {
        alert("Check your connection");
      }
    };


    $scope.sendChangePassword = function () {
      //   if(verificationForm($scope.password)){
      //         $ionicLoading.show({
      //           template: '<ion-spinner></ion-spinner>'
      //         });
      //         var accessToken = Main.getSession("token").access_token;
      //         var urlApi = Main.getUrlApi() + '/api/myprofile/changepassword';
      //         var data = JSON.stringify($scope.password);
      //         Main.postRequestApi(accessToken,urlApi,data,successRequest,$scope.errorRequest);
      //     }else {
      //         alert(messageValidation);
      //     }

      if ($scope.password.newPassword != $scope.password.confirmPassword) {
        $scope.errorAlert("Your password and confirmation password do not match.");
      } else {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        // var accessToken = Main.getSession("token").access_token;
        var id = $stateParams.id;
        var accessToken = $stateParams.token;
        $scope.password = {
          "userId": id,
          "token": accessToken,
          "newPassword": $scope.password.newPassword,
          "confirmPassword": $scope.password.confirmPassword
        };
        var urlApi = Main.getUrlApi() + '/api/actionresetpassword';
        var data = JSON.stringify($scope.password);
        Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
      }
    };


    // function verificationForm(newPassword){
    //   if(newPassword.oldPassword == ""){
    //     messageValidation = "Old password can not empty";
    //     return false;
    //   }

    //   if(newPassword.newPassword == ""){
    //     messageValidation = "New password can not empty";
    //     return false;
    //   }

    //   if(newPassword.confirmPassword == ""){
    //     messageValidation = "Confirm New password can not empty";
    //     return false;
    //   }

    //     return true;


    // }

  })

;
