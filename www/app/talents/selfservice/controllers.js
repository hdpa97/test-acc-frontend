angular
  .module("selfservice.controllers", [])
  .controller("SelfServiceCtrl", function (
    $scope,
    $rootScope,
    $state,
    $timeout,
    $ionicLoading,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");

    }

    $scope.isLead = Main.getSession("profile").isLeader;
    $scope.isAdmin = Main.getSession("profile").isAdmin;
    $scope.isAttendanceAdmin = Main.getSession("profile").isAttendanceAdmin;
    $scope.isSpd = Main.getSession("profile").isSpd;

    $scope.openBrowser = function () {
      if (Main.getPhpHost() == "https://talents-api.acc.co.id") {
        window.open(
          "https://hdff.fs.ap1.oraclecloud.com/homePage/faces/FuseWelcome",
          "_blank",
          "location=yes"
        );
      } else {
        window.open(
          "https://hdff-test.fs.ap1.oraclecloud.com/homePage/faces/FuseWelcome",
          "_blank",
          "location=yes"
        );
      }
    };

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "selfservice"
    );

    $scope.HideErrorRequest = function (err, status) {
      $ionicLoading.hide();
    }

    $scope.nDate = new Date();
    $scope.Ytoday = $scope.nDate.getFullYear();

    function getUrlPP() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var urlApi = Main.getPPUrlApi() + "/getHistUrl";
      var body = {
        "getHistUrl": {
          "YEAR": $scope.Ytoday
        }
      };

      var data = JSON.stringify(body);
      Main.postRequestApiPP(urlApi, data, successGetDataUrlPP, $scope.HideErrorRequest);
    }

    var successGetDataUrlPP = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
      }, 500);
      $scope.pp = res;
      if ($scope.pp.OUT_MESS == "Success") {
        $scope.url = $scope.pp.OUT_DATA.URL;
        $scope.periode = $scope.pp.OUT_DATA.PERIODE_PP.replace(/[\s]/g, '');;
      }
    }

    $scope.openPP = function () {
      window.open($scope.url);
      // window.open('https://bit.ly/PeraturanPerusahaan2020ACC');
    };

    function initMethod() {
      $scope.Result = true;
      getUrlPP();
      if (refMenu != undefined && refMenu != "") {
        $scope.menu = JSON.parse(refMenu);
      }
      // var status = Main.getSession("profile").employeeTransient.assignment.employeeStatus;
      // if (status != "Karyawan Tetap") {
      //   $scope.Result = false;
      // }
    }

    initMethod();
  })

  .controller("ListAttendanceGroupCtrl", function (
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.isLead = Main.getSession("profile").isLeader;

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "listattendancegroup"
    );

    function initMethod() {
      if (refMenu != undefined && refMenu != "") {
        $scope.menu = JSON.parse(refMenu);
      }

      // var condition1 = !(Main.getSession("profile").isFeatureTester == true && Main.getSession("profile").isAttendanceAdmin == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      // // (condition1);
    });
  })

  .controller("SubmitAttendanceCtrl", function (
    $scope
  ) {
    alert("This feature is not available");
    $scope.goBack("app.selfservice");

    /*var serverTime = $filter('date')(Date.now(), "yyyy-MM-dd HH:mm:ss");
    var clockTick = null;
    $scope.tickInterval = 1000;
    $scope.clock = "Load server time";
    $scope.actionStatus = 3; // default is 1 disabled checkin and checkout
    $scope.attendance = {};
    $scope.attendance.longitude ='106.827153';
    $scope.attendance.latitude = '-6.175392' ;
    $scope.attendance.address = "searching location ...";
    var whichTaken = 0;
    var options = {timeout: 10000, enableHighAccuracy: true};
    var tick = function() {
        //
        if(clockTick == null){
             $scope.clock = new Date(serverTime);
             clockTick = $scope.clock;
        }else {
            var time = new Date(clockTick).getTime();
            $scope.clock = new Date(time+1000);
            clockTick = $scope.clock ;
        }
        // // ("tick")

        //$scope.clock = Date.now() // get the current time
        $timeout(tick, $scope.tickInterval); // reset the timer
    }

    var successPreRequest = function (res){
      // // ("res " + res);
      $ionicLoading.hide();
      if( res!= null) {
            serverTime = $filter('date')(res.serverTime, "yyyy-MM-dd HH:mm:ss");
            $timeout(tick, $scope.tickInterval);
            // // ("res is not null");
            if(res.punchInUtcTime != null && res.punchOutUtcTime != null) {
                $scope.actionStatus = 3;
            }else {
                if(res.punchInUtcTime != null)
                    $scope.actionStatus = 2;
                else
                    $scope.actionStatus = 1;
            }
      }

    }

    var successRequest = function (res){
      $ionicLoading.hide();
      if(whichTaken == 1)
        $scope.actionStatus = 2;
      else
        $scope.actionStatus = 3;

      alert(res.message);
      //goBack('app.family');
      // // (res);
      //$scope.family = res;
    }

    $scope.checkin = function(){
        whichTaken = 1;
        //alert("Checkin");
        var strTime = $filter('date')(Date.now(), "yyyy-MM-dd HH:mm:ss");
        // // ("str Time "+ strTime);
        var obj = {mode:"in",punchInUserTime:strTime,longitudePunchIn:$scope.attendance.longitude,latitudePunchIn:$scope.attendance.latitude};
        $ionicLoading.show({
          template: 'Processing...'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/self/attendance';
        var data = JSON.stringify(obj);
        // // (data);
        Main.postRequestApi(accessToken,urlApi,data,successRequest,$scope.errorRequest);
    }

    $scope.checkout = function(){
       // alert("Ceckout");
        whichTaken =2;
        var strTime = $filter('date')(Date.now(), "yyyy-MM-dd HH:mm:ss");
        var obj = {mode:"out",punchOutUserTime:strTime,longitudePunchOut:$scope.attendance.longitude,latitudePunchOut:$scope.attendance.latitude};

        $ionicLoading.show({
          template: 'Processing...'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/self/attendance';
        var data = JSON.stringify(obj);
        Main.postRequestApi(accessToken,urlApi,data,successRequest,$scope.errorRequest);
    }

     function prepareSubmit(){
        var urlApi = Main.getUrlApi() + '/api/user/self/attendance/prepare';
        $ionicLoading.show({
          template: 'Loading ...'
        });
        var accessToken = Main.getSession("token").access_token;
        Main.requestApi(accessToken,urlApi,successPreRequest, $scope.errorRequest);
      }



    function initialize(lat,lng) {
      // // ("Initialize " + lat + ","+lng);
      var mapOptions = {
        // the Teide ;-)
        center: {lat: parseFloat(lat), lng: parseFloat(lng)},
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {
          mapTypeIds: []
        },
        panControl: false,
        streetViewControl: false,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.SMALL
        }
      };

      var map = new google.maps.Map(document.getElementById("map"), mapOptions);
      var myLatlng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));
      //Marker + infowindow + angularjs compiled ng-click
        var contentString = "<div><a ng-click='clickTest()'>Click me!</a></div>";
        var compiled = $compile(contentString)($scope);

        var infowindow = new google.maps.InfoWindow({
          content: compiled[0]
        });

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          title: 'Uluru (Ayers Rock)'
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });

      $scope.map = map;
    }

    function getAddressLocation(lat,lng){
        var googleAddressUrl = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBrOyfr9TYYTYArfbF4_DCG2T3m67Fuzco&latlng="+lat+","+lng;
        Main.requestUrl(googleAddressUrl,function(res){
          // // (res);
          if(res.results[0] != undefined)
              $scope.attendance.address = res.results[0].formatted_address;
        }, function(err,status) {
          // // (err);
            $scope.attendance.address = "Address not found. Please acitivate your GPS and try again";
        });
    }


    function initModule() {

        prepareSubmit();
        getCurrentLocation();
    }

    initModule();*/
  })

  .controller("ChoicePayslipCtrl", function (
    $filter,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
    var messageValidation = "";
    $scope.selectMonth = Main.getSelectMonth();
    $scope.payslip = {};
    $scope.choice = {};
    $scope.choiceSelect = "";
    $scope.choice.select = "monthly";
    $scope.payslipType = Main.getSession("profile").companySettings.payslipType;
    // var arrCompanyRef = Main.getSession("profile").companyReference;
    // var refYear = Main.getDataReference(
    //   arrCompanyRef,
    //   "payslip",
    //   null,
    //   "selectYear"
    // );
    var lastYear = new Date().getFullYear() - 1;
    var lastYearStr = lastYear.toString();
    var thisYear = new Date().getFullYear();
    var thisYearStr = thisYear.toString();

    var refYear = [lastYearStr, thisYearStr];
    var year = "0";
    var month = "0";

    var successRequest = function (res) {
      $ionicLoading.hide();
      if (res.length == 0) {
        $scope.warningAlert("Data Not Found");
      } else {
        $rootScope.payslipSelected = res;
        $state.go("app.detailpayslip", {
          year: year,
          month: month,
          type: $scope.choiceSelect
        });
      }
    };

    $scope.submitForm = function (choice) {
      $scope.choiceSelect = choice.select;
      if (verificationForm(choice.select, $scope.payslip)) {
        $ionicLoading.show({
          template: "Loading..."
        });
        var url = "";
        var strData = "";
        var data = "";
        if (choice.select == "monthly") {
          url = Main.getUrlApi() + "/api/user/payroll";
          var strData = "";
          if ($scope.payslipType == "monthly") {
            month = Main.getIdfromValue(
              $scope.selectMonth,
              $scope.payslip.month
            );
            year = $scope.payslip.year;
            strData = {
              payrollType: $scope.payslipType,
              month: month,
              year: year
            };
          } else {
            strData = {
              payrollType: $scope.payslipType
            };
          }

          data = JSON.stringify(strData);
        } else {
          year = $scope.payslip.year;
          strData = {
            year: year
          };
          data = JSON.stringify(strData);
          url = Main.getUrlApi() + "/api/user/payroll/yearly";
        }
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/user/payroll";
        Main.postRequestApi(
          accessToken,
          url,
          data,
          successRequest,
          $scope.errorRequest
        );
      } else {
        $scope.warningAlert(messageValidation);
      }
    };

    var successLatestPeriod = function (res) {
      $ionicLoading.hide();
      if (res != null && res.periodDate != null) {
        arrPeriodDate = res.periodDate.split("-");
        $scope.payslip.year = arrPeriodDate[0];
        $scope.payslip.month = Main.getValuefromId(
          $scope.selectMonth,
          arrPeriodDate[1]
        );
      }
    };

    function getLatestMonth() {
      var urlApi = Main.getUrlApi() + "/api/user/payroll/latestperiod";
      $ionicLoading.show({
        template: "Loading ..."
      });
      var accessToken = Main.getSession("token").access_token;
      Main.requestApi(
        accessToken,
        urlApi,
        successLatestPeriod,
        $scope.errorRequest
      );
    }

    function initModule() {
      if (refYear != undefined && refYear != "") {
        // $scope.selectYear = JSON.parse(refYear);
        $scope.selectYear = refYear;
      }
      $scope.payslip.year = $filter("date")(new Date(), "yyyy");
      $scope.payslip.month = $filter("date")(new Date(), "MMM").toUpperCase();
      if ($scope.payslipType == "latest") {
        getLatestMonth();
      }
    }

    function verificationForm(type, payroll) {
      if (type == "monthly") {

        // assign year
        if (payroll.year != "" || payroll.year != undefined) {
          $scope.payslip.year = payroll.year;
        }

        if (payroll.year == undefined || payroll.year == "") {
          messageValidation = "Year can't be empty";
          return false;
        }

        if (payroll.month == undefined || payroll.month == "") {
          messageValidation = "Month can't be empty";
          return false;
        }
      }
      return true;
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  .controller("DetailPayslipCtrl", function (
    $stateParams,
    $rootScope,
    $scope,
    $state,
    Main
  ) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    $scope.listHeader = [];
    // // ('is year');
    // // ($stateParams.year);
    var year = $stateParams.year;
    var month = $stateParams.month;
    $scope.type = $stateParams.type;
    $scope.payslipType = Main.getSession("profile").companySettings.payslipType;

    function getPaySlip() {
      if ($rootScope.payslipSelected != null) {
        $scope.listHeader = $rootScope.payslipSelected;
      }
    }

    function initMethod() {
      getPaySlip();
    }

    $scope.printPdf = function (type) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }
      var employment_id = profile.employeeTransient.assignment.employment;
      var accessToken = Main.getSession("token").access_token;
      var url = "";
      if (type == "monthly") {
        url =
          Main.getPrintBaseUrl() +
          "/monthly/latest?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken;
        if ($scope.payslipType == "monthly")
          url =
          Main.getPrintBaseUrl() +
          "/monthly?employment_id=" +
          employment_id +
          "&year=" +
          year +
          "&month=" +
          month +
          "&ses_id=" +
          accessToken;
      } else {
        url =
          Main.getPrintBaseUrl() +
          "/yearly?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&year=" +
          year;
      }

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };

    initMethod();
  })

  .controller("SubmitClaimCtrl", function (
    $timeout,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.categoryType = [];

    $scope.gotoListType = function (name, extId, index, workflow) {
      $rootScope.needReportSelected = undefined;
      if (index != undefined)
        $rootScope.selectedBenefitCategory = Main.getSession("categoryType")[
          index
        ]; //$rootScope.benefitCategory[index];

      // // ($rootScope.selectedBenefitCategory);
      if (name.toLowerCase() == "spd advance") {
        $state.go("app.spdadvanceadd", {
          categoryType: name,
          extId: extId,
          workflow: workflow
        });
      } else if (name.toLowerCase() == "perjalanan dinas") {
        $state.go("app.benefitlisttype", {
          categoryType: name,
          extId: extId,
          workflow: workflow
        });
      } else if (name.toLowerCase() == "reimbursement") {
        $state.go("app.benefitlisttype", {
          categoryType: name,
          extId: extId,
          workflow: workflow,
          singleinput: true
        });
      } else {
        $state.go("app.benefitlisttype", {
          categoryType: name,
          extId: extId,
          workflow: workflow
        });
      }
    };

    var successRequest = function (res) {
      $timeout(function () {
        if (res.length > 0) {
          Main.setSession("categoryType", res);
          $scope.categoryType = res;
        }
        $ionicLoading.hide();
      }, 1000);
    };

    var successBalance = function (res) {
      if (res.length > 0) {
        var sessionBalance = [];
        for (var i = res.length - 1; i >= 0; i--) {
          var type = res[i].type;
          var value = res[i].balanceEnd;
          var obj = {
            id: type.toLowerCase(),
            name: value
          };
          sessionBalance.push(obj);
        }
        Main.setSession("balance", sessionBalance);
      }
    };

    function getBalanceSaveToSession() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/tmbalance/type";
      var body = {
        module: "Benefit"
      };
      var data = JSON.stringify(body);
      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successBalance,
        $scope.errorRequest
      );
    }

    function getListCategory() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/tmrequest/category?module=benefit";
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function initModule() {
      if (Main.getSession("categoryType") == undefined) {
        // // ('undefined');
        getListCategory();
      } else {
        // // ('unundefined');
        $scope.categoryType = Main.getSession("categoryType");

        // // ($scope.categoryType);
      }

      if (Main.getSession("balance") == undefined) getBalanceSaveToSession();
    }

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initModule();
    });
  })

  .controller("BenefitListtypeCtrl", function (
    ionicSuperPopup,
    ionicDatePicker,
    $stateParams,
    $filter,
    $timeout,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var categoryType = $stateParams.categoryType;
    var categoryType2 = categoryType;
    // child list
    $scope.childrenList = "";

    // for handle some  different code
    if (categoryType == "Medical Family") {
      categoryType = "Medical";
      categoryType2 = "Medical Family";
    } else if (categoryType == "Kacamata Family") {
      categoryType2 = "Kacamata Family";
    }

    // indentity category type
    $scope.categoryTypeIndentity = categoryType2;

    var categoryTypeExtId = $stateParams.extId;
    // deffine default workflow
    var workflow = $stateParams.workflow;

    // conditional if category type is medical family or  kacamata family
    if (categoryType == "Medical Family" || categoryType == "Kacamata Family") {
      workflow = "SUBMITBENEFIT";
    }

    var messageValidation = "";

    // child list
    $scope.bankList = {};
    $scope.plafonFrame = {};
    $scope.plafonLensa = {};
    $scope.claimFrame = {};
    $scope.claimLensa = {};
    $scope.directType = false;
    $scope.labelCategory = "Label";
    $scope.defaultValue = 1;
    $scope.total = 0;
    var objDirectType = {};
    $scope.defaultImage = "img/placeholder.png";
    $scope.checkLoginSession();
    $scope.arrLensa = [];
    $scope.listtype = [];
    $scope.images = [];

    $scope.dataAccountShow = 0;

    var listTypeSelected = [];
    $scope.titleCategory = categoryType2;
    $scope.category = categoryType.toLowerCase();
    $scope.requestHeader = {};
    $scope.requestHeader.requestForFamily = "";
    $scope.requestHeader.noRek = "";
    $scope.requestHeader.nameRek = "";
    $scope.requestHeader.nameBank = "";
    $scope.requestHeader.categoryType = categoryType.toLowerCase();
    $scope.arrSpdType = [{
        id: "regular"
      },
      {
        id: "pulang kampung"
      },
      {
        id: "mutasi"
      },
      {
        id: "training"
      },
      {
        id: "assessment"
      }
    ];

    $scope.dataLensa = {};
    $scope.listLensa = [];

    var datepicker = {
      callback: function (val) {
        //Mandatory
        $scope.requestHeader.startDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: "popup" //Optional
    };

    var datepicker1 = {
      callback: function (val) {
        //Mandatory
        $scope.requestHeader.endDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: "popup" //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };

    $scope.changeBalance = function (selected, index) {
      var qty = $scope.listtype[index].qty;
      changeMultiple(selected, qty, index);
    };

    $scope.change = function (event, index) {
      var value = event.target.value;
      value = value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");

      if ($scope.listtype[index] != undefined)
        $scope.listtype[index].amount = value;
    };

    $scope.showAccount = function (data) {
      if (data == "0" || data == 0) {
        $scope.dataAccountShow = 0;
      } else {
        $scope.dataAccountShow = 1;
      }
    };

    function getBalance(type) {
      type = type.toLowerCase();
      if (Main.getSession("balance") != undefined) {
        return Main.getValuefromId(Main.getSession("balance"), type);
      }
      return 0;
    }

    function changeMultiple(name, qty, index) {
      var balance = getBalance(name);
      if (balance == undefined || balance == 0) balance = 1;
      else balance = Number(balance);

      if (qty != undefined && qty != "") {
        value = "" + qty * balance;
        value = value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        $scope.listtype[index].amount = value;
      }
    }
    $scope.multiple = function (event, index) {
      var value = event.target.value;
      var name = $scope.listtype[index].name;

      if (name == "Hotel") return false;

      if ($scope.listtype[index].type == "select") {
        name = $scope.listtype[index].value;
      }

      var balance = getBalance(name);
      if (balance == undefined || balance == 0) balance = 1;
      else balance = Number(balance);

      value = "" + value * balance;

      value = value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      $scope.listtype[index].amount = value;
    };

    $scope.getTotal = function () {
      var total = 0;
      listTypeSelected = [];
      for (var i = 0; i < $scope.listtype.length; i++) {
        var obj = $scope.listtype[i];
        var objpush = {};
        if (obj.amount != null) {
          var number = obj.amount.toString();
          if (number != "0") {
            objpush = obj;
            var amount = obj.amount;
            obj.amount = number;
            number = amount.replace(/\./g, "");
          }
          total += Number(number);
        }
      }
      return total;
    };

    var successRequest1 = function (res) {
      $ionicLoading.hide();
      alert(res.message);
      $scope.goBack("app.submitclaim");
    };

    function getListChildrenBenefit() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      // list child get benefit medical or kacamata
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/eligiblefamily";

      Main.requestApi(
        accessToken,
        urlApi,
        successGetFamily,
        $scope.errorRequest
      );
    }

    function getListBank() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      // list bank get benefit medical or kacamata
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/bank";

      Main.requestApi(accessToken, urlApi, successBank, $scope.errorRequest);
    }

    function getPlafonFrame() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      // list child get benefit medical or kacamata
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/talentsParameter/plafonFrame";

      Main.requestApi(
        accessToken,
        urlApi,
        successGetPlafonFrame,
        $scope.errorRequest
      );
    }

    function getPlafonLensa() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      // list child get benefit medical or kacamata
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/talentsParameter?groupKey=PLAFON_LENSA";

      Main.requestApi(
        accessToken,
        urlApi,
        successGetPlafonLensa,
        $scope.errorRequest
      );
    }

    function getClaimFrame() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      // list child get benefit medical or kacamata
      var accessToken = Main.getSession("token").access_token;

      var urlApi =
        Main.getUrlApi() +
        "/api/user/tmbalance/allowedClaimDate?module=Benefit&category=kacamata&type=frame";

      Main.requestApi(
        accessToken,
        urlApi,
        successGetClaimFrame,
        $scope.errorRequest
      );
    }

    function getClaimLensa() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      // list child get benefit medical or kacamata
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() +
        "/api/user/tmbalance/allowedClaimDate?module=Benefit&category=kacamata&type=lensa";

      Main.requestApi(
        accessToken,
        urlApi,
        successGetClaimLensa,
        $scope.errorRequest
      );
    }

    var successRequest = function (res) {
      $timeout(function () {
        $rootScope.data.requestBenefitVerification = res;
        $ionicLoading.hide();
        if (
          $scope.requestHeader.categoryType.toLowerCase() == "medical overlimit"
        ) {
          $scope.goTo("app.selfservicesuccess");
        } else {
          $state.go("app.benefitconfirmation");
        }
      }, 1000);
    };

    var successBank = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        $scope.bankList = res;
      }, 1000);
    };

    var successGetPlafonFrame = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        $scope.plafonFrame = res;
      }, 1000);
    };

    var successGetFamily = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        $scope.childrenList = res;
      }, 1000);
    };

    var successGetPlafonLensa = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        $scope.plafonLensa = res;
      }, 1000);
    };

    var successGetClaimFrame = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        $scope.claimFrame = res;
      }, 1000);
    };

    var successGetClaimLensa = function (res) {
      $timeout(function () {
        $ionicLoading.hide();
        $scope.claimLensa = res;
      }, 1000);
    };

    function verificationForm(reqHeader) {
      if (reqHeader.categoryType == "Perjalanan Dinas") {
        if (reqHeader.origin == undefined || reqHeader.origin == "") {
          messageValidation = "Origin can't be empty";
          return false;
        }

        if (reqHeader.destination == undefined || reqHeader.destination == "") {
          messageValidation = "Destination can't be empty";
          return false;
        }

        if (reqHeader.remark == undefined || reqHeader.remark == "") {
          messageValidation = "Remark can't be empty";
          return false;
        }

        if (reqHeader.spdType == undefined || reqHeader.spdType == "") {
          messageValidation = "SPD Type can't be empty";
          return false;
        }
      }
      return true;
    }

    $scope.submitForm = function () {
      // // ("get request header");
      // // ($scope.requestHeader);
      $scope.requestHeader.module = "Benefit";
      $scope.requestHeader.startDate = $filter("date")(
        new Date($scope.requestHeader.startDate),
        "yyyy-MM-dd"
      );
      $scope.requestHeader.endDate = $filter("date")(
        new Date($scope.requestHeader.endDate),
        "yyyy-MM-dd"
      );
      $scope.requestHeader.categoryType = categoryType;
      $scope.requestHeader.categoryTypeExtId = categoryTypeExtId;
      $scope.requestHeader.workflow = workflow;

      if (verificationForm($scope.requestHeader)) {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        if ($scope.directType != true || $scope.defaultValue == 0) {
          var requestDetail = [];

          angular.forEach($scope.listtype, function (value, key) {
            var obj = {};
            if (value.type == "select") {
              obj.type = value.value;
            } else {
              obj.type = value.name;
            }

            if (value.amount != undefined) {
              var number = value.amount.toString();
              if (number != "0") {
                number = number.replace(/\./g, "");
              }
              obj.amount = number;
            }

            if (value.qty != undefined) obj.qty = value.qty;
            requestDetail.push(obj);
          });
          $scope.requestHeader.details = requestDetail;

          var attachment = [];
          if ($scope.requestHeader.attachments.length > 0) {
            for (
              var i = $scope.requestHeader.attachments.length - 1; i >= 0; i--
            ) {
              var objAttchament = {
                image: $scope.requestHeader.attachments[i].image
              };
              attachment.push(objAttchament);
            }
          }
          $scope.requestHeader.attachments = attachment;
        } else {
          var requestDetail = [];
          var obj = {};
          obj.type = objDirectType.type;
          obj.amount = $scope.defaultValue;
          requestDetail.push(obj);
          $scope.requestHeader.details = requestDetail;
        }

        var accessToken = Main.getSession("token").access_token;
        // var urlApi = Main.getUrlApi() + '/api/user/tmrequestheader/benefit';
        var urlApi =
          Main.getUrlApi() + "/api/user/tmrequestheader/verificationbenefit";

        if (
          $scope.requestHeader.categoryType.toLowerCase() == "medical overlimit"
        ) {
          urlApi = Main.getUrlApi() + "/api/user/tmrequestheader/benefit";
        }
        var data = JSON.stringify($scope.requestHeader);

        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successRequest,
          $scope.errorRequest
        );
      } else {
        $scope.warningAlert(messageValidation);
        // // ('dt 101');
      }
    };

    // show peraturan
    $scope.showPopupPeraturan = function () {
      ionicSuperPopup.show({
        title: "Peraturan",
        text: "Plafon Frame :" +
          $scope.plafonFrame.value +
          "\n" +
          $scope.plafonLensa[0].description +
          ":" +
          $scope.plafonLensa[0].value +
          "\n" +
          $scope.plafonLensa[1].description +
          ":" +
          $scope.plafonLensa[1].value +
          "\n" +
          $scope.plafonLensa[2].description +
          ":" +
          $scope.plafonLensa[2].value +
          "\n" +
          $scope.plafonLensa[3].description +
          ":" +
          $scope.plafonLensa[2].value
      });
    };

    $scope.showPopupKaryawan = function () {
      ionicSuperPopup.show({
        title: "Peraturan",
        text: "Plafon Frame :" +
          $scope.plafonFrame +
          "\n" +
          "Plafon Lensa:" +
          $scope.plafonLensa
      });
    };

    function initData() {
      $scope.requestHeader.startDate = new Date();
      $scope.requestHeader.endDate = new Date();
      $scope.requestHeader.origin = "";
      $scope.requestHeader.pulangKampung = false;
      $scope.requestHeader.destination = "";
      $scope.requestHeader.remark = "";
      $scope.requestHeader.bankAccount = "";
      $scope.requestHeader.bankName = "";
      $scope.requestHeader.accountName = "";
      $scope.requestHeader.attachments = [];
      $scope.images = [];
    }

    function initModule() {
      initData();
      // condition get list children benefit
      if (
        categoryType2 == "Medical Family" ||
        categoryType2 == "Kacamata Family"
      ) {
        getListChildrenBenefit();
      }
      getListBank();

      // condition get list plafon frame dan lensa
      if (categoryType2 == "Kacamata Family" || categoryType2 == "Kacamata") {
        getPlafonFrame();
        getPlafonLensa();
      }

      getClaimLensa();
      getClaimFrame();

      $rootScope.data.requestBenefitVerification = {};
      if ($scope.category == "kacamata") {
        var arrLensa = [{
            id: "Lensa Monofocus Non Cylindris"
          },
          {
            id: "Lensa Monofocus Cylindris"
          },
          {
            id: "Lensa Bifokus Non Cylindris"
          },
          {
            id: "Lensa Bifokus Cylindris"
          }
        ];
        $scope.listtype = [{
            id: "frame",
            name: "Frame"
          },
          {
            id: "lensa",
            name: "Lensa",
            type: "select",
            options: arrLensa,
            value: "Lensa Monofocus Non Cylindris"
          }
        ];
      } else if ($scope.category == "perjalanan dinas") {
        var uangsaku = [{
            id: "Uang Saku Dalam Negeri"
          },
          {
            id: "Uang Saku Luar Negeri"
          }
        ];
        var uangmakan = [{
            id: "Uang Makan Dalam Negeri"
          },
          {
            id: "Uang Makan Luar Negeri"
          }
        ];
        $scope.listtype = [{
            id: "ticket",
            name: "Ticket"
          },
          {
            id: "taxi",
            name: "Taxi"
          },
          {
            id: "transport",
            name: "Transport"
          },
          {
            id: "hotel",
            name: "Hotel",
            input: true,
            satuan: "days"
          },
          {
            id: "rentalmobil",
            name: "Rental Mobil"
          },
          {
            id: "mileage",
            name: "Mileage",
            input: true,
            satuan: "KM"
          },
          {
            id: "uangmakan",
            name: "Uang Makan",
            value: "Uang Makan Dalam Negeri",
            type: "select",
            options: uangmakan,
            input: true,
            satuan: "days"
          },
          {
            id: "uangsaku",
            name: "Uang Saku",
            value: "Uang Saku Dalam Negeri",
            type: "select",
            options: uangsaku,
            input: true,
            satuan: "days"
          },
          {
            id: "laundry",
            name: "Laundry"
          },
          {
            id: "tolparkirbensin",
            name: "Tol Parkir Bensin"
          },
          {
            id: "other",
            name: "Other"
          }
        ];
      } else if ($scope.category == "reimbursement") {
        $scope.listtype = [{
            id: "personalexpenses",
            name: "Personal Expenses"
          },
          {
            id: "communication",
            name: "Communication (pulsa)"
          },
          {
            id: "sportmembership",
            name: "Sport membership"
          }
        ];
      } else if ($scope.category == "other") {
        $scope.listtype = [{
          id: "cutibesar",
          name: "Cuti Besar"
        }];
      } else if ($scope.category == "medical") {
        $scope.listtype = [{
            id: "dokter",
            name: "Dokter"
          },
          {
            id: "obat",
            name: "Apotik / Obat"
          },
          {
            id: "lab",
            name: "Lab / R.S"
          },
          {
            id: "lainlain",
            name: "Lain-lain"
          }
        ];
      } else if ($scope.category == "mutasi") {
        $scope.listtype = [{
            id: "sekolah",
            name: "Pendaftaran sekolah"
          },
          {
            id: "rumahoperasional",
            name: "Sumbangan rumah operasional"
          }
        ];
      } else if ($scope.category == "kacamata family") {
        var arrLensa = [{
            id: "Lensa Monofocus Non Cylindris"
          },
          {
            id: "Lensa Monofocus Cylindris"
          },
          {
            id: "Lensa Bifokus Non Cylindris"
          },
          {
            id: "Lensa Bifokus Cylindris"
          }
        ];
        $scope.listtype = [{
            id: "frame",
            name: "Frame"
          },
          {
            id: "lensa",
            name: "Lensa",
            type: "select",
            options: arrLensa,
            value: "Lensa Monofocus Non Cylindris"
          }
        ];
      } else if ($scope.category == "medical family") {
        $scope.listtype = [{
            id: "dokter",
            name: "Dokter"
          },
          {
            id: "obat",
            name: "Apotik / Obat"
          },
          {
            id: "lab",
            name: "Lab / R.S"
          },
          {
            id: "lainlain",
            name: "Lain-lain"
          }
        ];
      }

      if ($rootScope.selectedBenefitCategory != undefined) {
        $scope.directType = $rootScope.selectedBenefitCategory.directType;
        $scope.labelCategory = $rootScope.selectedBenefitCategory.label;
        $scope.defaultValue = $rootScope.selectedBenefitCategory.defaultValue;
        if ($scope.directType == true) {
          if (
            $scope.defaultValue != 0 &&
            $rootScope.selectedBenefitCategory.listRequestType.length > 0
          )
            objDirectType =
            $rootScope.selectedBenefitCategory.listRequestType[0];

          if ($scope.defaultValue == 0) {
            var arrListType =
              $rootScope.selectedBenefitCategory.listRequestType;
            $scope.listtype = [];
            for (var i = arrListType.length - 1; i >= 0; i--) {
              var obj = {
                name: arrListType[i].type
              };
              $scope.listtype.push(obj);
            }
          }
        }
      }

      if ($rootScope.needReportSelected != undefined) {
        $scope.requestHeader.origin = $rootScope.needReportSelected.origin;
        $scope.requestHeader.remark = $rootScope.needReportSelected.remark;
        $scope.requestHeader.destination =
          $rootScope.needReportSelected.destination;
        $scope.requestHeader.spdType = $rootScope.needReportSelected.spdType;
        $scope.requestHeader.linkRefHeader = $rootScope.needReportSelected.id;
        $scope.requestHeader.startDate = $filter("date")(
          new Date($rootScope.needReportSelected.startDate),
          "yyyy-MM-dd"
        );
        $scope.requestHeader.endDate = $filter("date")(
          new Date($rootScope.needReportSelected.endDate),
          "yyyy-MM-dd"
        );
      }
    }

    initModule();
  })

  .controller("BenefitConfirmationCtrl", function (
    appService,
    $ionicActionSheet,
    $cordovaCamera,
    $filter,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var benefitVerification = $rootScope.data.requestBenefitVerification;
    $scope.defaultImage = "img/placeholder.png";
    $scope.images = [];
    $scope.requestHeader = {};
    $scope.requestHeader.attachments = [];
    $scope.appMode = Main.getAppMode();
    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initMethod();
    });

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.goTo("app.selfservicesuccess");
    };

    $scope.removeChoice = function () {
      var lastItem = $scope.requestHeader.attachments.length - 1;
      $scope.requestHeader.attachments.splice(lastItem);
      $scope.images.splice(lastItem);
    };

    $scope.addPicture = function () {
      if ($scope.images.length > 2) {
        alert("Only 3 pictures can be upload");
        return false;
      }
      $ionicActionSheet.show({
        buttons: [{
            text: "Take Picture"
          },
          {
            text: "Select From Gallery"
          }
        ],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener(
                "deviceready",
                function () {
                  $cordovaCamera.getPicture(appService.getCameraOptions()).then(
                    function (imageData) {
                      $scope.images.push({
                        image: "data:image/jpeg;base64," + imageData
                      });
                      $scope.requestHeader.attachments.push({
                        image: imageData
                      });
                    },
                    function (err) {
                      appService.showAlert(
                        "Error",
                        err,
                        "Close",
                        "button-assertive",
                        null
                      );
                    }
                  );
                },
                false
              );

              break;
            case 1: // Select From Gallery
              document.addEventListener(
                "deviceready",
                function () {
                  $cordovaCamera
                    .getPicture(appService.getLibraryOptions())
                    .then(
                      function (imageData) {
                        $scope.images.push({
                          image: "data:image/jpeg;base64," + imageData
                        });
                        $scope.requestHeader.attachments.push({
                          image: imageData
                        });
                      },
                      function (err) {
                        appService.showAlert(
                          "Error",
                          err,
                          "Close",
                          "button-assertive",
                          null
                        );
                      }
                    );
                },
                false
              );
              break;
          }
          return true;
        }
      });
    };

    $scope.submitForm = function () {
      if ($scope.requestHeader.attachments.length > 0) {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/user/tmrequestheader/benefit";
        var attachment = [];
        if ($scope.requestHeader.attachments.length > 0) {
          for (
            var i = $scope.requestHeader.attachments.length - 1; i >= 0; i--
          ) {
            var objAttachment = {
              image: null
            };
            if ($scope.appMode == "mobile") {
              objAttachment = {
                image: $scope.requestHeader.attachments[i].image
              };
            } else {
              if (
                $scope.requestHeader.attachments[i].compressed.dataURL !=
                undefined
              ) {
                var webImageAttachment = $scope.requestHeader.attachments[
                  i
                ].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
                objAttachment = {
                  image: webImageAttachment
                };
              }
            }
            attachment.push(objAttachment);
          }
        }
        benefitVerification.startDate = $filter("date")(
          new Date(benefitVerification.startDate),
          "yyyy-MM-dd"
        );
        benefitVerification.endDate = $filter("date")(
          new Date(benefitVerification.endDate),
          "yyyy-MM-dd"
        );
        benefitVerification.attachments = attachment;

        var data = JSON.stringify(benefitVerification);

        // // added fungsi untuk handle double submit insert data
        // // // ('get data benefit');
        // // // (Main.getDataBenefit());
        // // var getbenefitData = Main.getDataBenefit();
        // if (Main.getDataBenefit() == null || Main.getDataBenefit() == "") {
        //   // // ('is null');
        //   Main.postRequestApi(
        //     accessToken,
        //     urlApi,
        //     data,
        //     successRequest,
        //     $scope.errorRequest
        //   );
        //   Main.saveDataBenefit(data); // setter
        //   getbenefitData = Main.getDataBenefit(); //getter
        //   // $route.reload();
        // } else {
        //   if (data === Main.getDataBenefit()) {
        //     // cek dulu takut double data
        //     // // ('is same');
        //     $scope.errorAlert("You Already success post before ");
        //     return false;
        //   } else {
        //     // // ('the new record');
        //     Main.postRequestApi(
        //       accessToken,
        //       urlApi,
        //       data,
        //       successRequest,
        //       $scope.errorRequest
        //     );
        //     Main.saveDataBenefit(data); // setter
        //     getbenefitData = Main.getDataBenefit(); //getter
        //     // $route.reload();
        //   }
        // }

        if (Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest)) {
          $route.reload();;
        }
      } else {
        $scope.warningAlert("You must add at least 1 attachment.");
      }
    };

    function initMethod() {
      benefitVerification = $rootScope.data.requestBenefitVerification;
      $scope.totalClaim = 0;
      $scope.totalSubmitedClaim = 0;
      $scope.totalCurrentClaim = 0;
      $scope.lastClaimDate = "";
      $scope.images = [];
      $scope.requestHeader = {};
      $scope.requestHeader.attachments = [];
      // // ('loggin verifications');
      // // (benefitVerification);
      // // ('loggin verification details');
      // // (benefitVerification.details);
      if (
        benefitVerification != null &&
        benefitVerification.details.length > 0
      ) {
        $scope.categoryType = benefitVerification.categoryType;

        for (var i = benefitVerification.details.length - 1; i >= 0; i--) {
          $scope.totalClaim += benefitVerification.details[i].totalClaim;
          $scope.totalSubmitedClaim +=
            benefitVerification.details[i].totalSubmitedClaim;
          $scope.totalCurrentClaim +=
            benefitVerification.details[i].totalCurrentClaim;
        }

        if (benefitVerification.details.length == 1) {
          if (benefitVerification.details[0].lastClaimDate != undefined)
            $scope.lastClaimDate = $filter("date")(
              new Date(benefitVerification.details[0].lastClaimDate),
              "yyyy-MM-dd"
            );
        }
      }
    }

    initMethod();
  })

  .controller("ClaimChoice", function (
    $ionicLoading
  ) {})

  .controller("BenefitDetailCtrl", function (
    ionicSuperPopup,
    $ionicPopup,
    $stateParams,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    $scope.confirm = {};
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
    $scope.isHr = Main.getSession("profile").isHr;
    $scope.header = {};
    var id = $stateParams.id;

    var successApprove = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.formrequestsearching");
    };

    var successCancel = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.benefitclaimlist");
    };

    var sendApproval = function (action, id, reason) {
      var data = {};
      if (action == "approved") data = {
        id: id,
        status: action
      };
      else data = {
        id: id,
        status: action,
        reasonReject: reason
      };

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/workflow/actionapproval";
      var data = JSON.stringify(data);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successApprove,
        $scope.errorRequest
      );
    };

    $scope.confirmCancel = function (idDataApproval) {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Cancel this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function (isConfirm) {
          if (isConfirm) {
            sendApproval("cancelled", idDataApproval, "");
          }
        }
      );
    };

    $scope.confirmReject = function (idDataApproval) {
      var confirmPopup = $ionicPopup
        .confirm({
          title: "Reason Rejected",
          template: '<textarea rows="10" maxlength="255" class="calm" style="width:100%; border-color:#ddd; border: solid 2px #c9c9c9;border-radius:2px" ng-model="confirm.reasonReject" ></textarea>',
          cancelText: "Cancel",
          scope: $scope,
          okText: "Yes"
        })
        .then(function (res) {
          if (res) {
            var reason = $scope.confirm.reasonReject;

            if (reason == undefined || reason == "")
              alert("Reason reject can not empty");
            else sendApproval("rejected", idDataApproval, reason);
          }
        });
    };

    function successRequest(res) {
      $ionicLoading.hide();
      if (res != null) {
        $scope.header = res;
        if (
          $scope.header.categoryType == "Medical" ||
          $scope.header.categoryType == "Medical Family"
        ) {
          // // ('is medical');
          var arrDetail = [];
          if ($scope.header.details.length > 0) {
            arrDetail = JSON.parse($scope.header.details[0].data);
          }
          $scope.header.details = arrDetail;
        }
      }
    }

    $scope.printReport = function (employee, uuid) {
      var url =
        Main.getPrintReportUrl() +
        "/spd?employee=" +
        employee +
        "&uuid=" +
        uuid +
        "&category=" +
        $scope.header.categoryType;
      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };

    function getDetailHeader(id) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/tmrequest/" + id;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function initMethod() {
      getDetailHeader(id);
    }
    initMethod();
  })

  .controller("BenefitClaimNeedReportCtrl", function (
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requests = [];
    $scope.needReportRequests = [];
    $scope.module = {};

    $scope.gotoListType = function (index) {
      // // ('latest');
      $rootScope.needReportSelected = $scope.needReportRequests[index];

      if (Main.getSession("categoryType") != undefined) {
        var arrCategoryType = Main.getSession("categoryType");
        var objPerjalananDinas = null;
        for (var i = 0; i < arrCategoryType.length; i++) {
          obj = arrCategoryType[i];
          if (obj.categoryType.toLowerCase() == "perjalanan dinas")
            objPerjalananDinas = obj;
        }

        if (objPerjalananDinas != null) {
          //var perjalananDinas = {id:"0124D0000008guqQAA",name:"Perjalanan Dinas",workflow:"SUBMITBENEFIT2"};
          $rootScope.selectedBenefitCategory = undefined;
          var perjalananDinas = {
            id: objPerjalananDinas.categoryTypeExtId,
            name: objPerjalananDinas.categoryType,
            workflow: objPerjalananDinas.workflow
          };
          $state.go("app.benefitlisttype", {
            categoryType: perjalananDinas.name,
            extId: perjalananDinas.id,
            workflow: perjalananDinas.workflow,
            singleinput: false
          });
        }
      }
    };

    $scope.gotoBenefitDetail = function (index) {
      $state.go("app.benefitdetail");
    };

    $scope.refresh = function () {
      initMethod();
    };

    var successRequest = function (res) {
      $scope.needReportRequests = [];
      for (var i = 0; i < res.length; i++) {
        var obj = res[i];
        obj.idx = i;
        $scope.needReportRequests.push(obj);
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    };

    var successBalance = function (res) {
      if (res.length > 0) {
        var sessionBalance = [];
        for (var i = res.length - 1; i >= 0; i--) {
          var type = res[i].type;
          var value = res[i].balanceEnd;
          var obj = {
            id: type.toLowerCase(),
            name: value
          };
          sessionBalance.push(obj);
        }
        Main.setSession("balance", sessionBalance);
      }
    };

    function getBalanceSaveToSession() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/tmbalance/type";
      var body = {
        module: "Benefit"
      };
      var data = JSON.stringify(body);
      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successBalance,
        $scope.errorRequest
      );
    }

    initMethod();

    function initMethod() {
      getNeedReportList();
      if (Main.getSession("balance") == undefined) getBalanceSaveToSession();
    }

    function getNeedReportList() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/tmrequest/needreport?module=Benefit";
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }
  })

  .controller("BenefitClaimListCtrl", function (
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requests = [];
    $scope.module = {};
    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initMethod();
    });

    $scope.gotoBenefitDetail = function (index) {
      $state.go("app.benefitdetail");
    };

    $scope.refresh = function () {
      initMethod();
    };

    var successRequest = function (res) {
      $scope.requests = [];
      for (var i = 0; i < res.length; i++) {
        var obj = res[i];
        obj.idx = i;
        $scope.requests.push(obj);
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    };

    function initMethod() {
      getListBenefit();
    }

    function getListBenefit() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/tmrequest?module=Benefit";
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }
  })

  .controller("SpdAdvanceAdd", function (
    ionicDatePicker,
    $stateParams,
    $filter,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var categoryType = $stateParams.categoryType;
    var categoryTypeExtId = $stateParams.extId;
    var workflow = $stateParams.workflow;
    $scope.requestHeader = {};
    var messageValidation = "";
    $scope.arrSpdType = [{
        id: "regular"
      },
      {
        id: "pulang kampung"
      },
      {
        id: "mutasi"
      },
      {
        id: "training"
      },
      {
        id: "assessment"
      }
    ];

    var datepicker = {
      callback: function (val) {
        //Mandatory
        $scope.requestHeader.startDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: "popup" //Optional
    };

    var datepicker1 = {
      callback: function (val) {
        //Mandatory
        $scope.requestHeader.endDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: "popup" //Optional
    };

    $scope.change = function (event, index) {
      var value = event.target.value;
      value = value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      $scope.detail.amount = value;
    };

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initModule();
    });

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      alert(res.message);
      $scope.goBack("app.submitclaim");
    };

    $scope.submitForm = function () {
      var detailSubmit = {};
      detailSubmit = $scope.detail;
      if (verificationForm(detailSubmit)) {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });

        detailSubmit.amount = "" + detailSubmit.amount;
        var dataAmount;
        if (detailSubmit.amount.indexOf(".") !== -1) {
          dataAmount = detailSubmit.amount.replace(/\./g, "");
        } else {
          dataAmount = detailSubmit.amount;
        }

        dataAmount = Number(dataAmount);
        detailSubmit.amount = dataAmount;
        $scope.requestHeader.module = "Benefit";
        $scope.requestHeader.workflow = workflow;
        $scope.requestHeader.startDate = $filter("date")(
          new Date($scope.requestHeader.startDate),
          "yyyy-MM-dd"
        );
        $scope.requestHeader.endDate = $filter("date")(
          new Date($scope.requestHeader.endDate),
          "yyyy-MM-dd"
        );
        $scope.requestHeader.categoryType = categoryType;
        $scope.requestHeader.categoryTypeExtId = categoryTypeExtId;
        $scope.requestHeader.origin = $scope.detail.origin;
        $scope.requestHeader.spdType = $scope.detail.spdType;
        $scope.requestHeader.destination = $scope.detail.destination;
        $scope.requestHeader.bankAccount = $scope.detail.bankAccount;
        $scope.requestHeader.bankName = $scope.detail.bankName;
        $scope.requestHeader.accountName = $scope.detail.accountName;

        var requestDetail = [];
        requestDetail.push(detailSubmit);
        $scope.requestHeader.details = requestDetail;

        /*var attachment = [];
        if($scope.requestHeader.attachments.length > 0) {
            for (var i = $scope.requestHeader.attachments.length - 1; i >= 0; i--) {
                var objAttchament = {"image":$scope.requestHeader.attachments[i].image};
                attachment.push(objAttchament);
            };
        }
        $scope.requestHeader.attachments = attachment;
        */

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/user/tmrequestheader/benefit";
        var data = JSON.stringify($scope.requestHeader);

        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successRequest,
          $scope.errorRequest
        );
      } else {
        alert(messageValidation);
      }
    };

    function verificationForm(dataSpd) {
      if (dataSpd.origin == undefined || dataSpd.origin == "") {
        messageValidation = "Origin can't be empty";
        return false;
      }

      if (dataSpd.destination == undefined || dataSpd.destination == "") {
        messageValidation = "Destination can't be empty";
        return false;
      }

      if (dataSpd.spdType == undefined || dataSpd.spdType == "") {
        messageValidation = "SPD Type can't be empty";
        return false;
      }

      if (dataSpd.data == undefined || dataSpd.data == "") {
        messageValidation = "Estimated Expenses & Item can't be empty";
        return false;
      }

      if (dataSpd.amount == undefined || dataSpd.amount == 0) {
        messageValidation = "Amount can't be empty";
        return false;
      }

      if (dataSpd.remark == undefined || dataSpd.remark == "") {
        messageValidation = "Remark can't be empty";
        return false;
      }

      return true;
    }

    function initModule() {
      $scope.detail = {
        amount: 0,
        type: "Advance",
        remark: "",
        origin: "",
        destination: "",
        data: ""
      };
      $scope.requestHeader.startDate = new Date();
      $scope.requestHeader.endDate = new Date();
    }
  })

  .controller("ChoiceSptaiCtrl", function (
    $filter,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
    var messageValidation = "";
    $scope.selectMonth = Main.getSelectMonth();
    $scope.sptai = {};
    $scope.choice = {};
    $scope.choiceSelect = "";
    $scope.choice.select = "monthly";
    $scope.sptaiType = Main.getSession("profile").companySettings.payslipType;
    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refYear = Main.getDataReference(
      arrCompanyRef,
      "payslip",
      null,
      "selectYear"
    );
    var year = "0";
    var month = "0";
    var successRequest = function (res) {
      $ionicLoading.hide();
      if (res.length == 0) {
        $scope.warningAlert("Data Not Found");
      } else {
        $rootScope.payslipSelected = res;
        $state.go("app.detailsptai", {
          year: year,
          month: month,
          type: $scope.choiceSelect
        });
      }
    };

    $scope.submitForm = function (choice) {
      $scope.choiceSelect = choice.select;
      if (verificationForm($scope.sptai)) {
        $ionicLoading.show({
          template: "Loading..."
        });
        var url = "";
        var strData = "";
        var data = "";
        if (choice.select == "monthly") {
          url = Main.getUrlApi() + "/api/user/payroll";
          var strData = "";
          if ($scope.sptaiType == "monthly") {
            month = Main.getIdfromValue($scope.selectMonth, $scope.sptai.month);
            year = $scope.sptai.year;
            strData = {
              payrollType: $scope.sptaiType,
              month: month,
              year: year
            };
          } else {
            strData = {
              payrollType: $scope.sptaiType
            };
          }

          data = JSON.stringify(strData);
        } else {
          url = Main.getUrlApi() + "/api/user/payroll/yearly";
        }
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/user/payroll";
        Main.postRequestApi(
          accessToken,
          url,
          data,
          successRequest,
          $scope.errorRequest
        );
      } else {
        $scope.warningAlert(messageValidation);
      }
    };

    // function to print data choice sptai
    $scope.printPdf = function (year) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }
      var employment_id = profile.employeeTransient.assignment.employment;
      var accessToken = Main.getSession("token").access_token;
      var url = "";
      url =
        Main.getPrintSptBaseUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&year=" +
        year;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };

    var successLatestPeriod = function (res) {
      $ionicLoading.hide();
      if (res != null && res.periodDate != null) {
        arrPeriodDate = res.periodDate.split("-");
        $scope.sptai.month = Main.getValuefromId(
          $scope.selectMonth,
          arrPeriodDate[1]
        );
      }
    };

    function initModule() {
      if (refYear != undefined && refYear != "") {
        $scope.selectYear = JSON.parse(refYear);
      }

      // calculate one year ago from date now
      var date = new Date().getFullYear() - 1;
      var dateString = date.toString();
      var data = $filter("date")(new Date(), "yyyy");

      $scope.sptai.year = dateString;
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  .controller("ChoiceLetterCtrl", function (
    $timeout,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.printletter");
    $scope.listLetter = [];
    $scope.manipulateLetter = {};
    $scope.dataEmpStatus = "";
    $scope.profile = {};

    function getDataLetter() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/talentsParameter?groupKey=LIST_LETTER";
      Main.requestApi(accessToken, urlApi, successLetter, $scope.errorRequest);
    }

    var successLetter = function (res) {
      var data = null;

      // manipulate data with condition status karyawan
      if ($scope.dataEmpStatus == "Karyawan Kontrak - 1") {
        var data = $.grep(res, function (e) {
          return e.key != "SRT_AMANDEMENT" && e.key != "SRT_PENGANGKATAN";
        });
      } else if ($scope.dataEmpStatus == "Karyawan Kontrak - 2") {
        var data = $.grep(res, function (e) {
          return e.key != "SRT_PKWT" && e.key != "SRT_PENGANGKATAN";
        });
      } else if ($scope.dataEmpStatus == "Karyawan Tetap") {
        var data = $.grep(res, function (e) {
          return e.key != "SRT_PKWT" && e.key != "SRT_AMANDEMENT";
        });
      }


      $scope.manipulateLetter = data;

      $timeout(function () {
        if ($scope.manipulateLetter.length > 0) {
          for (var i = 0; i < $scope.manipulateLetter.length; i++) {
            $scope.listLetter.push([{
              groupKey: $scope.manipulateLetter[i].groupKey,
              key: $scope.manipulateLetter[i].key,
              value: $scope.manipulateLetter[i].value,
              description: $scope.manipulateLetter[i].description
            }]);
          }
        }
      }, 1000);
    };

    $scope.HideErrorRequest = function (err, status) {
      $ionicLoading.hide();
    }

    $scope.nDate = new Date();
    $scope.Ytoday = $scope.nDate.getFullYear();

    function cekSubmitPP() {
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getPPUrlApi() + "/getHist";
      var body = {
        "getHistInput": {
          "NPK": $scope.profile.employeeTransient.assignment.employeeNo,
          "YEAR": '' + $scope.Ytoday + ''
        }
      };

      var data = JSON.stringify(body);
      Main.postRequestApiPP(urlApi, data, successCekSubmitPP, $scope.HideErrorRequest);
    }

    var successCekSubmitPP = function (res) {
      $scope.pp = res;
      if ($scope.pp.OUT_DATA.ID) {
        $scope.SudahSubmit = true;
      }
      $timeout(function () {
        $ionicLoading.hide();
      }, 1000);
    }

    $scope.profile = {};

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initModule();
    });

    function initModule() {
      $scope.profile = Main.getSession("profile");
      $scope.dataEmpStatus =
        $scope.profile.employeeTransient.assignment.employeeStatus;
      getDataLetter();
      cekSubmitPP();
    }
  })

  .controller("ApprovalDelegationCtrl", function (
    $ionicModal,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    // modal

    var infiniteLimit = 15;
    $scope.approvalType = {};
    $scope.requestHeader = {
      npk: "",
      empName: ""
    };
    $scope.isLoadMoreShow = false;
    $scope.findStatus = false;
    $scope.nameItems = [];
    $scope.npkItems = [];
    $scope.items = [];
    // var size = Main.getDataDisplaySize();
    var size = 1;
    var j = 0;

    $scope.loadMoreName = function () {
      // $scope.infiniteLimit += $scope.infiniteLimit;
      // $scope.$broadcast('scroll.infiniteScrollComplete');

      j++;

      $scope.searchByName(j);
    };

    $scope.loadMoreNpk = function () {
      // $scope.infiniteLimit += $scope.infiniteLimit;
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      j++;

      $scope.searchByNpk(j);
    };

    $scope.findByName = {
      search: ""
    };
    $scope.searchByName = function (page) {
      $scope.findStatus = false;
      if ($scope.findByName.search == null || $scope.findByName.search == "")
        $ionicPopup.show({
          title: "Text field can't be empty!",
          buttons: [{
            text: "OK"
          }]
        });
      else {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() +
          "/api/employee/find?page=" +
          page +
          "&size=" +
          infiniteLimit +
          "&name=" +
          $scope.findByName.search;
        Main.requestApi(
          accessToken,
          urlApi,
          successRequestName,
          $scope.errorRequest
        );

      }
    };

    $scope.findByNpk = {
      search: ""
    };
    $scope.searchByNpk = function (page) {




      $scope.findStatus = false;
      if ($scope.findByNpk.search == null || $scope.findByNpk.search == "")
        $ionicPopup.show({
          title: "Text field can't be empty",
          buttons: [{
            text: "OK"
          }]
        });
      else {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() +
          "/api/employee/find?page=" +
          page +
          "&size=" +
          size +
          "&npk=" +
          $scope.findByNpk.search;
        // // (urlApi);
        Main.requestApi(
          accessToken,
          urlApi,
          successRequestNpk,
          $scope.errorRequest
        );
      }
    };

    $scope.doRefresh = function (index) {
      if (index == 1) {
        $scope.isLoadMoreShow = false;
        $scope.npkItems = [];
      } else if (index == 2) {
        $scope.isLoadMoreShow = false;
        $scope.nameItems = [];
      }
    };
    // // ($scope.searchByName(0));

    $scope.getValue = function (name, npk, employeeId, index) {
      $scope.requestHeader.empName = name;
      $scope.requestHeader.npk = npk;

      alert(employeeId);
      // $scope.requestHeader.employeeId = employeeId;

      // setter employeeId
      Main.saveDataEmployeeId(employeeId);

      if (index == 1) $scope.modal1.hide();
      else $scope.modal2.hide();
    };

    $ionicModal
      .fromTemplateUrl("modal1.html", {
        id: "1",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modal1 = modal;
      });
    $ionicModal
      .fromTemplateUrl("modal2.html", {
        id: "2",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modal2 = modal;
      });

    $ionicModal
      .fromTemplateUrl("modal3.html", {
        id: "3",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modal3 = modal;
      });

    $scope.openModal = function (index) {
      alert(index);
      if (index == 1) $scope.modal1.show();
      else if (index == 2) $scope.modal2.show();
      else if (index == 3) $scope.modal3.show();
    };

    $scope.closeModal = function (index) {
      if (index == 1) $scope.modal1.hide();
      else if (index == 2) $scope.modal2.hide();
      else if (index == 3) $scope.modal3.hide();
    };

    var successRequestNpk = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $ionicLoading.hide();



      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.npkItems.push(obj);

        if ($scope.npkItems.length >= infiniteLimit) {
          $scope.isLoadMoreShow = true;
        } else if (
          $scope.npkItems.length <= res.numberOfElements ||
          $scope.npkItems.length == 0
        ) {
          $scope.isLoadMoreShow = false;
        }

        $scope.$broadcast("scroll.refreshComplete");
        $scope.$broadcast("scroll.infiniteScrollComplete");


      }
    };

    var successRequestName = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $ionicLoading.hide();


      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.nameItems.push(obj);

        if ($scope.nameItems.length >= infiniteLimit) {
          $scope.isLoadMoreShow = true;
        } else if (
          $scope.nameItems.length <= res.numberOfElements ||
          $scope.nameItems.length == 0
        ) {
          $scope.isLoadMoreShow = false;
        }

        $scope.$broadcast("scroll.refreshComplete");
        $scope.$broadcast("scroll.infiniteScrollComplete");
      }

    };

    var successRequestCount = function (res) {
      if (res != null) {
        $rootScope.countApproval = res.count;
        $scope.general.countApproval = res.count;
      }
    };

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initModule();
    });

    function initModule() {
      $scope.profile = Main.getSession("profile");
      $scope.findStatus = false;
      $scope.isLoadMoreShow = false;
      // getNeedApproval('benefit',0);
      // getCountNeedApproval();
      // $scope.loadMoreName();
    }
  })

  .controller("PerfomanceCtrl", function (
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.perfomance");

    // open browser
    $scope.openBrowser = function () {
      if (Main.getPhpHost() == "https://talents-api.acc.co.id") {
        window.open(
          "https://hdff.fs.ap1.oraclecloud.com/homePage/faces/FuseWelcome",
          "_blank",
          "location=yes"
        );
      } else {
        window.open(
          "https://hdff-test.fs.ap1.oraclecloud.com/homePage/faces/FuseWelcome",
          "_blank",
          "location=yes"
        );
      }
    };

    function initModule() {
      $scope.profile = Main.getSession("profile");
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });
  })

  .controller("PerfomanceListCtrl", function (
    $scope,
    $state,
    Main
  ) {
    $scope.detail = {};

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.perfomance_list");

    $scope.gotoDetailPerfomance = function (id) {
      $state.go("app.perfomance_detail", {
        idx: id
      });
    };

    // get data list perfomance
    function getDataPerfomance() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/score";
      Main.requestApi(
        accessToken,
        urlApi,
        successPerfomance,
        $scope.errorRequest
      );
    }

    var successPerfomance = function (res) {
      $scope.detail = res;
    };

    function initModule() {
      $scope.profile = Main.getSession("profile");
      getDataPerfomance();
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });
  })

  .controller("PerfomanceInputCtrl", function (
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.perfomance_input");

    function initModule() {
      $scope.profile = Main.getSession("profile");
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });
  })

  .controller("PerfomanceDetailCtrl", function (
    $stateParams,
    $scope,
    $state,
    Main
  ) {
    var id = $stateParams.idx;

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.perfomance_detail");

    $scope.detail = {};

    // get data list perfomance
    function getDataDetailPerfomance() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/score/" + id;
      Main.requestApi(
        accessToken,
        urlApi,
        successPerfomance,
        $scope.errorRequest
      );
    }

    var successPerfomance = function (res) {
      $scope.detail = res;
    };

    function initModule() {
      $scope.profile = Main.getSession("profile");
      getDataDetailPerfomance();
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });
  })

  .controller("AmandementLetterCtrl", function (
    $ionicModal,
    $scope,
    $state,
    Main
  ) {
    $scope.numberLetter = "";

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.SRT_AMANDEMENT");

    $ionicModal
      .fromTemplateUrl("app/intro/image-preview.html", {
        scope: $scope,
        animation: "fade-in-scale"
      })
      .then(function (modal) {
        $scope.modalPopupImage = modal;
      });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function getNumberLetter() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/talentsTransactionNo/SRT_AMANDEMENT";
      Main.requestApi(
        accessToken,
        urlApi,
        successGetnumber,
        $scope.errorRequest
      );
    }

    // function to print
    $scope.printPdf = function (year) {
      getNumberLetter();
    };

    var successGetnumber = function (res) {
      $scope.numberLetter = res.letter_no;

      if (res != null) {
        var dataNumber = $scope.numberLetter;

        var profile = Main.getSession("profile");
        if (profile.employeeTransient.assignment.employment == null) {
          alert("Employment is null");
          return false;
        }
        var employment_id = profile.employeeTransient.assignment.employment;
        var accessToken = Main.getSession("token").access_token;
        var url = "";
        url =
          Main.getPrintAmandmentBaseUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&transactionNo=" +
          dataNumber;

        window.open(encodeURI(url), "_system", "location=yes");
        return false;
      }
    };
  })

  .controller("PkwtLetterCtrl", function (
    $ionicModal,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.SRT_PKWT");

    $ionicModal
      .fromTemplateUrl("app/intro/image-preview.html", {
        scope: $scope,
        animation: "fade-in-scale"
      })
      .then(function (modal) {
        $scope.modalPopupImage = modal;
      });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function getNumberLetter() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/talentsTransactionNo/SRT_PKWT";
      Main.requestApi(
        accessToken,
        urlApi,
        successGetnumber,
        $scope.errorRequest
      );
    }

    // function to print
    $scope.printPdf = function () {
      getNumberLetter();
    };

    var successGetnumber = function (res) {
      $scope.numberLetter = res.letter_no;

      if (res != null) {
        var dataNumber = $scope.numberLetter;


        // var dataNumber = "5218218271";
        var profile = Main.getSession("profile");
        if (profile.employeeTransient.assignment.employment == null) {
          alert("Employment is null");
          return false;
        }
        var employment_id = profile.employeeTransient.assignment.employment;
        var accessToken = Main.getSession("token").access_token;
        var url = "";
        url =
          Main.getPrintPkwtBaseUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&transactionNo=" +
          dataNumber;

        window.open(encodeURI(url), "_system", "location=yes");
        return false;
      }
    };
  })

  .controller('PPLetterCtrl', ['$scope', '$ionicModal', 'Main', '$ionicLoading', '$ionicPopup', '$timeout', '$state',
    function ($scope, $ionicModal, Main, $ionicLoading, $ionicPopup, $timeout, $statey) {

      $scope.nDate = new Date();
      $scope.Ytoday = $scope.nDate.getFullYear();

      $scope.openPP = function () {
        window.open($scope.url);
        // var ref = cordova.InAppBrowser.open('https://bit.ly/PeraturanPerusahaan2020ACC', '_blank', 'location=yes');
      };

      $scope.HideErrorRequest = function (err, status) {
        $ionicLoading.hide();
      }

      function getUrlPP() {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        var urlApi = Main.getPPUrlApi() + "/getHistUrl";
        var body = {
          "getHistUrl": {
            "YEAR": $scope.Ytoday
          }
        };

        var data = JSON.stringify(body);
        Main.postRequestApiPP(urlApi, data, successGetDataUrlPP, $scope.HideErrorRequest);
      }

      var successGetDataUrlPP = function (res) {
        $timeout(function () {
          $ionicLoading.hide();
        }, 500);
        $scope.pp = res;
        if ($scope.pp.OUT_MESS == "Success") {
          $scope.url = $scope.pp.OUT_DATA.URL;
          $scope.periode = $scope.pp.OUT_DATA.PERIODE_PP.replace(/[\s]/g, '');
        }
      }

      function getAddress() {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/address';
        Main.requestApi(accessToken, urlApi, successRequestAddress, $scope.errorRequest);
      }

      var successRequestAddress = function (res) {
        $timeout(function () {
          $ionicLoading.hide();
        }, 500);
        $scope.item = res[0];
      }

      $scope.acceptButton = function () {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var urlApi = Main.getPPUrlApi() + "/insertHist";

        $scope.div = $scope.profile.employeeTransient.assignment.division;

        if ($scope.div == null || $scope.div == "") {
          $scope.div = $scope.profile.employeeTransient.assignment.organizationName;
        }

        var body = {
          "insertHistInput": {
            "PERIODE_PP": $scope.periode.replace(/[\s]/g, ''),
            "NPK": $scope.profile.employeeTransient.assignment.employeeNo,
            "NAMA": $scope.profile.employeeTransient.name,
            "POSISI": $scope.profile.employeeTransient.assignment.positionName,
            "DEPT": $scope.profile.employeeTransient.assignment.organizationName,
            "DIVISI": $scope.div
          }
        };

        var data = JSON.stringify(body);
        Main.postRequestApiPP(urlApi, data, successAccept, $scope.HideErrorRequest);
      };

      var successAccept = function (res) {
        $ionicLoading.hide();

        var myPop = $ionicPopup.alert({
          title: 'Info',
          template: 'Submit berhasil..'
        });

        $timeout(function () {
          myPop.close();
          $scope.goBack('app.printletter');
        }, 2000);
      }

      $scope.$on('$ionicView.beforeEnter', function () {
        getUrlPP();
        getAddress();
      });

    }
  ])

  .controller("StatmentLetterCtrl", function (
    ionicSuperPopup,
    $ionicModal,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requestHeader = {};
    $scope.requestHeader.module = "SRT_KET_KERJA";

    $state.go("app.SRT_KET_KERJA");

    $ionicModal
      .fromTemplateUrl("app/intro/image-preview.html", {
        scope: $scope,
        animation: "fade-in-scale"
      })
      .then(function (modal) {
        $scope.modalPopupImage = modal;
      });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function sendData() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var data = JSON.stringify($scope.requestHeader);
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/statementLetter";

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successRequest,
        $scope.errorRequest
      );
    }

    $scope.requestLetter = function () {
      if (
        $scope.requestHeader.purpose == "" ||
        $scope.requestHeader.purpose == null
      ) {
        $scope.warningAlert("Purpose field is required");
      } else if (
        $scope.requestHeader.instansi == "" ||
        $scope.requestHeader.instansi == null
      ) {
        $scope.warningAlert("Instance field is required");
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }
          }
        );
      }
    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      ionicSuperPopup.show({
        title: "Letter Requested",
        text: "Please Contact PIC HC-PCBM for your Statement Letter!"
      });
      $scope.goBack("app.choiceletter");

      // list reset data post statment if success
      $scope.requestHeader.purpose = null;
      $scope.requestHeader.instansi = null;
    };

    $scope.cancelLetter = function () {
      $scope.goBack("app.choiceletter");
    };
  })

  .controller("AssignationLetterCtrl", function (
    $ionicModal,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.SRT_PENGANGKATAN");
    var dataNumber = "";

    $ionicModal
      .fromTemplateUrl("app/intro/image-preview.html", {
        scope: $scope,
        animation: "fade-in-scale"
      })
      .then(function (modal) {
        $scope.modalPopupImage = modal;
      });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function getNumberLetter() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/talentsTransactionNo/SRT_PENGANGKATAN";

      Main.requestApi(
        accessToken,
        urlApi,
        successGetnumber,
        $scope.errorRequest
      );
    }

    // function to print
    $scope.printPdf = function () {
      getNumberLetter();
    };

    var successGetnumber = function (res) {
      $scope.numberLetter = res.letter_no;

      if (res != null) {


        var profile = Main.getSession("profile");
        var dataNumber = $scope.numberLetter;
        if (profile.employeeTransient.assignment.employment == null) {
          alert("Employment is null");
          return false;
        }
        var employment_id = profile.employeeTransient.assignment.employment;
        var accessToken = Main.getSession("token").access_token;

        var url = "";

        url =
          Main.getPrintAssignationBaseUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&transactionNo=" +
          dataNumber;


        window.open(encodeURI(url), "_system", "location=yes");
        return false;
      }
    };
  })

  .controller("TaxLetterCtrl", function (
    $ionicModal,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $state.go("app.SRT_PERNYATAAN_PAJAK");

    $ionicModal
      .fromTemplateUrl("app/intro/image-preview.html", {
        scope: $scope,
        animation: "fade-in-scale"
      })
      .then(function (modal) {
        $scope.modalPopupImage = modal;
      });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    // function to print
    $scope.printPdf = function (year) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }
      var employment_id = profile.employeeTransient.assignment.employment;
      var accessToken = Main.getSession("token").access_token;
      var url = "";
      url =
        Main.getPrintTaxBaseUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };
  })

  .controller("VisaLetterCtrl", function (
    ionicSuperPopup,
    $ionicModal,
    ionicDatePicker,
    $ionicLoading,
    $filter,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requestHeader = {};
    $scope.requestHeader.module = "SRT_VISA";

    $state.go("app.SRT_VISA");

    $ionicModal
      .fromTemplateUrl("app/intro/image-preview.html", {
        scope: $scope,
        animation: "fade-in-scale"
      })
      .then(function (modal) {
        $scope.modalPopupImage = modal;
      });

    var datepicker = {
      callback: function (val) {
        //Mandatory
        $scope.requestHeader.departure_date_from = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: "popup" //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    var datepicker2 = {
      callback: function (val) {
        //Mandatory
        $scope.requestHeader.departure_date_to = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: "popup" //Optional
    };

    $scope.openDatePicker2 = function () {
      ionicDatePicker.openDatePicker(datepicker2);
    };

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function sendData() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var data = JSON.stringify($scope.requestHeader);
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/statementLetter";

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successRequest,
        $scope.errorRequest
      );
    }

    $scope.requestLetter = function () {
      var fromDate = $filter("date")(
        new Date($scope.requestHeader.departure_date_from),
        "yyyy-MM-dd"
      );
      var toDate = $filter("date")(
        new Date($scope.requestHeader.departure_date_to),
        "yyyy-MM-dd"
      );
      var today = new Date();

      // manipulate
      today.setHours(0, 0, 0, 0);

      if (fromDate <= today || toDate <= today) {
        $scope.warningAlert(
          "Cannot Back date, please check your departure date from and to"
        );
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }
          }
        );
      }
    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      ionicSuperPopup.show({
        title: "Letter Requested",
        text: "Please Contact PIC HC-PCBM for your Visa Letter!"
      });

      $scope.goBack("app.choiceletter");

      // list reset data post visa if success
      $scope.requestHeader.country_destination = null;
      $scope.requestHeader.passport_number = null;
      $scope.requestHeader.immigration_office = null;
      $scope.requestHeader.purpose = null;
      $scope.requestHeader.departure_date_from = null;
      $scope.requestHeader.departure_date_to = null;
      $scope.requestHeader.embassy_name = null;
      $scope.requestHeader.embassy_location = null;
      $scope.requestHeader.financed_by = null;
    };

    $scope.cancelLetter = function () {
      $scope.goBack("app.choiceletter");
    };
  })

  .controller("PayrollAccountCtrl", function (
    ionicSuperPopup,
    $ionicModal,
    $ionicActionSheet,
    appService,
    $cordovaCamera,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requestHeader = {};
    $scope.detail = {};
    $scope.bankList = {};
    $scope.isReset = 0;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.requestHeader.images = [];
    $scope.requestHeader.imagesData = [];
    $scope.modalHistory = {};
    $scope.history = {};
    // $scope.methodPayment = [{name:'Cash'},{name:'Transfer'}];
    // $scope.requestHeader.populateDataPayment = {};

    var messageValidation = "";

    $scope.removeChoice = function () {
      $("#inputImage").val("");
      var lastItem = $scope.requestHeader.imagesData.length - 1;
      $scope.requestHeader.imagesData.splice(lastItem);
      $scope.requestHeader.images.splice(lastItem);
    };

    $scope.addPicture = function () {
      if ($scope.requestHeader.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }

      $ionicActionSheet.show({
        buttons: [{
            text: "Take Picture"
          },
          {
            text: "Select From Gallery"
          }
        ],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener(
                "deviceready",
                function () {
                  $cordovaCamera.getPicture(appService.getCameraOptions()).then(
                    function (imageData) {
                      $scope.requestHeader.images.push({
                        image: "data:image/jpeg;base64," + imageData
                      });
                      $scope.requestHeader.imagesData.push({
                        image: imageData
                      });
                    },
                    function (err) {
                      appService.showAlert(
                        "Error",
                        err,
                        "Close",
                        "button-assertive",
                        null
                      );
                    }
                  );
                },
                false
              );

              break;
            case 1: // Select From Gallery
              document.addEventListener(
                "deviceready",
                function () {
                  $cordovaCamera
                    .getPicture(appService.getLibraryOptions())
                    .then(
                      function (imageData) {
                        $scope.requestHeader.images.push({
                          image: "data:image/jpeg;base64," + imageData
                        });
                        $scope.requestHeader.imagesData.push({
                          image: imageData
                        });
                      },
                      function (err) {
                        appService.showAlert(
                          "Error",
                          err,
                          "Close",
                          "button-assertive",
                          null
                        );
                      }
                    );
                },
                false
              );
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(
        function (imageData) {
          $scope.image = "data:image/jpeg;base64," + imageData;
          $scope.imageData = imageData;
        },
        function (err) {
          // An error occured. Show a message to the user
          $scope.errorAlert("an error occured while take picture");
        }
      );
    };

    $scope.resetForm = function () {
      $scope.requestHeader = {};

      if ($scope.detail != null) {
        $scope.detail = {};
      }

      $scope.isReset = 1;
    };

    function getListBank() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/bank";

      Main.requestApi(accessToken, urlApi, successBank, $scope.errorRequest);
    }

    function getDetailAccount() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/bankAccount";

      Main.requestApi(accessToken, urlApi, successDetail, $scope.errorRequest);
    }

    function getHistoryBankAccount() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/bankAccountHistory";

      Main.requestApi(accessToken, urlApi, successHistory, $scope.errorRequest);
    }

    var successBank = function (res) {
      if (res != null) {
        $ionicLoading.hide();

        var dataBank = res;

        $scope.bankList = dataBank;
      }
    };

    var successDetail = function (res) {
      if (res != null) {
        $ionicLoading.hide();

        $scope.detail = res;

        // if not null detail bank account
        $scope.requestHeader.bankBranch = res.bankBranch;
        $scope.requestHeader.accountName = res.accountName;
        $scope.requestHeader.bankAccount = res.bankAccount;
        $scope.requestHeader.populateData = {
          extId: res.bankNameExtId
        };
        $scope.requestHeader.bankNameExtId = res.bankNameExtId;
        $scope.requestHeader.bankName = res.bankName;
      }
    };

    var successHistory = function (res) {
      if (res != null) {
        $ionicLoading.hide();

        $scope.history = res;
      }
    };

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token;
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500) $scope.errorAlert(err.message);
        else $scope.errorAlert("Please Check your connection");
      }
    };

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    };

    var errRefreshToken = function (err, status) {};

    function sendData() {
      var idRef = Main.getSession("profile").employeeTransient.id;
      var employeePayrollId = $scope.detail.employeePayrollId;
      if ($scope.requestHeader.bankName === undefined) {
        $scope.requestHeader.bankName = $scope.detail.bankName;
      }
      // if ($scope.requestHeader.paymentMethod === undefined){ $scope.requestHeader.paymentMethod = $scope.detail.paymentMethod; }

      var jsonData =
        '{"accountName":"' +
        $scope.requestHeader.accountName +
        '","bankExtId":"' +
        $scope.requestHeader.bankNameExtId +
        '","bankName":"' +
        $scope.requestHeader.bankName +
        '", "bankAccount":"' +
        $scope.requestHeader.bankAccount +
        '","bankBranch":"' +
        $scope.requestHeader.bankBranch +
        '" }';

      var attachment = [];



      if ($scope.requestHeader.imagesData.length > 0) {
        for (var i = $scope.requestHeader.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            image: null
          };
          if ($scope.appMode == "mobile") {
            objAttachment = {
              image: $scope.requestHeader.imagesData[i].image
            };
          } else {
            if (
              $scope.requestHeader.imagesData[i].compressed.dataURL != undefined
            ) {
              var webImageAttachment = $scope.requestHeader.imagesData[
                i
              ].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                image: webImageAttachment
              };
            }
          }
          attachment.push(objAttachment);
        }
      }

      var dataStr = {
        task: "CHANGEBANKACCOUNT",
        data: jsonData,
        idRef: employeePayrollId,
        attachments: attachment
      };
      $ionicLoading.show({
        template: "Submit Request..."
      });

      var accessToken = Main.getSession("token").access_token;

      var urlApi = Main.getUrlApi() + "/api/user/workflow/dataapproval";
      var data = JSON.stringify(dataStr);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successRequest,
        $scope.errorRequest
      );
    }

    $scope.send = function (bankname, bankid) {
      // init data bank
      $scope.requestHeader.bankNameExtId = bankid;
      $scope.requestHeader.bankName = bankname;



      if ($scope.requestHeader.imagesData.length > 0) {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }
          }
        );
      } else {
        $scope.warningAlert("You must add at least 1 attachment.");
      }
    };

    $scope.changeBankName = function (data) {};

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.selfservice");
    };

    $ionicModal
      .fromTemplateUrl("app/talents/selfservice/payrollaccount/history.html", {
        scope: $scope,
        animation: "fade-in-scale",
        backdropClickToClose: true
      })
      .then(function (modal) {
        $scope.modal = modal;
      });

    $scope.closeModal = function () {
      $scope.modal.hide();
    };

    $scope.getDataHistory = function () {
      getHistoryBankAccount();
      $scope.modal.show();
    };


    function initModule() {
      getListBank();
      getDetailAccount();

    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })


  .controller("CheckCtrl", function (
    $ionicLoading,
    $stateParams,
    $cordovaGeolocation,
    $ionicLoading,
    $scope,
    $rootScope,
    $state,
    mapsInit,
    $ionicPopup,
    Main,
    ionicSuperPopup,
    $http,
    $ionicModal,
    $timeout
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.checkin = {};
    $scope.toggled = {
      checked: false
    };

    $scope.checkin.type = $stateParams.type;

    $scope.checkin.timezone = Main.getSession("profile").employeeTransient.assignment.timezone;


    if ($stateParams.type === "CHECKIN") {
      $scope.title = "Check In";
    } else {
      $scope.title = "Check Out";
    }

    $scope.checkin.employeeId = Main.getSession("profile").employeeTransient.id; // to set employeeid

    $scope.worklocation = {};

    var marker;
    gmarkers = [];
    var initialize = function () {


      var im = "img";

      //region load map
      var mapOptions = {
        center: new google.maps.LatLng(
          $scope.worklocation.lat,
          $scope.worklocation.lng
        ),
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.map = new google.maps.Map(
        document.getElementById("map"),
        mapOptions
      );
      google.maps.event.addListener($scope.map, "tilesloaded", function () {
        var watchOptions = {
          timeout: 60000, //1 menit
          enableHighAccuracy: true
        };
        var watch = $cordovaGeolocation.watchPosition(watchOptions);
        watch.then(
          null,
          function (err) {
            // error

            if (err.code == 1) {
              $ionicPopup.alert({
                title: "Error",
                template: "Please give permission to Talents to access your location and try again."
              }).then(function (res) {
                $scope.goBack("app.checkinout");
              });
            } else if (err.code == 2) {
              //POSITION_UNAVAILABLE
              $ionicPopup.alert({
                title: "Error",
                template: "Please enable your location and try again."
              }).then(function (res) {
                $scope.goBack("app.checkinout");
              });
            } else {
              //TIMEOUT
              $ionicPopup.alert({
                title: "Error",
                template: "Please enable your location and try again."
              }).then(function (res) {
                $scope.goBack("app.checkinout");
              });
            }

          },
          function (position) {
            // var pos = new google.maps.LatLng(
            //   position.coords.latitude,
            //   position.coords.longitude
            // );
            // if (marker == undefined) {
            //   new google.maps.Marker({
            //     position: pos,
            //     map: $scope.map,
            //     icon: "img/bluecircle.png"
            //   });
            // } else {
            //   marker.setPosition(pos);
            // }

            //migrate dari getCurrentPosition
            var latLng = new google.maps.LatLng(
              position.coords.latitude,
              position.coords.longitude
            );

            // set lat long position

            $scope.checkin.actualLat = position.coords.latitude;
            $scope.checkin.actualLng = position.coords.longitude;
            if (marker == undefined) {
              marker = new google.maps.Marker({
                position: latLng,
                map: $scope.map,
                icon: "img/bluecircle.png"
              });
            } else {
              marker.setPosition(latLng);
            }
            $scope.map.setCenter(latLng);
            watch.clearWatch();
          }
        );
        //watch.clearWatch();

        // var options = { timeout: 30000, enableHighAccuracy: true };
        // $cordovaGeolocation.getCurrentPosition(options).then(
        //   function(position) {
        //     var latLng = new google.maps.LatLng(
        //       position.coords.latitude,
        //       position.coords.longitude
        //     );

        //     // set lat long position

        //     $scope.checkin.actualLat = position.coords.latitude;
        //     $scope.checkin.actualLng = position.coords.longitude;
        //     if (marker == undefined) {
        //       marker = new google.maps.Marker({
        //         position: latLng,
        //         map: $scope.map,
        //         icon: "img/bluecircle.png"
        //       });
        //     }
        //     $scope.map.setCenter(latLng);
        //   },
        //   function(error) {
        //     //   cordova.plugins.diagnostic.switchToSettings(function(){

        //     // }, function(error){
        //     //     console.error("The following error occurred: "+error);
        //     // });
        //     $ionicPopup.alert({
        //       title: "Error",
        //       template:
        //         error.message +
        //         " test " +
        //         error.code +
        //         "Please enable your location and try again."
        //     });
        //   }
        // );
      });
      if ($scope.worklocation.lat != 'null' && $scope.worklocation.lng != 'null') {
        // new google.maps.Circle({
        //     strokeColor: "#00ff6a",
        //     strokeOpacity: 0.8,
        //     strokeWeight: 2,
        //     fillColor: "#00ff6a",
        //     fillOpacity: 0.35,
        //     map: $scope.map,
        //     center: {
        //         lat: $scope.worklocation.lat,
        //         lng: $scope.worklocation.lng
        //     },
        //     radius: $scope.worklocation.tolleranceInMeter
        // });
      }
      if ($scope.worklocation.lat2 != 'null' && $scope.worklocation.lng2 != 'null') {
        // new google.maps.Circle({
        //     strokeColor: "#00ff6a",
        //     strokeOpacity: 0.8,
        //     strokeWeight: 2,
        //     fillColor: "#00ff6a",
        //     fillOpacity: 0.35,
        //     map: $scope.map,
        //     center: {
        //         lat: $scope.worklocation.lat2,
        //         lng: $scope.worklocation.lng2
        //     },
        //     radius: $scope.worklocation.tolleranceInMeter
        // });
      }
      //end region load map

      // var watchOptions = {
      //   timeout: 60000, //1 menit
      //   enableHighAccuracy: true, // may cause errors if true
      //   maximumAge: 300000 //5 menit
      // };
      // var watch = $cordovaGeolocation.watchPosition(watchOptions);
      // watch.then(
      //   null,
      //   function(err) {
      //     // error

      //   },
      //   function(position) {
      //     var pos = new google.maps.LatLng(
      //       position.coords.latitude,
      //       position.coords.longitude
      //     );
      //     if (marker == undefined) {
      //       new google.maps.Marker({
      //         position: pos,
      //         map: $scope.map,
      //         icon: "img/bluecircle.png"
      //       });
      //     } else {
      //       marker.setPosition(pos);
      //     }
      //   }
      // );
      // watch.clearWatch();
    };

    function getWorkLocation() {
      var employeeId = $scope.checkin.employeeId;

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/worklocation?employeeId=" + employeeId;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    var successRequest = function (res) {

      $ionicLoading.hide();
      $scope.worklocation = res;
      $scope.worklocation.lat = res.lat;
      $scope.worklocation.lng = res.lng;
      $scope.worklocation.lat2 = res.lat2;
      $scope.worklocation.lng2 = res.lng2;
      $scope.worklocation.tolleranceInMeter = res.tolleranceInMeter;
      mapsInit.mapsInitialized.then(
        function () {
          // Promised resolved
          initialize();
        },
        function () {
          // Promise rejected
        }
      );
    };

    $scope.validationSend = function () {
      if ($scope.toggled.checked == false) {

        // if in the office selected then clear the purpose selection
        $scope.checkin.purpose = null;

        var x = new google.maps.LatLng(
          $scope.worklocation.lat,
          $scope.worklocation.lng
        );
        var y = new google.maps.LatLng(
          $scope.checkin.actualLat,
          $scope.checkin.actualLng
        );
        if ($scope.worklocation.lat2 != 'null' && $scope.worklocation.lng2 != 'null') {
          var x2 = new google.maps.LatLng(
            $scope.worklocation.lat2,
            $scope.worklocation.lng2
          );
        }

        $scope.radius = google.maps.geometry.spherical.computeDistanceBetween(
          x,
          y
        );
        if (x2 != undefined) {

          $scope.radius2 = google.maps.geometry.spherical.computeDistanceBetween(
            x2,
            y
          );
        }
        var toleranRadius = $scope.worklocation.tolleranceInMeter;

        if ($scope.radius <= toleranRadius || ($scope.radius2 != undefined && $scope.radius2 <= toleranRadius)) {
          saveData();
        } else {
          $ionicPopup.alert({
            title: "Too far from Office",
            template: "Your position is beyond  " +
              toleranRadius +
              " meters from the office"
          });
        }
      } else {

        if ($scope.checkin.purpose == null) {
          $ionicPopup.alert({
            title: "Warning",
            template: "Please select purpose."
          });
        } else {
          if ($scope.checkin.remark == null || $scope.checkin.remark == "") {
            $ionicPopup.alert({
              title: "Warning",
              template: "Remark cannot be empty."
            });
          } else {
            saveData();

          }
        }
      }
    };



    var saveData = function () {
      if ($scope.checkin.actualLng == null || $scope.checkin.actualLat == null) {
        $ionicPopup.alert({
          title: "Warning",
          template: "Location is not found. Please try again."
        });
      } else {


        $scope.checkin.isOutOfOffice = $scope.toggled.checked;
        // $scope.

        $ionicLoading.show({
          template: "Submit Request..."
        });

        var accessToken = Main.getSession("token").access_token;
        var data = JSON.stringify($scope.checkin);


        var urlApi = Main.getUrlApi() + "/api/user/empDaily";
        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successSave,
          $scope.errorRequest
        );
      }
    };

    var successSave = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.checkinout");
    };

    $scope.isToggled = function () {
      if ($scope.toggled.checked) return true;
      else if (!$scope.toggled.checked) return false;
    };

    $scope.$on("$ionicView.beforeEnter", function () {

      getWorkLocation();
      // var condition1 = !(Main.getSession("profile").isFeatureTester == true);

      // if (condition1)
      //   $state.go("app.myhr");
    });
  })

  .controller("CheckInOutCtrl", function (
    $scope,
    $state,
    $ionicPopup,
    $rootScope,
    Main,
    $http,
    $ionicModal,
    $ionicLoading,
    ionicSuperPopup,
    $cordovaLocalNotification,
    $timeout
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    // var condition1 = !(Main.getSession("profile").isFeatureTester == true);

    // if (condition1)
    //   $state.go("app.myhr");

    //var options = { timeout: 1000, enableHighAccuracy: false };
    //$cordovaGeolocation.getCurrentPosition(options);
    $scope.isLead = Main.getSession("profile").isLeader;

    $scope.$on("$ionicView.beforeEnter", function () {
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      let day = new Date();
    });

    if ($scope.isLead == true) {
      function getTotalRecord() {
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?module=' + 'time_management';

        Main.requestApi(
          accessToken,
          urlApi,
          successTotalRecord,
          $scope.errorRequest
        );
      }


      var successTotalRecord = function (res) {
        var totalR = res.totalRecord; // 42
        var countPages = totalR / 5;
        var latestPage = Math.ceil(countPages);

        // save localstorage
        Main.saveDataTotalPage(latestPage);

      };
      getTotalRecord();

    }

    // FUNCTION DAILY QUIZ TECAT

    function myPop() {

      var myPop = $ionicPopup.alert({
        title: 'Warning',
        template: "popup",
        scope: $scope,
        buttons: [{
          text: '<b>Ok</b>',
          type: 'button-positive',
          onTap: function (e) {
            myPop.close();
          }
        }]
      });

      myPop.then(function () {
        $timeout(function () {}, 2500);
      })

      $timeout(function () {
        myPop.close();
      }, 2500);
    }

    $scope.generateQuestionIn = function () {
      if ($scope.isLead == false) {
        getSoalYesterdayIn();
        //getSoalTodayIn();
      } else {
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      }
    }

    $scope.generateQuestionOut = function () {
      if ($scope.isLead == false) {
        getSoalYesterdayOut();
      } else {
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      }
    }

    // ** FUNGSI SHUFFLE JAWABAN **

    function shuffle(array) {
      let counter = array.length;

      while (counter > 0) {
        let index = Math.floor(Math.random() * counter);

        counter--;

        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
      }

      return array;
    }

    var succesSoalYesterdayInn = function (res) {
      $scope.soal = res;
      let day = new Date().getDate();
      let start = $scope.soal[0].time_start;
      $scope.start_at = new Date(start).getDate();

      if ($scope.start_at == day) {
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      } else {
        if (res != 0) {
          $scope.hideSoal = 1;
          let start = $scope.soal[0].time_start;
          $scope.start_at = new Date(start);
          $scope.question = $scope.soal[0].mst_question.question;
          $scope.attempts = $scope.soal[0].attempts_id;
          var shuffleData = shuffle($scope.soal[0].mst_question.mst_question_answers);
          $scope.radioButtons = [{
              opsi: "A",
              text: shuffleData[0].answer,
              value: shuffleData[0].question_answer_id
            },
            {
              opsi: "B",
              text: shuffleData[1].answer,
              value: shuffleData[1].question_answer_id
            },
            {
              opsi: "C",
              text: shuffleData[2].answer,
              value: shuffleData[2].question_answer_id
            },
            {
              opsi: "D",
              text: shuffleData[3].answer,
              value: shuffleData[3].question_answer_id
            },
          ];
          $scope.modalSoal.show();
        } else {
          getSoalTodayIn();
        }
      }
    }

    function giveSoalFromBucket() {
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-quiz?npk=" + npk;
      Main.getApi(
        urlApi,
        successGiveSoalFromBucket,
        $scope.errorRequestIn
      );
    }

    var successGiveSoalFromBucket = function (res) {
      $scope.soal = res;

      if (res != 0) {
        $scope.hideSoal = 1;
        let start = $scope.soal[0].time_start;
        $scope.start_at = new Date(start);
        $scope.question = $scope.soal[0].mst_question.question;
        $scope.attempts = $scope.soal[0].attempts_id;
        var shuffleData = shuffle($scope.soal[0].mst_question.mst_question_answers);
        $scope.radioButtons = [{
            opsi: "A",
            text: shuffleData[0].answer,
            value: shuffleData[0].question_answer_id
          },
          {
            opsi: "B",
            text: shuffleData[1].answer,
            value: shuffleData[1].question_answer_id
          },
          {
            opsi: "C",
            text: shuffleData[2].answer,
            value: shuffleData[2].question_answer_id
          },
          {
            opsi: "D",
            text: shuffleData[3].answer,
            value: shuffleData[3].question_answer_id
          },
        ];
        $scope.modalSoalOut.show();
      } else {
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      }
    }

    // ** FUNGSI TECAT CHECKIN **

    function getSoalYesterdayIn() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-quiz?npk=" + npk;
      Main.getApi(
        urlApi,
        succesSoalYesterdayIn,
        $scope.errorRequestIn
      );
    }

    var succesSoalYesterdayIn = function (res) {
      $scope.soal = res;
      $ionicLoading.hide();
      let day = new Date().getDate();
      let start = $scope.soal[0].time_start;
      $scope.start_at = new Date(start).getDate();

      if ($scope.start_at == day) {
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      } else {
        if (res != 0) {
          $scope.hideSoal = 1;
          let start = $scope.soal[0].time_start;
          $scope.start_at = new Date(start);
          $scope.question = $scope.soal[0].mst_question.question;
          $scope.attempts = $scope.soal[0].attempts_id;
          var shuffleData = shuffle($scope.soal[0].mst_question.mst_question_answers);
          $scope.radioButtons = [{
              opsi: "A",
              text: shuffleData[0].answer,
              value: shuffleData[0].question_answer_id
            },
            {
              opsi: "B",
              text: shuffleData[1].answer,
              value: shuffleData[1].question_answer_id
            },
            {
              opsi: "C",
              text: shuffleData[2].answer,
              value: shuffleData[2].question_answer_id
            },
            {
              opsi: "D",
              text: shuffleData[3].answer,
              value: shuffleData[3].question_answer_id
            },
          ];
          $scope.modalSoal.show();
        } else {
          getSoalTodayIn(); // FAILED GET SOAL
        }
      }
    }

    function getSoalTodayIn() {
      var job_name = $scope.profile.employeeTransient.assignment.jobTitleName;
      var new_job_name = encodeURIComponent(job_name);
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-quiz-today?npk=" + npk + "&job_name=" + new_job_name;
      Main.getApi(
        urlApi,
        succesSoalTodayIn,
        $scope.errorRequestIn
      );
    }

    var succesSoalTodayIn = function (res) {
      $ionicLoading.hide();
      $scope.soal = res;
      if (res == 0) {
        $scope.modalSoal.hide();
        var data = "Tidak ada soal lagi untuk hari ini.";
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      } else if (res == 404) {
        var data = "End date soal tidak mencapai hari ini.";
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      } else if (res == "Anda sudah mendapatkan quiz hari ini" || res == 'tidak ada soal' || res == 'Tidak ada soal untuk hari minggu' || res == 'Anda masih memiliki quiz yang belum di kerjakan') {
        $scope.modalSoal.hide();
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      } else {
        $scope.hideSoal = 0;
        let start = $scope.soal[0].time_start;
        $scope.start_at = new Date(start);
        $scope.question = $scope.soal[0].mst_question.question;
        $scope.attempts = $scope.soal[0].attempts_id;
        var shuffleData = shuffle($scope.soal[0].mst_question.mst_question_answers);
        $scope.radioButtons = [{
            opsi: "A",
            text: shuffleData[0].answer,
            value: shuffleData[0].question_answer_id
          },
          {
            opsi: "B",
            text: shuffleData[1].answer,
            value: shuffleData[1].question_answer_id
          },
          {
            opsi: "C",
            text: shuffleData[2].answer,
            value: shuffleData[2].question_answer_id
          },
          {
            opsi: "D",
            text: shuffleData[3].answer,
            value: shuffleData[3].question_answer_id
          },
        ];
        $scope.modalSoal.show();
      }
    }

    $scope.errorRequestIn = function () {
      $ionicLoading.hide();
      $state.go("app.check", {
        'type': 'CHECKIN'
      });
    }

    // ** FUNGSI TECAT CHECKOUT **

    function getSoalYesterdayOut() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-quiz?npk=" + npk;
      Main.getApi(
        urlApi,
        succesSoalYesterdayOut,
        $scope.errorRequestOut
      );
    }

    var succesSoalYesterdayOut = function (res) {
      $scope.soal = res;
      $ionicLoading.hide();
      if (res != 0) {
        $scope.hideSoal = 1;
        let start = $scope.soal[0].time_start;
        $scope.start_at = new Date(start);
        $scope.question = $scope.soal[0].mst_question.question;
        $scope.attempts = $scope.soal[0].attempts_id;
        var shuffleData = shuffle($scope.soal[0].mst_question.mst_question_answers);
        $scope.radioButtons = [{
            opsi: "A",
            text: shuffleData[0].answer,
            value: shuffleData[0].question_answer_id
          },
          {
            opsi: "B",
            text: shuffleData[1].answer,
            value: shuffleData[1].question_answer_id
          },
          {
            opsi: "C",
            text: shuffleData[2].answer,
            value: shuffleData[2].question_answer_id
          },
          {
            opsi: "D",
            text: shuffleData[3].answer,
            value: shuffleData[3].question_answer_id
          },
        ];
        $scope.modalSoalOut.show();
      } else {
        getSoalTodayOut();
      }
    }

    function getSoalTodayOut() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var job_name = $scope.profile.employeeTransient.assignment.jobTitleName;
      var new_job_name = encodeURIComponent(job_name);
      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-quiz-today?npk=" + npk + "&job_name=" + new_job_name;
      Main.getApi(
        urlApi,
        succesSoalTodayOut,
        $scope.errorRequestOut
      );
    }

    var succesSoalTodayOut = function (res) {
      $ionicLoading.hide();
      $scope.soal = res;
      if (res == 0) {
        $scope.modalSoalOut.hide();
        var data = "Tidak ada soal lagi untuk hari ini.";
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      } else if (res == 404) {
        var data = "End date soal tidak mencapai hari ini.";
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      } else if (res == "Anda sudah mendapatkan quiz hari ini" || res == 'tidak ada soal' || res == 'Tidak ada soal untuk hari minggu' || res == 'Anda masih memiliki quiz yang belum di kerjakan') {
        $scope.modalSoalOut.hide();
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      } else {
        $scope.hideSoal = 0;
        let start = $scope.soal[0].time_start;
        $scope.start_at = new Date(start);
        $scope.question = $scope.soal[0].mst_question.question;
        $scope.attempts = $scope.soal[0].attempts_id;
        var shuffleData = shuffle($scope.soal[0].mst_question.mst_question_answers);
        $scope.radioButtons = [{
            opsi: "A",
            text: shuffleData[0].answer,
            value: shuffleData[0].question_answer_id
          },
          {
            opsi: "B",
            text: shuffleData[1].answer,
            value: shuffleData[1].question_answer_id
          },
          {
            opsi: "C",
            text: shuffleData[2].answer,
            value: shuffleData[2].question_answer_id
          },
          {
            opsi: "D",
            text: shuffleData[3].answer,
            value: shuffleData[3].question_answer_id
          },
        ];
        $scope.modalSoalOut.show();
      }
    }

    $scope.errorRequestOut = function (res, status) {
      $ionicLoading.hide();
      $state.go("app.check", {
        'type': 'CHECKOUT'
      });
    }

    // ** MODAL DAILY QUESTION **

    $ionicModal.fromTemplateUrl('app/intro/showModal.html', {
      scope: $scope,
      animation: 'fade-in-scale',
      backdropClickToClose: false
    }).then(function (modal) {
      $scope.modalSoal = modal;
    });

    $scope.openSoal = function () {
      getSoalYesterdayIn();
    };

    $scope.closeSoal = function () {
      if ($scope.hideSoal == 0) {
        $scope.modalSoal.hide();
        $state.go("app.check", {
          'type': 'CHECKIN'
        });
      } else if ($scope.hideSoal == 1) {
        $ionicPopup.confirm({
          title: "Warning",
          template: "<h4>You must finish the daily quiz.</h4>",
          buttons: [{
            text: 'Cancel'
          }, {
            text: 'Ok',
            type: 'button-positive',
            onTap: function (e) {
              $scope.modalSoal.show();
            }
          }]
        });
      }
      $scope.modalSoal.hide();
    };

    $ionicModal.fromTemplateUrl('app/intro/showModalOut.html', {
      scope: $scope,
      animation: 'fade-in-scale',
      backdropClickToClose: false
    }).then(function (modal) {
      $scope.modalSoalOut = modal;
    });

    $scope.openSoalOut = function () {
      getSoalYesterdayOut();
    };

    $scope.closeSoalOut = function () {
      if ($scope.hideSoal == 0) {
        $ionicPopup.confirm({
          title: "Warning",
          template: "<h4>You must finish the daily quiz.</h4>",
          buttons: [{
            text: 'Cancel'
          }, {
            text: 'Ok',
            type: 'button-positive',
            onTap: function (e) {
              $scope.modalSoalOut.show();
            }
          }]
        });
      } else if ($scope.hideSoal == 1) {
        $ionicPopup.confirm({
          title: "Warning",
          template: "<h4>You must finish the daily quiz.</h4>",
          buttons: [{
            text: 'Cancel'
          }, {
            text: 'Ok',
            type: 'button-positive',
            onTap: function (e) {
              $scope.modalSoalOut.show();
            }
          }]
        });
      }
      $scope.modalSoalOut.hide();
    };

    $ionicModal.fromTemplateUrl('app/intro/showModalNotif.html', {
      scope: $scope,
      animation: 'fade-in-scale',
      backdropClickToClose: false
    }).then(function (modal) {
      $scope.modalSoalNotif = modal;
    });

    $scope.closeSoalNotif = function () {
      $scope.modalSoalNotif.hide();
    };

    $scope.openSoalNotif = function () {
      getSoalFromNotif();
    };

    // ** SUBMIT ANSWER IN **

    $scope.soal = {};

    function verificationForm(usertalent) {
      if ($scope.soal.answer == undefined || $scope.soal.answer == '') {
        messageValidation = "Please answer the question";
        return false;
      }
      return true;
    }

    $scope.submitForm = function () {
      if (verificationForm($scope.soal)) {

        var urlAPI = Main.getTecatUrlApi() + '/api/answer-quiz';
        var keyMitra;
        var kodePerusahaan;

        var data = {
          npk: $scope.profile.employeeTransient.assignment.employeeNo,
          attempts_id: $scope.attempts,
          question_answer_id: $scope.soal.answer,
        }
        data = JSON.stringify(data);
        var result = Main.postApi(
          urlAPI,
          data,
          $scope.successRequestIn,
          $scope.errorRequest
        );

        //$scope.disableButton  = "true";
        $scope.modalSoal.hide();

      } else {

        $ionicPopup.confirm({
          title: "Warning",
          template: messageValidation
        });
      }
    };

    $scope.successRequestIn = function (res) {
      $scope.modalSoal.hide();

      $scope.succesSubmit = res;
      if (res == 4) // jika salah dan tepat waktu
      {
        notifOTUnCorrectAnswer();
      } else if (res == 3) // jika salah tapi tidak tepat
      {
        notifUnCorrectAnswer();
      } else if (res == 2) // jika benar dan tepat waktu
      {
        notifOTCorrectAnswer();
      } else if (res == 1) // jika benar tapi tidak tepat waktu
      {
        notifCorrectAnswer();
      }
      getSoalYesterdayIn();

    };

    // ** SUBMIT ANSWER OUT **

    $scope.submitFormOut = function () {
      if (verificationForm($scope.soal)) {

        var urlAPI = Main.getTecatUrlApi() + '/api/answer-quiz';
        var keyMitra;
        var kodePerusahaan;

        var data = {
          npk: $scope.profile.employeeTransient.assignment.employeeNo,
          attempts_id: $scope.attempts,
          question_answer_id: $scope.soal.answer,
        }

        data = JSON.stringify(data);

        var result = Main.postApi(
          urlAPI,
          data,
          $scope.successRequestOut,
          $scope.errorRequestOut
        );

        //$scope.disableButton  = "true";
        $scope.modalSoalOut.hide();

      } else {

        $ionicPopup.confirm({
          title: "Warning",
          template: messageValidation
        });
      }
    };

    $scope.successRequestOut = function (res) {
      $scope.succesSubmit = res;
      if (res == 4) // jika salah dan tepat waktu
      {
        notifOTUnCorrectAnswer();
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      } else if (res == 3) // jika salah tapi tidak tepat
      {
        notifUnCorrectAnswer();
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      } else if (res == 2) // jika benar dan tepat waktu
      {
        notifOTCorrectAnswer();
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      } else if (res == 1) // jika benar tapi tidak tepat waktu
      {
        notifCorrectAnswer();
        $state.go("app.check", {
          'type': 'CHECKOUT'
        });
      }

    };

    // ** SUBMIT ANSWER NOTIF **

    $scope.submitFormNotif = function () {
      if (verificationForm($scope.soal)) {

        var urlAPI = Main.getTecatUrlApi() + '/api/answer-quiz';
        var keyMitra;
        var kodePerusahaan;

        var data = {
          npk: $scope.profile.employeeTransient.assignment.employeeNo,
          attempts_id: $scope.attempts,
          question_answer_id: $scope.soal.answer,
        }

        data = JSON.stringify(data);

        var result = Main.postApi(
          urlAPI,
          data,
          $scope.successRequestNotif,
          $scope.errorRequest
        );

        $scope.disableButton = "true";

      } else {

        $ionicPopup.confirm({
          title: "Warning",
          template: messageValidation
        });
      }
    };

    $scope.successRequestNotif = function (res) {
      $scope.modalSoalNotif.hide();

      if (res == 4) // jika salah dan tepat waktu
      {
        notifOTUnCorrectAnswer();
      } else if (res == 3) // jika salah tapi tidak tepat
      {
        notifUnCorrectAnswer();
      } else if (res == 2) // jika benar dan tepat waktu
      {
        notifOTCorrectAnswer();
      } else if (res == 1) // jika benar tapi tidak tepat waktu
      {
        notifCorrectAnswer();
      }
    };

    // ** FEEDBACK NOTIFICATION **

    function notifOTUnCorrectAnswer() {
      var urlApi = Main.getTecatUrlApi() + "/api/get-notif-ot-uncorrect-answer";
      Main.getApi(
        urlApi,
        successOTUnCorrect,
        $scope.errorRequest
      );
    }

    var successOTUnCorrect = function (res) {
      $scope.correct = res;

      var message1 = $scope.correct[0].default[0].template;
      var message2 = $scope.correct[0].on_time[0].template;

      var myPop = $ionicPopup.alert({
        title: 'Warning',
        template: message1 + "<br><br>" + message2
      });


      myPop.then(function () {
        $timeout(function () {}, 2500);
      });

      $timeout(function () {
        myPop.close();
      }, 2500);

      // ** POST LOG NOTIF **

      var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
      var keyMitra;
      var kodePerusahaan;

      var data = {
        npk: $scope.profile.employeeTransient.assignment.employeeNo,
        notification_id: $scope.correct[0].default[0].notification_id,
        keterangan: "on time answered",
      }

      data = JSON.stringify(data);
      var result = Main.postApi(
        urlAPI,
        data,
        $scope.successRequest,
        $scope.errorPushNotif
      );

    }

    function notifOTCorrectAnswer() {

      var urlApi = Main.getTecatUrlApi() + "/api/get-notif-ot-correct-answer";
      Main.getApi(
        urlApi,
        successOTCorrect,
        $scope.errorRequest
      );
    }

    var successOTCorrect = function (res) {
      $scope.correct = res;
      var message1 = $scope.correct[0].default[0].template;
      var message2 = $scope.correct[0].on_time[0].template;

      var myPop = $ionicPopup.alert({
        title: 'Success',
        template: message1 + "<br><br>" + message2
      });

      myPop.then(function () {
        $timeout(function () {}, 2500);
      });

      $timeout(function () {
        myPop.close();
      }, 2500);

      // ** POST LOG NOTIF **

      var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
      var keyMitra;
      var kodePerusahaan;

      var data = {
        npk: $scope.profile.employeeTransient.assignment.employeeNo,
        notification_id: $scope.correct[0].default[0].notification_id,
        keterangan: "on time answered",
      }

      data = JSON.stringify(data);
      var result = Main.postApi(
        urlAPI,
        data,
        $scope.successRequest,
        $scope.errorPushNotif
      );

    }

    function notifCorrectAnswer() {
      var urlApi = Main.getTecatUrlApi() + "/api/get-notif-correct-answer";
      Main.getApi(
        urlApi,
        successCorrect,
        $scope.errorRequest
      );
    }

    var successCorrect = function (res) {

      $scope.correct = res;
      var message = $scope.correct[0].template;

      var myPop = $ionicPopup.alert({
        title: 'Success',
        template: message
      });

      myPop.then(function () {
        $timeout(function () {}, 2500);
      });

      $timeout(function () {
        myPop.close();
      }, 2500);

      // ** POST LOG NOTIF **

      var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
      var keyMitra;
      var kodePerusahaan;

      var data = {
        npk: $scope.profile.employeeTransient.assignment.employeeNo,
        notification_id: $scope.correct[0].notification_id,
        keterangan: "-",
      }

      data = JSON.stringify(data);
      var result = Main.postApi(
        urlAPI,
        data,
        $scope.successRequest,
        $scope.errorPushNotif
      );

    }

    function notifUnCorrectAnswer() {
      var urlApi = Main.getTecatUrlApi() + "/api/get-notif-uncorrect-answer";
      Main.getApi(
        urlApi,
        successUnCorrect,
        $scope.errorRequest
      );
    }

    var successUnCorrect = function (res) {
      $scope.uncorrect = res;
      var message = $scope.uncorrect[0].template;

      var myPop = $ionicPopup.alert({
        title: 'Warning',
        template: message
      });

      myPop.then(function () {
        $timeout(function () {}, 2500);
      });

      $timeout(function () {
        myPop.close();
      }, 2500);

      // ** POST LOG NOTIF **

      var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
      var keyMitra;
      var kodePerusahaan;

      var data = {
        npk: $scope.profile.employeeTransient.assignment.employeeNo,
        notification_id: $scope.uncorrect[0].notification_id,
        keterangan: "-",
      }

      data = JSON.stringify(data);
      var result = Main.postApi(
        urlAPI,
        data,
        $scope.successRequest,
        $scope.errorPushNotif
      );

    }

    $scope.errorPushNotif = function (err, status) {
      $ionicLoading.hide();
    }

    // END OF FUNCTION DAILY QUIZ TECAT
  })



  .controller("ApprovalCheckinoutCtrl", function (
    ionicSuperPopup,
    $ionicHistory,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    $ionicScrollDelegate,
    mapsInit,
    $ionicPopup,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var i = 0;
    var size = 5;
    var j = 0;
    var indexPaging = 0;

    $scope.dataApproval = [];
    $scope.isLoadMoreShow = false;
    $scope.approval = {};
    $scope.listId = [];
    $scope.confirm = {
      reasonReject: "",
      approvalStatus: ""
    };
    $scope.checkItems = {};
    $scope.dataLocations = [];
    $scope.hasSelectedAnItem = false;
    $scope.executeTotal = 0;

    $scope.totalR = 0;
    $scope.latestPage = 0;
    $scope.latest = 0;
    $scope.isfirst = 0;

    // for google maps purpose
    var gmarkers = [];
    var accMap;
    var infoWin;
    // end for google maps purpose

    $scope.loadMoreName = function () {
      indexPaging++;
      $scope.dataLocations = [];
      getListApproval(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevName = function () {
      indexPaging--;
      $scope.dataLocations = [];
      getListApproval(indexPaging);
    };


    $scope.pointMarker = function (id) {
      javascript: google.maps.event.trigger(gmarkers[id], "click");
    };

    function initialize() {
      accMap = new google.maps.Map(document.getElementById("mapApproval"), {
        zoom: 17,
        center: {
          lat: -6.304693,
          lng: 106.8465555
        }
      });
    };

    function createMarker(location) {
      infoWin = new google.maps.InfoWindow();
      var marker = new google.maps.Marker({
        position: location,
        map: accMap
      });

      var purposeView = "";

      if (location.purpose == "MTGL")
        purposeView = "Meeting";
      else if (location.purpose == "DNS")
        purposeView = "Perjalanan Dinas";
      else if (location.purpose == "TRN")
        purposeView = "Training/Seminar";
      else if (location.purpos == "DNSL")
        purposeView = "Dinas di Luar Kantor";
      else
        purposeView = "undefined";

      google.maps.event.addListener(marker, "click", function () {
        infoWin.setContent(
          "<p><h5>" +
          location.npk +
          " - " +
          location.name +
          "</h5></p>" +
          "<p>Purpose : " +
          purposeView +
          "</p>" +
          "<p>Date/Time : " +
          location.datetime +
          "</p>" +
          "<p>Remark : " +
          location.remark +
          "</p>"
        );
        infoWin.setOptions({
          maxHeight: 200
        });
        infoWin.open(accMap, marker);
      });
      return marker;
    }

    function getListApproval(page) {
      if (page == null || page === "undefined") {
        page = 0;
      }

      if (page == 0) {
        $scope.isfirst = 1;
      }

      // calculate last
      var dataLatestPage = Main.getDataTotalPage();
      var fixLatestPage = dataLatestPage - 1;

      // reset
      $scope.latest = 0;

      if (page == fixLatestPage) {
        $scope.latest = 1;
      }

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() +
        "/api/user/workflow/needapproval?page=" +
        page +
        "&size=" +
        size +
        "&module=" +
        "time_management";

      Main.requestApi(
        accessToken,
        urlApi,
        successApproval,
        $scope.errorRequest
      );
    }

    var successApproval = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.dataApproval = [];

      var record = res.data;

      var totalRecord = record.length;
      var currentPage = 1;
      var totalPages = 1;
      var startPage = 1;
      var endPage = 1;

      var countPages = function () {
        if (totalRecord % size == 0 && totalRecord > size)
          var totalPages = totalRecord / size;
        else if (totalRecord % size != 0 && totalRecord > size)
          var totalPages = Math.ceil(totalRecord / size);
        else if (totalRecord < size || totalRecord == size) var totalPages = 1;

        return totalPages;
      };

      var endPage = countPages();

      $scope.nextPage = function () {
        if (currentPage == endPage) {} else currentPage++;
      };

      $scope.prevPage = function () {
        if (currentPage == 1) {} else currentPage--;
      };

      $scope.isLast = function () {
        if (currentPage == endPage) return true;
        else return false;
      };

      $scope.isFirst = function () {
        if (currentPage == 1) return true;
        else return false;
      };

      $ionicLoading.hide();

      //added by JUW, clear the array first
      $scope.dataLocations = [];

      if (res != null) {
        for (var i = 0; i < res.data.length; i++) {
          var obj = res.data[i];

          $scope.dataApproval.push(obj);
          $scope.dataLocations.push(JSON.parse(obj.data));

          $scope.dataApproval[i].data = JSON.parse(res.data[i].data);

          if (res.last == false) {
            $scope.isLoadMoreShow = true;
          } else {
            $scope.isLoadMoreShow = false;
          }

          $ionicScrollDelegate.$getByHandle('listApprovalScroll').scrollTop();
          $ionicLoading.hide();
          $scope.$broadcast("scroll.refreshComplete");
          $scope.$broadcast("scroll.infiniteScrollComplete");
        }
      }

      mapsInit.mapsInitialized.then(
        function () {
          // Promised resolved
          // put the marker.
          // clear the gmarkers array first.

          clearGMarkers();
          for (var i = 0; i < $scope.dataLocations.length; i++) {
            gmarkers[$scope.dataLocations[i].id] = createMarker(
              $scope.dataLocations[i]
            );
          }
        },
        function () {
          // Promise rejected
        }
      );
    };

    function clearGMarkers() {
      gmarkers.forEach(function (marker) {
        marker.setMap(null);
      });

      gmarkers = [];
    }

    function getCountNeedApprovalTM() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/countneedapprovalTM';
      Main.requestApi(accessToken, urlApi, successRequestCountTM, $scope.errorRequest);
    }

    var successRequestCountTM = function (res) {
      if (res != null) {
        $rootScope.needApprovalTM = res.count;
        $scope.general.needApprovalTM = res.count;
      }
    }

    $scope.saveData = function (type) {
      $scope.approval.status = type;
      $scope.approval.listId = $scope.listId;

      $ionicLoading.show({
        template: "Submit Request..."
      });

      var accessToken = Main.getSession("token").access_token;
      var data = JSON.stringify($scope.approval);

      var urlApi = Main.getUrlApi() + "/api/user/workflow/actionapproval";
      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successSave,
        $scope.errorRequest,
        resetCheckItems
      );
    };

    var resetCheckItems = function () {
      console.log('cek jalan ga ni ?')
      $scope.checkItems = [];
    };

    var successSave = function (res) {
      $ionicLoading.hide();

      // $scope.successAlert(res.message);

      ionicSuperPopup.show({
          title: "Success",
          text: "Request has been " + $scope.confirm.approvalStatus,
          type: "success",
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ok",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            $scope.checkItems = [];
            getCountNeedApprovalTM();
            indexPaging = 0;
            getListApproval(indexPaging);
            // location.reload();
          }
        }
      );
    };

    $scope.confirmAccept = function () {
      var array = [];
      for (i in $scope.checkItems) {

        if ($scope.checkItems[i] == true) {
          array.push(i);
        }
      }

      $scope.listId = array;

      if ($scope.listId.length == 0) {
        $scope.warningAlert("Please Checked Before Approved");
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure want to Approve this request ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              $scope.confirm.approvalStatus = "Approved";
              $scope.saveData("approved");
              $ionicHistory.clearCache();
            }
          }
        );
      }
    };

    $scope.clicked = function () {
      var array = [];
      for (i in $scope.checkItems) {
        if ($scope.checkItems[i] == true) {
          array.push(i);
        }
      }
      $scope.listId = array;

      if ($scope.listId.length == 0) return true;
    };

    $scope.confirmReject = function () {
      var array = [];
      for (i in $scope.checkItems) {
        if ($scope.checkItems[i] == true) {
          array.push(i);
        }
      }

      $scope.listId = array;

      if ($scope.checkItems == null) {
        $scope.warningAlert("Please Checked Before Rejected");
      } else {
        $scope.confirm.reasonReject = "";
        var confirmPopup = $ionicPopup
          .confirm({
            title: "Reason Rejected",
            //template: '<input class="calm" type="text " ng-model="confirm.reasonReject" >',
            template: '<textarea rows="10" maxlength="255" class="calm" style="width:100%; border-color:#ddd; border: solid 2px #c9c9c9;border-radius:2px" ng-model="confirm.reasonReject" ></textarea>',
            cancelText: "Cancel",
            scope: $scope,
            okText: "Yes"
          })
          .then(function (res) {
            if (res) {
              var reason = $scope.confirm.reasonReject;
              if (reason == "")
                $scope.warningAlert("Reason reject can not empty");
              else {
                $scope.approval.reasonReject = reason;
                $scope.confirm.approvalStatus = "Rejected";
                $scope.saveData("rejected");
                $ionicHistory.clearCache();
              }
            }
          });
      }
    };

    $scope.$on("$ionicView.beforeEnter", function () {
      // var condition1 = !(Main.getSession("profile").isFeatureTester == true);

      // if (condition1)
      //   $state.go("app.myhr");
    });

    $scope.$on("$ionicView.afterEnter", function () {
      initialize();
      indexPaging = 0;
      getListApproval();
      getListApproval(indexPaging);
    });

  })


  .controller("ApprovalOvertimeCtrl", function (
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
  })




  .controller("HistoryCheckinCtrl", function (
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
  })

  .controller("DailyCheckinCtrl", function (
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.dataDaily = [];
    var size = 15;
    var indexPaging = 0;

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshDailyCheckinCtrl) {
        initMethod();
      }
      $rootScope.refreshDailyCheckinCtrl = false;
    });


    $scope.refresh = function () {
      initMethod();
    }


    function initMethod() {
      getListDaily(0);


      // var condition1 = !(Main.getSession("profile").isFeatureTester == true);

      // if (condition1)
      //   $state.go("app.myhr");
    }


    $scope.loadMoreListDaily = function () {
      indexPaging++;
      getListDaily(indexPaging);
    };


    $scope.detailPurpose = function (id) {
      $state.go('app.detailpurpose', {
        'id': id
      });
    };




    function getListDaily(page) {
      if (page == null || page === "undefined") {
        page = 0;
      }

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/empDaily?page=" + page + "&size=" + size;
      Main.requestApi(accessToken, urlApi, successDaily, $scope.errorRequest);
    }

    $scope.isOutOfOffice = "";

    var successDaily = function (res) {
      $scope.isLoadMoreShow = false;
      $scope.dataDaily = res.content;
      $scope.purposeDaily = "";

      $ionicLoading.hide();
      if (res != null) {
        for (var i = 0; i < res.content.length; i++) {
          var obj = res.content[i];
          // $scope.dataDaily.push(obj);

          if (res.last == false) $scope.isLoadMoreShow = true;
          else $scope.isLoadMoreShow = false;



          if (obj.isOutOfOffice == true) $scope.isOutOfOffice = "Out Of Office";
          else $scope.isOutOfOffice = "In The Office";

          $ionicLoading.hide();
          $scope.$broadcast("scroll.refreshComplete");
          $scope.$broadcast("scroll.infiniteScrollComplete");
        }
      }
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };


  })



  .controller("DetailPurposeCtrl", function (
    ionicSuperPopup,
    $stateParams,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
  ) {

    var idApproval = $stateParams.id;
    $scope.dataDetail = {};

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }


    $scope.$on("$ionicView.beforeEnter", function () {
      initMethod();
    });


    function initMethod() {
      getDetail($stateParams.id);
    }


    function getDetail() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/empDaily/' + idApproval;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    var successRequest = function (res) {
      $scope.dataDetail = res;


      //('data detail');
      //($scope.dataDetail);
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");

    };

    var sucessSubmitData = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshDailyCheckinCtrl = true;
      $scope.goBack("app.dailycheckin");
    };



    function sendData() {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/empDaily/" + idApproval;

      var data = JSON.stringify($scope.dataDetail);
      Main.putRequestApi(
        accessToken,
        urlApi,
        data,
        sucessSubmitData,
        $scope.errorRequest
      );
    }



    $scope.submit = function () {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure the data submitted is correct ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendData();
          }
        }
      );
    }






  })

  .controller("SubordinateCheckinCtrl", function (
    ionicDatePicker,
    $filter,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.dataSubordinates = [];
    var size = 15;
    var indexPaging = 0;
    $scope.filterDate = new Date();

    var datepicker = {
      callback: function (val) {
        //Mandatory
        $scope.dataSubordinates = [];
        $scope.filterDate = val;
        indexPaging = 0;
        getListSubordinates(0, $scope.filterDate);
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: true, //Optional
      templateType: "popup" //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.loadMoreSubordinates = function () {
      indexPaging++;
      getListSubordinates(indexPaging, $scope.filterDate);
    };

    function getListSubordinates(page, filterDate) {
      if (filterDate == null)
        filterDate = $filter("date")(new Date(), "yyyy-MM-dd");
      else
        filterDate = $filter("date")(new Date($scope.filterDate), "yyyy-MM-dd");

      if (page == null || page === "undefined") page = 0;

      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      // var urlApi = Main.getUrlApi() + '/api/user/empDaily/subordinates?page=' + page + '&size=' + size + '&date=2018-07-25';
      var urlApi =
        Main.getUrlApi() +
        "/api/user/empDaily/subordinates?page=" +
        page +
        "&size=" +
        size +
        "&date=" +
        filterDate;
      // var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?page=1' + '&size=' + size + '&module=' + 'time_management';

      Main.requestApi(
        accessToken,
        urlApi,
        successSubordinates,
        $scope.errorRequest
      );
    }

    $scope.isOutOfOffice = "";

    var successSubordinates = function (res) {
      $scope.isLoadMoreShow = false;

      $ionicLoading.hide();
      if (res != null) {
        for (var i = 0; i < res.content.length; i++) {
          var obj = res.content[i];
          $scope.dataSubordinates.push(obj);

          // if ($scope.dataDaily[i].type === "CHECKIN")
          //   $scope.dataDaily[i].type = "Check In";
          // else if($scope.dataDaily[i].type === "CHECKOUT")
          //   $scope.dataDaily[i].type = "Check Out";

          // if ($scope.dataDaily[i].isOutOfOffice === true)
          //   $scope.dataDaily[i].isOutOfOffice = "Out Of Office";
          // else if($scope.dataDaily[i].isOutOfOffice === false)
          //   $scope.dataDaily[i].isOutOfOffice = "In The Office";


          $ionicLoading.hide();
          $scope.$broadcast("scroll.refreshComplete");
          $scope.$broadcast("scroll.infiniteScrollComplete");
        }

        $scope.isLast = function () {
          if (res.last == true) {
            return true;
          }
        };
      }
    };

    function initModule() {
      getListSubordinates(0);
    }

    $scope.$on("$ionicView.beforeEnter", function (event, data) {

      initModule();
      // var condition1 = !(Main.getSession("profile").isFeatureTester == true);

      // if (condition1)
      //   $state.go("app.myhr");
    });
  })


  .controller("PrintReportPhpCtrl", function (
    $ionicLoading,
    ionicDatePicker,
    $ionicScrollDelegate,
    $filter,
    $scope,
    $state,
    Main,
    $ionicModal
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    // admin & leader validation
    $scope.profile = Main.getSession("profile");
    $scope.isLead = Main.getSession("profile").isLeader;
    $scope.isAdmin = Main.getSession("profile").isAdmin;
    $scope.isSpd = Main.getSession("profile").isSpd;

    $scope.report = {};

    $scope.isAttendanceAdmin = Main.getSession("profile").isAttendanceAdmin;


    // time and default value validation variable declaration
    var today = new Date();
    var realMonth = today.getMonth() + 1;
    $scope.currentYear = today.getFullYear();
    $scope.yearBefore = today.getFullYear() - 1;

    // search variable declaration
    $scope.isLoadMoreShow = false
    $scope.noDataShow = false;
    $scope.findStatus = false;
    $scope.npkItems = [];

    var indexPaging = 0;




    $scope.loadMoreNpk = function () {
      indexPaging++;
      $scope.npkItems = [];

      $scope.searchByNpk(indexPaging);




      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevNpk = function () {
      indexPaging--;
      $scope.npkItems = [];
      $scope.searchByNpk(indexPaging);
    };


    $scope.loadMoreLocation = function () {
      indexPaging++;
      $scope.locationItems = [];
      $scope.searchByLocation(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevLocation = function () {
      indexPaging--;
      $scope.locationItems = [];
      $scope.searchByLocation(indexPaging);
    };

    $scope.loadMoreOrganization = function () {


      indexPaging++;
      $scope.organizationItems = [];
      $scope.searchByOrganization(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevOrganization = function () {
      indexPaging--;
      $scope.organizationItems = [];
      $scope.searchByOrganization(indexPaging);
    };


    $scope.loadMorePosition = function () {

      indexPaging++;
      $scope.positionItems = [];
      $scope.searchByPosition(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevPosition = function () {
      indexPaging--;
      $scope.positionItems = [];
      $scope.searchByPosition(indexPaging);
    };


    // search controller
    $scope.getValue = function (fullName, employeeNo, employment) {
      $scope.report.fullName = fullName;
      $scope.report.employeeNo = employeeNo;
      $scope.report.employment = employment;


      // reset search npk
      $scope.findByNpk.search = '';

      $scope.closeModal(0);

      //reset
      $scope.npkItems = [];
    }



    $scope.getValueLocation = function (name, id) {
      $scope.report.location = name;
      $scope.report.locationId = id;
      $scope.closeModal(1);

      // reset search location
      $scope.findByLocation.search = '';

      //reset
      $scope.locationItems = [];
    }




    $scope.getValueOrganization = function (name, id) {
      $scope.report.organization = name;
      $scope.report.organizationId = id;


      $scope.closeModal(2);

      // reset search location
      $scope.findByOrganization.search = '';

      //reset
      $scope.organizationItems = [];
      $scope.report.positionId = '';
      $scope.report.position = '';
      $scope.report.fullName = '';
      $scope.report.employeeNo = '';
      $scope.report.employment = '';

    }

    $scope.getValuePosition = function (name, id) {
      $scope.report.position = name;
      $scope.report.positionId = id;
      $scope.closeModal(3);

      // reset search position
      $scope.findByLocation.position = '';

      //reset
      $scope.positionItems = [];

      $scope.report.fullName = '';
      $scope.report.employeeNo = '';
      $scope.report.employment = '';
    }



    $scope.findByNpk = {
      search: ""
    };
    $scope.findByLocation = {
      search: ""
    };
    $scope.findByOrganization = {
      search: ""
    };
    $scope.findByPosition = {
      search: ""
    };

    $scope.searchByNpk = function (page) {
      var size = 15;
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;

      if ($scope.report.organizationId !== undefined && $scope.report.positionId !== undefined) {
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + size + '&npk=' + $scope.findByNpk.search + '&organizationId=' + $scope.report.organizationId + '&positionId=' + $scope.report.positionId;
      } else if ($scope.report.organizationId !== undefined) {
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + size + '&npk=' + $scope.findByNpk.search + '&organizationId=' + $scope.report.organizationId;
      } else if ($scope.report.positionId !== undefined) {
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + size + '&npk=' + $scope.findByNpk.search + '&positionId=' + $scope.report.positionId;
      } else {
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + size + '&npk=' + $scope.findByNpk.search;
      }
      Main.requestApi(accessToken, urlApi, successRequestNpk, $scope.errorRequest);


    }

    var successRequestNpk = function (res) {
      var infiniteLimit = 15
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();
      $scope.npkItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.npkItems.push(obj);


        if ($scope.npkItems.length >= infiniteLimit || $scope.npkItems.length <= res.numberOfElements && $scope.npkItems.length != 1) {

          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.npkItems.length == 0 || $scope.npkItems.length == 1) {


          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.scrollTop();

      }
    }




    $scope.searchByLocation = function (page) {
      var size = 15;
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/worklocation/search?page=" + page + "&size=" + size + "&name=" + $scope.findByLocation.search;
      Main.requestApi(accessToken, urlApi, successRequestLocation, $scope.errorRequest);


    }

    var successRequestLocation = function (res) {
      var infiniteLimit = 15
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();
      $scope.locationItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.locationItems.push(obj);


        if ($scope.locationItems.length >= infiniteLimit || $scope.locationItems.length <= res.numberOfElements && $scope.locationItems.length != 1) {

          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.locationItems.length == 0 || $scope.locationItems.length == 1) {


          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.scrollTop();

      }
    }


    $scope.searchByOrganization = function (page) {
      var size = 15;
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/organization/find?page=" + page + "&size=" + size + "&name=" + $scope.findByOrganization.search;
      Main.requestApi(accessToken, urlApi, successRequestOrganization, $scope.errorRequest);


    }

    var successRequestOrganization = function (res) {
      var infiniteLimit = 15
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();
      $scope.organizationItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.organizationItems.push(obj);


        if ($scope.organizationItems.length >= infiniteLimit || $scope.organizationItems.length <= res.numberOfElements && $scope.organizationItems.length != 1) {

          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.organizationItems.length == 0 || $scope.organizationItems.length == 1) {


          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.scrollTop();

      }
    }

    $scope.searchByPosition = function (page) {
      var size = 15;
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      if ($scope.report.organizationId != null || $scope.report.organizationId !== undefined) {
        var urlApi = Main.getUrlApi() + "/api/position/find?page=" + page + "&size=" + size + "&name=" + $scope.findByPosition.search + "&organizationId=" + $scope.report.organizationId;
      } else {
        var urlApi = Main.getUrlApi() + "/api/position/find?page=" + page + "&size=" + size + "&name=" + $scope.findByPosition.search;
      }
      Main.requestApi(accessToken, urlApi, successRequestPosition, $scope.errorRequest);


    }

    var successRequestPosition = function (res) {
      var infiniteLimit = 15
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();
      $scope.positionItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.positionItems.push(obj);


        if ($scope.positionItems.length >= infiniteLimit || $scope.positionItems.length <= res.numberOfElements && $scope.positionItems.length != 1) {

          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.positionItems.length == 0 || $scope.positionItems.length == 1) {


          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.scrollTop();

      }
    }


    $scope.doRefresh = function (index) {
      $scope.noDataShow = false;
      indexPaging = 0;

      if (index == 0) {
        $scope.isLoadMoreShow = false;
        $scope.npkItems = [];
      }

      if (index == 1) {
        $scope.isLoadMoreShow = false;
        $scope.locationItems = [];
      }


      if (index == 2) {
        $scope.isLoadMoreShow = false;
        $scope.organizationItems = [];
      }

      if (index == 3) {
        $scope.isLoadMoreShow = false;
        $scope.positionItems = [];
      }
    }

    // modal
    $ionicModal.fromTemplateUrl('modal.html', {
      id: '0',
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });


    $ionicModal.fromTemplateUrl('modalLocation.html', {
      id: '0',
      scope: $scope
    }).then(function (modal) {
      $scope.modalLocation = modal;
    });



    $ionicModal.fromTemplateUrl('modalPosition.html', {
      id: '0',
      scope: $scope
    }).then(function (modal) {
      $scope.modalPosition = modal;
    });


    $ionicModal.fromTemplateUrl('modalOrganization.html', {
      id: '0',
      scope: $scope
    }).then(function (modal) {
      $scope.modalOrganization = modal;
    });

    $scope.openModal = function (index) {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      if (index == 0) $scope.modal.show();
      if (index == 1) $scope.modalLocation.show();
      if (index == 2) $scope.modalOrganization.show();
      if (index == 3) $scope.modalPosition.show();
    }

    $scope.closeModal = function (index) {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      if (index == 0) $scope.modal.hide();
      $scope.findByNpk.search = '';
      if (index == 1) $scope.modalLocation.hide();
      $scope.findByLocation.search = '';
      if (index == 2) $scope.modalOrganization.hide();
      $scope.findByOrganization.search = '';
      if (index == 3) $scope.modalPosition.hide();
      $scope.findByPosition.search = '';
    }

    // function to print
    $scope.downloadReport = function () {

      if ($scope.report.type == "") {
        $scope.warningAlert('Please select type.');
      } else if ($scope.report.year == "") {
        $scope.warningAlert('Please select year.');
      } else if ($scope.report.month == "") {
        $scope.warningAlert('Please select month.');
      } else if (($scope.report.type != null || $scope.report.year != null || $scope.report.month != null)) {
        if (Number($scope.report.month) > realMonth && $scope.report.year >= $scope.currentYear) {
          $scope.warningAlert("You can't see future report.");
        } else {
          if ($scope.report.type == "CIO") {
            // validasi wajib npk jika is leader
            if ($scope.isLead == true) {
              if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
                $scope.warningAlert("NPK can't be empty!");
              } else {
                printCheckIo(btoa(String($scope.report.year)), btoa(convertMonth($scope.report.month)), btoa(String($scope.report.employeeNo)));
              }
            } else {
              printCheckIo(btoa(String($scope.report.year)), btoa(convertMonth($scope.report.month)), btoa(String($scope.report.employeeNo)));
            }
          } else if ($scope.report.type == "OVT") {
            // edited by JUW 14 Mar 2019
            // if ($scope.isLead == true) {
            //   if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
            //     $scope.warningAlert("NPK can't be empty!");
            //   } else {
            //     printReportOvt(btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))), btoa(String($scope.report.employeeNo)));
            //   }
            // } else {
            //   printReportOvt(btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))), btoa(String($scope.report.employeeNo)));
            // }

            if ($scope.isLead == true || $scope.isAttendanceAdmin == true) {
              if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
                $scope.warningAlert("NPK can't be empty!");
                return;
              }
            }
            printReportOvt(btoa(String($scope.report.year)), btoa(convertMonth($scope.report.month)), btoa(String($scope.report.employeeNo)));

          } else if ($scope.report.type == "OVR") {
            // edited by JUW 14 Mar 2019
            // if ($scope.isLead == true) {
            //   if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
            //     $scope.warningAlert("NPK can't be empty!");
            //   } else {
            //     printReportOvtRealization(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
            //   }
            // } else {
            //   printReportOvtRealization(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
            // }
            if ($scope.isLead == true || $scope.isAttendanceAdmin == true) {
              if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
                $scope.warningAlert("NPK can't be empty!");
                return;
              }
            }
            printReportOvtRealization(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($scope.report.year)), btoa(convertMonth($scope.report.month)));

          } else if ($scope.report.type == "MGK") {
            printReportMangkir(btoa(String($scope.report.year)), btoa(convertMonth($scope.report.month)), btoa(String($scope.report.employeeNo)));
          } else if ($scope.report.type == "KTK") {
            printReportKehadiranTanpaKabar(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
          } else if ($scope.report.type == "RAT") {
            printReportAttendanceHr(btoa(String($scope.report.organizationId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.minhour)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))), btoa(String($scope.report.employeeNo)));
          } else if ($scope.report.type == "TMS") {
            printReportTms(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($scope.report.year)), btoa(convertMonth($scope.report.month)));
          } else if ($scope.report.type == "KDI") {
            printReportKehadiranDptIn(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
          } else if ($scope.report.type == "KKI") {
            // validasi wajib npk jika is leader
            if ($scope.isLead == true) {
              if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
                $scope.warningAlert("NPK can't be empty!");
              } else {
                printReportKehadiranIn(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
              }
            } else {
              printReportKehadiranIn(btoa(String($scope.report.organizationId)), btoa(String($scope.report.positionId)), btoa(String($scope.report.locationId)), btoa(String($scope.report.employeeNo)), btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))), btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
            }
          } else if ($scope.report.type == "LVE") {
            // validasi wajib npk jika is leader

            if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
              $scope.warningAlert("NPK can't be empty!");
            } else {
              printReportCutiPerKaryawan(btoa(String($scope.report.employeeNo)), btoa(String($scope.report.year)));
            }

          } else if ($scope.report.type == "PRM") {
            // validasi wajib npk jika is leader
            if ($scope.report.employeeNo == undefined || $scope.report.employeeNo == '') {
              $scope.warningAlert("NPK can't be empty!");
            } else {
              printReportIzinPerKaryawan(btoa(String($scope.report.employeeNo)), btoa(String($scope.report.year)));
            }

          } else if ($scope.report.type == "RCT") {
            // validasi wajib npk jika is leader
            if ($scope.report.employeeNo == undefined)
              $scope.report.employeeNo = "";
            printReportCuti(btoa(String($scope.report.employeeNo)), btoa(String($scope.report.year)));

          } else if ($scope.report.type == "SPD") {
            if ($scope.isAdmin == false)
              $scope.report.employment = $scope.profile.employeeTransient.assignment.employment;
            if ($scope.report.categorySpd == "" || $scope.report.categorySpd == undefined) {
              $scope.warningAlert("Category can't be empty!");
            } else {
              printReportSpd(
                btoa(String($scope.report.employment)),
                btoa(String($scope.report.categorySpd)),
                btoa(String($scope.report.typeSpd)),
                btoa(String($scope.report.statusSpd)),
                btoa(String($filter("date")($scope.report.periodStart, "yyyy-MM-dd"))),
                btoa(String($filter("date")($scope.report.periodEnd, "yyyy-MM-dd"))));
            }
          }
        }
      }
    }

    $scope.resetData = function () {

      $scope.report.positionId = '';
      $scope.report.organizationId = '';
      $scope.report.locationId = '';
      $scope.report.position = '';
      $scope.report.location = '';
      $scope.report.organization = '';
      $scope.report.employeeId = Main.getSession("profile").employeeTransient.assignment.employment;
      // $scope.report.employeeNo = Main.getSession("profile").employeeTransient.assignment.employeeNo;
      // $scope.report.fullName = Main.getSession("profile").employeeTransient.name;
    }


    $scope.resetNpk = function () {
      $scope.report.employeeNo = '';
      $scope.report.fullName = '';
      $scope.report.employment = '';
    }

    $scope.resetLocation = function () {
      $scope.report.location = '';
      $scope.report.locationId = '';
    }



    $scope.resetPosition = function () {
      $scope.report.position = '';
      $scope.report.positionId = '';
    }


    $scope.resetOrganization = function () {
      $scope.report.organization = '';
      $scope.report.organizationId = '';
    }


    var convertMonth = function (mo) {
      if (mo == "JAN")
        return "01";
      else if (mo == "FEB")
        return "02";
      else if (mo == "MAR")
        return "03";
      else if (mo == "APR")
        return "04";
      else if (mo == "MAY")
        return "05";
      else if (mo == "JUN")
        return "06";
      else if (mo == "JUL")
        return "07";
      else if (mo == "AUG")
        return "08";
      else if (mo == "SEP")
        return "09";
      else if (mo == "OCT")
        return "10";
      else if (mo == "NOV")
        return "11";
      else if (mo == "DEC")
        return "12";
    }

    var printCheckIo = function (year, month, npk) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));


      var accessToken = Main.getSession("token").access_token;
      var url = "";
      if ($scope.isAttendanceAdmin == true) {
        url =
          Main.getPrintReportCheckUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&year=" +
          year +
          "&month=" +
          month +
          "&npk=" +
          npk;
      } else if ($scope.isLead == true) {
        url =
          Main.getPrintReportCheckUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&year=" +
          year +
          "&month=" +
          month +
          "&npk=" +
          npk;
      } else {
        url =
          Main.getPrintReportCheckUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&year=" +
          year +
          "&month=" +
          month;
      }
      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };





    var printReportOvt = function (year, month, employeeNo) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";
      url =
        Main.getPrintReportOvtUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&npk=" +
        employeeNo +
        "&year=" +
        year +
        "&month=" +
        month;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };

    var printReportMangkir = function (year, month, npk) {
      // var start = $filter('date')(periodStart, "yyyy-MM-dd");
      // var end = $filter('date')(periodEnd, "yyyy-MM-dd");

      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";
      url =
        Main.getPrintReportMangkirUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&year=" +
        year +
        "&month=" +
        month +
        "&npk=" +
        npk;

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };





    var printReportKehadiranTanpaKabar = function (organization, position, location, npk, periodStart, periodEnd) {

      var start = $filter('date')(periodStart, "yyyy-MM-dd");
      var end = $filter('date')(periodEnd, "yyyy-MM-dd");


      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }


      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";

      if ($scope.isAttendanceAdmin == true) {
        url =
          Main.getPrintReportKehadiranTanpaKabar() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&organization_id=" +
          organization +
          "&position_id=" +
          position +
          "&work_location_ext_id=" +
          location +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      } else if ($scope.isLead == true) {
        url =
          Main.getPrintReportKehadiranTanpaKabar() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      } else {
        url =
          Main.getPrintReportKehadiranTanpaKabar() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      }

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };



    var printReportOvtRealization = function (organization, position, location, npk, year, month) {

      // var start = $filter('date')(periodStart, "yyyy-MM-dd");
      // var end = $filter('date')(periodEnd, "yyyy-MM-dd");



      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }


      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));


      var accessToken = Main.getSession("token").access_token;
      var url = "";

      if ($scope.isAttendanceAdmin == true) {

        url =
          Main.getPrintReportOvtRealUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&organization_id=" +
          organization +
          "&position_id=" +
          position +
          "&work_location_ext_id=" +
          location +
          "&npk=" +
          npk +
          "&year=" +
          year +
          "&month=" +
          month;
      } else if ($scope.isLead == true) {
        url =
          Main.getPrintReportOvtRealUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&year=" +
          year +
          "&month=" +
          month;
      } else {
        url = Main.getPrintReportOvtRealUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&year=" +
          year +
          "&month=" +
          month;
      }
      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };



    var printReportAttendanceHr = function (organization, location, minhour, periodStart, periodEnd, npk) {

      var start = $filter('date')(periodStart, "yyyy-MM-dd");
      var end = $filter('date')(periodEnd, "yyyy-MM-dd");


      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";


      url =
        Main.getPrintReportAttendanceUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&npk=" +
        npk +
        "&organization_id=" +
        organization +
        "&work_location_ext_id=" +
        location +
        "&minhour=" +
        minhour +
        "&periodStart=" +
        start +
        "&periodEnd=" +
        end;

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };



    var printReportTms = function (organization, position, location, npk, year, month) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";

      if ($scope.isAttendanceAdmin == true) {

        url =
          Main.getPrintReportTmsUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&organization_id=" +
          organization +
          "&position_id=" +
          position +
          "&work_location_ext_id=" +
          location +
          "&npk=" +
          npk +
          "&year=" +
          year +
          "&month=" + month;
      } else if ($scope.isLead == true) {
        url =
          Main.getPrintReportTmsUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&year=" +
          year +
          "&month=" +
          month;
      } else {
        url = Main.getPrintReportTmsUrl() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&year=" +
          year +
          "&month=" +
          month;
      }

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };



    var printReportKehadiranDptIn = function (organization, position, location, npk, periodStart, periodEnd) {

      var start = $filter('date')(periodStart, "yyyy-MM-dd");
      var end = $filter('date')(periodEnd, "yyyy-MM-dd");

      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";

      if ($scope.isAttendanceAdmin == true) {

        url =
          Main.getPrintReportKehadiranDptIn() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&organization_id=" +
          organization +
          "&position_id=" +
          position +
          "&work_location_ext_id=" +
          location +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      } else if ($scope.isLead == true) {
        url =
          Main.getPrintReportKehadiranDptIn() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      } else {
        url = Main.getPrintReportKehadiranDptIn() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      }
      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };


    var printReportKehadiranIn = function (organization, position, location, npk, periodStart, periodEnd) {

      var start = $filter('date')(periodStart, "yyyy-MM-dd");
      var end = $filter('date')(periodEnd, "yyyy-MM-dd");

      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }


      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";

      if ($scope.isAttendanceAdmin == true) {

        url =
          Main.getPrintReportKehadiranIn() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&organization_id=" +
          organization +
          "&position_id=" +
          position +
          "&work_location_ext_id=" +
          location +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      } else if ($scope.isLead == true) {
        url =
          Main.getPrintReportKehadiranIn() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      } else {
        url = Main.getPrintReportKehadiranIn() +
          "?employment_id=" +
          employment_id +
          "&ses_id=" +
          accessToken +
          "&npk=" +
          npk +
          "&periodStart=" +
          start +
          "&periodEnd=" +
          end;
      }

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };


    var printReportCutiPerKaryawan = function (npk, year) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";


      url =
        Main.getPrintCutiPerkaryawanUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&npk=" +
        npk +
        "&year=" +
        year;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };



    var printReportIzinPerKaryawan = function (npk, year) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";


      url =
        Main.getPrintReportIzinKaryawanUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&npk=" +
        npk +
        "&year=" +
        year;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };


    var printReportSpd = function (employment_id, categorySpd, typeSpd, statusSpd, periodStart, periodEnd) {
      var profile = Main.getSession("profile");

      var start = $filter('date')(periodStart, "yyyy-MM-dd");
      var end = $filter('date')(periodEnd, "yyyy-MM-dd");

      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var accessToken = Main.getSession("token").access_token;
      var url = "";


      url =
        Main.getPrintSpd() +
        "?ses_id=" +
        accessToken +
        "&employment_id=" +
        employment_id +
        "&categorySpd=" +
        categorySpd +
        "&typeSpd=" +
        typeSpd +
        "&statusSpd=" +
        statusSpd +
        "&periodStart=" +
        start +
        "&periodEnd=" +
        end;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };


    var printReportCuti = function (npk, year) {
      var profile = Main.getSession("profile");
      if (profile.employeeTransient.assignment.employment == null) {
        alert("Employment is null");
        return false;
      }

      var employment_id = btoa(String(profile.employeeTransient.assignment.employment));

      var accessToken = Main.getSession("token").access_token;
      var url = "";


      url =
        Main.getPrintCutiUrl() +
        "?employment_id=" +
        employment_id +
        "&ses_id=" +
        accessToken +
        "&npk=" +
        npk +
        "&year=" +
        year;


      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };





    var getMonthForInit = function (mo) {
      if (mo == 1)
        return "JAN";
      else if (mo == 2)
        return "FEB";
      else if (mo == 3)
        return "MAR";
      else if (mo == 4)
        return "APR";
      else if (mo == 5)
        return "MAY";
      else if (mo == 6)
        return "JUN";
      else if (mo == 7)
        return "JUL";
      else if (mo == 8)
        return "AUG";
      else if (mo == 9)
        return "SEP";
      else if (mo == 10)
        return "OCT";
      else if (mo == 11)
        return "NOV";
      else if (mo == 12)
        return "DEC";
    }



    var datepicker1 = {
      callback: function (val) { //Mandatory
        $scope.report.periodStart = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };




    var datepicker2 = {
      callback: function (val) { //Mandatory
        $scope.report.periodEnd = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker2 = function () {
      ionicDatePicker.openDatePicker(datepicker2);
    };


    var initMethod = function () {
      $scope.report = {
        "type": "",
        "positionId": '',
        "locationId": '',
        "organizationId": '',
        "year": String($scope.currentYear),
        "month": getMonthForInit(realMonth),
        "periodStart": new Date(),
        "employeeId": Main.getSession("profile").employeeTransient.assignment.employment,
        "periodEnd": new Date(),
        "minhour": 0
      };

    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

  })


  .controller("ListReassignPicCtrl", function (
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requests = [];

    $scope.$on("$ionicView.beforeEnter", function () {
      initMethod();
    });


    $scope.gotoDetail = function (location) {
      $state.go("app.reassigndetail", {
        location: location
      });

    };

    $scope.refresh = function () {
      getMoreWorkLocation();
    };

    function initMethod() {
      $scope.requests = [];
      getMoreWorkLocation();
    }

    function getMoreWorkLocation(page) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/approval/worklocation";
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }




    var successRequest = function (res) {
      $scope.requests = res;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };



  })


  .controller("ReassigndetailCtrl", function (
    $ionicLoading,
    $ionicModal,
    $scope,
    $state,
    $stateParams,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }


    $scope.locationName = $stateParams.location;



    var size = Main.getDataDisplaySize();
    var page = 0;
    $scope.requests = [];
    $scope.reassign = {};
    $scope.npkItems = [];
    $scope.isLast = false;

    $scope.$on("$ionicView.beforeEnter", function () {
      initMethod();
    });



    $scope.searchByNpk = function (npk) {

      // reset
      $scope.npkItems = [];

      if (npk == null || npk == '')
        $ionicPopup.show({
          title: "Text field can't be empty!",
          buttons: [{
            text: 'OK'
          }]
        });
      else {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/employee/find?page=' + page + '&size=' + size + '&npk=' + npk;
        Main.requestApi(accessToken, urlApi, successRequestNpk, $scope.errorRequest);

      }

      $ionicLoading.hide();
    }




    var successRequestNpk = function (res) {
      $scope.totalRecord = res.totalElements;
      $ionicLoading.hide();

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.npkItems.push(obj);

        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }
    }

    $ionicModal
      .fromTemplateUrl("modalnpk.html", {
        id: "1",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modalnpk = modal;
      });



    $ionicModal
      .fromTemplateUrl("modalsearch.html", {
        id: "1",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modalsearch = modal;
      });


    $scope.reassignPic = function (id, npk) {
      $scope.modalnpk.show();
      $scope.reassign.id = id;
      $scope.reassign.employeeNo = npk;
    };


    $scope.openModalSearch = function () {
      $scope.modalsearch.show();
    }

    $scope.setNpk = function (npk) {
      $scope.modalsearch.hide();
      $scope.reassign.employeeNo = npk;
      $scope.npkItems = [];

    }



    $scope.closeModal = function (index) {
      $scope.modalnpk.hide();
    };

    $scope.closeModalSearch = function (index) {
      $scope.modalsearch.hide();
    };



    function verificationForm(reassign) {
      if (reassign.employeeNo == undefined || reassign.employeeNo == '') {
        messageValidation = "Npk No can't be empty";
        return false;
      }

      return true;
    }


    $scope.reassignSubmit = function () {
      if (verificationForm($scope.reassign)) {
        $ionicLoading.show({
          template: 'Submit Request...'
        });

        var accessToken = Main.getSession("token").access_token;
        var data = [$scope.reassign];

        var urlApi = Main.getUrlApi() + '/api/approval/worklocation';
        Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
      } else {
        $scope.warningAlert(messageValidation);
      }
    }



    var successSave = function (res) {
      $scope.modalnpk.hide();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack('app.reassignpic');

    }


    function initMethod() {
      $scope.requests = [];
      page = 0;
      getDataLocation();

    }

    function getDataLocation() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/approval/worklocation/" + $scope.locationName;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);

    }



    var successRequest = function (res) {
      $scope.requests = res;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };




  })



  .controller('ApplicationSelectionCtrl',
    function (
      $ionicLoading,
      $scope,
      $state,
      Main
    ) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      $scope.profile = {};
      $scope.dataApp = {};

      function getAppSelection() {
        var id = 2;
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/applicant/' + id;
        Main.requestApi(accessToken, urlApi, sucessData, $scope.errorRequest);


      }

      var sucessData = function (res) {
        $scope.dataApp = res;
        $ionicLoading.hide();
        $scope.$broadcast("scroll.refreshComplete");
        $scope.$broadcast("scroll.infiniteScrollComplete");
      };



      function initModule() {
        $scope.profile = Main.getSession("profile");
        $scope.profile.fullName = $scope.profile.employeeTransient.firstName;
        if ($scope.profile.employeeTransient.middleName != null)
          $scope.profile.fullName += " " + $scope.profile.employeeTransient.middleName;

        if ($scope.profile.employeeTransient.lastName != null)
          $scope.profile.fullName += " " + $scope.profile.employeeTransient.lastName;

        getAppSelection();

      }


      $scope.$on('$ionicView.beforeEnter', function () {
        initModule();
      });
    })



  .controller('ProfileCandidateCtrl', function (
    $ionicLoading,
    $scope,
    $state,
    Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    localStorage.setItem("value", "profile");

    $scope.requests = [];
    $scope.chooseTab = {};
    var profile = Main.getSession("profile");
    $scope.isHr = profile.isHr;

    $scope.dataProfile = {};
    $scope.dataEducation = {};
    $scope.dataWork = {};
    $scope.dataOrganization = {};
    $scope.module = {};

    $scope.chooseTab = function (tab) {
      $scope.module.type = tab;
      if (tab === 'profile') {
        localStorage.setItem("value", "profile");
        getProfile(2); // harcode id
      } else if (tab === 'education') {
        localStorage.setItem("value", "education");
        getEducation(2);
      } else if (tab === 'organization') {
        localStorage.setItem("value", "organization");
        getOrganization(2);
      } else if (tab === 'working') {
        $scope.attendanceRequests = [];
        localStorage.setItem("value", "working");
        getWorkExperience(2);
      }

    }


    $scope.refresh = function () {
      $scope.chooseTab($scope.module.type);
    }


    function getProfile(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id;
      Main.requestApi(accessToken, urlApi, successProfile, $scope.errorRequest);
    }


    function getEducation(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id + '/education';
      Main.requestApi(accessToken, urlApi, successEducation, $scope.errorRequest);
    }



    function getWorkExperience(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id + '/workexperience';
      Main.requestApi(accessToken, urlApi, successWorkexperience, $scope.errorRequest);
    }



    function getOrganization(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id + '/organization';
      Main.requestApi(accessToken, urlApi, successOrganization, $scope.errorRequest);
    }




    var successProfile = function (res) {
      if (res != null) {
        $scope.dataProfile = res;
        $ionicLoading.hide();
      }

    }


    var successEducation = function (res) {
      if (res != null) {
        $scope.dataEducation = res;
        $ionicLoading.hide();
      }
    }


    var successWorkexperience = function (res) {
      if (res != null) {
        $scope.dataWork = res;
        $ionicLoading.hide();
      }

    }


    var successOrganization = function (res) {
      if (res != null) {
        $scope.dataOrganization = res;
        $ionicLoading.hide();
      }
    }





    function initMethod() {
      getProfile(2);
      getOrganization(2);
      getWorkExperience(2);
      getEducation(2);

      $scope.chooseTab(localStorage.getItem("value"));
    }

    initMethod();

  })


  .controller("ChoiceCheckSaldoDPACtrl", function (
    $ionicLoading,
    $filter,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
    var messageValidation = "";
    $scope.totalSaldo;
    $scope.checksaldoType = Main.getSession("profile").companySettings.payslipType;
    $scope.hireDate = Main.getSession("profile").employeeTransient.hireDate;
    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refYear = Main.getDataReference(
      arrCompanyRef,
      "payslip",
      null,
      "selectYear"
    );
    var year = "0";
    var month = "0";

    var successRequest = function (res) {
      $ionicLoading.hide();
      if (res.length == 0 || res.items['length'] == '0') {
        $scope.warningAlert("Karyawan belum terdaftar DPA");
        $scope.toogling = false;
      } else {
        $scope.showResult = function () {
          if (!$scope.Result)
            $scope.Result = true;
          else
            $scope.Result = false;
        }
        if (res.items['0']['TotalPK'] == "" ||
          res.items['0']['TotalPK'] == 'undefined' ||
          res.items['0']['TotalPK'] == "" ||
          res.items['0']['TotalPK'] == 'undefined') {
          $scope.totalSaldo = "-";
          $scope.totalPK = "-";
          $scope.totalPS = "-";
        } else {
          $scope.totalSaldo = $filter('number')(parseInt(res.items['0']['TotalPK']) + parseInt(res.items['0']['TotalPS']));
          $scope.totalPK = $filter('number')(parseInt(res.items['0']['TotalPK']));
          $scope.totalPS = $filter('number')(parseInt(res.items['0']['TotalPS']));
          $scope.period = res.items['0']['Periode'].substring(0, 4) + "-" + res.items['0']['Periode'].substring(4, 6);
        }
        $scope.Result = true;

      }
    };

    $scope.submitForm = function () {
      $ionicLoading.show({
        template: "Loading..."
      });
      var urlAPI = Main.getDPAUrlApi() + '/API/data/getSaldoByNPK'; // Production
      //var urlAPI = Main.getDPAUrlApi() + '/API_dev/data/getSaldoByNPK'; //Development
      //"https://www.dapenastra.com/API_dev/data/getSaldoByNPK"
      var keyMitra;
      var kodePerusahaan;
      var NPK = Main.getSession("profile").employeeTransient.assignment['employeeNo'];
      //var data = 'keyMitra='+keyMitra+'&NPK='+NPK+'&KodePerusahaan='+kodePerusahaan;
      var data = 'keyMitra=a40f3598eadddf46a36ab59621bee89b&NPK=' + NPK; // Production
      //var data = 'keyMitra=84eabfedca14f5c3f70ad822a0222fd1&NPK=' + NPK ; //Development
      var accessToken = Main.getSession("token").access_token;

      var result = Main.postRequestDPAApi(
        accessToken,
        urlAPI,
        data,
        successRequest,
        $scope.errorRequest
      );

      $scope.toogling = true;

    };


    function initModule() {
      $scope.toogling = false;
      $scope.Result = false;
      //console.log(Main.getSession("profile"));
      if ($scope.hireDate > 704678399000) {
        $scope.status = "DPA2";
      } else {
        $scope.status = "DPA1";
      }
      //this.navCtrl.setRoot(this.navCtrl.getActive().component);

    }

    initModule();

    /*$scope.$on('$ionicView.beforeEnter', function(event, data) {
      initModule();
    });*/
  })

  .controller("MenuDPACtrl", function (
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.openBrowser = function () {
      if (Main.getPhpHost() == "https://talents-api.acc.co.id") {
        window.open(
          "https://hdff.fs.ap1.oraclecloud.com/homePage/faces/FuseWelcome",
          "_blank",
          "location=yes"
        );
      } else {
        window.open(
          "https://hdff-test.fs.ap1.oraclecloud.com/homePage/faces/FuseWelcome",
          "_blank",
          "location=yes"
        );
      }
    };

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "selfservice"
    );

    function initMethod() {
      $scope.showMenu = false;
      var status = Main.getSession("profile").employeeTransient.assignment.employeeStatus;
      if (status == "Karyawan Tetap") {
        $scope.showMenu = true;
      }
    }

    initMethod();
  })

  .controller("MenuDPAListCtrl", function (
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requests = [];
    $scope.module = {};
    $scope.$on("$ionicView.beforeEnter", function (event, data) {
      initMethod();
    });


    $scope.refresh = function () {
      initMethod();
    };

    var successRequest = function (res) {
      $scope.requests = [];
      for (var i = 0; i < res.data.length; i++) {
        var obj = res.data[i];
        obj.idx = i;

        if (obj.task == "SUBMITDPA") {
          $scope.requests.push(obj);
          //console.log(obj);
        }
      }

      $scope.processingStatus = obj.processingStatus;

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    };

    function initMethod() {
      getListBenefit();
    }

    function getListBenefit() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/workflow/myrequest?module=personalia";
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }
  })

  .controller("DaftarDPACtrl", function (
    ionicSuperPopup,
    $ionicLoading,
    $scope,
    $state,
    $cordovaInAppBrowser,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.requestHeader = {};
    $scope.detail = {};
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.requestHeader.images = [];
    $scope.requestHeader.imagesData = [];

    var messageValidation = "";

    // $scope.toogling = true;

    $scope.removeChoice = function () {
      $("#inputImage").val("");
      var lastItem = $scope.requestHeader.imagesData.length - 1;
      $scope.requestHeader.imagesData.splice(lastItem);
      $scope.requestHeader.images.splice(lastItem);
    };

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token;
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500) $scope.errorAlert(err.message);
        else $scope.errorAlert("Please Check your connection");
      }
    };

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    };

    var errRefreshToken = function (err, status) {};



    function sendData() {
      var idRef = Main.getSession("profile").employeeTransient.id;
      var employeePayrollId = $scope.detail.employeePayrollId;
      if ($scope.requestHeader.bankName === undefined) {
        $scope.requestHeader.bankName = $scope.detail.bankName;
      }

      var attachment = [];

      if ($scope.requestHeader.imagesData.length > 0) {
        for (var i = $scope.requestHeader.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            image: null
          };
          if ($scope.appMode == "mobile") {
            objAttachment = {
              image: $scope.requestHeader.imagesData[i].image
            };
          } else {
            if (
              $scope.requestHeader.imagesData[i].compressed.dataURL != undefined
            ) {
              var webImageAttachment = $scope.requestHeader.imagesData[
                i
              ].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");

              objAttachment = {
                image: webImageAttachment
              };
            }
          }
          attachment.push(objAttachment);
        }
      }


      var dataStr = {
        task: "SUBMITDPA",
        data: "test",
        attachments: attachment
      };

      $ionicLoading.show({
        template: "Submit Request..."
      });

      var accessToken = Main.getSession("token").access_token;

      var urlApi = Main.getUrlApi() + "/api/user/workflow/dataapproval";
      var data = JSON.stringify(dataStr);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successRequest,
        $scope.errorRequest
      );
    }

    $scope.upload = function () {
      if ($scope.requestHeader.imagesData.length == 1) {
        if ($scope.requestHeader.imagesData['0'].file.size > "300000") {
          $scope.warningAlert("File size maks. 300KB");
          $scope.toogling = false;
          //console.log($scope.warningAlert);
        } else {
          ionicSuperPopup.show({
              title: "Are you sure?",
              text: "Are you sure the data submitted is correct ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              closeOnConfirm: true
            },
            function (isConfirm) {
              if (isConfirm) {
                sendData();
              }
            }
          );
        }
      } else {
        $scope.warningAlert("You must add 1 attachment.");
      }
    }


    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.selfservice");
    };

    var options = {
      location: 'yes',
      clearcache: 'yes',
      toolbar: 'no'
    };

    $scope.downloadForm = function () {
      $cordovaInAppBrowser.open('https://drive.google.com/file/d/1oimbHznaUhE3oAqNv4p7D8yTLyUss2F1/view?usp=sharing', '_system', options);
    };

    function initModule() {

    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });
  })

  .controller("DetailRequestCtrl", function (
    ionicSuperPopup,
    $stateParams,
    $ionicLoading,
    $scope,
    $state,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }
    var id = $stateParams.id;

    var successRequest = function (res) {
      $ionicLoading.hide();
      //console.log(res.data);
      angular.forEach(res.data, function (value) {
        if (value.id == id)
          $scope.detail = value;
      });

      $scope.description = $scope.detail.description;
      $scope.dateRequest = $scope.detail.createdDate;
      $scope.status = $scope.detail.processingStatus;
      $scope.reason = $scope.detail.reasonReject;
      getDetailDummy($scope.detail.id);
      //console.log($scope.detail);
    }


    var successApprove = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.menudpalist");
    }

    $scope.cancel = function () {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Cancel this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendCancel('cancelled', $scope.detail.id, "");
          }
        });
    }

    var sendCancel = function (action, id, reason) {
      var data = {
        "id": $scope.detail.id,
        "status": action,
        "reasonReject": reason
      };
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/actionapproval';
      var data = JSON.stringify(data);
      Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);
    }

    function getDetailRequest() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/user/workflow/myrequest?module=personalia";
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function getDetailDummy(idApprove) {
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + idApprove;
      Main.requestApi(accessToken, urlApi, successDummy, $scope.errorRequest);
    }

    var successDummy = function (res) {
      // console.log(res);
      $scope.imagesAttach = res.attachments;
    }

    function initModule() {
      getDetailRequest();
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })

  // CONTROLLER MENU DAILY TEST & DAILY TEST REPORT TECAT

  .controller("DetailPoinCtrl", function (
    $ionicLoading,
    $compile,
    $filter,
    $timeout,
    $ionicHistory,
    $rootScope,
    $scope,
    $state,
    $http,
    AuthenticationService,
    Main,
    $stateParams
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "selfservice"
    );

    function getTotalPoints() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-history-poin?npk=" + npk;
      Main.getApi(
        urlApi,
        successTotal,
        $scope.errorRequestGetTotalPoints
      );

    }

    $scope.errorRequestGetTotalPoints = function (err, status) {
      if (status == 401) {
        $scope.goTo('login');
      } else if (status == 400) {
        $scope.errorAlert(err.message);
      } else if (status == 500) {
        $scope.errorAlert("Could not process this operation. Please contact Admin.");
      } else {
        $scope.errorAlert("Please Check your connection.");
      }
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");

      $scope.totalPoints = '-';
    }

    var successTotal = function (res) {
      $scope.total = res;
      var totalPoints = 0;

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");

      for (var i = 0; i < res.length; i++) {
        totalPoints += parseInt($scope.total[i].sum_score);
      }

      $scope.totalPoints = totalPoints;
    }

    /*

    function getFunctionJob(){
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-function-job/";
      Main.getApi(
        urlApi,
        successFunctionJob,
        $scope.errorRequestFunctionJob
      );
    }

    $scope.errorRequestFunctionJob = function(err, status) {
      if(status == 401) {
      $scope.goTo('login');
      }else if(status == 400){
            $scope.errorAlert(err.message);
      }else if(status == 500) {
            $scope.errorAlert("Could not process this operation. Please contact Admin.");
      }else  {
            $scope.errorAlert("Please Check your connection.");
      }
      $ionicLoading.hide();
    }

    // var api get function name + job

    var successFunctionJob1 = function(res){
      var arr = [];
      var arrr = [];
      //console.log(res);
      $scope.functionJob = res;
      //console.log($scope.functionJob)
      for(var i=0; i< res.length; i++){
          console.log($scope.functionJob[i].function_name);
          arr.push({FUNCTION: $scope.functionJob[i].function_name});
        for(var j=0; j< $scope.functionJob[i].mst_jobs.length; j++){
          //console.log($scope.functionJob[i].function_name, $scope.functionJob[i].mst_jobs[j].job_name);
          arrr.push({FUNCTION: $scope.functionJob[i].function_name, JOB:$scope.functionJob[i].mst_jobs[j].job_name, JOB_ID: $scope.functionJob[i].mst_jobs[j].job_id });
        }
      }
      $scope.functionName = arr;
      $scope.jobName = arrr;
      $ionicLoading.hide();
    }

    // var api get function name
    var successFunctionJob = function(res){
      $scope.nameFunction = res;

      var arr = [];

      console.log($scope.nameFunction);

      for (var i = 0; i < res.length; i++) {
        arr.push({ FUNCTION: $scope.nameFunction[i].function_name, JOBID: $scope.nameFunction[i].function_id});
      }
      $scope.functionName = arr;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    }

    $scope.functionJob = {};

    */

    $scope.refresh = function () {
      initMethod();
    };

    function initMethod() {
      getTotalPoints();
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      $scope.isLead = Main.getSession("profile").isLeader;
      if ($scope.isLead == true) {
        $scope.goTo("app.myhr");
      }
    });

    initMethod();

  })

  .controller("PendingPoinCtrl", function (
    $ionicLoading,
    $ionicPopover,
    $compile,
    $filter,
    $timeout,
    $ionicHistory,
    $rootScope,
    $state,
    AuthenticationService,
    $scope,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "selfservice"
    );

    function initMethod() {

    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      $scope.isLead = Main.getSession("profile").isLeader;
      if ($scope.isLead == true) {
        $scope.goTo("app.myhr");
      }
    });

    initMethod();

  })

  .controller("HistoryPoinCtrl", function (
    $ionicLoading,
    $compile,
    $filter,
    $timeout,
    $ionicHistory,
    $rootScope,
    $scope,
    $state,
    $http,
    AuthenticationService,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      $scope.isLead = Main.getSession("profile").isLeader;
      if ($scope.isLead == true) {
        $scope.goTo("app.myhr");
      }
    });

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "selfservice"
    );


    function getSummary() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-history-poin?npk=" + npk;
      Main.getApi(
        urlApi,
        successSummary,
        $scope.errorRequest
      );

    }
    var successSummary = function (res) {

      var arr = [];
      $scope.infiniteLimit = 5;

      $scope.summary = res;

      for (var i = 0; i < res.length; i++) {
        if ($scope.summary[i].question_type_id == 2) {
          if ($scope.summary[i].type == 1) {
            arr.push({
              penjelasan: "Correct Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: "General Knowledge",
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          } else if ($scope.summary[i].type == 2) {
            arr.push({
              penjelasan: "Correct & On Time Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: "General Knowledge",
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          } else if ($scope.summary[i].type == 3) {
            arr.push({
              penjelasan: "Incorrect Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: "General Knowledge",
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          } else if ($scope.summary[i].type == 4) {
            arr.push({
              penjelasan: "Incorrect & On Time Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: "General Knowledge",
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          }
        } else {

          if ($scope.summary[i].type == 1) {
            arr.push({
              penjelasan: "Correct Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: $scope.summary[i].description,
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          } else if ($scope.summary[i].type == 2) {
            arr.push({
              penjelasan: "Correct & On Time Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: $scope.summary[i].description,
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          } else if ($scope.summary[i].type == 3) {
            arr.push({
              penjelasan: "Incorrect Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: $scope.summary[i].description,
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          } else if ($scope.summary[i].type == 4) {
            arr.push({
              penjelasan: "Incorrect & On Time Answered Test",
              sum_score: $scope.summary[i].sum_score,
              competency_name: $scope.summary[i].description,
              total_benar: $scope.summary[i].total_benar,
              time_finish: new Date($scope.summary[i].time_finish)
            });
          }
        }
      }

      $scope.dataHistory = arr;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");

      $scope.moreDataCanBeLoaded = function () {
        return $scope.dataHistory.length > $scope.infiniteLimit;
      }
    }

    $scope.refresh = function () {
      initMethod();
    };

    $scope.loadMore = function () {
      $scope.infiniteLimit += 5;

      $scope.$broadcast("scroll.infiniteScrollComplete");
    }, 1000;




    function initMethod() {
      getSummary();
    }

    initMethod();

  })

  .controller("SummaryProfileCtrl", function (
    $ionicLoading,
    $compile,
    $filter,
    $timeout,
    $ionicHistory,
    $rootScope,
    $scope,
    $state,
    $http,
    AuthenticationService,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.refresh = function () {
      initMethod();
    };

    function getSummary() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var npk = $scope.profile.employeeTransient.assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-individual-report?npk=" + npk;
      Main.getApi(
        urlApi,
        successSummary,
        $scope.errorRequestSummary
      );
    }

    $scope.errorRequestSummary = function (err, status) {
      if (status == 401) {
        $scope.goTo('login');
      } else if (status == 400) {
        $scope.errorAlert(err.message);
      } else if (status == 500) {
        $scope.errorAlert("Could not process this operation. Please contact Admin.");
      } else {
        $scope.errorAlert("Please Check your connection.");
      }
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");

      $scope.totalBenar = '-';
      $scope.totalSoal = '-';
      $scope.totalSalah = '-';
    }

    var successSummary = function (res) {
      $scope.summary = res;

      var arr = [];
      var totalBenar = 0;
      var totalSoal = 0;
      var totalSalah = 0;

      for (var i = 0; i < res.length; i++) {
        if ($scope.summary[i].question_type_id == 2) {
          arr.push({
            competencyName: "General",
            correct: $scope.summary[i].total_benar,
            totalQuestion: $scope.summary[i].total_soal
          });
        } else {
          arr.push({
            competencyName: $scope.summary[i].description,
            correct: $scope.summary[i].total_benar,
            totalQuestion: $scope.summary[i].total_soal
          });
        }
        totalBenar += parseInt($scope.summary[i].total_benar);
        totalSoal += parseInt($scope.summary[i].total_soal);
        totalSalah = (totalSoal - totalBenar);
      }
      $scope.totalBenar = totalBenar;
      $scope.totalSoal = totalSoal;
      $scope.totalSalah = totalSalah;

      $scope.dataIndividualReport = arr;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      $scope.isLead = Main.getSession("profile").isLeader;
      if ($scope.isLead == true) {
        $scope.goTo("app.myhr");
      }
    });

    function initMethod() {
      getSummary();
    }

    initMethod();

  })

  .controller("DailyTestTeamReportCtrl", function (
    $ionicLoading,
    $compile,
    $filter,
    $timeout,
    $ionicHistory,
    $rootScope,
    $scope,
    $state,
    AuthenticationService,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var areaCabang = $scope.profile.employeeTransient.assignment.organizationName;
    var departName = $scope.profile.employeeTransient.assignment.jobTitleName;
    var areaCabang2 = $scope.profile.employeeTransient.assignment.workLocationName;

    $scope.dataPribadi = {
      idDepartemen: 1,
      departemenName: departName,
      area: areaCabang2
    }

    function getTeam() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/myprofile/team';
      Main.requestApi(accessToken, urlApi, successProfile, $scope.errorRequest);
    }

    var successProfile = function (res) {
      $scope.team = res;
      var individual = [];
      var npkIn;

      var npk0 = $scope.team[0].assignment.employeeNo;
      var urlApi = Main.getTecatUrlApi() + "/api/get-team-report?arrayNPK[]=" + npk0;

      for (var i = 1; i < res.length; i++) {
        var temp = $scope.team[i].assignment.employeeNo;
        urlApi = urlApi + "&arrayNPK[]=" + temp;
      }

      Main.getApi(
        urlApi,
        successSummaryTeam,
        $scope.errorRequest
      );

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");

    }

    var successSummaryTeam = function (res) {
      $scope.summary = res;

      var arr = [];
      var totalBenar = 0;
      var totalSoal = 0;
      var totalSalah = 0;

      for (var i = 0; i < res.length; i++) {
        if ($scope.summary[i].question_type_id == 2) {
          arr.push({
            competencyName: "General",
            correct: $scope.summary[i].total_benar,
            totalQuestion: $scope.summary[i].total_soal
          });
        } else {
          arr.push({
            competencyName: $scope.summary[i].description,
            correct: $scope.summary[i].total_benar,
            totalQuestion: $scope.summary[i].total_soal
          });
        }
        totalBenar += parseInt($scope.summary[i].total_benar);
        totalSoal += parseInt($scope.summary[i].total_soal);
        totalSalah = (totalSoal - totalBenar);
      }

      $scope.totalBenar = totalBenar;
      $scope.totalSoal = totalSoal;
      $scope.totalSalah = totalSalah;

      $scope.dataSummaryTeamReport = arr;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");

    }

    $scope.refresh = function () {
      initMethod();
    };

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      $scope.isLead = Main.getSession("profile").isLeader;
      if ($scope.isLead == false) {
        $scope.goTo("app.myhr");
      }
    });

    function initMethod() {
      getTeam();
    }

    initMethod();
  })

  .controller("DailyTestIndividualReportCtrl", function (
    $ionicLoading,
    $compile,
    $filter,
    $timeout,
    $ionicHistory,
    $rootScope,
    $scope,
    $state,
    $stateParams,
    AuthenticationService,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
      $scope.isLead = Main.getSession("profile").isLeader;
      if ($scope.isLead == false) {
        $scope.goTo("app.myhr");
      }
    });

    $scope.refresh = function () {
      initMethod();
    };

    function getTeam() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/myprofile/team';
      Main.requestApi(accessToken, urlApi, successProfile, $scope.errorRequest);
    }

    var successProfile = function (res) {
      var arr = [];
      $scope.team = res;

      for (var i = 0; i < res.length; i++) {
        arr.push({
          NPK: $scope.team[i].assignment.employeeNo,
          fullName: $scope.team[i].assignment.fullName
        });
      }
      $scope.employeeName = arr;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    }

    $scope.pegawai = {};
    $scope.submitForm = function () {

      if (verificationForm($scope.pegawai)) {
        var NPK = $scope.pegawai.NPK;
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });

        var urlApi = Main.getTecatUrlApi() + "/api/get-individual-report?npk=" + NPK;
        Main.getApi(
          urlApi,
          successSummary,
          $scope.errorRequest
        );
      } else {
        $scope.warningAlert(messageValidation);
      }
    };

    function verificationForm(usertalent) {
      if ($scope.pegawai.NPK == undefined || $scope.pegawai.NPK == '') {
        messageValidation = "Pick one employee name.";
        return false;
      }
      return true;
    }

    var successSummary = function (res) {
      $scope.summary = res;

      var arr = [];
      var totalBenar = 0;
      var totalSoal = 0;
      var totalSalah = 0;

      for (var i = 0; i < res.length; i++) {
        if ($scope.summary[i].question_type_id == 2) {
          arr.push({
            competencyName: "General",
            correct: $scope.summary[i].total_benar,
            totalQuestion: $scope.summary[i].total_soal
          });
        } else {
          arr.push({
            competencyName: $scope.summary[i].description,
            correct: $scope.summary[i].total_benar,
            totalQuestion: $scope.summary[i].total_soal
          });
        }
        totalBenar += parseInt($scope.summary[i].total_benar);
        totalSoal += parseInt($scope.summary[i].total_soal);
        totalSalah = (totalSoal - totalBenar);
      }

      $scope.totalBenar = totalBenar;
      $scope.totalSoal = totalSoal;
      $scope.totalSalah = totalSalah;

      $scope.dataIndividualReport = arr;
      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
    }

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refMenu = Main.getDataReference(
      arrCompanyRef,
      "menu",
      "right",
      "selfservice"
    );

    function initMethod() {
      getTeam();
    }

    initMethod();
  })

// END OF CONTROLLER MENU DAILY TEST & DAILY TEST REPORT TECAT
