angular.module('spd.controllers', [])

  .controller("SpdHomeCtrl", function (
    $state,
    Main
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }


  })

  .controller('SpdListCtrl',
    function (
      $scope,
      $ionicLoading,
      $ionicModal,
      $ionicScrollDelegate,
      $filter,
      $state,
      $ionicHistory,
      $location,
      $ionicPopup,
      ionicSuperPopup,
      ionicDatePicker,
      globalConstant,
      Main
    ) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      $scope.globalConstant = globalConstant;
      $scope.appMode = Main.getAppMode();
      var size = Main.getDataDisplaySize();
      var indexPaging = 0;

      $scope.formMpp = {};

      var datePicker = {
        callback: function (val) {
          $scope.formMpp.effectiveDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      var successRequest = function (res) {
        $ionicLoading.hide();
        $scope.successAlert(res.message);
        $scope.goBack('app.selfservice');
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
      };

      var successDeleteSpd = function (res) {
        $ionicLoading.hide();
        $scope.successAlert(res.message);
        getListSpd(indexPaging);
      };

      function verificationForm(formMpp) {
        var asOfDate = $filter('date')(new Date(formMpp.effectiveDate), globalConstant.dateFormatToDB);
        var todayDate = $filter('date')(new Date(), globalConstant.dateFormatToDB);

        if (asOfDate == undefined) {
          messageValidation = "As of Date can't be empty!";
          return false;
        }

        if (asOfDate < todayDate) {
          messageValidation = "As of Date must Future Date";
          return false;
        }

        if (parseInt(formMpp.mppAddChanges) < 0) {
          messageValidation = "Add / Switch must be positive number";
          return false;
        }

        return true;
      }

      $scope.gotoDetailSpd = function (id) {

        if ($scope.pageMode == 'ADVANCE') {
          $state.go('app.spd-advance-req', {
            'id': id
          });
        } else if ($scope.pageMode == 'REALIZATION') {
          $state.go('app.spd-realization-req', {
            'id': id
          });
        }

      };

      $scope.doResetPaging = function () {
        indexPaging = 0;
      };

      $scope.confirmDeleteSpd = function (id) {
        $ionicPopup.confirm({
          title: 'Confirm',
          template: '<h5>Are you sure to delete this SPD ?</h5>',
          cancelText: 'Cancel',
          okText: 'Yes'
        }).then(function (res) {
          if (res) {
            $ionicLoading.show({
              template: '<ion-spinner></ion-spinner>'
            });
            var data = {
              id: id,
            };
            data = JSON.stringify(data);
            var accessToken = Main.getSession("token").access_token;
            var urlApi = Main.getUrlApi() + '/api/user/dataapproval/spd/delete';
            Main.postRequestApi(accessToken, urlApi, data, successDeleteSpd, $scope.errorRequest, $scope.closeModal);
          }
        });
      };

      var successRequestListSpd = function (res) {
        if (res != null) {
          $scope.listSpd = res;
        }
        $ionicLoading.hide();
      };

      function getListSpd(page) {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;

        var urlApi = Main.getUrlApi();
        if ($scope.pageMode == "ADVANCE") {
          urlApi = urlApi + "/api/user/spd?requestType=SPD Advance&activeRequest=true";
        } else if ($scope.pageMode == "REALIZATION") {
          urlApi = urlApi + "/api/user/spd?requestType=SPD Realization&activeRequest=true";
        }

        urlApi = urlApi + "&page=" + page + "&size=" + size;

        Main.requestApi(accessToken, urlApi, successRequestListSpd, $scope.errorRequest);
      }

      function initMethod() {
        $scope.listSpd = [];

        //check if the category is Advance / Realization
        if ($location.path().startsWith("/app/spd-advance")) {
          $scope.pageMode = "ADVANCE";
          $scope.navTitle = "SPD Advance";
        } else if ($location.path().startsWith("/app/spd-realization")) {
          $scope.pageMode = "REALIZATION";
          $scope.navTitle = "SPD Realization";
        }

        getListSpd(indexPaging);
      }

      $scope.$on('$ionicView.beforeEnter', function () {
        initMethod();
      });

    })


  .controller("SpdRequestCtrl", function (
    $scope,
    $state,
    $ionicLoading,
    $ionicModal,
    $stateParams,
    $ionicPopup,
    $ionicScrollDelegate,
    $ionicHistory,
    $location,
    $filter,
    appService,
    ionicDatePicker,
    Main,
    globalConstant,
    Upload
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.appMode = Main.getAppMode();
    var size = Main.getDataDisplaySize();
    var indexPaging = 0;
    $scope.defaultImage = "img/placeholder.png";
    $scope.images = [];

    $ionicModal.fromTemplateUrl('modalSpdItem.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalSpdItem = modal;
    });

    // $ionicModal.fromTemplateUrl('components/modalLov/modalEmployee.html', {
    //   id: '0',
    //   scope: $scope,
    //   focusFirstInput: true,
    // }).then(function (modal) {
    //   $scope.modalEmployee = modal;
    // });

    var datePickerStartDate = {
      callback: function (val) { //Mandatory
        $scope.formSpd.startDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: globalConstant.dateFormat,
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDPStartDate = function () {
      ionicDatePicker.openDatePicker(datePickerStartDate);
    };

    var datePickerEndDate = {
      callback: function (val) { //Mandatory
        $scope.formSpd.endDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: globalConstant.dateFormat,
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDPEndDate = function () {
      ionicDatePicker.openDatePicker(datePickerEndDate);
    };

    function verificationForm(form) {

      var startDate = $filter('date')(new Date(form.startDate), globalConstant.dateFormatToDB);
      var endDate = $filter('date')(new Date(form.endDate), globalConstant.dateFormatToDB);

      if (form.startDate == undefined || form.startDate == "") {
        messageValidation = "Start Date can't be empty";
        return false;
      }
      if (form.endDate == undefined || form.endDate == "") {
        messageValidation = "End Date can't be empty";
        return false;
      }
      if (startDate > endDate) {
        messageValidation = "End Date must be greater than Start Date";
        return false;
      }
      return true;
    }

    $scope.saveFormSpd = function () {
      if (verificationForm($scope.formSpd)) {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });

        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() + "/api/user/dataapproval/spd/insert/";

        var data = $scope.formSpd;

        if ($scope.formSpd.requestType == 'SPD Realization') {
          delete data.id;
        }

        data.startDate = $filter('date')(new Date(data.startDate), globalConstant.dateFormatToDB);
        data.endDate = $filter('date')(new Date(data.endDate), globalConstant.dateFormatToDB);

        // var tmpAttachments = [];
        // if (data.attachments.length > 0) {
        //   for (var i = data.attachments.length - 1; i >= 0; i--) {
        //     var objAttachment = {
        //       image: null
        //     };
        //     if ($scope.appMode == "mobile") {
        //       objAttachment = data.attachments[i].image;
        //     } else {
        //       if (data.attachments[i].compressed.dataURL != undefined) {
        //         var webImageAttachment = 
        //         data.attachments[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
        //         objAttachment = webImageAttachment;
        //       }
        //     }
        //     tmpAttachments.push(objAttachment);
        //   }
        // }

        // data.attachments = tmpAttachments;
        data = JSON.stringify(data);

        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successSaveFormSpd,
          $scope.errorRequest
        );
      } else {
        $scope.warningAlert(messageValidation);
      }
    };

    $scope.addSpdItem = function () {
      $scope.formSpdItem = {
        name: '',
        employeeId: null,
      };

      // get category dropdown value
      getListCategorySPDItem();

      $scope.modalSpdItem.show();
    };

    $scope.editSpdItem = function (id) {
      getSpdItemById(id);

      $scope.modalSpdItem.show();
    };

    $scope.closeModal = function () {
      $scope.modalSpdItem.hide();
    };

    $scope.confirmDeleteSpdItem = function (id) {
      $ionicPopup.confirm({
        title: 'Confirm',
        template: '<h5>Are you sure to delete this SPD Item?</h5>',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var data = {
            id: id,
          };
          data = JSON.stringify(data);
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/user/dataapproval/spd/deleteSpdDetail';
          Main.postRequestApi(accessToken, urlApi, data, successDeleteSpdItem, $scope.errorRequest, $scope.closeModal);
        }
      });
    };

    $scope.submitFormSpdItem = function () {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/dataapproval/spd/insertSpdDetail";

      var data = $scope.formSpdItem;
      data.spdItemId = data.spdItem.id;
      data.spdRequestId = $scope.spdId;

      Upload.base64DataUrl($scope.formSpdItem.dataFile).then(function (url) {
        if (url != undefined) {
          var arrDocs = url.split(",");
          data.attachments = arrDocs[1];
        }
        data = JSON.stringify(data);

        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successSubmitFormSpdItem,
          $scope.errorRequest,
          $scope.closeModal()
        );
      });
    };

    // $scope.openModalEmployee = function () {
    //   $scope.doResetPaging(); 
    //   $scope.searchByEmployee(0);
    //   $scope.modalEmployee.show();
    // };


    // $scope.closeModalEmployee = function () {
    //   $scope.modalEmployee.hide();
    //   $scope.findByEmployee.search = '';
    // };

    // $scope.getValueEmployee = function (empl) {
    //   $scope.formApprGroupDetail.name = empl.name;
    //   $scope.formApprGroupDetail.employeeId = empl.employeeId;
    //   $scope.closeModalEmployee();

    //   // reset search position
    //   $scope.findByEmployee.search = '';

    //   //reset
    //   $scope.listEmployee = [];
    // };

    // $scope.doResetPaging = function () {
    //   indexPaging = 0;
    // };

    // $scope.loadMoreEmployee = function () {
    //   indexPaging++;
    //   $scope.listEmployee = [];
    //   $scope.searchByEmployee(indexPaging);

    //   // reset prev
    //   $scope.isfirst = 0;
    // };

    // $scope.loadPrevEmployee = function () {
    //   indexPaging--;
    //   $scope.listEmployee = [];
    //   $scope.searchByEmployee(indexPaging);
    // };

    // $scope.searchByEmployee = function (page) {
    //   $scope.findStatus = false;

    //   $ionicLoading.show({
    //     template: '<ion-spinner></ion-spinner>'
    //   });
    //   var accessToken = Main.getSession("token").access_token;
    //   var urlApi = Main.getUrlApi() + "/api/employee/find?page=" + page + "&size=" +
    //     size + "&q=" + encodeURIComponent($scope.findByEmployee.search);
    //   Main.requestApi(accessToken, urlApi, successRequestEmployee, $scope.errorRequest);
    // };

    // $scope.resetEmployee = function () {
    //   $scope.formApprGroupDetail.name = '';
    //   $scope.formApprGroupDetail.employeeId = '';
    // };

    $scope.chooseTab = function (tab) {
      $scope.module.type = tab;

      if (tab == 'general') {
        $scope.data = {};
        // check if SPD Realization mode
        if ($scope.formSpd.requestType == 'SPD Realization') {
          getListSpdAdvance();
        }

        if ($scope.spdId != 0) {
          getSpdById($scope.spdId);
        }
      } else if (tab == 'detail') {
        $scope.listSpdItem = [];
        getListSpdItem();
      }
    };

    $scope.submitFormSpd = function () {
      if ($scope.listSpdItem.length == 0) {
        $scope.warningAlert("Item detail must be filled!");
      } else {
        $ionicPopup.confirm({
          title: 'Confirm',
          template: '<h5>Are you sure to submit this ' + $scope.formSpd.requestType + ' ?</h5>',
          cancelText: 'Cancel',
          okText: 'Yes'
        }).then(function (res) {
          if (res) {
            $ionicLoading.show({
              template: "<ion-spinner></ion-spinner>"
            });
            var accessToken = Main.getSession("token").access_token;
            var urlApi =
              Main.getUrlApi() + "/api/user/dataApproval/spd/submit";
            var data = {
              id: $scope.formSpd.id == '' ? '' : $scope.formSpd.id,
            };

            data = JSON.stringify(data);
            Main.postRequestApi(
              accessToken,
              urlApi,
              data,
              successSubmitFormSpd,
              $scope.errorRequest
            );
          }
        });
      }
    };

    $scope.removeChoice = function () {
      var lastItem = $scope.formSpd.attachments.length - 1;
      $scope.formSpd.attachments.splice(lastItem);
      $scope.images.splice(lastItem);
    };

    $scope.addPicture = function () {
      if ($scope.images.length > 2) {
        alert("Only 3 pictures can be upload");
        return false;
      }
      $ionicActionSheet.show({
        buttons: [{
            text: "Take Picture"
          },
          {
            text: "Select From Gallery"
          }
        ],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener(
                "deviceready",
                function () {
                  $cordovaCamera.getPicture(appService.getCameraOptions()).then(
                    function (imageData) {
                      $scope.images.push({
                        image: "data:image/jpeg;base64," + imageData
                      });
                      $scope.formSpd.attachments.push({
                        image: imageData
                      });
                    },
                    function (err) {
                      appService.showAlert(
                        "Error",
                        err,
                        "Close",
                        "button-assertive",
                        null
                      );
                    }
                  );
                },
                false
              );

              break;
            case 1: // Select From Gallery
              document.addEventListener(
                "deviceready",
                function () {
                  $cordovaCamera
                    .getPicture(appService.getLibraryOptions())
                    .then(
                      function (imageData) {
                        $scope.images.push({
                          image: "data:image/jpeg;base64," + imageData
                        });
                        $scope.formSpd.attachments.push({
                          image: imageData
                        });
                      },
                      function (err) {
                        appService.showAlert(
                          "Error",
                          err,
                          "Close",
                          "button-assertive",
                          null
                        );
                      }
                    );
                },
                false
              );
              break;
          }
          return true;
        }
      });
    };

    $scope.getListItemByCategory = function () {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;
      var categorySpd = $scope.formSpdItem.category;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/item?spdType=" + typeSpd + "&category=" + categorySpd;
        Main.requestApi(accessToken, urlApi, successListItemByCategory, $scope.errorRequest);
      }
    };

    $scope.onChangeItem = function () {
      $scope.formSpdItem.quantity = 1;
      $scope.formSpdItem.amount = "";

      if ($scope.formSpdItem.spdItem) {
        $scope.formSpdItem.amount = $scope.formSpdItem.spdItem.amount;
      }
    };

    $scope.calculateAmountPayment = function () {
      $scope.formSpdItem.totalAmount = $scope.formSpdItem.amount * $scope.formSpdItem.quantity;
    };

    $scope.getSelectedSpdAdvance = function () {
      if ($scope.frmTmp.spdAdvance.id != 0) {
        // this is for spd realization case. 
        $scope.formSpd = $scope.frmTmp.spdAdvance;
        $scope.formSpd.requestType = "SPD Realization";
        $scope.formSpd.spdAdvanceId = $scope.frmTmp.spdAdvance.id;
      }
      if ($scope.frmTmp.spdAdvance.id == 0) {
        $scope.formSpd.type = "";
        $scope.formSpd.asalSpd = "";
        $scope.formSpd.tujuanSpd = "";
        $scope.formSpd.keperluan = "";
        $scope.formSpd.startDate = "";
        $scope.formSpd.endDate = "";
        $scope.formSpd.totalAmount = "";
        $scope.formSpd.totalAmountAdvance = "";
        $scope.formSpd.selisih = "";
        $scope.formSpd.remark = "";
      }
    };

    var successSaveFormSpd = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);

      // if create new and success then navigate to tab detail
      if ($scope.spdId == 0) {
        if (res.id != null && res.id != undefined) {
          $scope.spdId = res.id;
          $scope.chooseTab('detail');
        }
      }
      $ionicScrollDelegate.scrollTop();
    };

    var successSubmitFormSpd = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack('app.spd-advance');
    };

    var successRequestEmployee = function (res) {
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $scope.listEmployee = [];

      $scope.listEmployee = res.content;
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $ionicScrollDelegate.scrollTop();
    };

    var successSubmitFormSpdItem = function (res) {
      $scope.closeModal();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSpdItem();
    };

    var successRequestListSpdItem = function (res) {
      if (res != null) {
        $scope.listSpdItem = res;
      }
      $ionicLoading.hide();
    };

    var successRequestListSpdAdvance = function (res) {
      $scope.optSpdAdvance = [];
      $scope.optSpdAdvance.push({
        id: 0,
        requestNo: "-- Select --"
      });

      if (res != null) {
        $scope.optSpdAdvance = $scope.optSpdAdvance.concat(res.content);
      }

      $ionicLoading.hide();
    };

    var successDeleteSpdItem = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSpdItem();
    };

    function getSpdById(spdId) {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/spd/" + spdId;
      Main.requestApi(accessToken, urlApi, successRequestSpdById, $scope.errorRequest);
    }

    function getListSpdItem() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/spd/" + $scope.spdId + "/detail";
      Main.requestApi(accessToken, urlApi, successRequestListSpdItem, $scope.errorRequest);
    }

    function getListSpdAdvance() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/spd/pendingRealization";
      Main.requestApi(accessToken, urlApi, successRequestListSpdAdvance, $scope.errorRequest);
    }

    var successRequestSpdItemById = function (res) {
      if (res != null && res != '') {
        $scope.formSpdItem = res;
        $scope.formSpdItem.category = res.categoryType;
        // get category dropdown value
        getListCategorySPDItem();
      }
      $ionicLoading.hide();
    };

    var successRequestSpdById = function (res) {
      if (res != null && res != '') {
        $scope.formSpd = res;

        if ($scope.formSpd.requestType == 'SPD Realization') {
          $scope.frmTmp.spdAdvance = res.spdAdvance;
        }
      }
      getListSpdItem();
      $ionicLoading.hide();
    };


    function getSpdItemById(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/spd/' + $scope.spdId + '/detail/' + id;
      Main.requestApi(accessToken, urlApi, successRequestSpdItemById, $scope.errorRequest, $scope.closeModal);
    }

    function getListCategorySPDItem() {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/category?spdType=" + typeSpd;
        Main.requestApi(accessToken, urlApi, successListCategorySPDItem, $scope.errorRequest);
      }
    }

    var successListCategorySPDItem = function (res) {
      if (res != null) {
        $scope.optCategorySPDItem = res;
        $scope.getListItemByCategory();
        $scope.calculateAmountPayment();
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    var successListItemByCategory = function (res) {
      if (res != null) {
        $scope.optCatItemSPDItem = res;
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    function initModule() {
      $scope.spdId = $stateParams.id;

      $scope.pageMode = "EMPLOYEE";
      $scope.formSpd = {};
      // $scope.formSpd.attachments = [];
      $scope.frmTmp = {
        spdAdvance: {
          id: 0
        }
      };
      $scope.listSpdItem = [];
      $scope.globalConstant = globalConstant;
      $scope.module = {};
      $scope.findByEmployee = {
        search: ""
      };

      if ($location.path().startsWith("/app/spd-advance-req")) {
        $scope.formSpd.requestType = "SPD Advance";
      } else if ($location.path().startsWith("/app/spd-realization-req")) {
        $scope.formSpd.requestType = "SPD Realization";
      }
      // else if ($location.path().startsWith("/app/myteamperformanceeval")) {
      //   $scope.pageMode = "MYTEAM";
      // } else if ($location.path().startsWith("/app/adminperformanceeval")) {
      //   $scope.pageMode = "PERFORMANCEADMIN";
      // }
      $scope.chooseTab('general');
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });

  })

;
