angular.module('myteam.controllers', [])

  .controller('MyTeamCtrl', function ($ionicLoading, $rootScope, $scope, $state, Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.goToDetails = function (idx) {
      $state.go('app.myteamdetail', {
        'idx': idx
      });
    };

    $scope.refresh = function () {
      initMethod();
      $scope.$broadcast('scroll.refreshComplete');
    }


    $scope.team = [];
    var successProfile = function (res) {
      $ionicLoading.hide();
      $scope.team = res;
      $rootScope.team = [];
      for (var i = 0; i < $scope.team.length; i++) {
        var obj = $scope.team[i];
        obj.idx = i;

        if (obj.photoProfile == null || obj.photoProfile == '')
          obj.photoProfile = "img/1491892621_profle.png";

        $rootScope.team.push(obj);
      }
      $scope.team = $rootScope.team;
    }

    var errorProfile = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == -1) {
          $scope.errorAlert("Problem with your connection.");
        } else {
          $scope.errorAlert(err.message);
        }

      }
    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }
    var errRefreshToken = function (err, status) {}

    initMethod();
    //31acd2e6-e891-4628-a24e-58e408664516
    function initMethod() {
      getTeam();
    }
    // invalid access token error: "invalid_token" 401
    function getTeam() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/myprofile/team';
      Main.requestApi(accessToken, urlApi, successProfile, $scope.errorRequest);
    }
  })


  .controller('MyRequestCtrl',
    function (
      $ionicPopover,
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      $stateParams,
      Main
    ) {
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      $scope.module = {};
      $scope.requests = [];
      $scope.benefitRequests = [];
      $scope.attendanceRequests = [];
      $scope.letterRequests = [];

      var i = 0;
      var j = 0;
      var h = 0; //for attendance
      var u = 0;
      var size = Main.getDataDisplaySize();
      $scope.isLoadMoreBenefitShow = false;
      $scope.isLoadMorePersonalShow = false;
      $scope.isLoadMoreAttendanceShow = false;
      $scope.isLoadMoreLetterShow = false;
      $scope.confirmApprove = $ionicPopover.fromTemplate(contactTemplate, {
        scope: $scope
      });

      $scope.loadMorePersonal = function () {
        j++;
        // (j);
        getMyRequest('personalia', j);
      };

      $scope.loadMoreBenefit = function () {
        i++;
        // (i);
        getMyRequest('benefit', i);
      };

      $scope.loadMoreAttendance = function () {
        h++;
        // (h);
        getMyRequest('attendance', h);
      };

      $scope.loadMoreLetter = function () {
        u++;
        // (u);
        getMyRequest('letter', u);
      };

      $scope.chooseTab = function (tab) {
        i = 0;
        j = 0;
        h = 0;
        u = 0;
        $scope.module.type = tab.toLowerCase();
        if (tab == 'personalia') {
          $scope.requests = [];
          getMyRequest(tab, j);
        } else if (tab == 'benefit') {
          $scope.benefitRequests = [];
          getMyRequest(tab, i);
        } else if (tab == 'letter') {
          $scope.letterRequests = [];
          getMyRequest(tab, u);
        } else {
          $scope.benefitRequests = [];
          getMyRequest(tab, h);
        }

      };

      $scope.confirmReject = $ionicPopover.fromTemplate(contactTemplate, {
        scope: $scope
      });

      $scope.gotoDetailRequest = function (id) {

        $state.go('app.myrequestdetail', {
          'id': id
        });
      };

      $scope.gotoDetailRequestLetter = function (id) {

        $state.go('app.myrequestdetailletter', {
          'id': id
        });
      };



      $scope.refresh = function () {
        $scope.chooseTab($scope.module.type);
      };

      $scope.approval = function (action, id) {
        var data = {
          "id": id,
          "status": action
        };
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/workflow/actionapproval';
        data = JSON.stringify(data);

        Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);

      };

      $scope.goToDetails = function (idx) {
        $rootScope.requestSelected = $scope.requests[idx];
        $state.go('app.requestDetail', {
          'idx': idx
        });
      };


      var successRequest = function (res) {


        for (var i = 0; i < res.data.length; i++) {
          var obj = res.data[i];
          obj.idx = i;
          if ($scope.module.type == 'personalia') {
            if (obj.task == 'CHANGEMARITALSTATUS') {
              var change = obj.data;
              var objData = JSON.parse(change);
              obj.taskDescription = "Change marital status " + " to " + objData.maritalStatus;
            } else if (obj.task == 'SUBMITADDRESS') {
              obj.taskDescription = "Add new Address";
            } else if (obj.task == 'SUBMITFAMILY') {
              obj.taskDescription = "Add new family";
            }
          }
          $scope.log = function () {
            // (res);
          };

          if ($scope.module.type != 'letter') {
            obj.employeeRequest.fullName = obj.employeeRequest.firstName;
            if (obj.employeeRequest.middleName != null)
              obj.employeeRequest.fullName += " " + obj.employeeRequest.middleName;

            if (obj.employeeRequest.lastName != null)
              obj.employeeRequest.fullName += " " + obj.employeeRequest.lastName;
          }


          if ($scope.module.type == 'personalia') {
            $scope.requests.push(obj);
          } else if ($scope.module.type == 'benefit') {
            $scope.benefitRequests.push(obj);
          } else if ($scope.module.type == 'letter') {
            $scope.letterRequests.push(obj);
          } else {
            $scope.attendanceRequests.push(obj);
          }


          if ($scope.module.type == 'personalia') {
            if ($scope.requests.length == res.totalRecord)
              $scope.isLoadMorePersonalShow = false;
            else
              $scope.isLoadMorePersonalShow = true;
          } else if ($scope.module.type == 'benefit') {
            if ($scope.benefitRequests.length == res.totalRecord)
              $scope.isLoadMoreBenefitShow = false;
            else
              $scope.isLoadMoreBenefitShow = true;
          } else if ($scope.module.type == 'letter') {
            if ($scope.letterRequests.length == res.totalRecord)
              $scope.isLoadMoreLetterShow = false;
            else
              $scope.isLoadMoreLetterShow = true;
          } else {
            if ($scope.attendanceRequests.length == res.totalRecord)
              $scope.isLoadMoreAttendanceShow = false;
            else
              $scope.isLoadMoreAttendanceShow = true;
          }
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      };

      //initMethod();

      $scope.$on('$ionicView.beforeEnter', function () {
        initMethod();
      });


      function initMethod() {
        $scope.requests = [];
        $scope.benefitRequests = [];
        $scope.attendanceRequests = [];

        var subModule = $stateParams.submodule;
        $scope.subModule = subModule;

        if (subModule == '' || subModule == undefined) {
          subModule = 'Personalia';
        }

        $scope.chooseTab(subModule);
      }

      // invalid access token error: "invalid_token" 401
      function getMyRequest(module, page) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = "";

        if ($scope.module.type == 'letter') {
          urlApi = Main.getUrlApi() + '/api/statementLetter';
        } else {
          urlApi = Main.getUrlApi() + '/api/user/workflow/myrequest?module=' + module + '&page=' + page + '&size=' + size;
        }

        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }
    })

  .controller('MyRequestSubCtrl',
    function (
      ionicDatePicker,
      $filter,
      globalConstant,
      $ionicModal,
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      $stateParams,
      $ionicScrollDelegate,
      Main
    ) {
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      $scope.module = {};
      var submodule = $stateParams.submodule;
      $scope.requests = [];
      $scope.listRequests = [];
      $scope.filterRequest = {};
      $scope.globalConstant = globalConstant;
      $scope.submodule = submodule;

      var i = 0;
      var size = Main.getDataDisplaySize();
      $scope.isLoadMoreBenefitShow = false;

      var startDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterRequest.startDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openStartDatePicker = function () {
        ionicDatePicker.openDatePicker(startDatePicker);
      };

      var endDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterRequest.endDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openEndDatePicker = function () {
        ionicDatePicker.openDatePicker(endDatePicker);
      };

      $scope.loadMoreBenefit = function () {
        i++;
        getMyRequest(submodule, i);
      };

      $scope.gotoDetailRequest = function (obj) {
        var targetApp = "";

        switch (obj.categoryName) {
          case "SPD Advance":
            targetApp = "app.myrequestspdadvance";
            break;
          case "SPD Realization":
            targetApp = "app.myrequestspdrealization";
            break;
        }

        $state.go(targetApp, {
          'id': obj.dataApprovalId
        });
      };

      $scope.refresh = function () {
        getMyRequest(submodule, 0);
      };

      $ionicModal.fromTemplateUrl('modalMyRequest.html', {
        id: '0',
        scope: $scope
      }).then(function (modal) {
        $scope.modalMyRequest = modal;
      });

      $scope.closeModalMyRequest = function () {
        $scope.isLoadMoreShow = false;
        $scope.noDataShow = false;
        $scope.modalMyRequest.hide();
      };

      $scope.openModalMyRequest = function () {
        $scope.isLoadMoreShow = false;
        $scope.noDataShow = false;
        $scope.modalMyRequest.show();
      };

      var successRequest = function (res) {
        if (i == 0) {
          $scope.listRequests = res;
        } else {
          $scope.listRequests.content = $scope.listRequests.content.concat(res.content);
        }

        if ($scope.module.type == 'benefit') {
          if (!res.last)
            $scope.isLoadMoreBenefitShow = true;
          else
            $scope.isLoadMoreBenefitShow = false;
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.resize();
      };

      function getMyRequest(module, page) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = "";

        var tmpModule = module.toLowerCase();
        switch (module) {
          case "SPD Advance":
            tmpModule = 'spd';
            break;
          case "SPD Realization":
            tmpModule = 'spd';
            break;
        }

        urlApi = Main.getUrlApi() + '/api/user/dataApproval/' + tmpModule + '/myrequest?page=' + page + '&size=' + size;

        if ($scope.filterRequest.requestNo != undefined && $scope.filterRequest.requestNo != '') {
          urlApi = urlApi + '&requestNo=' + $scope.filterRequest.requestNo;
        }

        if ($scope.filterRequest.status != undefined && $scope.filterRequest.status != '') {
          urlApi = urlApi + '&status=' + $scope.filterRequest.status;
        }

        if ($scope.filterRequest.startDate != undefined && $scope.filterRequest.startDate != '') {
          urlApi = urlApi + '&requestDateStart=' + $filter("date")($scope.filterRequest.startDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterRequest.endDate != undefined && $scope.filterRequest.endDate != '') {
          urlApi = urlApi + '&requestDateEnd=' + $filter("date")($scope.filterRequest.endDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterRequest.action != undefined && $scope.filterRequest.action != '') {
          urlApi = urlApi + '&action=' + $scope.filterRequest.action;
        }

        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      $scope.doFilterRequest = function () {
        // check if date filter already fill as expected
        if (($scope.filterRequest.startDate == null && $scope.filterRequest.endDate == null) ||
          ($scope.filterRequest.startDate != null && $scope.filterRequest.endDate != null)
        ) {
          getMyRequest(submodule, 0);
          $scope.closeModalMyRequest();
        }

      };

      function initMethod() {
        $scope.requests = [];
        $scope.listRequests = [];
        var date = new Date();
        var vFirstDate = new Date(date.getFullYear(), date.getMonth(), 1);
        var vLastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        $scope.filterRequest.startDate = vFirstDate;
        $scope.filterRequest.endDate = vLastDate;
        getMyRequest(submodule, 0);
      }

      $scope.$on('$ionicView.beforeEnter', function (event, data) {
        if (data.direction != undefined && data.direction != 'back') {
          initMethod();
        }
        if ($rootScope.refreshMyRequest) {
          initMethod();
        }
        $rootScope.refreshMyRequest = false;
      });
    })

  .controller('RequestDetailLeterCtrl', function ($ionicLoading, $stateParams, $scope, $state, Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.detail = {};
    var id = $stateParams.id;

    var successRequest = function (res) {
      $scope.detail = res;

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    }

    //initMethod();

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

    function getMyRequest(module, page) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = "";


      var urlApi = Main.getUrlApi() + '/api/statementLetter/' + id;

      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }


    function initMethod() {
      getMyRequest();
    }

  })

  .controller('RequestApprovalCtrl',
    function (
      $ionicPopover,
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      $stateParams,
      Main
    ) {
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      var i = 0; // for benefit
      var j = 0; // for personalia
      var h = 0; //for attendance
      var k = 0;
      var size = Main.getDataDisplaySize();

      localStorage.setItem("value", "personalia");

      $scope.requests = [];
      $scope.benefitRequests = [];
      $scope.attendanceRequests = [];
      $scope.module = {};
      $scope.hasDataBenefit = false;
      $scope.isLoadMoreBenefitShow = false;
      $scope.isLoadMoreAttendanceShow = false;
      $scope.isLoadMorePersonalShow = false;
      $scope.isLoadMoreOvertimeShow = false;
      var profile = Main.getSession("profile");
      $scope.isHr = profile.isHr;

      $scope.goToSearch = function () {
        $state.go('app.formrequestsearching');
      };

      $scope.loadMorePersonal = function () {
        j++;
        // (j);
        getNeedApproval('personalia', j);
      };

      $scope.loadMoreBenefit = function () {
        i++;
        // (i);
        getNeedApproval('benefit', i);
      };

      $scope.loadMoreAttendance = function () {
        h++;
        // (h);
        getNeedApproval('attendance', h);
      };


      $scope.loadMoreOvertime = function () {
        k++;
        // (h);
        getNeedApproval('overtime', k);
      };

      $scope.$on('$ionicView.beforeEnter', function () {
        initMethod();
      });

      $scope.chooseTab = function (tab) {
        i = 0;
        j = 0;
        h = 0;
        $scope.module.type = tab.toLowerCase();
        if (tab === 'personalia') {
          $scope.requests = [];
          localStorage.setItem("value", "personalia");
          getNeedApproval(tab, j);
        } else if (tab === 'benefit') {
          $scope.benefitRequests = [];
          localStorage.setItem("value", "benefit");
          getNeedApproval(tab, i);
        } else if (tab === 'attendance') {
          $scope.attendanceRequests = [];
          localStorage.setItem("value", "attendance");
          getNeedApproval(tab, h);
        } else if (tab === 'overtime') {
          $scope.attendanceRequests = [];
          localStorage.setItem("value", "overtime");
          getNeedApproval(tab, h);
        }
        getCountNeedApproval();
      };


      $scope.confirmApprove = $ionicPopover.fromTemplate(contactTemplate, {
        scope: $scope
      });

      $scope.confirmReject = $ionicPopover.fromTemplate(contactTemplate, {
        scope: $scope
      });

      $scope.gotoDetailRequest = function (id) {
        $state.go('app.requestdetail', {
          'id': id,
          'needApproval': true
        });
      };

      $scope.refresh = function () {
        $scope.chooseTab($scope.module.type);
      };

      $scope.approval = function (action, id) {
        var data = {
          "id": id,
          "status": action
        };
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/workflow/actionapproval';
        data = JSON.stringify(data);
        Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);

      };

      $scope.goToDetails = function (idx) {
        $rootScope.requestSelected = $scope.requests[idx];
        $state.go('app.requestDetail', {
          'idx': idx
        });
      };


      var successRequest = function (res) {

        for (var i = 0; i < res.data.length; i++) {
          var obj = res.data[i];
          obj.idx = i;


          if ($scope.module.type == 'personalia') {
            if (res.data[i].task == 'CHANGEMARITALSTATUS') {
              var change = obj.data;
              var objData = JSON.parse(change);
              obj.taskDescription = "Change marital status from " + obj.maritalStatus + " to " + objData.maritalStatus;
            } else if (res.data[i].task == 'SUBMITFAMILY') {
              obj.taskDescription = "Add new family";
            }
          }

          //set the fullname to empty string first
          obj.fullName = '';

          // have to check if firstname is null or not
          if (obj.firstName != null) {
            obj.fullName = obj.firstName;
          }

          if (obj.middleName != null)
            obj.fullName += " " + obj.middleName;

          if (obj.lastName != null)
            obj.fullName += " " + obj.lastName;

          if ($scope.module.type == 'personalia') {
            $scope.requests.push(obj);
          } else if ($scope.module.type == 'attendance') {
            $scope.attendanceRequests.push(obj);
          } else {
            $scope.benefitRequests.push(obj);
          }

          obj.photoProfile = "img/1491892511_profle.png";
          if (obj.employeeRequestPhotoProfile != null)
            obj.photoProfile = obj.employeeRequestPhotoProfile;
        }

        if ($scope.module.type == 'personalia') {
          if ($scope.requests.length == res.totalRecord)
            $scope.isLoadMorePersonalShow = false;
          else
            $scope.isLoadMorePersonalShow = true;
        } else if ($scope.module.type == 'benefit') {
          if ($scope.benefitRequests.length == res.totalRecord)
            $scope.isLoadMoreBenefitShow = false;
          else
            $scope.isLoadMoreBenefitShow = true;
        } else if ($scope.module.type == 'overtime') {
          if ($scope.overtimeRequests.length == res.totalRecord)
            $scope.isLoadMoreOvertimeShow = false;
          else
            $scope.isLoadMoreOvertimeShow = true;
        } else {
          if ($scope.attendanceRequests.length == res.totalRecord)
            $scope.isLoadMoreAttendanceShow = false;
          else
            $scope.isLoadMoreAttendanceShow = true;

        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      };

      var successRequestCount = function (res) {
        if (res != null) {
          $rootScope.countApproval = res.count;
          // $scope.general.countApproval = res.count;
          $scope.general.countApproval = res;
        }
      };


      function initMethod() {
        $scope.requests = [];
        $scope.benefitRequests = [];
        $scope.attendanceRequests = [];

        var subModule = $stateParams.submodule.toLowerCase();
        $scope.subModule = subModule;

        if (subModule == '' || subModule == undefined) {
          subModule = 'personalia';
        }

        localStorage.setItem("value", subModule);
        $scope.chooseTab(localStorage.getItem("value"));
      }

      // invalid access token error: "invalid_token" 401
      function getCountNeedApproval() {
        var accessToken = Main.getSession("token").access_token;
        // var urlApi = Main.getUrlApi() + '/api/user/workflow/countneedapproval';
        var urlApi = Main.getUrlApi() + '/api/user/dataapproval/countneedapproval';
        Main.requestApi(accessToken, urlApi, successRequestCount, $scope.errorRequest);
      }

      function getNeedApproval(module, page) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?module=' + module + '&page=' + page + '&size=' + size;
        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

    })

  .controller('RequestDetailCtrl', function (ionicSuperPopup, $ionicPopup, $ionicModal, $ionicHistory, $ionicLoading, $stateParams, $rootScope, $scope, $state, Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.data = {}
    $scope.data.amount = 0;
    $scope.isYourData = true;
    $scope.needApproval = $stateParams.needApproval;
    var id = $stateParams.id;
    $scope.detail = {};
    $scope.confirm = {
      reasonReject: ""
    };
    $scope.attachment = "img/placeholder.png";
    $scope.dataBankAccount = [];
    $scope.isActiveCalender = 0;
    $scope.parsingData = {};

    //Ini untuk parameter approvalnya
    $scope.NPKDPA;
    $scope.tanggalApprove;
    $scope.flagDPA;

    $scope.change = function (event, index) {
      var value = event.target.value;
      value = value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      $scope.data.amount = value;
    }

    $scope.viewMoreFamily = function () {
      $state.go('app.detailfamily', {
        'idx': $scope.detail.ref.id,
        'edit': 'false'
      });
    }

    $scope.viewMoreAddress = function () {
      //// ("$scope.detail",$scope.detail);
      $state.go('app.detailaddress', {
        'idx': $scope.detail.ref.id,
        'edit': 'false'
      });
    }

    var successApprove = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      //console.log(res);
      if ($scope.flagDPA == 1) {
        var x = new Date();
        var y = x.getFullYear().toString();
        var m = (x.getMonth() + 1).toString();
        var d = x.getDate().toString();
        (d.length == 1) && (d = '0' + d);
        (m.length == 1) && (m = '0' + m);
        var yyyymmdd = y + m + d;
        $scope.tanggalApprove = yyyymmdd;

        var getExtension = $scope.detail.attachments[0].path;
        var explode = getExtension.split('.');
        var extension = explode[1];


        var string = $scope.detail.attachments[0].image;

        function toDataURL(url, callback) {
          var xhr = new XMLHttpRequest();
          xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
              callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
          };
          xhr.open('GET', url);
          xhr.responseType = 'blob';
          xhr.send();
        }

        toDataURL(string, function (dataUrl) {

          var attachmentDPA = [];
          var objAttachment = {
            image: null
          };
          var webAttachment = dataUrl.replace(/^data:image\/[a-z]+;base64,/, "");
          objAttachment = {
            image: webAttachment
          };
          attachmentDPA.push(objAttachment);

          var attach = attachmentDPA[0].image;

          var dataStr = {
            NPK: $scope.NPKDPA,
            fileBase64: attach,
            fileName: $scope.NPKDPA + '-' + $scope.tanggalApprove,
            fileExtension: extension,
            fileLocation: "/mnt/dpa/"
          };

          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getDpaUploadForm();
          var data = JSON.stringify(dataStr);
          Main.postRequestApproveDPAApi(
            accessToken,
            urlApi,
            data,
            successRequestApproval,
            $scope.errorRequest
          );

          //console.log(data);

          $rootScope.refreshRequestApprovalCtrl = true;
          // $scope.goBack("app.requestapproval");
          // $state.go("app.requestapproval");
          $ionicHistory.goBack();
        })
      } else {
        $rootScope.refreshRequestApprovalCtrl = true;
        // $scope.goBack("app.requestapproval");
        // $state.go("app.requestapproval");
        $ionicHistory.goBack();
      }
    }

    var successRequestApproval = function (res) {
      //console.log(res);
      // $state.go("app.requestapproval");
      $ionicHistory.goBack();
      ionicSuperPopup.show({
        title: "DPA",
        text: res.OUT_MESS,
        type: "success",
      });

    }

    var sendApproval = function (action, id, reason) {

      var data = {};
      if (action == 'approved') {
        var dataAmount = null;
        if ($scope.detail.ref != undefined && $scope.detail.ref.categoryType != undefined && $scope.detail.ref.categoryType == 'Medical Overlimit') {
          dataAmount = $scope.data.amount.replace(/\./g, '');
          dataAmount = Number(dataAmount);
        }

        data = {
          "id": id,
          "status": action,
          "amount": dataAmount
        };

      } else {
        data = {
          "id": id,
          "status": action,
          "reasonReject": reason
        };
      }


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/actionapproval';
      var data = JSON.stringify(data);
      Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);
      // successApprove();
    }

    $scope.confirmCancel = function () {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Cancel this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendApproval('cancelled', id, "");
          }


        });
    }

    $scope.confirmAccept = function () {
      if ($scope.detail.ref != undefined && $scope.detail.ref.categoryType != undefined && $scope.detail.ref.categoryType == 'Medical Overlimit' && $scope.detail.currentApprovalLevel > 0) {
        var validationAmount = $scope.data.amount.replace(/\./g, '');
        validationAmount = Number(validationAmount);
        if (parseInt(validationAmount) <= 1000) {
          $scope.warningAlert("Please fill out correct amount !!");
          return false;
        }
      }

      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Approve this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendApproval('approved', id, "");
          }


        });
    }

    $scope.confirmReject = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Reason Rejected',
        //template: '<input class="calm" type="text " ng-model="confirm.reasonReject" >',
        template: '<textarea rows="10" maxlength="255" class="calm" style="width:100%; border-color:#ddd; border: solid 2px #c9c9c9;border-radius:2px" ng-model="confirm.reasonReject" ></textarea>',
        cancelText: 'Cancel',
        scope: $scope,
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          var reason = $scope.confirm.reasonReject;
          if (reason == "")
            $scope.warningAlert("Reason reject can not empty");
          else
            sendApproval('rejected', id, reason);
        }

      });
    }

    $ionicModal.fromTemplateUrl('app/intro/image-preview.html', {
      scope: $scope,
      animation: 'fade-in-scale'
    }).then(function (modal) {
      $scope.modalPopupImage = modal;
    });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };
    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function jsonEscape(str) {
      return str.replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t");
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.detail = res;

      $scope.NPKDPA = res.employeeRequest.assignment.employeeNo;

      if (res.task != "SUBMITDPA" && res.data && res.data != null) {
        $scope.parsingData = JSON.parse(jsonEscape(res.data));
      }

      if (res.task == "SUBMITDPA")
        $scope.flagDPA = 1;

      if (res.ref != null) {
        if (res.ref.details[0].isCalendarDate == true) {
          $scope.isActiveCalender = 1;
        } else {
          $scope.isActiveCalender = 0;
        }
      }

      // initial parsing data account bank
      if (res.task != "SUBMITDPA" && $scope.detail.data && $scope.detail.data != null) {
        var parsing = JSON.parse(jsonEscape($scope.detail.data));
        $scope.dataBankAccount = [parsing];
      }

      // ($scope.detail);
      var detailEmpId = $scope.detail.empRequest;
      var profileEmpId = Main.getSession("profile").employeeTransient.id;
      // ("detailEmpId " + detailEmpId + " profileEmpId " + profileEmpId);
      if (profileEmpId != detailEmpId) {
        $scope.isYourData = false;
      }

      if ($scope.detail.task == 'SUBMITLEAVE') {
        $scope.detail.taskTitle = "Request Leave";
      } else if ($scope.detail.task == 'CHANGEMARITALSTATUS') {
        $scope.detail.taskTitle = "Change Marital Status";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change marital status from " + $scope.detail.employeeRequest.maritalStatus + " to " + objData.maritalStatus;
      } else if ($scope.detail.task == 'CHANGENPWP') {
        $scope.detail.taskTitle = "Change Npwp";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change npwp status from " + $scope.detail.employeeRequest.npwpNo + " to " + objData.npwpNo;
      } else if ($scope.detail.task == 'CHANGENIRCNO') {
        $scope.detail.taskTitle = "Change nircNo";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change nircno status from " + $scope.detail.employeeRequest.nircNo + " to " + objData.nircNo;
      } else if ($scope.detail.task == 'CHANGEKTPNAME') {
        $scope.detail.taskTitle = "Change ktpName";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change ktpname status from " + $scope.detail.employeeRequest.ktpName + " to " + objData.ktpName;
      } else if ($scope.detail.task == 'CHANGEFAMILYCARDNO') {
        $scope.detail.taskTitle = "Change Family Card No";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change family card no status from " + $scope.detail.employeeRequest.familyCardNo + " to " + objData.familyCardNo;
      } else if ($scope.detail.task == 'SUBMITFAMILY' || $scope.detail.task == 'CHANGEFAMILY') {
        $scope.detail.taskTitle = "Add new Family";
        $scope.detail.taskDescription = "Add new family";
        $rootScope.family = [];
        if ($scope.detail.task == 'CHANGEFAMILY' && $scope.detail.processingStatus == 'Reject')
          $scope.detail.ref = JSON.parse($scope.detail.data);

        $rootScope.family.push($scope.detail.ref);
      } else if ($scope.detail.task == 'SUBMITADDRESS' || $scope.detail.task == 'CHANGEADDRESS') {
        $rootScope.address = [];
        if ($scope.detail.task == 'CHANGEADDRESS' && $scope.detail.processingStatus == 'Reject')
          $scope.detail.ref = JSON.parse($scope.detail.data);

        $rootScope.address.push($scope.detail.ref);
      } else if ($scope.detail.task == 'SUBMITBENEFIT' || $scope.detail.task == 'SUBMITBENEFIT1' || $scope.detail.task == 'SUBMITBENEFIT2' || $scope.detail.task == 'SUBMITBENEFIT3' || $scope.detail.task == 'SUBMITBENEFIT4' || $scope.detail.task == 'SUBMITBENEFIT5') {
        if ($scope.detail.ref.categoryType == 'Medical') {
          var arrDetail = [];
          if ($scope.detail.ref.details.length > 0) {
            arrDetail = JSON.parse($scope.detail.ref.details[0].data);
          }
          $scope.detail.ref.details = arrDetail;
        } else if ($scope.detail.ref.categoryType == 'Medical Overlimit') {
          if ($scope.detail.ref.details.length > 0) {
            var obj = $scope.detail.ref.details[0];
            $scope.data.amount = "" + obj.amount;
            $scope.data.amount = $scope.data.amount.replace(/\D/g, "")
              .replace(/\B(?=(\d{3})+(?!\d))/g, ".");

          }

        }
      }

      $scope.detail.photoProfile = "img/1491892511_profle.png";
      if ($scope.detail.employeeRequestPhotoProfile != null)
        $scope.detail.photoProfile = $scope.detail.employeeRequestPhotoProfile;


      $scope.detail.employeeRequest.fullName = $scope.detail.employeeRequest.firstName;
      if ($scope.detail.employeeRequest.middleName != null)
        $scope.detail.employeeRequest.fullName += " " + $scope.detail.employeeRequest.middleName;

      if ($scope.detail.employeeRequest.lastName != null)
        $scope.detail.employeeRequest.fullName += " " + $scope.detail.employeeRequest.lastName;

      if ($scope.detail.attachments.length > 0) {

        $scope.attachment = $scope.detail.attachments[0].image;
      } else {
        $scope.detail.attachments = [];
      }

    }


    // var successRequestLetter = function(res) {
    //   $ionicLoading.hide();
    //   $scope.detail = res;

    // }



    function getDetailRequest() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + id;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    //   function getDetailRequestLetter() {
    //   $ionicLoading.show({
    //     template: '<ion-spinner></ion-spinner>'
    //   });
    //   var accessToken = Main.getSession("token").access_token;
    //   var urlApi = Main.getUrlApi() + '/api/statementLetter' + id;
    //   Main.requestApi(accessToken, urlApi, successRequestLetter, $scope.errorRequest);
    // }



    function initModule() {
      $scope.isYourData = true;
      $scope.detail = {};
      $scope.attachment = "img/placeholder.png";
      $scope.flagDPA = 0;
      getDetailRequest();
      // getDetailRequestLetter();

    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });



    //initModule();

    // // (teamIdx);

  })

  .controller('MyRequestDetailCtrl', function (ionicSuperPopup, $ionicPopup, $ionicModal, $ionicLoading, $stateParams, $rootScope, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.data = {}
    $scope.data.amount = 0;
    $scope.isYourData = true;
    $scope.needApproval = $stateParams.needApproval;
    var id = $stateParams.id;
    $scope.detail = {};
    $scope.confirm = {
      reasonReject: ""
    };
    $scope.attachment = "img/placeholder.png";
    $scope.dataBankAccount = [];
    $scope.isActiveCalender = 0;
    $scope.parsingData = {};

    $scope.change = function (event, index) {
      var value = event.target.value;
      value = value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      $scope.data.amount = value;
    }

    $scope.viewMoreFamily = function () {
      $state.go('app.detailfamily', {
        'idx': $scope.detail.ref.id,
        'edit': 'false'
      });
    }

    $scope.viewMoreAddress = function () {
      //// ("$scope.detail",$scope.detail);
      $state.go('app.detailaddress', {
        'idx': $scope.detail.ref.id,
        'edit': 'false'
      });
    }

    var successApprove = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.myrequest");
    }

    var sendApproval = function (action, id, reason) {

      var data = {};
      if (action == 'approved') {
        var dataAmount = null;
        if ($scope.detail.ref != undefined && $scope.detail.ref.categoryType != undefined && $scope.detail.ref.categoryType == 'Medical Overlimit') {
          dataAmount = $scope.data.amount.replace(/\./g, '');
          dataAmount = Number(dataAmount);
        }
        data = {
          "id": id,
          "status": action,
          "amount": dataAmount
        };
      } else {
        data = {
          "id": id,
          "status": action,
          "reasonReject": reason
        };
      }


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/actionapproval';
      var data = JSON.stringify(data);
      Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);

    }

    $scope.confirmCancel = function () {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Cancel this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendApproval('cancelled', id, "");
          }


        });
    }

    $scope.confirmAccept = function () {
      if ($scope.detail.ref != undefined && $scope.detail.ref.categoryType != undefined && $scope.detail.ref.categoryType == 'Medical Overlimit' && $scope.detail.currentApprovalLevel > 0) {
        var validationAmount = $scope.data.amount.replace(/\./g, '');
        validationAmount = Number(validationAmount);
        if (parseInt(validationAmount) <= 1000) {
          $scope.warningAlert("Please fill out correct amount !!");
          return false;
        }
      }

      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to Approve this request ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendApproval('approved', id, "");
          }


        });
    }

    $scope.confirmReject = function () {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Reason Rejected',
        //template: '<input class="calm" type="text " ng-model="confirm.reasonReject" >',
        template: '<textarea rows="10" maxlength="255" class="calm" style="width:100%; border-color:#ddd; border: solid 2px #c9c9c9;border-radius:2px" ng-model="confirm.reasonReject" ></textarea>',
        cancelText: 'Cancel',
        scope: $scope,
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          var reason = $scope.confirm.reasonReject;
          if (reason == "")
            $scope.warningAlert("Reason reject can not empty");
          else
            sendApproval('rejected', id, reason);
        }

      });
    }

    $ionicModal.fromTemplateUrl('app/intro/image-preview.html', {
      scope: $scope,
      animation: 'fade-in-scale'
    }).then(function (modal) {
      $scope.modalPopupImage = modal;
    });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };
    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };

    function jsonEscape(str) {
      return str.replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t");
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.detail = res;

      if (res.task != "SUBMITDPA")
        $scope.parsingData = JSON.parse(jsonEscape(res.data));

      if (res.ref != null) {
        if (res.ref.details[0].isCalendarDate == true) {
          $scope.isActiveCalender = 1;
        } else {
          $scope.isActiveCalender = 0;
        }
      }

      // initial parsing data account bank
      if (res.task != "SUBMITDPA") {
        var parsing = JSON.parse(jsonEscape($scope.detail.data));
        $scope.dataBankAccount = [parsing];
      }

      // ($scope.detail);
      var detailEmpId = $scope.detail.empRequest;
      var profileEmpId = Main.getSession("profile").employeeTransient.id;
      // ("detailEmpId " + detailEmpId + " profileEmpId " + profileEmpId);
      if (profileEmpId != detailEmpId) {
        $scope.isYourData = false;
      }

      if ($scope.detail.task == 'SUBMITLEAVE') {
        $scope.detail.taskTitle = "Request Leave";
      } else if ($scope.detail.task == 'CHANGEMARITALSTATUS') {
        $scope.detail.taskTitle = "Change Marital Status";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change marital status from " + $scope.detail.employeeRequest.maritalStatus + " to " + objData.maritalStatus;
      } else if ($scope.detail.task == 'CHANGENPWP') {
        $scope.detail.taskTitle = "Change Npwp";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change npwp status from " + $scope.detail.employeeRequest.npwpNo + " to " + objData.npwpNo;
      } else if ($scope.detail.task == 'CHANGENIRCNO') {
        $scope.detail.taskTitle = "Change nircNo";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change nircno status from " + $scope.detail.employeeRequest.nircNo + " to " + objData.nircNo;
      } else if ($scope.detail.task == 'CHANGEKTPNAME') {
        $scope.detail.taskTitle = "Change ktpName";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change ktpname status from " + $scope.detail.employeeRequest.ktpName + " to " + objData.ktpName;
      } else if ($scope.detail.task == 'CHANGEFAMILYCARDNO') {
        $scope.detail.taskTitle = "Change Family Card No";
        var change = $scope.detail.data;
        var objData = JSON.parse(change);
        $scope.detail.taskDescription = "Change family card no status from " + $scope.detail.employeeRequest.familyCardNo + " to " + objData.familyCardNo;
      } else if ($scope.detail.task == 'SUBMITFAMILY' || $scope.detail.task == 'CHANGEFAMILY') {
        $scope.detail.taskTitle = "Add new Family";
        $scope.detail.taskDescription = "Add new family";
        $rootScope.family = [];
        if ($scope.detail.task == 'CHANGEFAMILY' && $scope.detail.processingStatus == 'Reject')
          $scope.detail.ref = JSON.parse(jsonEscape($scope.detail.data));

        $rootScope.family.push($scope.detail.ref);
      } else if ($scope.detail.task == 'SUBMITADDRESS' || $scope.detail.task == 'CHANGEADDRESS') {
        $rootScope.address = [];
        if ($scope.detail.task == 'CHANGEADDRESS' && $scope.detail.processingStatus == 'Reject')
          $scope.detail.ref = JSON.parse(jsonEscape($scope.detail.data));

        $rootScope.address.push($scope.detail.ref);
      } else if ($scope.detail.task == 'SUBMITBENEFIT' || $scope.detail.task == 'SUBMITBENEFIT1' || $scope.detail.task == 'SUBMITBENEFIT2' || $scope.detail.task == 'SUBMITBENEFIT3' || $scope.detail.task == 'SUBMITBENEFIT4' || $scope.detail.task == 'SUBMITBENEFIT5') {
        if ($scope.detail.ref.categoryType == 'Medical') {
          var arrDetail = [];
          if ($scope.detail.ref.details.length > 0) {
            arrDetail = JSON.parse($scope.detail.ref.details[0].data);
          }
          $scope.detail.ref.details = arrDetail;
        } else if ($scope.detail.ref.categoryType == 'Medical Overlimit') {
          if ($scope.detail.ref.details.length > 0) {
            var obj = $scope.detail.ref.details[0];
            $scope.data.amount = "" + obj.amount;
            $scope.data.amount = $scope.data.amount.replace(/\D/g, "")
              .replace(/\B(?=(\d{3})+(?!\d))/g, ".");

          }

        }
      }

      $scope.detail.photoProfile = "img/1491892511_profle.png";
      if ($scope.detail.employeeRequestPhotoProfile != null)
        $scope.detail.photoProfile = $scope.detail.employeeRequestPhotoProfile;


      $scope.detail.employeeRequest.fullName = $scope.detail.employeeRequest.firstName;
      if ($scope.detail.employeeRequest.middleName != null)
        $scope.detail.employeeRequest.fullName += " " + $scope.detail.employeeRequest.middleName;

      if ($scope.detail.employeeRequest.lastName != null)
        $scope.detail.employeeRequest.fullName += " " + $scope.detail.employeeRequest.lastName;

      if ($scope.detail.attachments.length > 0) {

        $scope.attachment = $scope.detail.attachments[0].image;
      } else {
        $scope.detail.attachments = [];
      }

    }




    function getDetailRequest() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + id;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }



    function initModule() {
      $scope.isYourData = true;
      $scope.detail = {};
      $scope.attachment = "img/placeholder.png";
      getDetailRequest();
      // getDetailRequestLetter();

    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });



    //initModule();

    // // (teamIdx);

  })

  .controller('FormRequestSearchingCtrl', function ($ionicLoading, $scope, $state, Main) {
    $scope.formSearching = {
      search: ""
    };
    $scope.lov = {
      selectby: "NAME"
    };
    $scope.requests = [];
    $scope.actionFind = false;
    $scope.isLoadMoreShow = false;
    // var page = 0;
    $scope.totalR = 0;
    $scope.latestPage = 0;
    $scope.latest = 0;
    $scope.isfirst = 0;
    var h = 0; //for attendance
    var size = Main.getDataDisplaySize();

    var successRequestLoadMore = function (res) {
      $scope.requests = $scope.requests.concat(res.data);


      var totalR = res.totalRecord; // 42
      var countPages = totalR / size;
      var latestPage = Math.ceil(countPages);


      // calculate last 
      var fixLatestPage = latestPage - 1;

      if (res.page == fixLatestPage) {
        $scope.latest = 1;
      }


      if ($scope.requests.length < size)
        $scope.isLoadMoreShow = false;
      else
        $scope.isLoadMoreShow = true;


      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }


    var successRequest = function (res) {
      $scope.requests = [];
      $scope.requests = res.data;

      var totalR = res.totalRecord; // 42
      var countPages = totalR / size;
      var latestPage = Math.ceil(countPages);


      // calculate last 
      var fixLatestPage = latestPage - 1;

      if (res.page == fixLatestPage) {
        $scope.latest = 1;
      }

      if (res.data != null) {
        if ($scope.requests.length < size)
          $scope.isLoadMoreShow = false;
        else
          $scope.isLoadMoreShow = true;
      } else {
        $scope.isLoadMoreShow = false;
      }


      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }

    $scope.loadMore = function () {
      h++;
      getNeedApproval(h);
      // $scope.find(h);
    }

    $scope.find = function (page) {
      if (page == null || page === "undefined") {
        page = 0;
      }

      if (page == 0) {
        $scope.isfirst = 1;
      }


      // calculate last 
      var dataLatestPage = Main.getDataTotalPageApproval();
      var fixLatestPage = dataLatestPage - 1;

      // reset
      $scope.latest = 0;

      if (page == fixLatestPage) {
        $scope.latest = 1;
      }

      $scope.actionFind = true;
      var searchText = $scope.formSearching.search;
      // if(searchText == "" ||  searchText.length < 5) {
      //     $scope.warningAlert("Min Request No must be of 5 characters length");
      //     return false;
      // }
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;

      // serach by request number
      // var urlApi = Main.getUrlApi() + '/api/user/tmrequest/findRequestNo?requestNo='+$scope.formSearching.search;
      if ($scope.lov.selectby == "NAME" || $scope.lov.selectby == "")
        var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?employeeName=' + $scope.formSearching.search + '&page=' + page + '&size=' + size;
      else if ($scope.lov.selectby == "NPK")
        var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?employeeNo=' + $scope.formSearching.search + '&page=' + page + '&size=' + size;


      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);

    }

    function getNeedApproval(page) {


      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      // var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?employeeName=' + $scope.formSearching.search + '&page=' + page + '&size=' + size; 

      if ($scope.lov.selectby == "NAME" || $scope.lov.selectby == "")
        var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?employeeName=' + $scope.formSearching.search + '&page=' + page + '&size=' + size;
      else if ($scope.lov.selectby == "NPK")
        var urlApi = Main.getUrlApi() + '/api/user/workflow/needapproval?employeeNo=' + $scope.formSearching.search + '&page=' + page + '&size=' + size;

      Main.requestApi(accessToken, urlApi, successRequestLoadMore, $scope.errorRequest);
    }

    $scope.refresh = function () {
      initModule();
    }

    $scope.gotoDetailRequest = function (id) {
      $state.go('app.requestdetail', {
        'id': id,
        'needApproval': true
      });
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

    function initModule() {
      $scope.formSearching = {
        search: ""
      };
      $scope.lov = {
        selectby: ""
      };
      $scope.requests = [];
      getNeedApproval(0);
    }


  })

  .controller('DetailTeamCtrl', function ($stateParams, $filter, $rootScope, $scope, $state, Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.detail = {};
    $scope.birthDate = "";
    $scope.hireDate = "";
    var teamIdx = $stateParams.idx;
    if (teamIdx != null && $rootScope.team != undefined && $rootScope.team[teamIdx] != undefined) {
      $scope.detail = $rootScope.team[teamIdx];
      $scope.detail.fullName = $scope.detail.firstName + " " + $scope.detail.lastName;
      $scope.birthDate = $filter('date')(new Date($scope.detail.birthDate), 'dd MMMM yyyy');
      $scope.hireDate = $filter('date')(new Date($scope.detail.hireDate), 'dd MMMM yyyy');
    }

    // // (teamIdx);

  })

  .controller('FormHistoryApprovalCtrl', function ($ionicLoading, $filter, ionicDatePicker, $rootScope, $scope, $state, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.formHistory = {};

    var datepickerFrom = {
      callback: function (val) { //Mandatory
        $scope.formHistory.from = val;
        setFormHistoryTo(val);
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    var datepickerTo = {
      callback: function (val) { //Mandatory
        $scope.formHistory.to = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePickerFrom = function () {
      ionicDatePicker.openDatePicker(datepickerFrom);
    };

    $scope.openDatePickerTo = function () {
      ionicDatePicker.openDatePicker(datepickerTo);
    };

    function setFormHistoryTo(fromDate) {

      var toDate = new Date(fromDate);
      var toDateTemp = $filter('date')(toDate.getTime() + (7 * (1000 * 60 * 60 * 24)), 'yyyy-MM-dd');
      var currentDateTemp = $filter('date')(new Date(), 'yyyy-MM-dd');
      if (toDateTemp > currentDateTemp)
        $scope.formHistory.to = currentDateTemp;
      else
        $scope.formHistory.to = toDateTemp;

    }

    function successRequest(res) {
      $ionicLoading.hide();
      $rootScope.data.historyApproval = res;
      var startDate = $filter('date')(new Date($scope.formHistory.from), 'yyyy-MM-dd');
      var endDate = $filter('date')(new Date($scope.formHistory.to), 'yyyy-MM-dd');
      $state.go("app.historyapproval", {
        'startDate': startDate,
        'endDate': endDate
      });
    }

    $scope.submitForm = function () {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var data = {};
      data.from = $filter('date')(new Date($scope.formHistory.from), 'yyyy-MM-dd') + ' 00:00:00';
      data.to = $filter('date')(new Date($scope.formHistory.to), 'yyyy-MM-dd') + ' 23:59:59';
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/historyapproval?fromdate=' + data.from + '&todate=' + data.to;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);

    }
    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();
    });

    function initData() {
      var currentDate = new Date();
      var fromDate = $filter('date')((currentDate.getTime() - (7 * (1000 * 60 * 60 * 24))), 'yyyy-MM-dd');
      $scope.formHistory = {
        from: fromDate,
        to: currentDate
      };
    }

    function initMethod() {
      initData();
      $rootScope.data.historyApproval = [];
    }
  })


  .controller('HistoryApprovalCtrl', function ($stateParams, $rootScope, $scope, $state, Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    // ("history", $rootScope.data.historyApproval);
    $scope.request = $rootScope.data.historyApproval;

    $scope.gotoDetailRequest = function (id) {
      $state.go('app.myrequestdetail', {
        'id': id
      });
    }

    $scope.startDate = $stateParams.startDate;
    $scope.endDate = $stateParams.endDate;

  })


  .controller('RequestApprovalHomeCtrl',
    function (
      $state,
      $scope,
      $ionicLoading,
      $rootScope,
      Main
    ) {
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      $scope.isSpd = Main.getSession("profile").isSpd;


      var successRequestCountNeedApproval = function (res) {
        if (res != null) {
          dataCountNeedApproval = res;
        }

        for (var i = 0, len = dataCountNeedApproval.length; i < len; i++) {
          switch (dataCountNeedApproval[i].name) {
            case 'Benefit':
              $scope.cntBenefit = dataCountNeedApproval[i].value;
              break;
            case 'Personalia':
              $scope.cntPersonalia = dataCountNeedApproval[i].value;
              break;
            case 'Attendance':
              $scope.cntAttendance = dataCountNeedApproval[i].value;
              break;
            case 'SPD':
              $scope.cntSpd = dataCountNeedApproval[i].value;
              break;
          }
        }
        $ionicLoading.hide();
      };

      var successRequestCountSideMenu = function (res) {
        if (res != null) {
          $rootScope.countApproval = res.count;
          // $scope.general.countApproval = res.count;
          $scope.general.countApproval = res;
        }
      };

      function getCountNeedApproval() {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() + "/api/user/dataapproval/countneedapproval/allmodule";
        Main.requestApi(accessToken, urlApi, successRequestCountNeedApproval, $scope.errorRequest);
      }

      function getCountNeedApprovalSideMenu() {
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/dataapproval/countneedapproval';
        // var urlApi = Main.getUrlApi() + '/api/user/workflow/countneedapproval';
        Main.requestApi(accessToken, urlApi, successRequestCountSideMenu, $scope.errorRequest);
      }

      function initMethod() {
        $scope.dataCountNeedApproval = [];
        $scope.cntPersonalia = 0;
        $scope.cntBenefit = 0;
        $scope.cntAttendance = 0;
        $scope.cntSpd = 0;

        getCountNeedApproval();
        getCountNeedApprovalSideMenu();
      }

      $scope.$on('$ionicView.beforeEnter', function (event, data) {
        initMethod();
      });

    })


  .controller('RequestApprovalSubCtrl',
    function (
      $ionicLoading,
      $rootScope,
      $scope,
      $state,
      $ionicModal,
      $filter,
      $ionicPopup,
      $stateParams,
      ionicSuperPopup,
      ionicDatePicker,
      Main,
      globalConstant) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      var i = 0; // for benefit
      var size = Main.getDataDisplaySize();

      var submodule = $stateParams.submodule;
      $scope.submodule = submodule;

      $scope.listApproval = [];
      $scope.filterApproval = {};
      $scope.module = {};
      $scope.isLoadMoreShow = false;

      $scope.globalConstant = globalConstant;

      $scope.loadMore = function () {
        i++;
        getNeedApproval(submodule, i);
      };

      $scope.gotoDetailRequest = function (obj) {
        var targetApp = "";
        switch (obj.categoryName) {
          case "SPD Advance":
            targetApp = "app.requestapprovalspdadvance";
            break;
          case "SPD Realization":
            targetApp = "app.requestapprovalspdrealization";
            break;
        }

        $state.go(targetApp, {
          'id': obj.dataApprovalId
        });

      };

      $scope.refresh = function () {
        getNeedApproval(submodule, i);
      };

      $ionicModal.fromTemplateUrl('modalFilterApproval.html', {
        id: '0',
        scope: $scope,
        focusFirstInput: true,
      }).then(function (modal) {
        $scope.modalFilterApproval = modal;
      });

      $scope.openModalFilterApproval = function () {
        $scope.modalFilterApproval.show();
      };

      $scope.closeModalFilterApproval = function () {
        $scope.modalFilterApproval.hide();
      };

      $scope.doFilterApproval = function () {
        // check if date filter already fill as expected
        if (($scope.filterApproval.startDate == null && $scope.filterApproval.endDate == null) ||
          ($scope.filterApproval.startDate != null && $scope.filterApproval.endDate != null)
        ) {
          getNeedApproval(submodule, i);
          $scope.closeModalFilterApproval();
        }
      };

      $scope.chooseAll = function () {
        var mode = !$scope.isChooseAll;
        for (i = 0; i < $scope.listApproval.content.length; i++) {
          $scope.checkItems[$scope.listApproval.content[i].dataApprovalId] = mode;
        }
        $scope.isChooseAll = mode;
      };

      $scope.selectGoals = function () {
        var array = [];
        for (i in $scope.checkItems) {
          if ($scope.checkItems[i] == true) {
            array.push(i);
          }
        }

        $scope.listId = array;
        if ($scope.listApproval.content && $scope.listApproval.content.length == $scope.listId.length)
          $scope.isChooseAll = true;
        else $scope.isChooseAll = false;

        if ($scope.listId.length == 0)
          return true;
      };

      $scope.confirmAction = function (actionType) {
        if ($scope.checkItems == null) {
          $scope.warningAlert("Please Checked Before " + actionType);
        } else {
          $scope.confirm.reasonReject = "";
          var confirmPopup = $ionicPopup
            .confirm({
              title: "Reason " + actionType,
              template: '<textarea rows="10" maxlength="255" class="calm" style="width:100%; border-color:#ddd; border: solid 2px #c9c9c9;border-radius:2px" ng-model="confirm.reasonReject" ></textarea>',
              cancelText: "Cancel",
              scope: $scope,
              okText: "Yes"
            })
            .then(function (res) {
              if (res) {
                var reason = $scope.confirm.reasonReject;
                if (reason == "")
                  $scope.warningAlert("Reason " + actionType + " can not empty");
                else {
                  $scope.approval.actionNote = reason;
                  if (actionType == 'Approve') {
                    $scope.confirm.approvalStatus = "Approved";
                    $scope.approval.status = "Approved";
                  } else if (actionType == 'Reject') {
                    $scope.confirm.approvalStatus = "Rejected";
                    $scope.approval.status = "Rejected";
                  }

                  $scope.approval.dataApprovalIdList = $scope.listId;
                  $ionicLoading.show({
                    template: "Submit Request..."
                  });

                  var accessToken = Main.getSession("token").access_token;
                  var data = JSON.stringify($scope.approval);

                  var urlApi = Main.getUrlApi() + "/api/user/dataApproval/actionApprovalList2";
                  Main.postRequestApi(
                    accessToken,
                    urlApi,
                    data,
                    successSave,
                    $scope.errorRequest,
                    resetCheckItems
                  );
                }
              }
            });
        }
      };

      var resetCheckItems = function () {
        $ionicHistory.clearCache();
        $scope.checkItems = [];
      };

      var successRequest = function (res) {

        if (i == 0) {
          $scope.listApproval = res;
        } else {
          $scope.listApproval.content = $scope.listApproval.content.concat(res.content);
        }

        if (!res.last)
          $scope.isLoadMoreShow = true;
        else
          $scope.isLoadMoreShow = false;

        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicLoading.hide();
      };

      var successRequestCount = function (res) {
        if (res != null) {
          $rootScope.needApproval = res;
          $scope.general.needApproval = res;
          $scope.profile.needApproval = res;
          Main.setSession('profile', $scope.profile);
        }
      };

      var successSave = function (res) {
        $ionicLoading.hide();
        ionicSuperPopup.show({
            title: "Success",
            text: "Request has been " + $scope.confirm.approvalStatus,
            type: "success",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              $scope.checkItems = [];
              i = 0;
              getNeedApproval(submodule, i);
              getCountNeedApproval();
            }
          }
        );

      };

      var startDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterApproval.startDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openStartDatePicker = function () {
        ionicDatePicker.openDatePicker(startDatePicker);
      };

      var endDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterApproval.endDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openEndDatePicker = function () {
        ionicDatePicker.openDatePicker(endDatePicker);
      };


      function getCountNeedApprovalSidemenu() {
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/dataapproval/countneedapproval';
        Main.requestApi(accessToken, urlApi, successRequestCount, $scope.errorRequest);
      }

      function getNeedApproval(module, page) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        var tmpModule = module.toLowerCase();
        switch (module) {
          case 'SPD Advance':
            tmpModule = 'spd';
            break;
          case 'SPD Realization':
            tmpModule = 'spd';
            break;
        }

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/dataApproval/' + tmpModule + '/needapproval?page=' + page + '&size=' + size;

        if ($scope.filterApproval.ppl != undefined && $scope.filterApproval.ppl != '') {
          urlApi = urlApi + '&ppl=' + $scope.filterApproval.ppl;
        }

        if ($scope.filterApproval.startDate != undefined && $scope.filterApproval.startDate != '') {
          urlApi = urlApi + '&requestDateStart=' + $filter("date")($scope.filterApproval.startDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterApproval.endDate != undefined && $scope.filterApproval.endDate != '') {
          urlApi = urlApi + '&requestDateEnd=' + $filter("date")($scope.filterApproval.endDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterApproval.action != undefined && $scope.filterApproval.action != '') {
          urlApi = urlApi + '&action=' + $scope.filterApproval.action;
        }

        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      function initMethod() {
        $scope.listApproval = [];
        $scope.checkItems = [];
        $scope.isChooseAll = false;
        $scope.approval = {};

        $scope.filterApproval.startDate = '';
        $scope.filterApproval.endDate = '';
        i = 0;
        getNeedApproval(submodule, i);
        getCountNeedApprovalSidemenu();
      }

      $scope.$on('$ionicView.beforeEnter', function (event, data) {
        if (data.direction != undefined && data.direction != 'back') {
          initMethod();
        }

        if ($rootScope.refreshReqApproval) {
          initMethod();
        }
        $rootScope.refreshReqApproval = false;
      });

    })

  .controller('RequestApprovalSpdCtrl', function (
    ionicSuperPopup,
    $ionicPopup,
    $ionicModal,
    $ionicLoading,
    $stateParams,
    $rootScope,
    $scope,
    $state,
    $ionicHistory,
    $location,
    Main,
    globalConstant
  ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.data = {};
    var id = $stateParams.id;
    $scope.detail = [];

    $scope.attachment = "img/placeholder.png";
    $scope.readOnly = false;
    $scope.globalConstant = globalConstant;
    $scope.module = {};

    $scope.action = "needapproval";

    $ionicModal.fromTemplateUrl('modalSpdItem.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalSpdItem = modal;
    });

    $scope.chooseTab = function (tab) {
      i = 0;
      $scope.module.type = tab;

      if (tab == 'general') {
        $scope.detail = [];
        getDetailRequest();
      } else if (tab == 'detail') {
        $scope.listSpdItem = [];
        getListSpdItem();
      }

    };

    var successApprove = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshReqApproval = true;
      $scope.goBack('app.requestapprovalsub', {
        'submodule': 'SPD Advance'
      });
    };

    var sendApproval = function (action, reason) {
      var data = {};

      data.dataApprovalId = $scope.detail.dataApprovalId;
      data.actionNote = reason;
      if (action == 'Approve') {
        data.status = "Approved";
      } else if (action == 'Reject') {
        data.status = "Rejected";
      }

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/dataApproval/actionApproval2';
      data = JSON.stringify(data);
      Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);

    };

    $scope.confirmAction = function (actionType) {
      $scope.confirm.notes = '';
      $ionicPopup.confirm({
        title: actionType + ' - Notes',
        template: '<textarea rows="10" maxlength="255" style="resize:none; width:100%; border-color:#ddd; border: solid 2px #c9c9c9;border-radius:2px" ng-model="confirm.notes" ></textarea>',
        cancelText: 'Cancel',
        scope: $scope,
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          var reason = $scope.confirm.notes;
          if (reason == "" && actionType == 'Reject')
            $scope.warningAlert(actionType + " Notes can not empty");
          else
            sendApproval(actionType, reason);
        }
      });
    };

    $scope.closeModal = function () {
      $scope.modalSpdItem.hide();
    };

    $scope.downloadDocs = function (location) {
      window.open(encodeURI(location), "_system", "location=yes");
      return false;
    };

    $scope.editSpdItem = function (id) {
      getSpdItemById(id);
      $scope.modalSpdItem.show();
    };

    $scope.onChangeItem = function () {
      $scope.formSpdItem.quantity = "";
      $scope.formSpdItem.amount = "";

      if ($scope.formSpdItem.spdItem) {
        $scope.formSpdItem.amount = $scope.formSpdItem.spdItem.amount;
      }
    };

    $scope.calculateAmountPayment = function () {
      $scope.formSpdItem.totalAmount = $scope.formSpdItem.amount * $scope.formSpdItem.quantity;
    };

    $scope.submitFormSpdItem = function () {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/dataapproval/spd/insertSpdDetail";

      var data = $scope.formSpdItem;
      data.spdRequestId = $scope.formSpd.id;

      data = JSON.stringify(data);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successSubmitFormSpdItem,
        $scope.errorRequest,
        $scope.closeModal()
      );
    };

    $scope.getListItemByCategory = function () {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;
      var categorySpd = $scope.formSpdItem.category;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/item?spdType=" + typeSpd + "&category=" + categorySpd;
        Main.requestApi(accessToken, urlApi, successListItemByCategory, $scope.errorRequest);
      }
    };

    $scope.getListItemByCategory = function () {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;
      var categorySpd = $scope.formSpdItem.category;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/item?spdType=" + typeSpd + "&category=" + categorySpd;
        Main.requestApi(accessToken, urlApi, successListItemByCategory, $scope.errorRequest);
      }
    };

    var successListItemByCategory = function (res) {
      if (res != null) {
        $scope.optCatItemSPDItem = res;
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    var successListItemByCategory = function (res) {
      if (res != null) {
        $scope.optCatItemSPDItem = res;
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.detail = res;
      $scope.formSpd = res.request;
    };

    function getDetailRequest() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = '';

      urlApi = Main.getUrlApi() + '/api/user/dataApproval/spd/needapproval/' + id;
      // else if (module == 'apprHistory')
      //   urlApi = Main.getUrlApi() + '/api/user/dataApproval/spd/historyApprovalById/' + id;

      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function getListSpdItem() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/spd/" + $scope.formSpd.id + "/detail";
      Main.requestApi(accessToken, urlApi, successRequestListSpdItem, $scope.errorRequest);
    }

    var successRequestListSpdItem = function (res) {
      if (res != null) {
        $scope.listSpdItem = res;
      }
      $ionicLoading.hide();
    };

    function getSpdItemById(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/spd/' + $scope.formSpd.id + '/detail/' + id;
      Main.requestApi(accessToken, urlApi, successRequestSpdItemById, $scope.errorRequest, $scope.closeModal);
    }

    function getListCategorySPDItem() {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/category?spdType=" + typeSpd;
        Main.requestApi(accessToken, urlApi, successListCategorySPDItem, $scope.errorRequest);
      }
    }

    var successRequestSpdItemById = function (res) {
      if (res != null && res != '') {
        $scope.formSpdItem = res;
        $scope.formSpdItem.category = res.categoryType;
        // get category dropdown value
        getListCategorySPDItem();
      }
      $ionicLoading.hide();
    };

    var successListCategorySPDItem = function (res) {
      if (res != null) {
        $scope.optCategorySPDItem = res;
        $scope.getListItemByCategory();
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    var successSubmitFormSpdItem = function (res) {
      $scope.closeModal();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSpdItem();
    };

    function initModule() {
      $scope.detail = [];
      $scope.attachment = "img/placeholder.png";
      $scope.chooseTab('general');


      $scope.pageMode = "NEEDAPPROVAL";

      if ($location.path().startsWith("/app/requestapprovalspdadvance")) {
        $scope.spdMode = "ADVANCE"
      } else if ($location.path().startsWith("/app/requestapprovalspdrealization")) {
        $scope.spdMode = "REALIZATION"
      }


      $scope.confirm = {
        notes: ""
      };
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });

  })


  .controller('MyRequestHomeCtrl',
    function (
      $state,
      $scope,
      Main
    ) {
      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      $scope.isSpd = Main.getSession("profile").isSpd;
    })


  .controller('MyRequestSpdCtrl',
    function (
      globalConstant,
      ionicSuperPopup,
      $ionicPopup,
      $ionicPopover,
      $ionicModal,
      $ionicLoading,
      $stateParams,
      $rootScope,
      $scope,
      $state,
      $location,
      Main) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      var id = $stateParams.id;
      $scope.detail = [];
      $scope.attachment = "img/placeholder.png";
      $scope.globalConstant = globalConstant;
      $scope.module = {};

      $ionicModal.fromTemplateUrl('modalSpdItem.html', {
        id: '0',
        scope: $scope,
        focusFirstInput: true,
      }).then(function (modal) {
        $scope.modalSpdItem = modal;
      });

      $scope.chooseTab = function (tab) {
        i = 0;
        $scope.module.type = tab;

        if (tab == 'general') {
          $scope.detail = [];
          getDetailRequest();
        } else if (tab == 'detail') {
          $scope.listSpdItem = [];
          getListSpdItem();
        }
      };

      $scope.downloadDocs = function (location) {
        window.open(encodeURI(location), "_system", "location=yes");
        return false;
      };

      var successApprove = function (res) {
        $ionicLoading.hide();
        $scope.successAlert(res.message);
        $rootScope.refreshMyRequest = true;
        $scope.goBack('app.myrequestsub', {
          'submodule': 'SPD Advance'
        });
      };

      var sendApproval = function (action, id, reason) {

        var data = {
          "dataApprovalId": id,
          "status": action,
          "reasonReject": ""
        };

        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/dataApproval/actionApproval2';
        data = JSON.stringify(data);
        Main.postRequestApi(accessToken, urlApi, data, successApprove, $scope.errorRequest);

      };

      $scope.printSpd = function () {
        printForm(btoa(String($scope.formSpd.id)), btoa(String($scope.formSpd.requestType)));
      };

      var printForm = function (id, categorySpd) {
        var accessToken = Main.getSession("token").access_token;
        var url = "";

        url =
          Main.getPrintFormSpd() +
          "?ses_id=" + accessToken +
          "&id=" + id +
          "&category=" + categorySpd;

        window.open(encodeURI(url), "_system", "location=yes");
        return false;
      };

      $scope.confirmCancel = function () {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure want to Cancel this request ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendApproval('Canceled', id, "");
            }
          });
      };

      $scope.editSpdItem = function (id) {
        getSpdItemById(id);
        $scope.modalSpdItem.show();
      };

      $scope.closeModal = function () {
        $scope.modalSpdItem.hide();
      };

      $scope.getListItemByCategory = function () {
        $scope.findStatus = false;
        var typeSpd = $scope.formSpd.type;
        var categorySpd = $scope.formSpdItem.category;

        //check if type spd is not undefined
        if (typeSpd != undefined && typeSpd != '') {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + "/api/spd/item?spdType=" + typeSpd + "&category=" + categorySpd;
          Main.requestApi(accessToken, urlApi, successListItemByCategory, $scope.errorRequest);
        }
      };

      var successListItemByCategory = function (res) {
        if (res != null) {
          $scope.optCatItemSPDItem = res;
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      };

      var successRequest = function (res) {
        $scope.detail = res;
        $scope.formSpd = res.request;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      };

      function getDetailRequest() {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
          $state.go("login");
        }

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/dataApproval/spd/myrequest/' + id;

        // } else if (module == 'apprHistory') {
        //   urlApi = Main.getUrlApi() + '/api/user/dataApproval/spd/historyApprovalById/' + id;
        // }
        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      function getListSpdItem() {
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() + "/api/user/spd/" + $scope.formSpd.id + "/detail";
        Main.requestApi(accessToken, urlApi, successRequestListSpdItem, $scope.errorRequest);
      }

      var successRequestListSpdItem = function (res) {
        if (res != null) {
          $scope.listSpdItem = res;
        }
        $ionicLoading.hide();
      };

      function getSpdItemById(id) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/spd/' + $scope.formSpd.id + '/detail/' + id;
        Main.requestApi(accessToken, urlApi, successRequestSpdItemById, $scope.errorRequest, $scope.closeModal);
      }

      function getListCategorySPDItem() {
        $scope.findStatus = false;
        var typeSpd = $scope.formSpd.type;

        //check if type spd is not undefined
        if (typeSpd != undefined && typeSpd != '') {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + "/api/spd/category?spdType=" + typeSpd;
          Main.requestApi(accessToken, urlApi, successListCategorySPDItem, $scope.errorRequest);
        }
      }

      var successRequestSpdItemById = function (res) {
        if (res != null && res != '') {
          $scope.formSpdItem = res;
          $scope.formSpdItem.category = res.categoryType;
          // get category dropdown value
          getListCategorySPDItem();
        }
        $ionicLoading.hide();
      };

      var successListCategorySPDItem = function (res) {
        if (res != null) {
          $scope.optCategorySPDItem = res;
          $scope.getListItemByCategory();
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      };

      function initModule() {
        $scope.isYourData = true;
        $scope.detail = [];
        $scope.attachment = "img/placeholder.png";

        $scope.pageMode = "MYREQUEST";

        if ($location.path().startsWith("/app/myrequestspdadvance")) {
          $scope.spdMode = "ADVANCE"
        } else if ($location.path().startsWith("/app/myrequestspdrealization")) {
          $scope.spdMode = "REALIZATION"
        }

        $scope.chooseTab('general');
      }

      $scope.$on('$ionicView.beforeEnter', function () {
        initModule();
      });

    })

  .controller('ApprovalHistoryHomeCtrl',
    function (
      $state,
      $scope,
      Main
    ) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }
      $scope.isSpd = Main.getSession("profile").isSpd;


    })

  .controller('ApprovalHistorySubCtrl',
    function (
      $ionicLoading,
      $filter,
      ionicDatePicker,
      $stateParams,
      $rootScope,
      $scope,
      $state,
      $ionicModal,
      $ionicScrollDelegate,
      globalConstant,
      Main
    ) {

      if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
        $state.go("login");
      }

      var i = 0; // for benefit
      var size = Main.getDataDisplaySize();

      var profile = Main.getSession("profile");
      var submodule = $stateParams.submodule;
      $scope.submodule = submodule;

      $scope.listApproval = [];
      $scope.filterApproval = {};
      $scope.module = {};
      $scope.isLoadMoreShow = false;

      $scope.globalConstant = globalConstant;

      $scope.loadMore = function () {
        i++;
        getHistoryApproval(submodule, i);
      };

      $scope.gotoDetailApprovalHistory = function (obj) {
        var targetApp = "";

        switch (obj.categoryName) {
          case "SPD Advance":
            targetApp = "app.approvalhistoryspdadvance";
            break;
          case "SPD Realization":
            targetApp = "app.approvalhistoryspdrealization";
            break;
        }

        $state.go(targetApp, {
          'id': obj.dataApprovalDetailId
        });

      };

      $scope.refresh = function () {
        $scope.chooseTab($scope.module.type);
      };

      $ionicModal.fromTemplateUrl('modalFilterApproval.html', {
        id: '0',
        scope: $scope,
        focusFirstInput: true,
      }).then(function (modal) {
        $scope.modalFilterApproval = modal;
      });

      $scope.openModalFilterApproval = function () {
        $scope.modalFilterApproval.show();
      };

      $scope.closeModalFilterApproval = function () {
        $scope.modalFilterApproval.hide();
      };

      $scope.doFilterApproval = function () {
        // check if date filter already fill as expected
        if (($scope.filterApproval.startDate == null && $scope.filterApproval.endDate == null) ||
          ($scope.filterApproval.startDate != null && $scope.filterApproval.endDate != null)
        ) {
          getHistoryApproval(submodule, i);
          $scope.closeModalFilterApproval();
        }
      };

      var successRequest = function (res) {
        if (i == 0) {
          $scope.listApproval = res;
        } else {
          $scope.listApproval.content = $scope.listApproval.content.concat(res.content);
        }

        if (!res.last)
          $scope.isLoadMoreShow = true;
        else
          $scope.isLoadMoreShow = false;

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $ionicScrollDelegate.resize();
      };

      var startDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterApproval.startDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openStartDatePicker = function () {
        ionicDatePicker.openDatePicker(startDatePicker);
      };

      var endDatePicker = {
        callback: function (val) { //Mandatory
          $scope.filterApproval.endDate = val;
        },
        inputDate: new Date(), //Optional
        mondayFirst: true, //Optional
        dateFormat: globalConstant.dateFormat,
        closeOnSelect: false, //Optional
        templateType: 'popup' //Optional
      };

      $scope.openEndDatePicker = function () {
        ionicDatePicker.openDatePicker(endDatePicker);
      };

      function getHistoryApproval(module, page) {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        var tmpModule = module.toLowerCase();

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/dataApproval/' + tmpModule + '/historyApproval?page=' + page + '&size=' + size;

        if ($scope.filterApproval.status != undefined && $scope.filterApproval.status != '') {
          urlApi = urlApi + '&status=' + $scope.filterApproval.status;
        }

        if ($scope.filterApproval.ppl != undefined && $scope.filterApproval.ppl != '') {
          urlApi = urlApi + '&ppl=' + $scope.filterApproval.ppl;
        }

        if ($scope.filterApproval.startDate != undefined && $scope.filterApproval.startDate != '') {
          urlApi = urlApi + '&requestDateStart=' + $filter("date")($scope.filterApproval.startDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterApproval.endDate != undefined && $scope.filterApproval.endDate != '') {
          urlApi = urlApi + '&requestDateEnd=' + $filter("date")($scope.filterApproval.endDate, globalConstant.dateFormatToDB);
        }

        if ($scope.filterApproval.action != undefined && $scope.filterApproval.action != '') {
          urlApi = urlApi + '&action=' + $scope.filterApproval.action;
        }

        Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
      }

      function initMethod() {
        $scope.listApproval = [];
        var date = new Date();
        var vFirstDate = new Date(date.getFullYear(), date.getMonth(), 1);
        var vLastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

        $scope.filterApproval.startDate = vFirstDate;
        $scope.filterApproval.endDate = vLastDate;
        i = 0;
        getHistoryApproval(submodule, i);
      }

      $scope.$on('$ionicView.beforeEnter', function (event, data) {
        if (data.direction != undefined && data.direction != 'back') {
          initMethod();
        }

        if ($rootScope.refreshHistoryApproval) {
          initMethod();
        }
        $rootScope.refreshHistoryApproval = false;
      });
    })


  .controller('ApprovalHistorySpdCtrl', function (
    $ionicLoading,
    $stateParams,
    $scope,
    $state,
    $location,
    $ionicModal,
    Main,
    globalConstant
  ) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.data = {};
    var id = $stateParams.id;
    $scope.detail = [];
    $scope.confirm = {
      reasonReject: ""
    };
    $scope.attachment = "img/placeholder.png";
    $scope.readOnly = false;
    $scope.globalConstant = globalConstant;
    $scope.module = {};

    $scope.action = "approvalhistory";

    $ionicModal.fromTemplateUrl('modalSpdItem.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalSpdItem = modal;
    });

    $scope.editSpdItem = function (id) {
      getSpdItemById(id);
      $scope.modalSpdItem.show();
    };

    $scope.chooseTab = function (tab) {
      i = 0;
      $scope.module.type = tab;

      if (tab == 'general') {
        $scope.detail = [];
        getDetailRequest();
      } else if (tab == 'detail') {
        $scope.listSpdItem = [];
        getListSpdItem();
      }
    };

    $scope.closeModal = function () {
      $scope.modalSpdItem.hide();
    };

    $scope.getListItemByCategory = function () {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;
      var categorySpd = $scope.formSpdItem.category;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/item?spdType=" + typeSpd + "&category=" + categorySpd;
        Main.requestApi(accessToken, urlApi, successListItemByCategory, $scope.errorRequest);
      }
    };

    $scope.printSpd = function () {
      printForm(btoa(String($scope.formSpd.id)), btoa(String($scope.formSpd.requestType)));
    };

    var printForm = function (id, categorySpd) {
      var accessToken = Main.getSession("token").access_token;
      var url = "";

      url =
        Main.getPrintFormSpd() +
        "?ses_id=" + accessToken +
        "&id=" + id +
        "&category=" + categorySpd;

      window.open(encodeURI(url), "_system", "location=yes");
      return false;
    };

    var successListItemByCategory = function (res) {
      if (res != null) {
        $scope.optCatItemSPDItem = res;
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    var successRequest = function (res) {
      // set data approval id from general payload, because different parameter usage in API.
      if ($scope.module.type == 'general')
        $scope.dataApprovalId = res.dataApprovalId;
      $scope.detail = res;
      $scope.formSpd = res.request;
      $ionicLoading.hide();

    };

    function getDetailRequest() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = '';

      urlApi = Main.getUrlApi() + '/api/user/dataApproval/spd/historyApproval/' + id;
      // else if (module == 'apprHistory')
      //   urlApi = Main.getUrlApi() + '/api/user/dataApproval/spd/historyApprovalById/' + $scope.dataApprovalId;

      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    function getListSpdItem() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/user/spd/" + $scope.formSpd.id + "/detail";
      Main.requestApi(accessToken, urlApi, successRequestListSpdItem, $scope.errorRequest);
    }

    var successRequestListSpdItem = function (res) {
      if (res != null) {
        $scope.listSpdItem = res;
      }
      $ionicLoading.hide();
    };

    function getSpdItemById(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/spd/' + $scope.formSpd.id + '/detail/' + id;
      Main.requestApi(accessToken, urlApi, successRequestSpdItemById, $scope.errorRequest, $scope.closeModal);
    }

    function getListCategorySPDItem() {
      $scope.findStatus = false;
      var typeSpd = $scope.formSpd.type;

      //check if type spd is not undefined
      if (typeSpd != undefined && typeSpd != '') {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + "/api/spd/category?spdType=" + typeSpd;
        Main.requestApi(accessToken, urlApi, successListCategorySPDItem, $scope.errorRequest);
      }
    }

    var successRequestSpdItemById = function (res) {
      if (res != null && res != '') {
        $scope.formSpdItem = res;
        $scope.formSpdItem.category = res.categoryType;
        // get category dropdown value
        getListCategorySPDItem();
      }
      $ionicLoading.hide();
    };

    var successListCategorySPDItem = function (res) {
      if (res != null) {
        $scope.optCategorySPDItem = res;
        $scope.getListItemByCategory();
      }

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };

    function initModule() {
      $scope.detail = [];
      $scope.attachment = "img/placeholder.png";
      $scope.chooseTab('general');

      $scope.pageMode = "APPROVALHISTORY";

      if ($location.path().startsWith("/app/approvalhistoryspdadvance")) {
        $scope.spdMode = "ADVANCE"
      } else if ($location.path().startsWith("/app/approvalhistoryspdrealization")) {
        $scope.spdMode = "REALIZATION"
      }


      $scope.confirm = {
        notes: ""
      };
    }

    $scope.downloadDocs = function (location) {
      window.open(encodeURI(location), "_system", "location=yes");
      return false;
    };

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });

  })

;
