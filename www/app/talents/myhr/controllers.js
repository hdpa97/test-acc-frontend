angular.module('myhr.controllers', [])

  .controller('MyHRCtrl',
    ['$timeout',
      '$rootScope',
      '$scope',
      '$state',
      'AuthenticationService',
      '$ionicModal',
      'Main',
      '$cordovaLocalNotification',
      '$ionicLoading',
      '$localStorage',
      '$ionicPopup',

      function (
        $timeout,
        $rootScope,
        $scope,
        $state,
        AuthenticationService,
        $ionicModal,
        Main,
        $cordovaLocalNotification,
        $ionicLoading,
        $localStorage,
        $ionicPopup,
        ionicSuperPopup) {

        if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
          $state.go("login");
        }

        // CHECK PERATURAN PERUSAHAAN

        /*
          function getAddress() {
            var accessToken = Main.getSession("token").access_token;
            var urlApi = Main.getUrlApi() + '/api/user/address';
            Main.requestApi(accessToken, urlApi, successRequestAddress, $scope.errorRequest);
          }
        */

        $scope.HideErrorRequest = function (err, status) {
          $ionicLoading.hide();
        }

        var successRequestAddress = function (res) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 500);
          $scope.item = res[0];
        }

        $scope.nDate = new Date();
        $scope.Ytoday = $scope.nDate.getFullYear();

        function cekSubmitPP() {
          var npk = $scope.profile.employeeTransient.assignment.employeeNo;
          var urlApi = Main.getPPUrlApi() + "/getHist";
          var body = {
            "getHistInput": {
              "NPK": npk,
              "YEAR": $scope.Ytoday
            }
          };

          var data = JSON.stringify(body);
          Main.postRequestApiPP(urlApi, data, successCekSubmitPP, $scope.HideErrorRequest);
        }

        var successCekSubmitPP = function (res) {
          $scope.pp = res;
          if ($scope.pp.OUT_DATA.ID) {
            $scope.SudahSubmit = true;
          } else {
            $scope.bukaModalTest.show();
          }
          $timeout(function () {
            $ionicLoading.hide();
          }, 1000);
        }

        $scope.errorRequestPP = function (err) {
          console.log(err);
        }
        var successGetData = function (res) {
          console.log(res);
        }
        // END CHECK PERATURAN PERUSAHAAN

        // DAILY REMINDER TECAT

        $scope.notif = {};

        $scope.errorCekNotif = function (err, status) {
          console.log('failed to cek notif')
          $ionicLoading.hide();
        }

        function cekNotifSiang() {
          var npk = $scope.profile.employeeTransient.assignment.employeeNo;
          var urlApi = Main.getTecatUrlApi() + "/api/get-quiz?npk=" + npk;
          Main.getApi(
            urlApi,
            successCekNotifSiang,
            $scope.errorCekNotif
          );

        }

        var successCekNotifSiang = function (res) {
          $scope.soal = res;
          if (res != 0) {
            let start = $scope.soal[0].time_start;
            $scope.start_at = new Date(start);
            $scope.question = $scope.soal[0].mst_question.question;
            $scope.attempts = $scope.soal[0].attempts_id;
            $scope.radioButtons = [{
                opsi: "A",
                text: $scope.soal[0].mst_question.mst_question_answers[0].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[0].question_answer_id
              },
              {
                opsi: "B",
                text: $scope.soal[0].mst_question.mst_question_answers[1].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[1].question_answer_id
              },
              {
                opsi: "C",
                text: $scope.soal[0].mst_question.mst_question_answers[2].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[2].question_answer_id
              },
              {
                opsi: "D",
                text: $scope.soal[0].mst_question.mst_question_answers[3].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[3].question_answer_id
              },
            ];
            getNotifSiang();
          }
        }

        var successGetNotifSiang = function (res) {

          $scope.notif = res;
          var text = $scope.notif[0].template;
          var id = $scope.notif[0].notification_id;

          $cordovaLocalNotification.add({
            id: id,
            title: "Daily Reminder",
            text: text,
            icon: 'https://i.ibb.co/6BjzQJg/Icon-36.png',
            led: 'FF0000'
          });

          cordova.plugins.notification.local.on("click", function (notification, state) {
            getSoalFromNotif();
          }, this);

        }

        function getNotifSiang() {
          var urlApi = Main.getTecatUrlApi() + "/api/get-notif-daily";
          Main.getApi(
            urlApi,
            successGetNotifSiang,
            $scope.errorCekNotif
          );
        }

        // ** SOAL FROM NOTIF **

        $scope.errorRequestGetSoalFromNotif = function () {
          $state.go("app.myhr");
        }

        var successGetSoalFromNotif = function (res) {
          $scope.soal = res;
          if (res != 0) {
            let start = $scope.soal[0].time_start;
            $scope.start_at = new Date(start);
            $scope.question = $scope.soal[0].mst_question.question;
            $scope.attempts = $scope.soal[0].attempts_id;
            $scope.radioButtons = [{
                opsi: "A",
                text: $scope.soal[0].mst_question.mst_question_answers[0].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[0].question_answer_id
              },
              {
                opsi: "B",
                text: $scope.soal[0].mst_question.mst_question_answers[1].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[1].question_answer_id
              },
              {
                opsi: "C",
                text: $scope.soal[0].mst_question.mst_question_answers[2].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[2].question_answer_id
              },
              {
                opsi: "D",
                text: $scope.soal[0].mst_question.mst_question_answers[3].answer,
                value: $scope.soal[0].mst_question.mst_question_answers[3].question_answer_id
              },
            ];
            $scope.modalSoalNotif.show();
          } else {
            $state.go("app.myhr");
          }
        }

        function getSoalFromNotif() {
          var npk = $scope.profile.employeeTransient.assignment.employeeNo;
          var urlApi = Main.getTecatUrlApi() + "/api/get-quiz?npk=" + npk;
          Main.getApi(
            urlApi,
            successGetSoalFromNotif,
            $scope.errorRequestGetSoalFromNotif
          );
        }

        // ** MODAL NOTIF **

        $ionicModal.fromTemplateUrl('app/intro/showModalNotif.html', {
          scope: $scope,
          animation: 'fade-in-scale',
          backdropClickToClose: false
        }).then(function (modal) {
          $scope.modalSoalNotif = modal;
        });

        $scope.closeSoalNotif = function () {
          $scope.modalSoalNotif.hide();
        };

        $scope.openSoalNotif = function () {
          $scope.modalSoalNotif.show();
        };

        // ** SUBMIT ANSWER NOTIF **

        $scope.soal = {};

        function verificationFormAnswer(usertalent) {
          if ($scope.soal.answer == undefined || $scope.soal.answer == '') {
            messageValidation = "Please answer the question";
            return false;
          }
          return true;
        }

        $scope.submitFormNotif = function () {
          if (verificationFormAnswer($scope.soal)) {

            var urlAPI = Main.getTecatUrlApi() + '/api/answer-quiz';
            var keyMitra;
            var kodePerusahaan;

            var data = {
              npk: $scope.profile.employeeTransient.assignment.employeeNo,
              attempts_id: $scope.attempts,
              question_answer_id: $scope.soal.answer,
            }

            data = JSON.stringify(data);

            var result = Main.postApi(
              urlAPI,
              data,
              $scope.successRequestNotif,
              $scope.errorRequest
            );

            $scope.disableButton = "true";

          } else {

            $ionicPopup.confirm({
              title: "Warning",
              template: messageValidation
            });
          }
        };

        $scope.successRequestNotif = function (res) {
          $scope.modalSoalNotif.hide();
          if (res == 4) // jika salah dan tepat waktu
          {
            notifOTUnCorrectAnswer();
          } else if (res == 3) // jika salah tapi tidak tepat
          {
            notifUnCorrectAnswer();
          } else if (res == 2) // jika benar dan tepat waktu
          {
            notifOTCorrectAnswer();
          } else if (res == 1) // jika benar tapi tidak tepat waktu
          {
            notifCorrectAnswer();
          }
        };

        // ** FEEDBACK NOTIFICATION **

        function notifOTUnCorrectAnswer() {
          var urlApi = Main.getTecatUrlApi() + "/api/get-notif-ot-uncorrect-answer";
          Main.getApi(
            urlApi,
            successOTUnCorrect,
            $scope.errorCekNotif
          );
        }

        var successOTUnCorrect = function (res) {
          $scope.correct = res;

          var message1 = $scope.correct[0].default[0].template;
          var message2 = $scope.correct[0].on_time[0].template;

          var myPop = $ionicPopup.alert({
            title: 'Warning',
            template: message1 + "<br><br>" + message2
          });


          myPop.then(function () {
            $timeout(function () {}, 2500);
          });

          $timeout(function () {
            myPop.close();
          }, 2500);

          // ** POST LOG NOTIF **

          var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
          var keyMitra;
          var kodePerusahaan;

          var data = {
            npk: $scope.profile.employeeTransient.assignment.employeeNo,
            notification_id: $scope.correct[0].default[0].notification_id,
            keterangan: "on time answered",
          }

          data = JSON.stringify(data);
          var result = Main.postApi(
            urlAPI,
            data,
            $scope.successRequest,
            $scope.errorPushNotif
          );

        }

        function notifOTCorrectAnswer() {

          var urlApi = Main.getTecatUrlApi() + "/api/get-notif-ot-correct-answer";
          Main.getApi(
            urlApi,
            successOTCorrect,
            $scope.errorRequest
          );
        }

        var successOTCorrect = function (res) {
          $scope.correct = res;
          var message1 = $scope.correct[0].default[0].template;
          var message2 = $scope.correct[0].on_time[0].template;

          var myPop = $ionicPopup.alert({
            title: 'Success',
            template: message1 + "<br><br>" + message2
          });


          myPop.then(function () {
            $timeout(function () {}, 2500);
          });

          $timeout(function () {
            myPop.close();
          }, 2500);

          // ** POST LOG NOTIF **

          var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
          var keyMitra;
          var kodePerusahaan;

          var data = {
            npk: $scope.profile.employeeTransient.assignment.employeeNo,
            notification_id: $scope.correct[0].default[0].notification_id,
            keterangan: "on time answered",
          }

          data = JSON.stringify(data);
          var result = Main.postApi(
            urlAPI,
            data,
            $scope.successRequest,
            $scope.errorPushNotif
          );

        }

        function notifCorrectAnswer() {
          var urlApi = Main.getTecatUrlApi() + "/api/get-notif-correct-answer";
          Main.getApi(
            urlApi,
            successCorrect,
            $scope.errorRequest
          );
        }

        var successCorrect = function (res) {

          $scope.correct = res;
          var message = $scope.correct[0].template;

          var myPop = $ionicPopup.alert({
            title: 'Success',
            template: message
          });

          myPop.then(function () {
            $timeout(function () {}, 2500);
          });

          $timeout(function () {
            myPop.close();
          }, 2500);

          // ** POST LOG NOTIF **

          var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
          var keyMitra;
          var kodePerusahaan;

          var data = {
            npk: $scope.profile.employeeTransient.assignment.employeeNo,
            notification_id: $scope.correct[0].notification_id,
            keterangan: "-",
          }

          data = JSON.stringify(data);
          var result = Main.postApi(
            urlAPI,
            data,
            $scope.successRequest,
            $scope.errorPushNotif
          );

        }

        function notifUnCorrectAnswer() {
          var urlApi = Main.getTecatUrlApi() + "/api/get-notif-uncorrect-answer";
          Main.getApi(
            urlApi,
            successUnCorrect,
            $scope.errorRequest
          );
        }

        var successUnCorrect = function (res) {
          $scope.uncorrect = res;
          var message = $scope.uncorrect[0].template;

          var myPop = $ionicPopup.alert({
            title: 'Warning',
            template: message
          });

          myPop.then(function () {
            $timeout(function () {}, 2500);
          });

          $timeout(function () {
            myPop.close();
          }, 2500);

          // ** POST LOG NOTIF **

          var urlAPI = Main.getTecatUrlApi() + '/api/post-notif-log';
          var keyMitra;
          var kodePerusahaan;

          var data = {
            npk: $scope.profile.employeeTransient.assignment.employeeNo,
            notification_id: $scope.uncorrect[0].notification_id,
            keterangan: "-",
          }

          data = JSON.stringify(data);
          var result = Main.postApi(
            urlAPI,
            data,
            $scope.successRequest,
            $scope.errorPushNotif
          );

        }

        $scope.errorPushNotif = function (err, status) {
          $ionicLoading.hide();
        }

        cekNotifSiang();

        // END OF DAILY REMINDER TECAT

        $scope.version = "";

        function checkChangePassword() {
          if ($scope.profile != undefined) {
            if ($scope.profile.isChangePassword == null || !$scope.profile.isChangePassword) {
              $scope.goToPage("app.changepassword");
            }
          }

        }


        function checkVersionApp() {
          if ($scope.profile != undefined && $scope.profile.companySettings != undefined) {
            if ($scope.profile.companySettings.mobileVersion != null && $scope.profile.companySettings.mobileVersion > Main.getVersion()) {
              $scope.mobileDownload = $scope.profile.companySettings.mobileDownload;
              $scope.modalUpdate.show();
            }

          }

        }
        $scope.refresh = function () {
          $scope.profile = Main.getSession("profile");
          $scope.$broadcast('scroll.refreshComplete');
        }


        var successProfile = function (res) {
          $scope.profile = res;
          $scope.profile.fullname = $scope.profile.firstName + " " + $scope.profile.lastName;

        }


        var successVersion = function (res) {
          $scope.version = res.description;

        }

        // show update modal
        $ionicModal.fromTemplateUrl('app/intro/update-version.html', {
          scope: $scope,
          animation: 'fade-in-scale',
          backdropClickToClose: false
        }).then(function (modal) {
          $scope.modalUpdate = modal;
        });

        $ionicModal.fromTemplateUrl('app/intro/suratPerPerusahaanModal.html', {
          scope: $scope,
          animation: 'fade-in-scale',
          backdropClickToClose: false
        }).then(function (modal) {
          $scope.bukaModalTest = modal;
        });

        $scope.openModal = function () {
          $scope.bukaModalTest.show();
        };

        $scope.closeModal = function () {
          $scope.bukaModalTest.hide();
        };

        $scope.openPP = function () {
          window.open($rootScope.url);
          // var ref = cordova.InAppBrowser.open('https://bit.ly/PeraturanPerusahaan2020ACC', '_blank', 'location=yes');
        };

        $scope.nDate = new Date();
        $scope.Ytoday = $scope.nDate.getFullYear();

        function getUrlPP() {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });

          var urlApi = Main.getPPUrlApi() + "/getHistUrl";
          var body = {
            "getHistUrl": {
              "YEAR": $scope.Ytoday
            }
          };

          var data = JSON.stringify(body);
          Main.postRequestApiPP(urlApi, data, successGetDataUrlPP, $scope.HideErrorRequest);
        }

        var successGetDataUrlPP = function (res) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 500);
          $scope.pp = res;
          if ($scope.pp.OUT_MESS == "Success") {
            $rootScope.url = $scope.pp.OUT_DATA.URL;
            $rootScope.periode = $scope.pp.OUT_DATA.PERIODE_PP.replace(/[\s]/g, '');;
          }
        }

        function getAddress() {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/user/address';
          Main.requestApi(accessToken, urlApi, successRequestAddress, $scope.errorRequest);
        }

        var successRequestAddress = function (res) {
          $timeout(function () {
            $ionicLoading.hide();
          }, 500);
          $scope.item = res[0];
        }
        $scope.acceptButton = function () {

          $ionicLoading.show({
            template: "<ion-spinner></ion-spinner>"
          });
          var urlApi = Main.getPPUrlApi() + "/insertHist";

          $scope.div = $scope.profile.employeeTransient.assignment.division;

          if ($scope.div == null || $scope.div == "") {
            $scope.div = $scope.profile.employeeTransient.assignment.organizationName;
          }

          var body = {
            "insertHistInput": {
              "PERIODE_PP": $scope.periode.replace(/[\s]/g, ''),
              "NPK": $scope.profile.employeeTransient.assignment.employeeNo,
              "NAMA": $scope.profile.employeeTransient.name,
              "POSISI": $scope.profile.employeeTransient.assignment.positionName,
              "DEPT": $scope.profile.employeeTransient.assignment.organizationName,
              "DIVISI": $scope.div
            }
          };
          var data = JSON.stringify(body);
          Main.postRequestApiPP(urlApi, data, successAccept, $scope.HideErrorRequest);

        };

        var successAccept = function (res) {
          $ionicLoading.hide();
          $scope.bukaModalTest.hide();

          var myPop = $ionicPopup.alert({
            title: 'Info',
            template: 'Submit berhasil..'
          });

          $timeout(function () {
            myPop.close();
          }, 2000);
        }

        $scope.$on('$ionicView.beforeEnter', function () {
          cekSubmitPP();
          getUrlPP();
          getAddress();
        });

        // $scope.acceptButton = function () {
        //   //   window.plugins.toast.showWithOptions({
        //   //     message: "Berhasil menyetujui surat peraturan perusahaan",
        //   //     duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
        //   //     position: "bottom",
        //   //     addPixelsY: -40 // added a negative value to move it up a bit (default 0)
        //   //   },
        //   //   function(args) {
        //   //     console.log(args.event);
        //   //     //This will print 'hide'
        //   // },
        //   // function(error) {
        //   //     console.error('toast error: ', error);
        //   // });

        //   $ionicLoading.show({
        //     template: 'Berhasil menyetujui surat!',
        //     noBackdrop: true,
        //     duration: 2000
        //   });

        //   $scope.closeModal();
        // }

        function initMethod() {

          $scope.profile = Main.getSession("profile");
          getVersion();
          if ($scope.profile == undefined)
            getProfile();

          $timeout(function () {
            //checkChangePassword();
            if (Main.getAppMode() == 'mobile')
              checkVersionApp();
          }, 2000);

        }
        // invalid access token error: "invalid_token" 401
        function getProfile() {
          if (Main.getSession("token") != null) {
            var accessToken = Main.getSession("token").access_token;
            var urlApi = Main.getUrlApi() + '/api/myprofile';
            Main.requestApi(accessToken, urlApi, successProfile, $scope.errorRequest);
          }

        }


        function getVersion() {
          if (Main.getSession("token") != null) {
            var accessToken = Main.getSession("token").access_token;
            var urlApi = Main.getUrlApi() + '/api/talentsVersion';
            Main.requestApi(accessToken, urlApi, successVersion, $scope.errorRequest);
          }

        }

        $scope.$on('$ionicView.beforeEnter', function () {
          initMethod();
        });

      }
    ])


  .controller('PersonalCtrl', ['$ionicLoading', 'ionicSuperPopup', '$rootScope', '$scope', '$state', 'AuthenticationService', 'Main', '$filter', function ($ionicLoading, ionicSuperPopup, $rootScope, $scope, $state, AuthenticationService, Main, $filter) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.goChangeMaritalStatus = function (currentStatus, dataApprovalId) {
      $state.go('app.changemaritalstatus', {
        'currentStatus': currentStatus,
        'dataApprovalId': dataApprovalId
      });
    };


    $scope.goChangeNpwp = function (currentStatus, dataApprovalId) {
      $state.go('app.changenpwp', {
        'currentStatus': currentStatus,
        'dataApprovalId': dataApprovalId
      });
    };

    $scope.goChangeNik = function (currentStatus, dataApprovalId) {
      $state.go('app.changenircno', {
        'currentStatus': currentStatus,
        'dataApprovalId': dataApprovalId
      });
    };


    $scope.goChangeKtpName = function (currentStatus, dataApprovalId) {
      $state.go('app.changektpname', {
        'currentStatus': currentStatus,
        'dataApprovalId': dataApprovalId
      });
    };



    $scope.goChangeFamilyCardNo = function (currentStatus, dataApprovalId) {

      // ('changefamilycardno');
      $state.go('app.changefamilycardno', {
        'currentStatus': currentStatus,
        'dataApprovalId': dataApprovalId
      });
    };


    $scope.profile = JSON.parse(localStorage.getItem("profile"));
    $scope.personal = {};
    $scope.birthDate = "";
    $scope.marriedDate = "";

    $scope.zipCode = "";



    var successRequest = function (res) {
      // ('response');
      // (res);
      $ionicLoading.hide();
      $scope.personal = res;
      $scope.birthDate = $filter('date')(new Date($scope.personal.birthDate), 'dd MMMM yyyy');

      if ($scope.personal.marriedDate != null) {
        $scope.marriedDate = $filter('date')(new Date($scope.personal.marriedDate), 'dd MMMM yyyy');
      }

      // ('address log');
      // (res.address);
      $scope.personal.showMaritalStatus = $scope.personal.maritalStatus;
      if ($scope.personal.maritalStatusDataApproval != null) {
        $scope.personal.showMaritalStatus = $scope.personal.changeMaritalStatus;
      }
      $scope.personal.addressShow = false;
      $scope.personal.addressFull = "";
      if (res.address != null) {
        // ('address log');
        // (address);
        address = res.address;
        $scope.personal.addressFull += " " + address.address + " RT. " + address.rt + " RW. " + address.rw + " " + address.city + ", " + address.province;
        $scope.zipCode = address.zipCode;
      }
      $scope.$broadcast('scroll.refreshComplete');

      // ($scope.personal);
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      //if(data.direction != undefined && data.direction!='back')
      initMethod();

    });

    function initMethod() {

      getPersonal();
    }

    function getPersonal() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/myprofile/personal';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    // ('Data personal : ');
    // (getPersonal());
    // Main.refreshToken("4c648f69-5158-4260-a47f-e7793c6a952e", resRefreshToken, errRefreshToken);

  }])




  .controller('FamilyCtrl', function ($ionicHistory, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.family = [];
    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshFamilyCtrl) {
        initMethod();
      }
      $rootScope.refreshFamilyCtrl = false;

    });

    $scope.goToAddFamily = function () {
      $state.go('app.addfamily', {
        'id': 0
      });
    };


    $scope.goToDetails = function (idx) {
      $state.go('app.detailfamily', {
        'idx': idx,
        'edit': 'true'
      });
    };

    $scope.refresh = function () {
      initMethod();
    }


    // $scope.goToAddFamily = function (page) {

    //     $state.go(page);
    //     $ionicHistory.nextViewOptions({
    //         disableAnimate: false,
    //         disableBack: false
    //     });
    // }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.family = res;

      // ($scope.family);
      $scope.$broadcast('scroll.refreshComplete');
    }



    initMethod();
    //31acd2e6-e891-4628-a24e-58e408664516
    function initMethod() {
      getFamily();
    }
    // invalid access token error: "invalid_token" 401
    function getFamily() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/family';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }


  })



  .controller('DetailFamilyCtrl', function ($ionicHistory, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.canEdit = $stateParams.edit;
    var familyId = $stateParams.idx;
    $scope.family = {};

    $scope.goEditFamily = function () {
      $state.go('app.addfamily', {
        id: familyId
      });
      $ionicHistory.nextViewOptions({
        disableAnimate: false,
        disableBack: false
      });
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.family = res;
    }

    function getDetailFamily(familyId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/family/' + familyId;
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initMethod();
    });

    function initMethod() {
      getDetailFamily(familyId);
    }


  })


  .controller('EditFamilyCtrl', function (ionicDatePicker, $filter, $ionicHistory, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var genderEmp = Main.getSession("profile").employeeTransient.gender;
    var familyIdx = $stateParams.idx;
    var messageValidation = "";
    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refSelectAlive = Main.getDataReference(arrCompanyRef, 'myhr', 'family', 'alive');
    $scope.selectAliveStatus = []; // Main.getSelectProvince();

    $scope.familyData = {};

    if ($scope.family.birthDate != null)
      $scope.family.birthDate = new Date($scope.family.birthDate);
    else
      $scope.family.birthDate = new Date();

    $scope.selectMaritalStatus = Main.getSelectMaritalStatus();
    $scope.selectRelationship = Main.getSelectFamilyRelationShip();
    $scope.selectBloodType = Main.getSelectBloodType();
    $scope.selectGender = Main.getSelectGender();
    $scope.isEdit = true;

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack('app.family');
    }

    var datepicker = {
      callback: function (val) { //Mandatory
        $scope.family.birthDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    // invalid access token error: "invalid_token" 401
    function verificationForm(family) {
      if (family.name == undefined || family.name == '') {
        messageValidation = "Name can't be empty";
        return false;
      } else if (family.birthPlace == undefined || family.birthPlace == '') {
        messageValidation = "Place of Birth can't be empty";
        return false;
      } else if (family.birthDate == undefined) {
        messageValidation = "Date of Birth can't be empty";
        return false;
      } else if (family.aliveStatus == undefined || family.aliveStatus == '') {
        messageValidation = "Alive Status can't be empty";
        return false;
      } else if (family.gender == undefined || family.gender == '') {
        messageValidation = "Gender can't be empty";
        return false;
      } else if (family.relationship == undefined || family.relationship == '') {
        messageValidation = "Relationship can't be empty";
        return false;
      } else if (family.maritalStatus == undefined || family.maritalStatus == '') {
        messageValidation = "Marital Status can't be empty";
        return false;
      } else if (family.bloodType == undefined || family.bloodType == '') {
        messageValidation = "Blood Type can't be empty";
        return false;
      }


      // if date of death
      if (family.aliveStatus == 'Passed Away') {
        if (family.dateOfDeath == undefined || family.dateOfDeath == '') {
          messageValidation = "Date Of Death can't be empty";
          return false;
        } else {
          // added by JUW 31 Jan 2019. Passed Away date cannot before than date of birth
          var dateOfDeath = new Date(family.dateOfDeath);
          if (dateOfDeath <= birthDate) {
            messageValidation = "Date Of Death have to be after Date of Birth";
            return false;
          }
        }
      }

      // if marital satus married
      if (family.maritalStatus == 'Married') {
        if (family.marriedDate == undefined || family.marriedDate == '') {
          messageValidation = "Date Of Married can't be empty";
          return false;
        } else {
          // added by JUW 31 Jan 2019. Married date cannot before than date of birth
          var marriedDate = new Date(family.marriedDate);
          if (marriedDate <= birthDate) {
            messageValidation = "Date Of Married have to be after Date of Birth";
            return false;
          }
        }
      }



      // if marital status divorce
      if (family.maritalStatus == 'Divorce') {
        if (family.divorcedDate == undefined || family.divorcedDate == '') {
          messageValidation = "Date Of divorce can't be empty";
          return false;
        } else {
          // added by JUW 31 Jan 2019. Date of Divorce cannot before than date of birth
          var divorcedDate = new Date(family.divorcedDate);
          if (divorcedDate <= birthDate) {
            messageValidation = "Date Of Divorce have to be after Date of Birth";
            return false;
          }
        }
      }





      if (genderEmp != undefined && family.relationship != undefined) {
        if (family.relationship == 'Suami' && genderEmp == "Male") {
          messageValidation = "You can't select this item in relationship. Your gender is '" + genderEmp + "'.";
          return false;
        }

        if (family.relationship == 'Istri' && genderEmp == "Female") {
          messageValidation = "You can't select this item in relationship. Your gender is '" + genderEmp + "'.";
          return false;
        }
      }

      return true;
    }


    $scope.submitForm = function () {

      var birthDate = new Date($scope.family.birthDate);
      var today = new Date();

      if (birthDate >= today) {
        $scope.warningAlert("Cannot Future date, please check your birthDate");
      } else {
        if (verificationForm($scope.family)) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });

          var birthDate = null;
          if ($scope.family.birthDate != null)
            birthDate = $filter('date')($scope.family.birthDate, 'yyyy-MM-dd');

          var dataEdit = {
            aliveStatus: $scope.family.aliveStatus,
            name: $scope.family.name,
            birthPlace: $scope.family.birthPlace,
            birthDate: birthDate,
            gender: $scope.family.gender,
            relationship: $scope.family.relationship,
            address: $scope.family.address,
            maritalStatus: $scope.family.maritalStatus,
            bloodType: $scope.family.bloodType,
            occupation: $scope.family.occupation,
            phone: $scope.family.phone
          };
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/user/family/' + $scope.family.id;
          var data = JSON.stringify(dataEdit);

          // ('get data edit');
          // (data);

          Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
        } else {
          $scope.warningAlert(messageValidation);
        }
      }
    }

    function initMethod() {
      if (refSelectAlive != undefined && refSelectAlive != '') {
        $scope.selectAliveStatus = JSON.parse(refSelectAlive);
      }
    }
    initMethod();


  })


  .controller('AddFamilyCtrl', function ($stateParams, ionicSuperPopup, $filter, ionicDatePicker, $ionicPopup, appService, $ionicActionSheet, $cordovaCamera, $ionicHistory, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    var genderEmp = Main.getSession("profile").employeeTransient.gender;
    var familyId = $stateParams.id;
    var arrCompanyRef = Main.getSession("profile").companyReference;
    // (arrCompanyRef);

    var refSelectAlive = Main.getDataReference(arrCompanyRef, 'myhr', 'family', 'alive');
    var refSelectNationality = Main.getDataReference(arrCompanyRef, 'myhr', 'family', 'nationality');
    var refSelectGender = Main.getDataReference(arrCompanyRef, 'myhr', 'family', 'gender');
    var refSelectMaritalStatus = Main.getDataReference(arrCompanyRef, 'myhr', 'family', 'maritalstatus');
    var refSelectRelationship = Main.getDataReference(arrCompanyRef, 'myhr', 'family', 'relationship');

    $scope.image = "img/placeholder.png";
    $scope.family = {};
    // (Main.getSession("profile"));
    $scope.appMode = Main.getAppMode();
    $scope.selectBloodType = Main.getSelectBloodType();
    $scope.selectAliveStatus = []; // Main.getSelectProvince();
    $scope.selectNationality = [];
    $scope.selectGender = [];
    $scope.selectRelationship = [];
    $scope.selectMaritalStatus = [];
    $scope.imageData = null;
    $scope.isEdit = false;
    $scope.family.images = [];
    $scope.family.imagesData = [];
    $scope.buttonDisable = null;
    var messageValidation = "";

    $scope.dataDiedShow = 0;
    $scope.dataMarriedShow = 0;
    $scope.dataDivorceShow = 0;

    var datepicker = {
      callback: function (val) { //Mandatory
        // (val);
        $scope.family.birthDate = val;

      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };



    var datepicker1 = {
      callback: function (val) { //Mandatory
        // (val);
        $scope.family.dateOfDeath = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };


    var datepicker2 = {
      callback: function (val) { //Mandatory
        // (val);
        $scope.family.marriedDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };


    var datepicker3 = {
      callback: function (val) { //Mandatory
        // (val);
        $scope.family.divorcedDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    $scope.openDatePicker1 = function () {
      ionicDatePicker.openDatePicker(datepicker1);
    };

    $scope.openDatePicker2 = function () {
      ionicDatePicker.openDatePicker(datepicker2);
    };

    $scope.openDatePicker3 = function () {
      ionicDatePicker.openDatePicker(datepicker3);
    };


    $scope.removeChoice = function () {
      var lastItem = $scope.family.imagesData.length - 1;
      $scope.family.imagesData.splice(lastItem);
      $scope.family.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.family.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }
      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  // alert(imageData);
                  // $rootScope.user.photo = "data:image/jpeg;base64," + imageData;
                  $scope.family.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.family.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.family.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.family.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.addPicture1 = function () {
      if ($scope.family.images.length > 3) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }

      $scope.family.images.push({
        'image': "img/placeholder.png"
      });
    }

    $scope.takePicture = function () {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: 750,
        targetHeight: 550,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }

    $scope.resetForm = function () {
      $scope.family = {};
    }

    function sendData() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var attachment = [];
      if ($scope.family.imagesData.length > 0) {
        for (var i = $scope.family.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.family.imagesData[i].image
            };
          } else {
            if ($scope.family.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.family.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);
        };
      }

      if ($scope.family.birthDate != undefined || $scope.family.birthDate != null)
        $scope.family.birthDate = $filter('date')($scope.family.birthDate, "yyyy-MM-dd");

      if ($scope.family.dateOfDeath != undefined && $scope.family.dateOfDeath != null)
        $scope.family.dateOfDeath = $filter('date')($scope.family.dateOfDeath, "yyyy-MM-dd");

      $scope.family.attachments = attachment;
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/family';
      if ($stateParams.id != undefined && $stateParams.id != '0') {
        urlApi = Main.getUrlApi() + '/api/user/family/' + $stateParams.id;
      }
      var data = JSON.stringify($scope.family);
      // ('data family');
      // (data);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);

    }
    $scope.submitForm = function () {

      var birthDate = new Date($scope.family.birthDate);
      var today = new Date();

      if (birthDate >= today) {
        $scope.warningAlert("Cannot Future date, please check your birthDate");
      } else if ($scope.family.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        if (verificationForm($scope.family)) {
          ionicSuperPopup.show({
              title: "Are you sure?",
              text: "Are you sure the data submitted is correct ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              closeOnConfirm: true
            },
            function (isConfirm) {
              if (isConfirm) {
                sendData();
                // disable button for handle repeat submit
                $scope.buttonDisable = 1;
              }


            });
        } else {
          $scope.warningAlert(messageValidation);
        }
      }
    }


    $scope.showDied = function (data) {
      // alert(data);
      if (data == "Passed Away" || data == "passed away") {
        $scope.dataDiedShow = 1;
      } else {
        $scope.family.dateOfDeath = null;
        $scope.dataDiedShow = 0;
      }
    }


    $scope.openDateStatus = function (data) {
      if (data == "Married" || data == "married") {
        $scope.dataMarriedShow = 1;
        $scope.dataDivorceShow = 0;
        $scope.family.divorcedDate = null;
      } else if (data == "Divorce" || data == "divorce") {
        $scope.dataDivorceShow = 1;
        $scope.dataMarriedShow = 0;
        $scope.family.marriedDate = null;
      } else {
        $scope.dataDivorceShow = 0;
        $scope.dataMarriedShow = 0;
      }
    }


    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshFamilyCtrl = true;
      $scope.goBack('app.family');
    }

    var successDetailRequest = function (res) {
      $ionicLoading.hide();
      $scope.family = res;

      if (res.deceaseDate != null) {
        $scope.family.dateOfDeath = res.deceaseDate;
      }

      $scope.family.images = [];
      $scope.family.imagesData = [];
    }

    function getDetailFamily(familyId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/family/' + familyId;
      Main.requestApi(accessToken, urlApi, successDetailRequest, $scope.errorRequest);
    }

    function initMethod() {
      if (refSelectAlive != undefined && refSelectAlive != '') {
        $scope.selectAliveStatus = JSON.parse(refSelectAlive);
      }

      if (refSelectNationality != undefined && refSelectNationality != '') {
        $scope.selectNationality = JSON.parse(refSelectNationality);
      }
      if (refSelectGender != undefined && refSelectGender != '') {
        $scope.selectGender = JSON.parse(refSelectGender);
      }

      if (refSelectMaritalStatus != undefined && refSelectMaritalStatus != '') {
        $scope.selectMaritalStatus = JSON.parse(refSelectMaritalStatus);
      }

      if (refSelectRelationship != undefined && refSelectRelationship != '') {
        $scope.selectRelationship = JSON.parse(refSelectRelationship);
      }

      if ($stateParams.id != undefined && $stateParams.id != '0') {
        getDetailFamily($stateParams.id);
      }
    }

    // invalid access token error: "invalid_token" 401
    function verificationForm(family) {
      if (family.name == undefined || family.name == '') {
        messageValidation = "Name can't be empty";
        return false;
      } else if (family.birthPlace == undefined || family.birthPlace == '') {
        messageValidation = "Place of Birth can't be empty";
        return false;
      } else if (family.birthDate == undefined) {
        messageValidation = "Date of Birth can't be empty";
        return false;
      } else if (family.aliveStatus == undefined || family.aliveStatus == '') {
        messageValidation = "Alive Status can't be empty";
        return false;
      } else if (family.gender == undefined || family.gender == '') {
        messageValidation = "Gender can't be empty";
        return false;
      } else if (family.relationship == undefined || family.relationship == '') {
        messageValidation = "Relationship can't be empty";
        return false;
      } else if (family.maritalStatus == undefined || family.maritalStatus == '') {
        messageValidation = "Marital Status can't be empty";
        return false;
      } else if (family.nircNo == undefined || family.nircNo == '') {
        messageValidation = "Identity Card No can't be empty";
        return false;
      } else if (family.familyCardNo == undefined || family.familyCardNo == '') {
        messageValidation = "Family Card No can't be empty";
        return false;
      } else if (family.district == undefined || family.district == '') {
        messageValidation = "District can't be empty";
        return false;
      } else if (family.subDistrict == undefined || family.subDistrict == '') {
        messageValidation = "Sub District can't be empty";
        return false;
      } else if (family.rt == undefined || family.rt == '') {
        messageValidation = "RT can't be empty";
        return false;
      } else if (family.rw == undefined || family.rw == '') {
        messageValidation = "RW can't be empty";
        return false;
      } else if (family.address == undefined || family.address == '') {
        messageValidation = "Address can't be empty";
        return false;
      } else if (family.nationality == undefined || family.nationality == '') {
        messageValidation = "Nationality can't be empty";
        return false;
      } else if (family.bloodType == undefined || family.bloodType == '') {
        messageValidation = "Blood Type can't be empty";
        return false;
      }


      // if date of death
      if (family.aliveStatus == 'Passed Away') {
        if (family.dateOfDeath == undefined || family.dateOfDeath == '') {
          messageValidation = "Date Of Death can't be empty";
          return false;
        } else {
          // added by JUW 31 Jan 2019. Passed Away date cannot before than date of birth
          var dateOfDeath = new Date(family.dateOfDeath);
          var birthDate = new Date(family.birthDate);
          if (dateOfDeath <= birthDate) {
            messageValidation = "Date Of Death have to be after Date of Birth";
            return false;
          }
        }
      }

      // if marital satus married
      if (family.maritalStatus == 'Married') {
        if (family.marriedDate == undefined || family.marriedDate == '') {
          messageValidation = "Date Of Married can't be empty";
          return false;
        } else {
          // added by JUW 31 Jan 2019. Married date cannot before than date of birth
          var marriedDate = new Date(family.marriedDate);
          var birthDate = new Date(family.birthDate);
          if (marriedDate <= birthDate) {
            messageValidation = "Date Of Married have to be after Date of Birth";
            return false;
          }
        }
      }



      // if marital status divorce
      if (family.maritalStatus == 'Divorce') {
        if (family.divorcedDate == undefined || family.divorcedDate == '') {
          messageValidation = "Date Of divorce can't be empty";
          return false;
        } else {
          // added by JUW 31 Jan 2019. Date of Divorce cannot before than date of birth
          var divorcedDate = new Date(family.divorcedDate);
          if (divorcedDate <= birthDate) {
            messageValidation = "Date Of Divorce have to be after Date of Birth";
            return false;
          }
        }
      }

      if (genderEmp != undefined && family.relationship != undefined) {
        if (family.relationship == 'Suami' && genderEmp == "Male") {
          messageValidation = "You can't select this item in relationship. Your gender is '" + genderEmp + "'.";
          return false;
        }

        if (family.relationship == 'Istri' && genderEmp == "Female") {
          messageValidation = "You can't select this item in relationship. Your gender is '" + genderEmp + "'.";
          return false;
        }
      }

      return true;
    }


    initMethod();

  })




  .controller('AddressCtrl', function ($stateParams, $ionicHistory, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initMethod();

      if ($rootScope.refreshAddressCtrl) {
        initMethod();
      }
      $rootScope.refreshAddressCtrl = false;

    });




    $scope.refresh = function () {
      initMethod();
    }

    $scope.address = [];

    $scope.goToAddAddress = function () {
      $state.go('app.addaddress', {
        'id': 0
      });
    };

    $scope.goToDetails = function (idx) {
      $state.go('app.detailaddress', {
        'idx': idx,
        'edit': 'true'
      });
    };


    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.address = res;
      $scope.$broadcast('scroll.refreshComplete');

    }


    initMethod();
    //31acd2e6-e891-4628-a24e-58e408664516
    function initMethod() {
      $scope.address = [];
      getAddress();
    }
    // invalid access token error: "invalid_token" 401
    function getAddress() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/address';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }

  })


  .controller('EditAddressCtrl', function ($stateParams, $ionicHistory, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var addressIdx = $stateParams.idx;
    var messageValidation = "";
    $scope.address = {};
    if (addressIdx != null)
      //$scope.address = $rootScope.address[addressIdx];

      $scope.selectStayStatus = Main.getSelectStayStatus();
    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refSelectProvince = Main.getDataReference(arrCompanyRef, 'address', 'province', 'indonesia');
    $scope.selectProvince = []; // Main.getSelectProvince();

    $scope.selectCountry = Main.getSelectCountry();

    $scope.submitForm = function () {
      if (verificationForm($scope.address)) {
        var dataSubmit = {
          address: $scope.address.address,
          rt: $scope.address.rt,
          rw: $scope.address.rw,
          country: $scope.address.country,
          province: $scope.address.province,
          city: $scope.address.city,
          zipCode: $scope.address.zipCode,
          phone: $scope.address.phone,
          stayStatus: $scope.address.stayStatus
        };
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/user/address/' + $scope.address.id;
        var data = JSON.stringify(dataSubmit);
        Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
      } else {
        $scope.warningAlert(messageValidation);
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack("app.address");
      //$scope.family = res;
    }

    initMethod();

    function initMethod() {
      if (refSelectProvince != undefined && refSelectProvince != '') {
        $scope.selectProvince = JSON.parse(refSelectProvince);
      }

    }

    function verificationForm(address) {
      if (address.country == undefined || address.country == '') {
        messageValidation = "Country can't be empty";
        return false;
      } else if (address.province == undefined || address.province == '') {
        messageValidation = "Province can't be empty";
        return false;
      } else if (address.city == undefined || address.city == '') {
        messageValidation = "City can't be empty";
        return false;
      } else if (address.district == undefined || address.district == '') {
        messageValidation = "District can't be empty";
        return false;
      } else if (address.subdistrict == undefined || address.subdistrict == '') {
        messageValidation = "Sub District can't be empty";
        return false;
      } else if (address.rt == undefined || address.rt == '') {
        messageValidation = "RT can't be empty";
        return false;
      } else if (address.rw == undefined || address.rw == '') {
        messageValidation = "RW can't be empty";
        return false;
      } else if (address.address == undefined || address.address == '') {
        messageValidation = "Address can't be empty";
        return false;
      } else if (address.stayStatus == undefined || address.stayStatus == '') {
        messageValidation = "Stay Status can't be empty";
        return false;
      }
      return true;
    }

  })


  .controller('AddAddressCtrl', function (ionicSuperPopup, $stateParams, $ionicPopup, $ionicHistory, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    var messageValidation = "";
    //$scope.selectStayStatus = Main.getSelectStayStatus();
    var arrCompanyRef = Main.getSession("profile").companyReference;
    var refSelectProvince = Main.getDataReference(arrCompanyRef, 'address', 'province', 'indonesia');
    var refStayStatus = Main.getDataReference(arrCompanyRef, 'myhr', 'address', 'staystatus');

    $scope.addr = {};
    $scope.selectProvince = []; // Main.getSelectProvince();
    $scope.selectStayStatus = [];
    $scope.image = 'img/placeholder.png';
    $scope.imageData = null;
    $scope.addr.images = [];
    $scope.addr.imagesData = [];

    // $scope.selectProvince = Main.getSelectProvince();
    $scope.selectCountry = Main.getSelectCountry();

    $scope.resetForm = function () {
      $scope.address = {};
    }

    $scope.onSelectProvince = function () {
      $scope.labelProvince = $scope.address.province;
    }

    $scope.populateDataList = [{
        label: "NRIC Address",
        value: "NRIC Address"
      },
      {
        label: "Stay Address",
        value: "Stay Address"
      }
    ];

    $scope.removeChoice = function () {
      var lastItem = $scope.addr.imagesData.length - 1;
      $scope.addr.imagesData.splice(lastItem);
      $scope.addr.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.addr.images.length > 3) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }
      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  // alert(imageData);
                  // $rootScope.user.photo = "data:image/jpeg;base64," + imageData;
                  $scope.addr.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.addr.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.addr.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.addr.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.addPicture1 = function () {
      if ($scope.addr.images.length > 3) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }

      $scope.addr.images.push({
        'image': "img/placeholder.png"
      });
    }

    $scope.takePicture = function () {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: 750,
        targetHeight: 550,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }



    function sendData() {
      $ionicLoading.show({
        template: 'Submit...'
      });
      var attachment = [];
      if ($scope.addr.imagesData.length > 0) {
        for (var i = $scope.addr.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.addr.imagesData[i].image
            };
          } else {
            if ($scope.addr.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.addr.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);
        };
      }
      $scope.address.attachments = attachment;


      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/address';
      if ($stateParams.id != undefined && $stateParams.id != '0') {
        urlApi = Main.getUrlApi() + '/api/user/address/' + $stateParams.id;
      }

      var data = JSON.stringify($scope.address);
      // ('get data post ');
      // (data);

      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);

    }

    $scope.submitForm = function (value) {
      $scope.address.type = value;
      if ($scope.addr.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        console.log(value);
        if (verificationForm($scope.address)) {
          ionicSuperPopup.show({
              title: "Are you sure?",
              text: "Are you sure the data submitted is correct ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              closeOnConfirm: true
            },
            function (isConfirm) {
              if (isConfirm) {
                sendData();
              }
            });

        } else {
          $scope.warningAlert(messageValidation);
        }
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshAddressCtrl = true;
      $scope.goBack("app.address");
      //$scope.family = res;
    }



    function initData() {
      $scope.address = {};
      $scope.labelProvince = "Select Province";
    }

    function successDetailRequest(res) {
      $ionicLoading.hide();
      $scope.address = res;
      $scope.labelProvince = res.province;
      var typeData = res.type;
      $scope.address.populateData = {
        value: typeData
      };
    }

    function getDetailAddress(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/address/' + id;
      Main.requestApi(accessToken, urlApi, successDetailRequest, $scope.errorRequest);
    }


    function initModule() {
      initData();

      if (refSelectProvince != undefined && refSelectProvince != '') {
        $scope.selectProvince = JSON.parse(refSelectProvince);
      }

      if (refStayStatus != undefined && refStayStatus != '') {
        $scope.selectStayStatus = JSON.parse(refStayStatus);
      }


      if ($stateParams.id != undefined && $stateParams.id != '0') {
        getDetailAddress($stateParams.id);
      }
    }
    // invalid access token error: "invalid_token" 401
    function verificationForm(address) {
      if (address.country == undefined || address.country == '') {
        messageValidation = "Country can't be empty";
        return false;
      } else if (address.province == undefined || address.province == '') {
        messageValidation = "Province can't be empty";
        return false;
      } else if (address.city == undefined || address.city == '') {
        messageValidation = "City can't be empty";
        return false;
      } else if (address.district == undefined || address.district == '') {
        messageValidation = "District can't be empty";
        return false;
      } else if (address.subdistrict == undefined || address.subdistrict == '') {
        messageValidation = "Sub District can't be empty";
        return false;
      } else if (address.rt == undefined || address.rt == '') {
        messageValidation = "RT can't be empty";
        return false;
      } else if (address.rw == undefined || address.rw == '') {
        messageValidation = "RW can't be empty";
        return false;
      } else if (address.address == undefined || address.address == '') {
        messageValidation = "Address can't be empty";
        return false;
      } else if (address.stayStatus == undefined || address.stayStatus == '') {
        messageValidation = "Stay Status can't be empty";
        return false;
      }
      return true;
    }

    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      if (data.direction != undefined && data.direction != 'back')
        initModule();
    });

  })



  .controller('DetailAddressCtrl', function ($ionicHistory, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    var addressIdx = $stateParams.idx;
    $scope.canEdit = $stateParams.edit;
    $scope.address = {};

    function successDetailRequest(res) {
      $ionicLoading.hide();
      $scope.address = res;
    }

    function getDetailAddress(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/address/' + id;
      Main.requestApi(accessToken, urlApi, successDetailRequest, $scope.errorRequest);
    }

    $scope.goEditAddress = function () {

      $state.go('app.addaddress', {
        'id': $stateParams.idx
      });
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      getDetailAddress($stateParams.idx);
    });


  })



  .controller('CertificationCtrl', function ($ionicHistory, $timeout, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    $scope.certification = [];
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.refresh = function () {
      initMethod();
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      if ($rootScope.refreshCertificationCtrl) {
        initMethod();
      }
      $rootScope.refreshCertificationCtrl = false;
    });


    $scope.goToAdd = function () {

      $state.go('app.addcertification');
      $ionicHistory.nextViewOptions({
        disableAnimate: false,
        disableBack: false
      });
    }


    $scope.goToDetails = function (idx) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      $timeout(function () {
        $ionicLoading.hide();
        $state.go('app.detailcertification', {
          'idx': idx
        });
      }, 1000);

    };

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.certification = res;
      $rootScope.certification = [];
      for (var i = 0; i < $scope.certification.length; i++) {
        var obj = $scope.certification[i];
        obj.idx = i;
        $rootScope.certification.push(obj);
      }
      $scope.certification = $rootScope.certification;
      $scope.$broadcast('scroll.refreshComplete');

    }

    initMethod();
    //31acd2e6-e891-4628-a24e-58e408664516
    function initMethod() {

      $scope.certification = [];
      getCertification();
    }
    // invalid access token error: "invalid_token" 401
    function getCertification() {
      $ionicLoading.show({
        template: 'Loading...'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/myprofile/certification';
      Main.requestApi(accessToken, urlApi, successRequest, $scope.errorRequest);
    }
  })

  .controller('AddCertificationCtrl', function ($filter, ionicDatePicker, $ionicPopup, appService, $ionicActionSheet, $cordovaCamera, $ionicHistory, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.certification = {};
    $scope.selectRelationship = {};
    $scope.selectCertificationType = {};
    $scope.imageData = null;
    $scope.image = "img/placeholder.png";
    $scope.imageCertification = {};
    $scope.imageCertification.images = [];
    $scope.imageCertification.imagesData = [];
    $scope.certification.expired = new Date();
    $scope.appMode = Main.getAppMode();
    var datepicker = {
      callback: function (val) { //Mandatory
        $scope.certification.expired = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openDatePicker = function () {
      ionicDatePicker.openDatePicker(datepicker);
    };

    var messageValidation = ""


    $scope.removeChoice = function () {
      var lastItem = $scope.imageCertification.imagesData.length - 1;
      $scope.imageCertification.imagesData.splice(lastItem);
      $scope.imageCertification.images.splice(lastItem);
    }



    $scope.addPicture = function () {
      if ($scope.imageCertification.images.length > 2) {
        $scope.warningAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  // alert(imageData);
                  // $rootScope.user.photo = "data:image/jpeg;base64," + imageData;
                  $scope.imageCertification.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.imageCertification.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  // $rootScope.user.photo = "data:image/jpeg;base64," + imageData;
                  $scope.imageCertification.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.imageCertification.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };





    $scope.takePicture = function () {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: 750,
        targetHeight: 550,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }

    $scope.resetForm = function () {
      $scope.certification = {};
    }


    $scope.submitForm = function () {
      if (verificationForm($scope.certification)) {

        if ($scope.imageCertification.imagesData.length < 1) {
          $scope.errorAlert("You must add at least 1 attachment.");
          return false;
        }

        if ($scope.certification.type != 'Period') {
          $scope.certification.expired = null;
        } else {
          $scope.certification.expired = $filter('date')(new Date($scope.certification.expired), 'yyyy-MM-dd');
        }



        var confirmPopup = $ionicPopup.confirm({
          title: 'Confirm',
          template: '<h5>Are you sure the data submitted is correct ?</h5>',
          cancelText: 'Cancel',
          okText: 'Yes'
        }).then(function (res) {
          if (res) {
            $ionicLoading.show({
              template: 'Processing...'
            });
            var attachments = [];
            if ($scope.imageCertification.imagesData.length > 0) {
              for (var i = $scope.imageCertification.imagesData.length - 1; i >= 0; i--) {
                var objAttachment = {
                  'image': null
                };
                if ($scope.appMode == 'mobile') {
                  objAttachment = {
                    "image": $scope.imageCertification.imagesData[i].image
                  };
                } else {
                  if ($scope.imageCertification.imagesData[i].compressed.dataURL != undefined) {
                    var webImageAttachment = $scope.imageCertification.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
                    objAttachment = {
                      "image": webImageAttachment
                    };
                  }

                }
                attachments.push(objAttachment);
              };
            }

            $scope.certification.attachments = attachments;

            var accessToken = Main.getSession("token").access_token;
            var urlApi = Main.getUrlApi() + '/api/myprofile/certification';
            var data = JSON.stringify($scope.certification);
            Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
          }

        });
      } else {
        $scope.warningAlert(messageValidation);
      }

    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $rootScope.refreshCertificationCtrl = true;
      $scope.goBack("app.certification");

    }


    initMethod();

    function initMethod() {

      $scope.selectCertificationType = [{
        id: "Lifetime"
      }, {
        id: "Period"
      }];
    }

    // invalid access token error: "invalid_token" 401
    function verificationForm(certification) {
      if (certification.name == undefined || certification.name == '') {
        messageValidation = "Name can't be empty";
        return false;
      } else if (certification.principle == undefined || certification.principle == '') {
        messageValidation = "Principle can't be empty";
        return false;
      } else if (certification.type == undefined || certification.type == '') {
        messageValidation = "Type can't be empty";
        return false;
      }
      return true;
    }





  })




  .controller('DetailCertificationCtrl', function ($stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var certificationIdx = $stateParams.idx;
    $scope.certification = {};
    if (certificationIdx != null)
      $scope.certification = $rootScope.certification[certificationIdx];

    $scope.goToViewImage = function (id) {
      $state.go('app.viewimagecertification', {
        'id': id
      });
    }

  })

  .controller('ViewImageCertificationCtrl', function ($ionicModal, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $ionicModal.fromTemplateUrl('app/intro/image-preview.html', {
      scope: $scope,
      animation: 'fade-in-scale'
    }).then(function (modal) {
      $scope.modalPopupImage = modal;
    });

    $scope.openImagePreview = function (item) {
      var product = {
        id: 1,
        image: item
      };
      $scope.detailImage = product;
      $scope.modalPopupImage.show();
    };

    $scope.closeImagePreview = function () {
      $scope.detailImage = undefined;
      $scope.modalPopupImage.hide();
    };


    var certificationId = $stateParams.id;
    $scope.attachments = [];
    $scope.image = "img/placeholder.png";

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.attachments = res;
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}




    function retreiveImage(certificationId) {

      $ionicLoading.show({
        template: 'Retrieve Image ...'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/myprofile/certification/' + certificationId + '/attachments';
      Main.requestApi(accessToken, urlApi, successRequest, errorRequest);

    }
    if (certificationId != null) {
      retreiveImage(certificationId);
    } else {
      $scope.errorAlert("ID Certification can not be null");
    }



  })



  .controller('ChangeMaritalStatusCtrl', function (ionicSuperPopup, $ionicPopup, $ionicActionSheet, appService, $ionicHistory, $cordovaCamera, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var arrCompanyRef = Main.getSession("profile").companyReference;
    var arrItens = Main.getDataReference(arrCompanyRef, 'personal', 'information', 'maritalstatus');
    $scope.itens = JSON.parse(arrItens);
    var dataapprovalId = $stateParams.dataApprovalId;
    $scope.currentStatus = $stateParams.currentStatus;
    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.maritalStatus = {};
    $scope.maritalStatus.images = [];
    $scope.maritalStatus.imagesData = [];
    $scope.imageData;
    var messageValidation = "";

    $scope.removeChoice = function () {
      var lastItem = $scope.maritalStatus.imagesData.length - 1;
      $scope.maritalStatus.imagesData.splice(lastItem);
      $scope.maritalStatus.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.maritalStatus.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.maritalStatus.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.maritalStatus.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.maritalStatus.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.maritalStatus.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }


    $scope.updateSelection = function (position, itens, title) {

      angular.forEach(itens, function (subscription, index) {
        if (position != index)
          subscription.checked = false;
        $scope.selected = title;
      });
    }

    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.biodata");
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}


    function validationForm(selected) {
      if (selected == undefined || selected == "") {
        messageValidation = "You must pick your new marital status!";
        return false;
      }

      if (selected == $scope.currentStatus) {
        messageValidation = "You can't pick the same marital status!";
        return false;
      }
      return true;
    }

    function sendData() {
      var idRef = Main.getSession("profile").employeeTransient.id;
      var jsonData = '{"maritalStatus":"' + $scope.selected + '"}';
      var attachment = [];

      if ($scope.maritalStatus.imagesData.length > 0) {
        for (var i = $scope.maritalStatus.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.maritalStatus.imagesData[i].image
            };
          } else {
            if ($scope.maritalStatus.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.maritalStatus.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);

        };
      }

      var dataStr = {
        task: "CHANGEMARITALSTATUS",
        data: jsonData,
        idRef: idRef,
        attachments: attachment
      };

      $ionicLoading.show({
        template: 'Submit Request...'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval';
      var data = JSON.stringify(dataStr);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    }

    $scope.send = function () {
      if ($scope.maritalStatus.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        if (validationForm($scope.selected)) {

          ionicSuperPopup.show({
              title: "Are you sure?",
              text: "Are you sure the data submitted is correct ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              closeOnConfirm: true
            },
            function (isConfirm) {
              if (isConfirm) {
                sendData();
              }


            });
        } else {
          $scope.warningAlert(messageValidation);
        }
      }



    }

    var successDataApproval = function (res) {
      $ionicLoading.hide();
      var dataApproval = res;
      $scope.dataApprovalStatus = dataApproval.processingStatus;
      if (dataApproval.processingStatus != null && dataApproval.processingStatus.toLowerCase() == 'request') {
        $scope.showButton = false;

      }

      if (dataApproval.attachments != null && dataApproval.attachments.length > 0)
        $scope.image = dataApproval.attachments[0].image;


    }

    function getDataApproval(dataapprovalId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + dataapprovalId;
      Main.requestApi(accessToken, urlApi, successDataApproval, $scope.errorRequest);
    }

    function initModule() {
      $scope.showButton = true;
      if (dataapprovalId != null && dataapprovalId != "" && dataapprovalId != "0") {
        getDataApproval(dataapprovalId);
      }
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })


  .controller('ChangeNpwpCtrl', function (ionicSuperPopup, $ionicPopup, $ionicActionSheet, appService, $ionicHistory, $cordovaCamera, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }



    var arrCompanyRef = Main.getSession("profile").companyReference;

    var getSession = Main.getSession("profile").employeeTransient.npwpNo;


    var getSession2 = Main.getSession("profile").employeeTransient;


    // (getSession2);

    // // (getSession);

    var arrItens = Main.getDataReference(arrCompanyRef, 'personal', 'information', 'npwp');
    $scope.itens = JSON.parse(arrItens);
    var dataapprovalId = $stateParams.dataApprovalId;
    $scope.currentStatus = $stateParams.currentStatus;
    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.statusApproval = null;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.npwp = {};
    $scope.npwp.npwpNo = getSession;
    $scope.npwp.images = [];
    $scope.npwp.imagesData = [];
    $scope.imageData;
    var messageValidation = "";

    $scope.removeChoice = function () {
      var lastItem = $scope.npwp.imagesData.length - 1;
      $scope.npwp.imagesData.splice(lastItem);
      $scope.npwp.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.npwp.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.npwp.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.npwp.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.npwp.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.npwp.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }


    // $scope.updateSelection = function(position, itens, title) {

    //     angular.forEach(itens, function(subscription, index) {
    //         if (position != index)
    //             subscription.checked = false;
    //             $scope.selected = title;
    //         }
    //     );
    // }

    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.biodata");
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}


    // function validationForm(selected){
    //     if(selected == undefined || selected == ""){
    //         messageValidation = "You must pick your new marital status!";
    //         return false;
    //     }

    //     if(selected == $scope.currentStatus) {
    //         messageValidation = "You can't pick the same marital status!";
    //         return false;
    //     }
    //     return true;
    // }

    function sendData() {
      // // ($scope.npwp.npwpNo);
      var idRef = Main.getSession("profile").employeeTransient.id;
      var jsonData = '{"npwp":"' + $scope.npwp.npwpNo + '"}';
      var attachment = [];

      if ($scope.npwp.imagesData.length > 0) {
        for (var i = $scope.npwp.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.npwp.imagesData[i].image
            };
          } else {
            if ($scope.npwp.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.npwp.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);

        };
      }

      var dataStr = {
        task: "CHANGENPWP",
        data: jsonData,
        idRef: idRef,
        attachments: attachment
      };

      // (dataStr);

      $ionicLoading.show({
        template: 'Submit Request...'
      });
      var accessToken = Main.getSession("token").access_token;

      // must change endpoint url to be change npwp personal biodata
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval';


      var data = JSON.stringify(dataStr);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    }

    $scope.send = function () {

      // // if(validationForm($scope.selected)){
      // // ("data imageData npwp");
      // // ($scope.npwp.imagesData);

      // validation for same data
      if ($scope.npwp.npwpNo == getSession) {
        $scope.errorAlert("Your current input same as the old input");
        return false;
      }

      if ($scope.npwp.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }


          });
      }
      // }else {
      //     $scope.warningAlert(messageValidation);
      // }
    }

    var successDataApproval = function (res) {
      $ionicLoading.hide();
      var dataApproval = res;
      $scope.statusApproval = dataApproval.employeeRequest.npwpStatusDataApproval;
      $scope.dataApprovalStatus = dataApproval.processingStatus;
      if (dataApproval.processingStatus != null && dataApproval.processingStatus.toLowerCase() == 'request') {
        $scope.showButton = false;

      }

      if (dataApproval.attachments != null && dataApproval.attachments.length > 0)
        $scope.image = dataApproval.attachments[0].image;


    }

    function getDataApproval(dataapprovalId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + dataapprovalId;
      Main.requestApi(accessToken, urlApi, successDataApproval, $scope.errorRequest);
    }

    function initModule() {
      $scope.showButton = true;
      if (dataapprovalId != null && dataapprovalId != "" && dataapprovalId != "0") {
        getDataApproval(dataapprovalId);
      }
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })


  .controller('ChangeNircNoCtrl', function (ionicSuperPopup, $ionicPopup, $ionicActionSheet, appService, $ionicHistory, $cordovaCamera, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {


    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var arrCompanyRef = Main.getSession("profile").companyReference;
    var getSession = Main.getSession("profile").employeeTransient.nircNo;
    var arrItens = Main.getDataReference(arrCompanyRef, 'personal', 'information', 'nircno');
    $scope.itens = JSON.parse(arrItens);
    var dataapprovalId = $stateParams.dataApprovalId;
    $scope.currentStatus = $stateParams.currentStatus;
    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.statusApproval = null;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.nircno = {};
    $scope.nircno.nircNo = getSession;
    $scope.nircno.images = [];
    $scope.nircno.imagesData = [];
    $scope.imageData;
    var messageValidation = "";

    $scope.removeChoice = function () {
      var lastItem = $scope.nircno.imagesData.length - 1;
      $scope.nircno.imagesData.splice(lastItem);
      $scope.nircno.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.nircno.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.nircno.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.nircno.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.nircno.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.nircno.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }

    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.biodata");
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}


    function sendData() {
      var idRef = Main.getSession("profile").employeeTransient.id;
      var jsonData = '{"nircno":"' + $scope.nircno.nircNo + '"}';
      var attachment = [];

      if ($scope.nircno.imagesData.length > 0) {
        for (var i = $scope.nircno.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.nircno.imagesData[i].image
            };
          } else {
            if ($scope.nircno.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.nircno.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);

        };
      }


      var dataStr = {
        task: "CHANGENIRCNO",
        data: jsonData,
        idRef: idRef,
        attachments: attachment
      };

      // (dataStr);

      $ionicLoading.show({
        template: 'Submit Request...'
      });
      var accessToken = Main.getSession("token").access_token;

      // must change endpoint url to be change nircno personal biodata
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval';


      var data = JSON.stringify(dataStr);

      // ('get datanya x');
      // (data);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    }

    $scope.send = function () {

      if ($scope.nircno.nircNo == getSession) {
        $scope.errorAlert("Your current input same as the old input");
        return false;
      }

      // if(validationForm($scope.selected)){
      if ($scope.nircno.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }


          });
      }
    }

    var successDataApproval = function (res) {
      $ionicLoading.hide();
      var dataApproval = res;
      $scope.statusApproval = dataApproval.employeeRequest.nircStatusDataApproval;
      $scope.dataApprovalStatus = dataApproval.npwpStatusDataApproval;
      if (dataApproval.processingStatus != null && dataApproval.processingStatus.toLowerCase() == 'request') {
        $scope.showButton = false;

      }

      if (dataApproval.attachments != null && dataApproval.attachments.length > 0)
        $scope.image = dataApproval.attachments[0].image;


    }

    function getDataApproval(dataapprovalId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + dataapprovalId;
      Main.requestApi(accessToken, urlApi, successDataApproval, $scope.errorRequest);
    }

    function initModule() {

      $scope.showButton = true;
      if (dataapprovalId != null && dataapprovalId != "" && dataapprovalId != "0") {
        getDataApproval(dataapprovalId);
      }
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })


  .controller('ChangeKtpNameCtrl', function (ionicSuperPopup, $ionicPopup, $ionicActionSheet, appService, $ionicHistory, $cordovaCamera, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var arrCompanyRef = Main.getSession("profile").companyReference;

    var getSession = Main.getSession("profile").employeeTransient.ktpName;

    var arrItens = Main.getDataReference(arrCompanyRef, 'personal', 'information', 'ktpname');
    $scope.itens = JSON.parse(arrItens);
    var dataapprovalId = $stateParams.dataApprovalId;
    $scope.currentStatus = $stateParams.currentStatus;
    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.statusApproval = null;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.datapost = {};
    $scope.datapost.ktpName = getSession;
    $scope.datapost.images = [];
    $scope.datapost.imagesData = [];
    $scope.imageData;
    var messageValidation = "";

    $scope.removeChoice = function () {
      var lastItem = $scope.datapost.imagesData.length - 1;
      $scope.datapost.imagesData.splice(lastItem);
      $scope.datapost.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.datapost.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.datapost.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.datapost.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.datapost.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.datapost.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }


    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.biodata");
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}


    function sendData() {
      // ('ktpNameeee');
      // ($scope.datapost.ktpName);
      var idRef = Main.getSession("profile").employeeTransient.id;
      var jsonData = '{"ktpname":"' + $scope.datapost.ktpName + '"}';

      var attachment = [];

      if ($scope.datapost.imagesData.length > 0) {
        for (var i = $scope.datapost.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.datapost.imagesData[i].image
            };
          } else {
            if ($scope.datapost.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.datapost.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);

        };
      }


      var dataStr = {
        task: "CHANGEKTPNAME",
        data: jsonData,
        idRef: idRef,
        attachments: attachment
      };

      // (dataStr);

      $ionicLoading.show({
        template: 'Submit Request...'
      });
      var accessToken = Main.getSession("token").access_token;

      // must change endpoint url to be change nircno personal biodata
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval';
      var data = JSON.stringify(dataStr);

      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    }

    $scope.send = function () {

      if ($scope.datapost.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }


          });
      }
    }

    var successDataApproval = function (res) {
      $ionicLoading.hide();
      var dataApproval = res;
      $scope.statusApproval = dataApproval.employeeRequest.ktpnameStatusDataApproval;
      $scope.dataApprovalStatus = dataApproval.processingStatus;
      if (dataApproval.processingStatus != null && dataApproval.processingStatus.toLowerCase() == 'request') {
        $scope.showButton = false;

      }

      if (dataApproval.attachments != null && dataApproval.attachments.length > 0)
        $scope.image = dataApproval.attachments[0].image;


    }

    function getDataApproval(dataapprovalId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + dataapprovalId;
      Main.requestApi(accessToken, urlApi, successDataApproval, $scope.errorRequest);
    }

    function initModule() {
      $scope.showButton = true;
      if (dataapprovalId != null && dataapprovalId != "" && dataapprovalId != "0") {
        getDataApproval(dataapprovalId);
      }
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })

  .controller('ChangeFamilyCardNoCtrl', function (ionicSuperPopup, $ionicPopup, $ionicActionSheet, appService, $ionicHistory, $cordovaCamera, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var arrCompanyRef = Main.getSession("profile").companyReference;
    var getSession = Main.getSession("profile").employeeTransient.familyCardNo;
    var arrItens = Main.getDataReference(arrCompanyRef, 'personal', 'information', 'familyCardNo');
    $scope.itens = JSON.parse(arrItens);
    var dataapprovalId = $stateParams.dataApprovalId;
    $scope.currentStatus = $stateParams.currentStatus;
    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.statusApproval = null;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.familycardno = {};
    $scope.familycardno.familyCardNo = getSession;
    $scope.familycardno.images = [];
    $scope.familycardno.imagesData = [];
    $scope.imageData;
    var messageValidation = "";

    $scope.removeChoice = function () {
      var lastItem = $scope.familycardno.imagesData.length - 1;
      $scope.familycardno.imagesData.splice(lastItem);
      $scope.familycardno.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.familycardno.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.familycardno.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.familycardno.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.familycardno.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.familycardno.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }


    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var successRequest = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.biodata");
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}



    function sendData() {
      var idRef = Main.getSession("profile").employeeTransient.id;

      // ('data family card no');

      // ($scope.familycardno.familyCardNo);


      var jsonData = '{"familyCardNo":"' + $scope.familycardno.familyCardNo + '"}';
      var attachment = [];

      if ($scope.familycardno.imagesData.length > 0) {
        for (var i = $scope.familycardno.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.familycardno.imagesData[i].image
            };
          } else {
            if ($scope.familycardno.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.familycardno.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);

        };
      }

      var dataStr = {
        task: "CHANGEFAMILYCARDNO",
        data: jsonData,
        idRef: idRef,
        attachments: attachment
      };

      // (dataStr);

      $ionicLoading.show({
        template: 'Submit Request...'
      });
      var accessToken = Main.getSession("token").access_token;

      // must change endpoint url to be change familycardno personal biodata
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval';


      var data = JSON.stringify(dataStr);


      // ('data yg di post :');
      // (data);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    }

    $scope.send = function () {

      if ($scope.familycardno.familyCardNo == getSession) {
        $scope.errorAlert("Your current input same as the old input");
        return false;
      }

      if ($scope.familycardno.imagesData.length < 1) {
        $scope.errorAlert("You must add at least 1 attachment.");
        return false;
      } else {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              sendData();
            }


          });
      }
    }

    var successDataApproval = function (res) {
      $ionicLoading.hide();
      var dataApproval = res;
      // ("family card no");
      // (dataApproval.employeeRequest);
      $scope.statusApproval = dataApproval.employeeRequest.familyCardNoStatusDataApproval;
      $scope.dataApprovalStatus = dataApproval.processingStatus;

      if (dataApproval.processingStatus != null && dataApproval.processingStatus.toLowerCase() == 'request') {
        $scope.showButton = false;

      }

      if (dataApproval.attachments != null && dataApproval.attachments.length > 0)
        $scope.image = dataApproval.attachments[0].image;


    }

    function getDataApproval(dataapprovalId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + dataapprovalId;
      Main.requestApi(accessToken, urlApi, successDataApproval, $scope.errorRequest);
    }

    function initModule() {
      $scope.showButton = true;
      if (dataapprovalId != null && dataapprovalId != "" && dataapprovalId != "0") {
        getDataApproval(dataapprovalId);
      }
    }


    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })


  .controller('BankAccountCtrl', function (ionicSuperPopup, $ionicPopup, $ionicActionSheet, appService, $ionicHistory, $cordovaCamera, $stateParams, $ionicLoading, $rootScope, $scope, $state, AuthenticationService, Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }


    var arrCompanyRef = Main.getSession("profile").companyReference;
    var arrItens = Main.getDataReference(arrCompanyRef, 'personal', 'information', 'bankaccount', '');
    $scope.itens = JSON.parse(arrItens);
    var dataapprovalId = $stateParams.dataApprovalId;
    $scope.currentStatus = $stateParams.currentStatus;
    $scope.showButton = true;
    $scope.dataApprovalStatus = null;
    $scope.appMode = Main.getAppMode();
    $scope.image = "img/placeholder.png";
    $scope.bankaccount = {};
    $scope.bankaccount.bankAccNo = null;
    $scope.bankaccount.bankId = null;
    $scope.bankaccount.bankAccBranch = null;
    $scope.bankaccount.dataId = null;
    $scope.bankaccount.images = [];
    $scope.bankaccount.imagesData = [];
    $scope.dataBank = {};
    $scope.imageData;
    var messageValidation = "";

    $scope.removeChoice = function () {
      var lastItem = $scope.bankaccount.imagesData.length - 1;
      $scope.bankaccount.imagesData.splice(lastItem);
      $scope.bankaccount.images.splice(lastItem);
    }

    $scope.addPicture = function () {
      if ($scope.bankaccount.images.length > 2) {
        $scope.errorAlert("Only 3 pictures can be upload");
        return false;
      }


      $ionicActionSheet.show({
        buttons: [{
          text: 'Take Picture'
        }, {
          text: 'Select From Gallery'
        }],
        buttonClicked: function (index) {
          switch (index) {
            case 0: // Take Picture
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getCameraOptions()).then(function (imageData) {
                  $scope.bankaccount.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.bankaccount.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);

              break;
            case 1: // Select From Gallery
              document.addEventListener("deviceready", function () {
                $cordovaCamera.getPicture(appService.getLibraryOptions()).then(function (imageData) {
                  $scope.bankaccount.images.push({
                    'image': "data:image/jpeg;base64," + imageData
                  });
                  $scope.bankaccount.imagesData.push({
                    'image': imageData
                  });
                }, function (err) {
                  appService.showAlert('Error', err, 'Close', 'button-assertive', null);
                });
              }, false);
              break;
          }
          return true;
        }
      });
    };

    $scope.takePicture = function () {
      var options = {
        quality: Main.getTakePictureOptions().quality,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        targetWidth: Main.getTakePictureOptions().targetWidth,
        targetHeight: Main.getTakePictureOptions().targetHeight,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.image = "data:image/jpeg;base64," + imageData;
        $scope.imageData = imageData;
      }, function (err) {
        // An error occured. Show a message to the user
        $scope.errorAlert("an error occured while take picture");
      });
    }


    function goBack(ui_sref) {
      var currentView = $ionicHistory.currentView();
      var backView = $ionicHistory.backView();
      if (backView) {
        //there is a back view, go to it
        if (currentView.stateName == backView.stateName) {
          //if not works try to go doubleBack
          var doubleBackView = $ionicHistory.getViewById(backView.backViewId);
          $state.go(doubleBackView.stateName, doubleBackView.stateParams);
        } else {
          backView.go();
        }
      } else {
        $state.go(ui_sref);
      }
    }

    var successRequest = function (res) {

      $ionicLoading.hide();
      $scope.successAlert(res.message);
      goBack("app.biodata");
    }

    var errorRequest = function (err, status) {
      $ionicLoading.hide();
      if (status == 401) {
        var refreshToken = Main.getSession("token").refresh_token
        Main.refreshToken(refreshToken, successRefreshToken, errRefreshToken);
      } else {
        if (status == 500)
          $scope.errorAlert(err.message);
        else
          $scope.errorAlert("Please Check your connection");
      }

    }

    var successRefreshToken = function (res) {
      Main.setSession("token", res);
    }

    var errRefreshToken = function (err, status) {}


    function sendData() {
      if ($scope.bankaccount.dataId == null) {
        var idRef = 0;
      } else {
        var idRef = Main.getSession("profile").employeeTransient.id;
      }
      var jsonData = '{"bankId":"' + $scope.bankaccount.bankId + '","bankAccNo":"' + $scope.bankaccount.bankAccNo + '","bankAccBranch":"' + $scope.bankaccount.bankAccBranch + '"}';
      var attachment = [];

      if ($scope.bankaccount.imagesData.length > 0) {
        for (var i = $scope.bankaccount.imagesData.length - 1; i >= 0; i--) {
          var objAttachment = {
            'image': null
          };
          if ($scope.appMode == 'mobile') {
            objAttachment = {
              "image": $scope.bankaccount.imagesData[i].image
            };
          } else {
            if ($scope.bankaccount.imagesData[i].compressed.dataURL != undefined) {
              var webImageAttachment = $scope.bankaccount.imagesData[i].compressed.dataURL.replace(/^data:image\/[a-z]+;base64,/, "");
              objAttachment = {
                "image": webImageAttachment
              };
            }

          }
          attachment.push(objAttachment);

        };
      }

      var dataStr = {
        task: "CHANGEBANKACCOUNT",
        data: jsonData,
        idRef: idRef,
        attachments: attachment
      };

      // (dataStr);

      $ionicLoading.show({
        template: 'Submit Request...'
      });
      var accessToken = Main.getSession("token").access_token;

      // must change endpoint url to be change bankaccount personal biodata
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval';


      var data = JSON.stringify(dataStr);
      Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    }

    $scope.send = function () {

      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure the data submitted is correct ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            sendData();
          }


        });
    }

    var successDataApproval = function (res) {
      $ionicLoading.hide();
      var dataApproval = res;
      $scope.dataApprovalStatus = dataApproval.processingStatus;
      if (dataApproval.processingStatus != null && dataApproval.processingStatus.toLowerCase() == 'request') {
        $scope.showButton = false;

      }

      if (dataApproval.attachments != null && dataApproval.attachments.length > 0)
        $scope.image = dataApproval.attachments[0].image;


    }

    function getDataApproval(dataapprovalId) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/workflow/dataapproval/' + dataapprovalId;
      Main.requestApi(accessToken, urlApi, successDataApproval, $scope.errorRequest);
    }

    var successDataBank = function (res) {
      $ionicLoading.hide();
      var dataBank = res;
      $scope.dataBank = dataBank;
    }


    function getDataBank() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/user/bankAccount/';
      Main.requestApi(accessToken, urlApi, successDataBank, $scope.errorRequest);
    }

    function initModule() {
      $scope.showButton = true;
      if (dataapprovalId != null && dataapprovalId != "" && dataapprovalId != "0") {
        getDataApproval(dataapprovalId);
      }
      getDataBank();
    }



    $scope.$on('$ionicView.beforeEnter', function (event, data) {
      initModule();
    });

  })
